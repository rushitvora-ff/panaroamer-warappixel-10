// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  leafletMapUrl: 'https://api.maptiler.com/maps/streets/{z}/{x}/{y}.png',
  leafletApiKey: 'G6zztR0WQ4xmXing3qCZ',

  onebApi: {
    contacts: {
      service: {
        serviceUrl: 'https://contacts.onebapi.com'
      }
    },
    interactions: {
      service: {
          serviceUrl: 'https://interactions.onebapi.com'
      }
    },
    ip: {
      service: {
          serviceUrl: 'https://ip.onebapi.com'
      }
    },
    address: {
      service: {
        serviceUrl: 'https://addresses.onebapi.com'
      }
    },
    schedules: {
      service: {
        serviceUrl: 'https://schedules.onebapi.com'
      }
    },
    contentServer: {
      service: {
        serviceUrl: 'https://contents.onebapi.com'
      },
      publishingServiceConfig: {
      },
      uploadServiceConfig: {
        senderAwsS3FactoryConfig: {
            awsAccess: {
                sessionName: '',
                durationSeconds: 900,
                region: 'us-west-2'
            },
            senderAwsS3Connector: {}
        }
      }
    }
  },
  // Credentials for the Aws S3 uploads
  accessKeyId: 'AKIAT3GXKBEL6WSS2PUG',
  secretAccessKey: '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',
  region: 'us-east-1',
  BucketName : 'panaroamer-file'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
