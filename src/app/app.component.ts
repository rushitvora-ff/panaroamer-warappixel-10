import { Component, ChangeDetectorRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { UtilityService } from './services/utility.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  isLoading = false;

  constructor(
    translate: TranslateService,
    public utility: UtilityService) {
    translate.setDefaultLang('English');
    translate.use('English');
    this.findMe();
    localStorage.removeItem('activeTab');
    localStorage.removeItem('language');
    localStorage.removeItem('assetsArray');
    localStorage.removeItem('starred');
    localStorage.removeItem('selectedIdx');
    localStorage.removeItem('info');
    localStorage.removeItem('publishBtn');
    localStorage.removeItem('fullIp');
    localStorage.removeItem('latlong');
    localStorage.removeItem('gestureArray');
    localStorage.removeItem('myInfo');
    localStorage.removeItem('loggedIn');
    localStorage.removeItem('interactionCoords');
    utility.loader$.subscribe(res => {
      setTimeout(() => {
        this.isLoading = res;
      }, 200);
    });
  }

  findMe(): void {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.utility.setCoordinates(position.coords);
        console.log('Lat: ' + position.coords.latitude + ' Long: ' + position.coords.longitude);
      });
    } else {
      alert('Geolocation is not supported by this browser.');
    }
  }
}
