import { Component, OnInit } from '@angular/core';
import { GlobalAuthenticationContext } from '@oneb/authentication-connector-api';
import { UtilityService } from '../services/utility.service';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  isLogin = false;

  constructor(public utility: UtilityService, public translate: TranslateService, public dialog: MatDialog) {
    if (GlobalAuthenticationContext.hasSignedInUserDetails() && GlobalAuthenticationContext.getSignedInUserDetails().email) {
      this.isLogin = true;
    } else {
      this.isLogin = false;
    }
  }

  ngOnInit(): void {
  }

}
