import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import { UtilityService } from 'src/app/services/utility.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { LoginInterface } from '../../interfaces/authenticationModule';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  login: LoginInterface = {
    form: Object.create(null),
    hide: true,
    loader: false,
    isDestroy: false
  };
  // public form: FormGroup = ;
  // hide = true;
  // loader = false;
  // isDestroy = false;

  constructor(
    private fb: FormBuilder,
    private router: Router, private toasr: ToastrService, public utility: UtilityService,
    private translateSevice: TranslateService) {
      this.utility.loader$.subscribe(value => {
        this.login.loader = value;
      });
    }

  ngOnInit(): void {
    this.login.form = this.fb.group({
      uname: [null, Validators.compose([Validators.required])],
      password: [null, Validators.compose([Validators.required])]
    });
  }

  ngOnDestroy(): void {
    if (this.login.loader) {
      this.login.isDestroy = true;
      this.utility.setLoader(false);
    }
  }

  onSubmit(): void {
    // this.router.navigate(['/dashboards/dashboard1']);
    this.utility.setLoader(true);
    const formData = this.login.form.value;
    this.utility.getAuthenticator().signIn(formData.uname, formData.password).then((response) => {
      this.utility.setLoader(false);
      this.router.navigate(['/home']);
    }).catch(error => {
      if (this.login.isDestroy) {
        return;
      }
      console.log(error.code);
      if (error.code === 'UserNotConfirmedException') {
        this.toasr.error('Please, confirm signUp process!');
      } else if (error.code === 'NotAuthorizedException') {
        // The error happens when the incorrect password is provided
        this.translateSevice.get('toast.Incorrect Username or Password').subscribe(res => {
          this.toasr.info(res, '');
        });
      } else if (error.code === 'UserNotFoundException') {
        // The error happens when the supplied username/email does not exist in the Cognito user pool
        this.toasr.error('User does not exist!');
      } else if (error.code === 'InvalidParameterException') {
        this.translateSevice.get('toast.Username or Password cannot be empty').subscribe(res => {
          this.toasr.info(res, '');
        });
      } else {
        if (error.message === 'Only radix 2, 4, 8, 16, 32 are supported') {
          this.translateSevice.get('toast.Incorrect Username or Password').subscribe(res => {
            this.toasr.info(res, '');
          });
        } else {
          this.toasr.error(error.message);
        }

      }
      this.utility.setLoader(false);
    });
  }
}
