import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import { CustomValidators } from 'ngx-custom-validators';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { UtilityService } from 'src/app/services/utility.service';
import { RegisterInterface } from '../../interfaces/authenticationModule';

const password: any = new FormControl('', Validators.required);
const confirm: any = CustomValidators.equalTo(password);
const confirmPassword = new FormControl('', confirm);

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {
  register: RegisterInterface = {
    form: Object.create(null),
    hide: true,
    hideConfirm: true,
    isSubmit: false,
  };

  // public form: FormGroup = Object.create(null);
  // hide = true;
  // hideConfirm = true;
  // confirmSignUp;
  // isSubmit = false;
  // confirmCode;

  constructor(
    private fb: FormBuilder, private router: Router,
    private toasr: ToastrService, private translateSevice: TranslateService,
    public utility: UtilityService) {
    if (localStorage.getItem('username')) {
      this.register.confirmSignUp = true;
    }
  }

  ngOnInit(): void {
    this.register.form = this.fb.group({
      email: [
        null,
        Validators.compose([Validators.required, Validators.email])
      ],
      password,
      confirmPassword,
      terms: ['', Validators.required]
    });
  }

  onSubmit(): void {
    const formData = this.register.form.value;
    if (this.register.form.valid) {
      this.register.isSubmit = true;
      if (!this.register.form.value.terms) {
        this.translateSevice.get('registration.AcceptTermsAndCondition').subscribe(res => {
          this.toasr.error(res);
        });
      } else {
        this.utility.setLoader(true);
        const passWord = formData.password;
        const username = formData.email;
        this.utility.getAuthenticator().signUp(username, passWord, { email: username }).then(response => {
          if (response) {
            this.utility.setLoader(false);
            this.translateSevice.get('registration.confirmCodeMessage').subscribe(res => {
              this.toasr.info(res, '');
            });
            localStorage.removeItem('username');
            localStorage.setItem('username', formData.email);
            this.register.confirmSignUp = true;
          }
        }).catch((error: any) => {
          console.log(error);
          if (error.code === 'InvalidParameterException') {
            if (error.message.indexOf('password') !== -1) {
              const errmsg = error.message.indexOf('length') !== -1 ? 'Minimum number of symbols must be 8' : '';
              this.toasr.error(errmsg);
            } else {
              this.toasr.error(error.message);
            }
          } else if (error.code === 'InvalidPasswordException') {
            this.toasr.error(error.message.replace('Password did not conform with policy: ', ''));
          } else {
            this.toasr.error(error.message);
          }

          this.utility.setLoader(false);
        });
      }
    } else {
      if (this.register.form.errors && this.register.form.errors.invalid) {
        this.toasr.error('Password does not matched!');
      } else {
        this.toasr.error('Enter valid Details!');
      }
    }
  }

  resendCode(): void {
    this.utility.setLoader(true);
    const username = localStorage.getItem('username');
    this.utility.getAuthenticator().sendSignUpCode(username).then(response => {
      this.utility.setLoader(false);
      this.translateSevice.get('registration.confirmCodeMessage').subscribe(res => {
        this.toasr.info(res, '');
      });
    }).catch(error => {
      this.toasr.error(error.message);
      this.utility.setLoader(false);
    });
  }

  resetSignUp(): void {
    if (localStorage.getItem('username')) {
      localStorage.removeItem('username');
      this.register.confirmSignUp = false;
    }
  }

  onConfirm(): void {
    if (this.register.confirmCode) {
      this.utility.setLoader(true);
      const username = localStorage.getItem('username');
      const code = this.register.confirmCode;
      this.utility.getAuthenticator().confirmSignUp(username, code).then(response => {
        localStorage.removeItem('username');
        this.utility.setLoader(false);
        this.translateSevice.get('registration.successFullyRegistered').subscribe(res => {
          this.toasr.success(res);
        });
        this.router.navigate(['/authentication/login']);
      }).catch(error => {
        this.utility.setLoader(false);
        this.translateSevice.get('registration.invalidCode').subscribe(res => {
          this.toasr.error(res);
        });
      });
    } else {
      this.toasr.error('Enter valid Details!');
    }
  }

  click(): void {
    this.router.navigate(['/authentication/login']);
  }
  ngOnDestroy(): void {
    this.utility.setLoader(false);
  }
}
