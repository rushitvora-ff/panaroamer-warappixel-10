import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import { CustomValidators } from 'ngx-custom-validators';
import { UtilityService } from 'src/app/services/utility.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';

const password: any = new FormControl('', Validators.required);
const confirm: any = CustomValidators.equalTo(password);
const confirmPassword = new FormControl('', confirm);


@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.scss']
})
export class ForgotComponent implements OnInit, OnDestroy {
  public form: FormGroup = Object.create(null);
  confirmation = false;
  forgotPass: FormGroup;
  hide = true;
  hideConfirm = true;

  constructor(
    private fb: FormBuilder, private router: Router,
    private toasr: ToastrService,
    private translateSevice: TranslateService,
    public utility: UtilityService) {
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      username: [
        null,
        Validators.compose([Validators.required, Validators.email])
      ]
    });

    this.forgotPass = this.fb.group({
      confirmCode: ['', [Validators.required]],
      password,
      confirmPassword
    });
  }

  onSubmit(formData): void {
    if (this.form.valid) {
      this.utility.setLoader(true);
      this.utility.getAuthenticator().forgotPassword(formData.username).then(data => {
        this.utility.setLoader(false);
        this.translateSevice.get('registration.confirmCodeMessage').subscribe(res => {
          this.toasr.info(res, '');
        });
        sessionStorage.setItem('username', formData.username);
        this.confirmation = true;
      })
      .catch(err => {
        this.utility.setLoader(false);
        this.toasr.error(err.message);
      });
    } else {
      this.toasr.error('Enter valid Username!');
    }
  }

  onConfirm(formData): void {
    if (this.forgotPass.valid) {
      this.utility.setLoader(true);
      const username = sessionStorage.getItem('username');
      const code = formData.confirmCode;
      this.utility.getAuthenticator().confirmPasswordChange(username, formData.password, code).then(response => {
        sessionStorage.removeItem('username');
        this.utility.setLoader(false);
        this.toasr.success('Password Reset successfully');
        this.router.navigate(['/authentication/login']);
      }).catch(error => {
        this.utility.setLoader(false);
        this.translateSevice.get('registration.invalidCode').subscribe(res => {
          this.toasr.error(res);
        });
      });
    } else {
      if (this.forgotPass.errors && this.forgotPass.errors.invalid) {
        this.toasr.error('Password does not match!');
      } else {
        this.toasr.error('All fields are required!');
      }
    }
  }

  ngOnDestroy(): void {
    this.utility.setLoader(false);
  }
}
