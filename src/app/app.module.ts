import * as $ from 'jquery';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AppRoutes } from './app.routing';
import { AppComponent } from './app.component';

import { FlexLayoutModule } from '@angular/flex-layout';
import { FullComponent } from './layouts/full/full.component';
import { AppBlankComponent } from './layouts/blank/blank.component';
import { AppHeaderComponent } from './layouts/full/header/header.component';
import { AppSidebarComponent } from './layouts/full/sidebar/sidebar.component';
import { AppBreadcrumbComponent } from './layouts/full/breadcrumb/breadcrumb.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DemoMaterialModule } from './demo-material-module';


import { SharedModule } from './shared/shared.module';
import { SpinnerComponent } from './shared/spinner.component';
import { ToastrModule } from 'ngx-toastr';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { HomeComponent } from './home/home.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { PaymentsComponent } from './components/payments/payments.component';
import { PaymentSuccessComponent } from './components/payments/payment-success/payment-success.component';
import { PlayComponent } from './components/play/play.component';
import { ChatComponent } from './components/chat/chat.component';
import { ContactsComponent } from './components/contacts/contacts.component';
import { GroupsComponent } from './components/groups/groups.component';
import { AngularContactsModule } from '@oneb-angular/angular-contacts';
import { AngularIpPlayerModule } from '@oneb-angular/angular-ip-player';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { CommonModule } from '@angular/common';
import { IpScheduleCalendarComponent } from './components/ip-schedule-calendar/ip-schedule-calendar.component';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { AngularSchedulingModule } from '@oneb-angular/angular-scheduling';
import { PaymentFailureComponent } from './components/payments/payment-failure/payment-failure.component';
import { SentryErrorHandler } from 'src/app/services/error.service';
import { APP_INITIALIZER, ErrorHandler, NgModule, NO_ERRORS_SCHEMA,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { PreInteractionsComponent } from './components/pre-interactions/pre-interactions.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SurveyComponent } from './components/survey/survey.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
  wheelSpeed: 2,
  wheelPropagation: true
};

export function HttpLoaderFactory(http: HttpClient): object {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
  AppComponent,
  FullComponent,
  AppHeaderComponent,
  AppBlankComponent,
  SpinnerComponent,
  AppSidebarComponent,
  AppBreadcrumbComponent,
  HomeComponent,
  PaymentsComponent,
  PaymentSuccessComponent,
  PlayComponent,
  ChatComponent,
  ContactsComponent,
  GroupsComponent,
  IpScheduleCalendarComponent,
  PaymentFailureComponent,
  PreInteractionsComponent,
  SurveyComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    DemoMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    AngularContactsModule,
    AngularIpPlayerModule,
    NgxDatatableModule,
    CommonModule,
    PerfectScrollbarModule,
    AngularSchedulingModule,
    InfiniteScrollModule,
    CarouselModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: 'toast-top-center',
      preventDuplicates: true,
    }),
    HttpClientModule,
    SharedModule,
    RouterModule.forRoot(AppRoutes)
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    // {
    //   provide: ErrorHandler,
    //   useClass: SentryErrorHandler
    // }
  ],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class AppModule {}
