import {
  Component,
  OnInit,
  ChangeDetectorRef,
  OnDestroy,
  Output,
  EventEmitter,
  Input,
  NgZone,
  ViewChild,
  ElementRef,
  HostListener
} from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { NgxImageCompressService } from 'ngx-image-compress';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { MatDialog } from '@angular/material/dialog';
import { EditImageDialogComponent } from 'src/app/shared/dialogs/edit-image-dialog/edit-image-dialog.component';
import { MatMenuTrigger } from '@angular/material/menu';
import { InteractionComponent } from 'src/app/shared/dialogs/interaction/interaction.component';
import { UtilityService } from 'src/app/services/utility.service';
import { UploadVideosService } from 'src/app/services/upload-videos.service';
import { AssetsDialogComponent } from 'src/app/shared/dialogs/assets-dialog/assets-dialog.component';
import { TranslateService } from '@ngx-translate/core';
import * as introJs from 'intro.js/intro.js';
import { ConfirmationComponent } from 'src/app/shared/dialogs/confirmation/confirmation.component';
import { makePage, Page } from '@oneb/ip-api-clients-models';
import {
  PageTypeEnum_Image,
  PageTypeEnum_Survey,
  PageTypeEnum_Video,
} from '@oneb/ip-api-clients-models';
import { IpPage, makeIpPage } from '@oneb/ip-api-clients-models';
import { Ip } from '@oneb/ip-api-clients-models';
import { ToastrService } from 'ngx-toastr';
import * as AWS from 'aws-sdk';
import { WebcamInitError } from 'ngx-webcam';
import { CaptureImageComponent } from 'src/app/shared/dialogs/capture-image/capture-image.component';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import {
  CallToActionTypeEnum_Buy,
  CallToActionTypeEnum_Call,
  CallToActionTypeEnum_Chat,
  CallToActionTypeEnum_Visit,
  EffectTypeEnum_Audio,
  EffectTypeEnum_Image,
  EffectTypeEnum_Survey,
  EffectTypeEnum_Text,
  EffectTypeEnum_Video,
  GestureTypeEnum_LongTap,
  GestureTypeEnum_Rub,
  GestureTypeEnum_Tap
} from '@oneb/interactions-api-clients-models';
import { InteractionServiceService } from '../../services/interaction-service.service';
import { PreviewSlideInterface } from '../../interfaces/builderModule';
declare var WaveSurfer: any;
declare var videojs: any;
declare var RecordRTC: any;
declare var StereoAudioRecorder: any;

@Component({
  selector: 'app-preview-slide',
  templateUrl: './preview-slide.component.html',
  styleUrls: ['./preview-slide.component.scss'],
})
export class PreviewSlideComponent implements OnInit, OnDestroy {
  previewSlide: PreviewSlideInterface = {
    surveyItems: [
      {
        id: 1,
        name: 'Survey-XYZ',
        description: 'This is very good survey',
        isActive: false,
      },
      {
        id: 2,
        name: 'Survey-ABC',
        description: 'This is very good survey',
        isActive: false,
      },
      {
        id: 3,
        name: 'Survey-DEF',
        description: 'This is very good survey',
        isActive: false,
      }
    ],
    s3: new AWS.S3({
      accessKeyId: environment.accessKeyId,
      secretAccessKey: environment.secretAccessKey,
      region: environment.region,
    }),
    slides: [],
    selectSoundsFrom: [],
    selectItemsFrom: [],
    completeIndex: 0,
    id: '',
    showVideoRec: false,
    isRecording: false,
    startRecording: false,
    isIos: false,
    finalData: [],
    front: true,
    temporarySlide: [],
    isRecorder: false,
    introJS: introJs(),
    ipPages: []
  };

  dark = true;
  // surveyItems = [
  //   {
  //     id: 1,
  //     name: 'Survey-XYZ',
  //     description: 'This is very good survey',
  //     isActive: false,
  //   },
  //   {
  //     id: 2,
  //     name: 'Survey-ABC',
  //     description: 'This is very good survey',
  //     isActive: false,
  //   },
  //   {
  //     id: 3,
  //     name: 'Survey-DEF',
  //     description: 'This is very good survey',
  //     isActive: false,
  //   },
  // ];
  // s3 = new AWS.S3({
  //   accessKeyId: environment.accessKeyId,
  //   secretAccessKey: environment.secretAccessKey,
  //   region: environment.region,
  // // });
  // // slides: any[] = [];
  // selectSoundsFrom: any = [];
  // selectItemsFrom = [];
  // completeIndex = 0;
  // id = '';
  // options: any;
  // showVideoRec = false;
  // isRecording = false;
  // startRecording = false;
  // vidPlayer: any;
  // isIos = false;
  // finalData = [];
  // localData;
  // isOpenMenu;
  // front = true;
  // temporarySlide = [];
  // isS3Loader;
  // isRecorder = false;
  // introJS = introJs();
  // abortParams;
  // ipPages: IpPage[] = [];
  // webcamImage;

  @Input() Ip: Ip;

  @ViewChild('menuImg') menuImg: MatMenuTrigger;
  @ViewChild('menuvideo') menuvideo: MatMenuTrigger;
  @ViewChild('menuAudio') menuAudio: MatMenuTrigger;
  @ViewChild('forVideo') MultipleVideo: ElementRef;
  @ViewChild('forImage') MultipleImage: ElementRef;

  @Output() slidesLength = new EventEmitter();
  @Output() addedPage = new EventEmitter();
  @Output() uploadData = new EventEmitter();

  @HostListener('window:offline')
  setNetworkOffline(): void {
    if (this.previewSlide.isS3Loader == true) {
      this.utility.cancelUpload = true;
    }
    this.utility.setLoader(false);
    this.toast.error('Network Failure');
    this.utility.isAbort = true;
  }

  @HostListener('window:online')
  setNetworkOnline(): void {
    this.utility.isAbort = true;
    if (this.utility.cancelUpload) {
      const th = this;
      this.abortUpload(th);
      this.utility.setLoader(false);
      this.utility.cancelUpload = false;
    }
  }

  get isMobile(): any {
    return navigator.userAgent.match(/(iPhone)|(android)|(webOS)/i);
  }

  constructor(
    private router: Router,
    private location: PlatformLocation,
    public ngZone: NgZone,
    public interactionService: InteractionServiceService,
    public dialog: MatDialog,
    private translateSevice: TranslateService,
    private utility: UtilityService,
    private uploadService: UploadVideosService,
    public toast: ToastrService,
    private cd: ChangeDetectorRef,
    private imageCompress: NgxImageCompressService,
    public sanitizer: DomSanitizer
  ) {
    const asstArr = JSON.parse(localStorage.getItem('assetsArray'));
    if (asstArr) {
      for (const item of asstArr) {
        this.previewSlide.selectItemsFrom.push(item.selectedItemIdx);
      }
    }
    this.location.onPopState(() => {
      const th = this;
      this.abortUpload(th);
    });
  }

  ngOnInit(): void {
    if (this.Ip.mainPages && this.Ip.mainPages.length) {
      this.previewSlide.ipPages = this.Ip.mainPages;
      console.log(this.previewSlide.ipPages);
      const surveyDetail = JSON.parse(localStorage.getItem('ipDataPre'));
      let j = 0;
      for (let i = 0; i < this.previewSlide.ipPages.length; i++) {
        if (this.previewSlide.ipPages[i].page.dataUrl && this.previewSlide.ipPages[i].page.dataUrl.substr(0, 4) == 'http' ||
          this.previewSlide.ipPages[i].page.dataUrl.substr(0, 6) == 'assets') {
          const effects = this.makeEffects(this.previewSlide.ipPages[i]);
          const id = Math.random().toString(36).substring(2);
          this.previewSlide.finalData.push({
            id: this.previewSlide.ipPages[i].pageId,
            data: this.previewSlide.ipPages[i].page.dataUrl,
            sound: this.previewSlide.ipPages[i].page.soundTrackUrl,
          });
          if (
            this.previewSlide.ipPages[i].page.dataUrl.split('.').pop() == 'png' ||
            this.previewSlide.ipPages[i].page.dataUrl.split('.').pop() == 'jpeg' ||
            this.previewSlide.ipPages[i].page.dataUrl.split('.').pop() == 'jpg'
          ) {
            this.previewSlide.slides.push({
              id,
              url: this.previewSlide.ipPages[i].page.dataUrl,
              safeurl: this.previewSlide.ipPages[i].page.dataUrl,
              music: this.previewSlide.ipPages[i].page.soundTrackUrl,
              safeMusicUrl: this.previewSlide.ipPages[i].page.soundTrackUrl,
              effects,
              type: 'image',
            });
          } else if (this.previewSlide.ipPages[i].page.dataUrl.substr(0, 6) == 'assets' || this.previewSlide.ipPages[i].page.dataUrl.split('.').pop() == 'html') {
            this.previewSlide.slides.push({
              id,
              url: 'assets/images/surveyPlaceholderHeader.png',
              safeurl: this.previewSlide.ipPages[i].page.dataUrl,
              music: this.previewSlide.ipPages[i].page.soundTrackUrl,
              safeMusicUrl: this.previewSlide.ipPages[i].page.soundTrackUrl,
              effects,
              type: 'survey',
              name: surveyDetail[j].name,
              link: surveyDetail[j].link
            });
          } else {
            this.previewSlide.slides.push({
              id,
              url: this.previewSlide.ipPages[i].page.dataUrl,
              safeurl: this.previewSlide.ipPages[i].page.dataUrl,
              music: this.previewSlide.ipPages[i].page.soundTrackUrl,
              safeMusicUrl: this.previewSlide.ipPages[i].page.soundTrackUrl,
              effects,
              type: 'video',
            });
          }
          j++;
        }
      }
      console.log(this.previewSlide.slides);
      console.log(this.previewSlide.finalData);
      if (this.previewSlide.slides.length) {
        this.addedPage.emit(this.previewSlide.ipPages);
        this.slidesLength.emit(this.previewSlide.ipPages.length);
      }
    }
    this.getOS();
    // const images = localStorage.getItem('ipDataPre');
    // if (images && images != 'undefined') {
    //   localStorage.removeItem('ipDataPre');
    //   const imgs = JSON.parse(images);
    //   this.localData = images;
    //   console.log('LocallDataaaaaaa', imgs);
    //   if (imgs && imgs.length) {
    //     for (const item of imgs) {
    //       if (item.type === 'video') {
    //         item.safeurl = this.getSafeUrl(item.url);
    //       }
    //       if (item.music) {
    //         this.selectSoundsFrom.push(item.id);
    //         item.safeMusicUrl = this.getSafeUrl(item.music);
    //       }
    //     }
    //     if (this.slides.length == 0) {
    //       for (let items of imgs) {
    //         if (items.safeurl && items.page.dataUrl.substr(0, 4) == 'http' || items.page.dataUrl.substr(0 , 6) == 'assets') {
    //           this.slides = imgs;
    //           console.log(this.slides);
    //         }
    //       }
    //     }
    //     this.slidesLength.emit(this.slides.length);
    //   }
    // }
    if (this.previewSlide.slides.length) {
      for (const item of this.previewSlide.slides) {
        this.previewSlide.temporarySlide.push(item);
      }
      console.log(this.previewSlide.temporarySlide);
      console.log(this.previewSlide.slides);
    }
    if(localStorage.getItem('isSurvey')){
      // this.addSurvey();
      localStorage.removeItem('isSurvey');
    }
    this.utility.getSurvey().subscribe(res => {
      console.log(res);
      if (res) {
        this.addSurveyToSlide(res);
      }
    });
    const getInteraction = JSON.parse(localStorage.getItem('interactionCoords'));
    if (getInteraction){
        this.openInteraction(getInteraction.item, getInteraction.id, {x: getInteraction.x, y: getInteraction.y});
        localStorage.removeItem('interactionCoords');
    }
  }

  abortUpload(th): void {
    if (this.previewSlide.isRecorder == true){
      this.previewSlide.isRecording = false;
      this.previewSlide.startRecording = false;
      this.previewSlide.vidPlayer.record().stop();
      this.exitRecord();
      console.log('Aborted');
    }
    if (this.previewSlide.isS3Loader == true) {
      this.utility.setLoader(false);
      this.previewSlide.isS3Loader = false;
      th.utility.isAbort = true;
      if (this.MultipleVideo.nativeElement.value) {
        this.MultipleVideo.nativeElement.value = '';
      }
      if (this.MultipleImage.nativeElement.value) {
        this.MultipleImage.nativeElement.value = '';
      }
      this.previewSlide.temporarySlide.splice(this.previewSlide.temporarySlide.length - 1, 1);
      console.log(this.previewSlide.temporarySlide);
      this.previewSlide.s3.abortMultipartUpload((this.previewSlide.abortParams), (err, data) => {
        if (err) {
          console.log(err, err.stack); // an error occurred
          th.utility.isAbort = true;
        }
        else {
          console.log(data); // successful response
          th.utility.isAbort = true;
        }
      });
    }
  }

  ngOnDestroy(): void {
    this.utility.isFullscreen = false;
    if (this.previewSlide.vidPlayer) {
      this.previewSlide.vidPlayer.record().destroy();
    }
    this.previewSlide.slides.map((eachUrl) => {
      if (eachUrl.player) {
        eachUrl.player.record().destroy();
      }
    });
    if (!localStorage.getItem('ipDataPre')) {
      localStorage.setItem('ipDataPre', this.previewSlide.localData);
    }
    this.utility.setSurvey(null);
  }

  public capture(): void {
    const dialogRef = this.dialog.open(CaptureImageComponent, {
      panelClass: this.utility.dark ? 'dark' : 'app-full-screen-dialog',
      maxWidth: '100vw',
      maxHeight: '100vh',
      height: '100%',
      width: '100%',
      disableClose: false,
    });
    dialogRef.afterClosed().subscribe(res => {
      console.log(res);
      if (res) {
        console.log('noooo');
        this.previewSlide.webcamImage = res._imageAsDataUrl;
        console.log(this.previewSlide.webcamImage);
        const id = Math.random().toString(36).substring(2);
        this.previewSlide.temporarySlide.push({
          id,
          url : this.previewSlide.webcamImage,
          music : '',
          type : 'image'
        });
        const page = makePage({}, true);
        page.type = PageTypeEnum_Image;
        page.dataUrl = this.previewSlide.webcamImage;
        const ippage = makeIpPage({}, true);
        ippage.setPage(page);
        // this.previewSlide.ipPages.push(ippage);
        // this.addedPage.emit(this.previewSlide.ipPages);
        // this.slidesLength.emit(this.slides.length);
        this.utility.isAbort = false;
        let blob = this.b64toBlob(this.previewSlide.webcamImage);
        blob.name = `${new Date().getTime()}`;
        console.log(blob);
        this.s3Upload(blob, ippage);
      }
    });
  };

  makeEffects(item) {
    let effects = [];
    if (item.page.interactions && item.page.interactions['effects'] && item.page.interactions['effects'].length) {
      for (const effect of item.page.interactions['effects']) {
        const activation = effect.activations[0].activation;
        const eff: any = {
          x: activation.x,
          y: activation.y,
          time: activation.timeInMs,
          emoji: activation.marker.emoji.name,
          emojiObj: activation.marker.emoji.data
        };
        if (activation.gesture.type === GestureTypeEnum_Tap) {
          eff.gesture = 'tap';
        } else if (activation.gesture.type === GestureTypeEnum_Rub) {
          eff.gesture = 'rub';
        } else if (activation.gesture.type === GestureTypeEnum_LongTap) {
          eff.gesture = 'long-tap';
        }
        if (effect.dataUrl) {
          eff.text = effect.dataUrl;
        }
        if (effect.callToActions && effect.callToActions.length) {
          const userActions = [];
          for (const action of effect.callToActions) {
            if (action.type === CallToActionTypeEnum_Buy) {
              userActions.push({
                action: 'buy', value: 'https://www.google.com'
              });
            } else if (action.type === CallToActionTypeEnum_Visit) {
              userActions.push({
                action: 'visit', value: 'https://www.google.com'
              });
            } else if (action.type === CallToActionTypeEnum_Call) {
              userActions.push({
                action: 'call', value: action.data
              });
            } else if (action.type === CallToActionTypeEnum_Chat) {
              userActions.push({
                action: 'text', value: action.data
              });
            }
          }
          eff.userActions = userActions;
        }
        if (effect.type === EffectTypeEnum_Text) {
          eff.type = 'text';
        } else if (effect.type === EffectTypeEnum_Audio) {
          eff.type = 'audio';
        } else if (effect.type === EffectTypeEnum_Image) {
          eff.type = 'image';
        } else if (effect.type === EffectTypeEnum_Video) {
          eff.type = 'video';
        } else if (effect.type === EffectTypeEnum_Survey) {
          eff.type = 'survey';
        }
        effects.push(eff);
      }
    }
    return effects;
  }

  getBackDrop(data): void {
    history.pushState(null, null, location.href);
    this.location.onPopState(() => {
      if (data == 'step1') {
        this.menuImg.closeMenu();
      } else if (data == 'step2') {
        this.menuvideo.closeMenu();
      } else {
        this.menuAudio.closeMenu();
      }
    });
  }

  addFile(event: any): void {
    const maxFiles = 10;
    if (event.target.files) {
      if (event.target.files.length && event.target.files.length <= maxFiles) {
        if (event.target.files.length > 1) {
          this.addMultipleImages(event.target.files);
        } else {
          this.selectFile(event.target.files);
          event.target.value = '';
        }
      } else {
        if (event.target.files.length > maxFiles) {
          // this.translateSevice.get('toast.Maximum 5 files are allow to be Uploaded at once.').subscribe(res => {
          //   this.toast.info(res, '');
          // });
        }
      }
    }
  }

  SlidestotemporarySlides(): void {
    if (this.previewSlide.slides.length) {
      this.previewSlide.temporarySlide = [];
      for (const item of this.previewSlide.slides) {
        this.previewSlide.temporarySlide.push(item);
      }
    } else {
      this.previewSlide.temporarySlide = [];
    }
    console.log(this.previewSlide.temporarySlide);
    console.log(this.previewSlide.slides);
  }

  addMultipleImages(files): void {
    this.SlidestotemporarySlides();
    if (files.length === 0) {
      return;
    }
    const Files = files;
    const data = [];
    this.previewSlide.completeIndex = 0;

    const imageReaders = [];
    this.utility.isAbort = false;
    this.utility.setLoader(true);
    for (let i = 0; i < files.length; i++) {
      const file = files[i];
      const mimeType = file.type;
      if (mimeType.match(/image\/*/) == null) {
        return;
      }
      // TODO: Rushit use uuid instead of Math.random as the randomness is not really random
      const id = Math.random().toString(36).substring(2);
      const reader = new FileReader();
      imageReaders.push({ file, reader });
      reader.onload = (event: any) => {
        try {
          this.imageCompress
          .compressFile(event.target.result, -2, 40, 40)
          .then((result) => {
            try {
              // TODO: Rushit, why are you calling abort? I am commented it out for now
              data.push({
                id,
                type: 'image',
                url: '',
                items: JSON.parse(JSON.stringify(this.previewSlide.surveyItems)),
              });
              const page = makePage({}, true);
              page.type = PageTypeEnum_Image;
              // page.dataUrl = result;
              const ippage = makeIpPage({}, true);
              ippage.setPage(page);
              // this.previewSlide.ipPages.push(ippage);
              this.previewSlide.completeIndex++;
              if (this.previewSlide.completeIndex === files.length) {
                if (this.previewSlide.temporarySlide.length > 0) {
                  for (const item of data) {
                    this.previewSlide.temporarySlide.push(item);
                  }
                } else {
                  this.previewSlide.temporarySlide = data;
                }
                // this.addedPage.emit(this.previewSlide.ipPages);
                // this.slidesLength.emit(this.slides.length);
                // const arr = this.clearSafeUrl();
                // localStorage.setItem('ipDataPre', JSON.stringify(arr));
                this.previewSlide.completeIndex = 0;
              }
              else {
                if (this.previewSlide.completeIndex < imageReaders.length){
                  imageReaders[this.previewSlide.completeIndex].reader.readAsDataURL(
                    imageReaders[this.previewSlide.completeIndex].file
                  );
                }
              }
              console.log(this.previewSlide.temporarySlide);
              const blob = this.b64toBlob(result);
              blob.name = `${new Date().getTime()}`;
              this.s3Upload(blob, ippage);
              console.log('Image', i);
            } catch (err) {
              // this.utility.logMessage('ERR:' + err);
              throw err;
            }
          });
        } catch (err) {
          // this.utility.logMessage('ERR:' + err);
          throw err;
        }
      };
    }
    // Sequential processing, one image at a time
    if (imageReaders.length > 0) {
      imageReaders[0].reader.readAsDataURL(imageReaders[0].file);
    }
  }

  selectFile(files: File[]): void {
    this.SlidestotemporarySlides();
    if (files.length === 0) {
      return;
    }
    const file = files[0];
    const mimeType = file.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }
    // TODO: Rushit use uuid instead of Math.random as the randomness is not really random
    const reader = new FileReader();
    reader.onload = (event: any) => {
      // this.utility.logMessage(' onload');
      try {
        this.imageCompress
        .compressFile(event.target.result, -2, 40, 40)
        .then((result) => {
          try {
            const dialogRef = this.dialog.open(EditImageDialogComponent, {
              panelClass: this.utility.dark ? 'dark' : 'edit-image-dialog',
              maxWidth: '500px',
              maxHeight: '90vh',
              minHeight: '200px',
              data: {
                imageFile: result,
              },
              disableClose: false,
            });
            dialogRef.afterClosed().subscribe((res) => {
              if (res && res.data) {
                const unid = Math.random().toString(36).substring(2);
                const data = [];
                // this.utility.logMessage(' onload');
                const imgObj = {
                  id: unid,
                  type: 'image',
                  url: '',
                  items: JSON.parse(JSON.stringify(this.previewSlide.surveyItems)),
                  filterType: res.type,
                };
                // this.utility.logMessage(' Image compressed');
                console.log(this.previewSlide.slides);
                if (this.previewSlide.temporarySlide.length > 0) {
                  // this.temporarySlide = this.slides
                  this.previewSlide.temporarySlide.push(imgObj);
                  console.log(this.previewSlide.temporarySlide);
                  console.log(this.previewSlide.slides);
                } else {
                  this.previewSlide.temporarySlide = [imgObj];
                }
                const page = makePage({}, true);
                page.type = PageTypeEnum_Image;
                const ippage = makeIpPage({}, true);
                ippage.setPage(page);
                // this.previewSlide.ipPages.push(ippage);
                // this.addedPage.emit(this.previewSlide.ipPages);
                // this.slidesLength.emit(this.slides.length);
                // const arr = this.clearSafeUrl();
                // localStorage.setItem('ipDataPre', JSON.stringify(arr));
                // this.utility.setLoader(false);
                this.utility.isAbort = false;
                const blob = this.b64toBlob(res.data);
                blob.name = `${new Date().getTime()}`;
                console.log(blob);
                this.s3Upload(blob, ippage);
              }
            });
          } catch (err) {
            // this.utility.setLoader(false);
            // this.utility.logMessage('ERR:' + err);
            throw err;
          }
        });
      } catch (err) {
        // this.utility.setLoader(false);
        // this.utility.logMessage('ERR:' + err);
        throw err;
      }
    };
    reader.readAsDataURL(file);
  }

  updateImage(item, i): void {
    // this.selectedItem = item;
    const dialogRef = this.dialog.open(EditImageDialogComponent, {
      panelClass: this.utility.dark ? 'dark' : 'edit-image-dialog',
      maxWidth: '500px',
      maxHeight: '90vh',
      minHeight: '200px',
      data: {
        imageFile: item.url,
        selectedItem: item.filterType,
      },
      disableClose: false,
    });
    dialogRef.afterClosed().subscribe((res) => {
      if (res && res.data) {
        this.previewSlide.ipPages[i].page.dataUrl = res.data;
        const idx = this.previewSlide.temporarySlide.findIndex((x) => x.id === item.id);
        if (idx > -1) {
          this.previewSlide.temporarySlide[idx].url = res.data;
          this.previewSlide.temporarySlide[idx].filterType = res.type;
          // const arr = this.clearSafeUrl();
          // localStorage.setItem('ipDataPre', JSON.stringify(arr));
          // this.selectedItem = undefined;
        }
        this.utility.isAbort = false;
        const blob = this.b64toBlob(res.data);
        blob.name = `${new Date().getTime()}`;
        console.log(blob);
        this.s3Upload(blob, this.previewSlide.ipPages[i]);
      }
    });
  }

  addVideos(evnt: any): void {
    this.SlidestotemporarySlides();
    console.log(evnt);
    const files = evnt.target.files;
    console.log(files);
    if (files.length === 0) {
      return;
    }
    const data: any = [];
    this.utility.isAbort = false;
    this.utility.setLoader(true);
    for (let i = 0; i < files.length; i++) {
      const mimeType = files[i].type;
      if (mimeType.match(/video\/*/) == null) {
        alert('Only videos are supported.' + i);
        return;
      }
      // TODO: Rushit use uuid instead of Math.random as the randomness is not really random
      const id = Math.random().toString(36).substring(2);
      const reader = new FileReader();
      reader.onload = (event: any) => {
        // const vurl = window.URL.createObjectURL(
        //   this.b64toBlob(event.target.result)
        // );
        data.push({
          id,
          type: 'video',
          safeurl: '',
          url: '',
          items: JSON.parse(JSON.stringify(this.previewSlide.surveyItems)),
        });
        this.previewSlide.completeIndex++;
        if (this.previewSlide.completeIndex === files.length) {
          if (this.previewSlide.temporarySlide.length) {
            for (const item of data) {
              this.previewSlide.temporarySlide.push(item);
            }
          } else {
            this.previewSlide.temporarySlide = data;
          }
          // this.slidesLength.emit(this.slides.length);
          // const arr = this.clearSafeUrl();
          // localStorage.setItem('ipDataPre', JSON.stringify(arr));
          this.previewSlide.completeIndex = 0;
          // this.utility.setLoader(false);
          console.log(event.target);
        }
        const page = makePage({}, true);
        page.type = PageTypeEnum_Video;
        const ippage = makeIpPage({}, true);
        ippage.setPage(page);
        // this.previewSlide.ipPages.push(ippage);
        // this.addedPage.emit(this.previewSlide.ipPages);
        this.s3Upload(files[i], ippage);
      };
      reader.readAsDataURL(files[i]);
    }
  }

  s3Upload(file: any, ippage: any): void {
    const fileType = file.type.split('/').pop();
    const id = Math.random().toString().substring(2);
    console.log(id);
    const BucketName = environment.BucketName;
    const s3Key = `${id}.${fileType}`;
    const params = {
      Bucket: BucketName,
      Key: s3Key,
    };
    let uploadID;

    const chunkSize = 5242880;
    let chunkArray = [];
    let uploadedChunks = [];
    let i = 0;
    for (let offset = 0; offset < file.size; offset += chunkSize) {
      const chunk = file.slice(offset, offset + chunkSize);
      chunk.name = `${new Date().getTime()}`;
      chunkArray.push({ blob: chunk, partNumber: i + 1 });
      console.log(chunkArray);
      i++;
    }
    const s = this;
    console.log(this.utility.isAbort);
    this.utility.setLoader(true);
    this.previewSlide.isS3Loader = true;
    if (s.utility.isAbort != true){
      this.previewSlide.s3.createMultipartUpload(params, (err, data) => {
        if (err) {
          s.utility.setLoader(false);
          this.previewSlide.isS3Loader = false;
        } else {
          console.log(s);
          console.log(data);
          uploadID = data.UploadId;
          this.previewSlide.abortParams = {
            Bucket: environment.BucketName,
            Key: s3Key,
            UploadId: data.UploadId
          }
          for (let j = 0; j < chunkArray.length + 1; j++) {
            if (j < chunkArray.length) {
              if (s.utility.isAbort != true) {
                s.utility.setLoader(true);
                s.previewSlide.isS3Loader = true;
                s.uploadPart(s, chunkArray, uploadedChunks, j, uploadID, s3Key, ippage);
              } else {
                s.utility.setLoader(false);
                s.previewSlide.isS3Loader = false;
              }
            }
          }
        }
      });
    } else {
      s.utility.setLoader(false);
      s.previewSlide.isS3Loader = false;
    }
  }
  uploadPart(s: any, chunkArray: Array<any>, uploadedChunks: Array<any>, j: number, uploadID: string, s3Key: string, ippage: any): void {
    try {
      this.previewSlide.s3.uploadPart(
        {
          Body: chunkArray[j].blob,
          Bucket: environment.BucketName,
          Key: s3Key,
          PartNumber: chunkArray[j].partNumber,
          UploadId: uploadID,
          ContentLength: chunkArray[j].size,
        },
        (err, data) => {
          if (s.utility.isAbort != true) {
            s.utility.setLoader(true);
            s.previewSlide.isS3Loader = true;
          } else {
            s.utility.setLoader(false);
            s.previewSlide.isS3Loader = false;
           }
          if (data) {
            console.log(data, j);
            uploadedChunks.push({
              ETag: JSON.parse(data.ETag),
              PartNumber: chunkArray[j].partNumber,
            });
            console.log(uploadedChunks);
            console.log(chunkArray);
            let sortedPartArray;
            if (uploadedChunks.length == chunkArray.length) {
              sortedPartArray = {
                Parts: uploadedChunks.sort( (a, b) => {
                  return a.PartNumber - b.PartNumber;
                }),
              };
              console.log(this.utility.isAbort);
              if (s.utility.isAbort == false) {
                console.log('MultiPart');
                s.MultiPart(s, sortedPartArray, uploadID, s3Key, ippage);
              }  else {
                s.utility.setLoader(false);
                s.previewSlide.isS3Loader = false;
              }
            }
          } else {
            s.utility.setLoader(false);
            s.previewSlide.isS3Loader = false;
            console.log(err);
          }
        }
      );
    } catch (err) {
      s.utility.setLoader(false);
      s.toast.error('Upload Failed');
      console.log(err);
    }
  }
  MultiPart(s: any, sortedPartArray: object, uploadID: string, s3Key: string, ippage: any): void {
    try {
      if (s.utility.isAbort == false) {
        this.previewSlide.s3.completeMultipartUpload(
          {
            Bucket: environment.BucketName,
            Key: s3Key,
            MultipartUpload: sortedPartArray,
            UploadId: uploadID,
          },
          (err, response) => {
            if (this.utility.isAbort == false) {
              if(response){
                let sound = false;
                const temporarySlideArr = s.clearSafeUrl();
                console.log(temporarySlideArr);
                if (s.previewSlide.finalData.length > 0) {
                  for (let loop = 0; loop < s.previewSlide.finalData.length; loop++) {
                    if (ippage.page.pageId == s.previewSlide.finalData[loop].id) {
                      s.previewSlide.finalData[loop].sound = response.Location;
                      sound = true;
                      break;
                    }
                  }
                  if (sound != true) {
                    console.log(sound);
                    s.previewSlide.finalData.push({
                      id: ippage.page.pageId,
                      data: response.Location,
                      sound: '',
                    });
                    this.previewSlide.ipPages.push(ippage);
                    this.addedPage.emit(this.previewSlide.ipPages);
                  }
                } else {
                  console.log('elseeeee');
                  s.previewSlide.finalData.push({
                    id: ippage.page.pageId,
                    data: response.Location,
                    sound: '',
                  });
                  this.previewSlide.ipPages.push(ippage);
                  this.addedPage.emit(this.previewSlide.ipPages);
                }
                s.previewSlide.slides = [];
                console.log(s.previewSlide.finalData);
                for (let a = 0; a < s.previewSlide.finalData.length; a++) {
                  console.log(s.previewSlide.finalData.length);
                  temporarySlideArr[a].url = s.previewSlide.finalData[a].data;
                  temporarySlideArr[a].music = s.previewSlide.finalData[a].sound;
                  temporarySlideArr[a].safeurl = s.previewSlide.finalData[a].data;
                  temporarySlideArr[a].safeMusicUrl = s.previewSlide.finalData[a].sound;
                }
                console.log(temporarySlideArr);
                for (let b = 0; b < temporarySlideArr.length; b++) {
                  if (temporarySlideArr[b].url != '') {
                    s.previewSlide.slides.push(temporarySlideArr[b]);
                    // s.temporarySlide.push(arr[b]);
                  }
                }
                console.log(temporarySlideArr);
                const SlideArr = this.clearSafeUrlSlide();
                console.log(s.previewSlide.slides);
                s.slidesLength.emit(s.previewSlide.slides.length);
                localStorage.setItem('ipDataPre', JSON.stringify(SlideArr));
                console.log(s.previewSlide.finalData);
                s.uploadData.emit(s.previewSlide.finalData);
                s.utility.setLoader(false);
                // if (s.utility.setLoader(false)) {
                //   s.MultipleImage.nativeElement.value = '';
                //   console.log('clear');
                // }
                s.previewSlide.isS3Loader = false;
                // s.temporarySlide = [];
                // for (let b = 0; b < s.slides.length; b++) {
                //     s.temporarySlide.push(s.slides[b]);
                // }
                s.toast.success('Successfully Uploaded');
                this.showIntro();
                console.log('Final Data', response);
                console.log('Final Err', err);
            } else{
              s.utility.setLoader(false);
              s.previewSlide.isS3Loader = false;
            }
          }
        }
        );
      } else {
        s.utility.setLoader(false);
        s.previewSlide.isS3Loader = false;
      }
    } catch (err) {
      s.utility.setLoader(false);
      s.previewSlide.isS3Loader = false;
      s.toast.error('Upload Failed');
      console.log(err);
    }
  }

  deleteFile(file): void {
    this.utility.setLoader(true);
    const fileType = file.split('/').pop();
    const params = {
      Bucket: environment.BucketName,
      Key: fileType
    };
    const s = this;
    this.previewSlide.s3.deleteObject(params,  (err, data) => {
      if (err) {
        s.utility.setLoader(false);
        s.toast.error(err.message);
        console.log('There was an error deleting your file: ', err.message);
        return;
      }
      else{
        s.utility.setLoader(false);
        s.previewSlide.isS3Loader = false;
        s.toast.success('Successfully deleted file.');
      }
    });
  }

  menuOpen(data): void {
    if (data == 'step1') {
      this.previewSlide.isOpenMenu = true;
      this.getBackDrop(data);
    } else if (data == 'step2') {
      this.previewSlide.isOpenMenu = true;
      this.getBackDrop(data);
    } else {
      this.previewSlide.isOpenMenu = true;
      this.getBackDrop(data);
    }
  }

  menuClose(): void{
    this.previewSlide.isOpenMenu = false;
  }

  b64toBlob(dataURI: string): any {
    const byteString = atob(dataURI.split(',')[1]);
    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);

    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ab], { type: this.base64MimeType(dataURI) });
  }

  base64MimeType(encoded: any): any {
    let result = null;
    if (typeof encoded !== 'string') {
      return result;
    }
    const mime = encoded.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);
    if (mime && mime.length) {
      result = mime[1];
    }
    return result;
  }

  selectMp3(event: any, item: any, menu: MatMenuTrigger, index?: number): void {
    const files = event.target.files;
    menu.closeMenu();
    console.log(this.previewSlide.ipPages[index].page.pageId);
    if (this.previewSlide.ipPages.length) {
      this.previewSlide.ipPages[index].page.soundTrackUrl = window.URL.createObjectURL(
        files[0]
      );
      // this.addedPage.emit(this.previewSlide.ipPages);
    }
    if (files.length) {
      this.previewSlide.selectSoundsFrom.push(item.id);
      item.isPlay = false;
      item.music = null;
      item.safeMusicUrl = null;
      // item.music = window.URL.createObjectURL(files[0]);
      // item.safeMusicUrl = this.getSafeUrl(item.music);
      const images = localStorage.getItem('ipDataPre');
      if (images) {
        const arr = JSON.parse(images);
        for (const data of arr) {
          if (data.id === item.id) {
            data.music = window.URL.createObjectURL(files[0]);
            // localStorage.setItem('ipDataPre', JSON.stringify(arr));
          }
        }
      }
      // const Blob = this.b64toBlob(item.url);
      // console.log(item)
      // console.log(Blob);
      this.utility.isAbort = false;
      this.s3Upload(files[0], this.previewSlide.ipPages[index]);
    }
    event.target.value = '';
    console.log(event);
    console.log(item);
  }

  deleteSound(row: any, index?: number): void {
    if (this.previewSlide.ipPages.length) {
      this.previewSlide.ipPages[index].page.soundTrackUrl = '';
      this.addedPage.emit(this.previewSlide.ipPages);
    }
    row.isPlay = false;
    delete row.music;
    this.previewSlide.selectSoundsFrom = this.previewSlide.selectSoundsFrom.filter(
      (x: any) => x !== row.id
    );
    const images = localStorage.getItem('ipDataPre');
    if (images) {
      const imgs = JSON.parse(images);
      if (imgs.length) {
        for (const item of imgs) {
          if (item.id === row.id) {
            delete item.music;
          }
        }
        localStorage.setItem('ipDataPre', JSON.stringify(imgs));
      }
    }
  }

  getSafeUrl(url: string): SafeUrl {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

  drop(event: CdkDragDrop<string[]>): void {
    moveItemInArray(this.previewSlide.temporarySlide, event.previousIndex, event.currentIndex);
    moveItemInArray(this.previewSlide.slides, event.previousIndex, event.currentIndex);
    moveItemInArray(this.previewSlide.ipPages, event.previousIndex, event.currentIndex);
    moveItemInArray(this.previewSlide.finalData, event.previousIndex, event.currentIndex);
    const arr = [];
    for (const item of this.previewSlide.slides) {
      const obj: any = { id: item.id, type: item.type, url: item.url };
      if (item.music) {
        obj.music = item.music;
      }
      if (item.surveyUrl) {
        obj.surveyUrl = item.surveyUrl;
      }
      obj.name = item.name;
      obj.link = item.link;
      arr.push(obj);
    }
    // for (let a = 0; a < this.finalData.length; a++) {
    //   arr[a].url = this.finalData[a].data;
    //   arr[a].music = this.finalData[a].sound;
    // }
    localStorage.setItem('ipDataPre', JSON.stringify(arr));
  }

  deletePages(): void{
    const tempArr = [];
    let ipValue;
    let newIP;
    for (let i = 0; i < this.previewSlide.ipPages.length; i++) {
       ipValue = this.previewSlide.ipPages[i].page.dataUrl.toString();
       newIP = ipValue.split('/').pop();
       tempArr.push({ key : newIP});
    }
    const temp = {
      Objects: JSON.stringify(tempArr)
    };
    const keyString = JSON.stringify(temp);
    console.log(keyString);
    const dialogRef = this.dialog.open(ConfirmationComponent, {
      panelClass: this.utility.dark ? 'dark' : '',
      minWidth: '250px',
      autoFocus: false,
      data: {
        message: this.translateSevice.instant('previewImages.deleteContentPages'),
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        // this.deleteFile(temp);
      //   for(let i=0; i<this.ipPages.length; i++){
      //     ipValue = this.ipPages[i].page.dataUrl.toString();
      //     this.deleteFile(ipValue);
      //     console.log(ipValue);
      //  }
        // this.deleteFile(item.url);
        // const idx = this.slides.findIndex((item) => item.id === id);
        // const data = this.slides.filter((item) => item.id !== id);
        this.previewSlide.ipPages = [];
        this.previewSlide.finalData = [];
        this.previewSlide.slides = [];
        this.previewSlide.temporarySlide = [];
        this.previewSlide.ipPages = [];
        this.addedPage.emit(this.previewSlide.ipPages);
        this.slidesLength.emit(this.previewSlide.slides.length);
        this.toast.success('Successfully deleted file.');
      }
    });
  }

  playSurvey(item): void{
    console.log(item);
    this.router.navigate(['/play'], { queryParams: { name: item.name, url: item.link } });
  }

  deleteItem(id: string, item): void {
    const dialogRef = this.dialog.open(ConfirmationComponent, {
      panelClass: this.utility.dark ? 'dark' : '',
      minWidth: '250px',
      autoFocus: false,
      data: {
        message: this.translateSevice.instant('previewImages.ConfirmationText'),
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      console.log(item);
      if (result) {
        const idx = this.previewSlide.slides.findIndex((item) => item.id === id);
        const data = this.previewSlide.slides.filter((item) => item.id !== id);
        this.deleteFile(item.url);
        this.previewSlide.ipPages.splice(idx, 1);
        this.previewSlide.finalData.splice(idx, 1);
        this.previewSlide.slides.splice(idx, 1);
        if (data.length) {
          this.previewSlide.slides = data;
          const arr = this.clearSafeUrlSlide();
          localStorage.setItem('ipDataPre', JSON.stringify(arr));
        } else {
          this.previewSlide.slides = data;
          localStorage.removeItem('ipDataPre');
          localStorage.removeItem('info');
          localStorage.removeItem('selectedIdx');
        }
        // this.temporarySlide = this.slides;
        this.previewSlide.temporarySlide = [];
        for (const item of this.previewSlide.slides) {
          this.previewSlide.temporarySlide.push(item);
        }
        this.slidesLength.emit(this.previewSlide.slides.length);
      }
    });
  }

  clearSafeUrl(): any[] {
    const arr = [];
    for (const item of this.previewSlide.temporarySlide) {
      arr.push({
        id: item.id,
        type: item.type,
        url: item.url ? item.url : '',
        music: item.music,
        item: item.item,
        surveyUrl: item.surveyUrl,
        effects: item.effects,
        mode: item.mode,
        filterType: item.filterType,
        name: item.name,
        link: item.link
      });
    }
    return arr;
  }

   clearSafeUrlSlide(): any[] {
    const arr = [];
    for (const item of this.previewSlide.slides) {
      arr.push({
        id: item.id,
        type: item.type,
        url: item.url ? item.url : '',
        music: item.music,
        item: item.item,
        surveyUrl: item.surveyUrl,
        effects: item.effects,
        mode: item.mode,
        filterType: item.filterType,
        name: item.name,
        link: item.link
      });
    }
    return arr;
  }

  openRecorder(item: any, index?: number): void {
    console.log(this.previewSlide.isRecorder);
    let flag = false;
    this.previewSlide.id = item.id;
    this.previewSlide.options = {
      controls: true,
      width: 600,
      height: 75,
      fluid: false,
      controlBar: {
        // hide fullscreen and volume controls
        fullscreenToggle: false,
        volumePanel: false,
      },
      plugins: {
        wavesurfer: {
          src: 'live',
          waveColor: '#ffffff',
          progressColor: '#7def68',
          debug: true,
          cursorWidth: 1,
          msDisplayMax: 20,
          hideScrollbar: true,
        },
        record: {
          audio: true,
          video: false,
          maxLength: 600,
          debug: true,
          timeSlice: 100000,
        },
      },
    };

    if (!item.initialized) {
      setTimeout(() => {
        // apply audio workarounds for certain browsers
        this.applyAudioWorkaround();
        // create player

        item.player = videojs('myAudio' + item.id, this.previewSlide.options, () => {
          // print version information at startup
          const msg =
            'Using video.js ' +
            videojs.VERSION +
            ' with videojs-record ' +
            videojs.getPluginVersion('record') +
            ', videojs-wavesurfer ' +
            videojs.getPluginVersion('wavesurfer') +
            ', wavesurfer.js ' +
            WaveSurfer.VERSION +
            ' and recordrtc ' +
            RecordRTC.version;
          videojs.log(msg);
        });

        setTimeout(() => {
          item.player.record().getDevice();
        }, 300);

        // error handling
        item.player.on('deviceError', () => {
          console.log('device error:', item.player.deviceErrorCode);
          // this.translateSevice.get('toast.Device not found!').subscribe(res => {
          //   this.toast.error(res);
          // });
          this.previewSlide.id = '';
        });

        item.player.on('error', (error: any) => {
          console.error(error);
        });

        // user clicked the record button and started recording
        item.player.on('startRecord', () => {
          console.log('started recording!');
          item.startRecord = true;
        });

        // user completed recording and stream is available
        item.player.on('finishRecord', () => {
          // the blob object contains the recorded data that
          // can be downloaded by the user, stored on server etc
          this.previewSlide.selectSoundsFrom.push(item.id);
          this.previewSlide.id = '';
          setTimeout(() => {
            this.cd.detectChanges();
          }, 500);
          console.log('finished recording: ', item.player.recordedData);
          item.isPlay = false;
          item.music = null;
          flag = true;
          item.safeMusicUrl = null;
          // item.music = window.URL.createObjectURL(item.player.recordedData);
          // item.safeMusicUrl = this.getSafeUrl(item.music);
          // this.addedPage.emit(this.previewSlide.ipPages);
          const images = localStorage.getItem('ipDataPre');
          // if (images) {
          //   const arr = JSON.parse(images);
          //   for (const data of arr) {
          //     if (data.id === item.id) {
          //       data.music = window.URL.createObjectURL(
          //         item.player.recordedData
          //       );
          //       this.ipPages[
          //         index
          //       ].page.soundTrackUrl = window.URL.createObjectURL(
          //         item.player.recordedData
          //       );
          //       // localStorage.setItem('ipDataPre', JSON.stringify(arr));
          //     }
          //   }
          // }
          this.utility.isAbort = false;
          this.s3Upload(
            item.player.recordedData,
            this.previewSlide.ipPages[index]
          );
          item.startRecord = false;
        });
        item.initialized = true;
        this.setCss();
      }, 100);
    }
  }

  applyAudioWorkaround(): void {
    const WINDOW: any = window;
    const isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
    const isEdge = /Edge/.test(navigator.userAgent);
    if (isSafari || isEdge) {
      if (isSafari && WINDOW.MediaRecorder !== undefined) {
        // this version of Safari has MediaRecorder
        return;
      }

      // support recording in safari 11/12
      // see https://github.com/collab-project/videojs-record/issues/295
      this.previewSlide.options.plugins.record.audioRecorderType = StereoAudioRecorder;
      this.previewSlide.options.plugins.record.audioSampleRate = 44100;
      this.previewSlide.options.plugins.record.audioBufferSize = 4096;
      this.previewSlide.options.plugins.record.audioChannels = 2;

      console.log('applied audio workarounds for this browser');
    }
  }

  playPause(item: any): void {
    item.isRecording = !item.isRecording;
    if (item.isRecording) {
      item.player.record().pause();
    } else {
      item.player.record().resume();
    }
  }

  openInteraction(item: any, index, coOrdinates): void {
    const arr = this.clearSafeUrl();
    localStorage.setItem('AddEffects', JSON.stringify(arr));
    this.dialog
    .open(InteractionComponent, {
      panelClass: this.utility.dark ? 'dark' : 'edit-image-dialog',
      maxWidth: '500px',
      minWidth: '315px',
      maxHeight: '90vh',
      data: {
        selectedItem: item,
        interaction: this.previewSlide.ipPages[index].page.interactions,
        coordinates: coOrdinates
      },
      closeOnNavigation: false
    })
    .afterClosed()
    .subscribe((result) => {
      if (this.previewSlide.ipPages[index].page) {
        if(result){
          console.log(result);
          if (result.x){
            console.log(result);
            const Obj = {
              item : item,
              id : index,
              x : result.x,
              y: result.y
            }
            console.log(Obj);
            localStorage.setItem('interactionCoords', JSON.stringify(Obj));
            if(result.interactions.effects && result.interactions.effects.length){
              this.previewSlide.ipPages[index].page.interactions = result;
              this.previewSlide.slides[index].effects = this.utility.gestArray;
            }
          }
          else{
            console.log(this.previewSlide.slides);
            console.log('else');
            this.previewSlide.ipPages[index].page.interactions = result;
            this.previewSlide.slides[index].effects = this.utility.gestArray;
          }
      }
      }
    });
  }

  addRouteForSurvey(): void {
    this.router.navigate(['survey']);
  }

  addSurveyToSlide(survey): void {
    const unid = Math.random().toString(36).substring(2);
    const data = [];
    const imgObj = {
      id: unid,
      type: 'survey',
      url: 'assets/images/surveyPlaceholderHeader.png',
      items: JSON.parse(JSON.stringify(this.previewSlide.surveyItems)),
      filterType: 'normal',
      name : survey.name,
      link : survey.link
    };
    if (this.previewSlide.slides.length > 0) {
      this.previewSlide.slides.push(imgObj);
    } else {
      this.previewSlide.slides = [imgObj];
    }
    if (this.previewSlide.temporarySlide.length > 0) {
      this.previewSlide.temporarySlide.push(imgObj);
    } else {
      this.previewSlide.temporarySlide = [imgObj];
    }
    const page = makePage({}, true);
    page.type = PageTypeEnum_Survey;
    page.dataUrl = survey.link;
    const ippage = makeIpPage({}, true);
    ippage.setPage(page);
    this.previewSlide.ipPages.push(ippage);
    this.addedPage.emit(this.previewSlide.ipPages);
    this.slidesLength.emit(this.previewSlide.slides.length);
    console.log(this.previewSlide.ipPages);
    console.log(this.previewSlide.slides);
    const arr = this.clearSafeUrlSlide();
    this.previewSlide.finalData.push({
      data: survey.link,
      id: ippage.pageId,
      name: survey.name,
      sound: ''
    });
    localStorage.setItem('ipDataPre', JSON.stringify(arr));
    this.showIntro();

}

  // addSurvey(): void {
  //   this.dialog
  //   .open(AssetsDialogComponent, {
  //     panelClass: this.utility.dark ? 'dark' : 'survey-dialog',
  //     width: '40vw',
  //     maxHeight: '90vh',
  //   })
  //   .afterClosed()
  //   .subscribe((survey) => {
  //     console.log(survey);
  //     if (survey) {
  //       const unid = Math.random().toString(36).substring(2);
  //       const data = [];
  //       const imgObj = {
  //         id: unid,
  //         type: 'survey',
  //         url: 'assets/images/surveyPlaceholderHeader.png',
  //         items: JSON.parse(JSON.stringify(this.previewSlide.surveyItems)),
  //         filterType: 'normal',
  //         name : survey.name,
  //         link : survey.link
  //       };
  //       if (this.previewSlide.slides.length > 0) {
  //         this.previewSlide.slides.push(imgObj);
  //       } else {
  //         this.previewSlide.slides = [imgObj];
  //       }
  //       if (this.previewSlide.temporarySlide.length > 0) {
  //         this.previewSlide.temporarySlide.push(imgObj);
  //       } else {
  //         this.previewSlide.temporarySlide = [imgObj];
  //       }
  //       const page = makePage({}, true);
  //       page.type = PageTypeEnum_Survey;
  //       page.dataUrl = survey.link;
  //       const ippage = makeIpPage({}, true);
  //       ippage.setPage(page);
  //       this.previewSlide.ipPages.push(ippage);
  //       this.addedPage.emit(this.previewSlide.ipPages);
  //       this.slidesLength.emit(this.previewSlide.slides.length);
  //       console.log(this.previewSlide.ipPages);
  //       console.log(this.previewSlide.slides);
  //       const arr = this.clearSafeUrlSlide();
  //       this.previewSlide.finalData.push({
  //         data: survey.link,
  //         id: ippage.pageId,
  //         name: survey.name,
  //         sound: ''
  //       });
  //       localStorage.setItem('ipDataPre', JSON.stringify(arr));
  //       this.showIntro();
  //     }
  //   });
  // }

  openAssets(item: any): any {
    localStorage.setItem('selectedIdx', item.id);
    this.dialog
    .open(AssetsDialogComponent, {
      panelClass: 'survey-dialog',
      width: '40vw',
      maxHeight: '90vh',
      data: {
        selectedItem: item,
      },
    })
    .afterClosed()
    .subscribe(() => {
      const images = localStorage.getItem('ipDataPre');
      if (images) {
        const imgs = JSON.parse(images);
        if (imgs && imgs.length) {
          for (const itm of imgs) {
            if (itm.type === 'video') {
              itm.safeurl = this.getSafeUrl(itm.url);
            }
            if (itm.music) {
              this.previewSlide.selectSoundsFrom.push(itm.id);
              itm.safeMusicUrl = this.getSafeUrl(itm.music);
            }
          }
          this.previewSlide.slides = imgs;
        }
      }
      const asstArr = JSON.parse(localStorage.getItem('assetsArray'));
      if (asstArr) {
        for (const data of asstArr) {
          this.previewSlide.selectItemsFrom.push(data.selectedItemIdx);
        }
      }
    });
  }

  removeSurvey(data): void {
    this.previewSlide.selectItemsFrom = this.previewSlide.selectItemsFrom.filter((x) => x !== data.id);

    const imgs = JSON.parse(localStorage.getItem('ipDataPre'));
    if (imgs && imgs.length) {
      const idx = imgs.findIndex((item) => item.id === data.id);
      if (idx !== -1) {
        delete imgs[idx].surveyUrl;
        localStorage.setItem('ipDataPre', JSON.stringify(imgs));
      }
    }

    const asstArr = JSON.parse(localStorage.getItem('assetsArray'));
    if (asstArr) {
      const index = asstArr.findIndex(
        (item) => item.selectedItemIdx === data.id
      );
      if (index !== -1) {
        asstArr.splice(index, 1);
        localStorage.setItem('assetsArray', JSON.stringify(asstArr));
      }
    }
  }

  recordVid(): void {
    this.SlidestotemporarySlides();
    this.previewSlide.isRecorder = true;
    this.utility.isFullscreen = true;
    this.previewSlide.showVideoRec = true;
    let flag = false;
    if (this.previewSlide.front) {
      this.previewSlide.options = {
        controls: true,
        width: 400,
        height: 200,
        fluid: false,
        controlBar: {
          // hide fullscreen and volume controls
          fullscreenToggle: false,
          volumePanel: false,
          pipToggle: false,
        },
        plugins: {
          record: {
            audio: true,
            video: {
              facingMode: 'user',
            },
            maxLength: 600,
            debug: true,
            pip: false,
            timeSlice: 72000,
          },
        },
      };
    } else {
      this.previewSlide.options = {
        controls: true,
        width: 400,
        height: 200,
        fluid: false,
        controlBar: {
          // hide fullscreen and volume controls
          fullscreenToggle: false,
          volumePanel: false,
          pipToggle: false,
        },
        plugins: {
          record: {
            audio: true,
            video: {
              facingMode: 'environment',
            },
            maxLength: 100,
            debug: true,
            pip: false,
            timeSlice: 72000,
          },
        },
      };
    }

    // apply audio workarounds for certain browsers
    this.applyAudioWorkaround();
    // create player
    this.previewSlide.vidPlayer = videojs('myvideo', this.previewSlide.options, () => {
      // print version information at startup
      const msg =
        'Using video.js ' +
        videojs.VERSION +
        ' with videojs-record ' +
        videojs.getPluginVersion('record') +
        ', videojs-wavesurfer ' +
        videojs.getPluginVersion('wavesurfer') +
        ', wavesurfer.js ' +
        WaveSurfer.VERSION +
        ' and recordrtc ' +
        RecordRTC.version;
      videojs.log(msg);
    });
    this.previewSlide.vidPlayer.record().getDevice();

    // error handgling
    this.previewSlide.vidPlayer.on('deviceError', () => {
      console.log('-----device-error-----');
      this.previewSlide.isIos = true;
      this.utility.isFullscreen = false;
      // this.uploadVid();
      // console.log('device error:', this.vidPlayer.deviceErrorCode);
      // this.translateSevice.get('toast.Device not found!').subscribe(res => {
      //   this.toast.error(res);
      // });
      this.previewSlide.showVideoRec = false;
    });

    this.previewSlide.vidPlayer.on('error', () => {
      console.log('-----error-----');
      this.previewSlide.isIos = true;
      this.utility.isFullscreen = false;
      // this.uploadVid();
      // console.log('device error:', this.vidPlayer.deviceErrorCode);
      // this.translateSevice.get('toast.webrtcerror').subscribe(res => {
      //   this.toast.error(res);
      // });
      this.previewSlide.showVideoRec = false;
    });

    // user completed recording and stream is available
    this.previewSlide.vidPlayer.on('finishRecord', () => {
      console.log('-----finishRecord-----');
      const data = [];
      this.previewSlide.showVideoRec = false;
      this.utility.isFullscreen = false;
      flag = true;
      this.previewSlide.vidPlayer.off('finishRecord');
      this.previewSlide.vidPlayer.record().stopDevice();
      this.previewSlide.isRecording = false;

      this.previewSlide.startRecording = false;

      const id = Math.random().toString(36).substring(2);
      data.push({
        id,
        type: 'video',
        safeurl: '',
        url: '',
        items: JSON.parse(JSON.stringify(this.previewSlide.surveyItems)),
      });
      if (this.previewSlide.temporarySlide.length) {
        for (const item of data) {
          this.previewSlide.temporarySlide.push(item);
        }
      } else {
        this.previewSlide.temporarySlide = data;
      }
      const page = makePage({}, true);
      page.type = PageTypeEnum_Video;
      // page.dataUrl = window.URL.createObjectURL(this.vidPlayer.recordedData);
      const ippage = makeIpPage({}, true);
      ippage.setPage(page);
      // this.previewSlide.ipPages.push(ippage);
      // this.addedPage.emit(this.previewSlide.ipPages);
      // this.slidesLength.emit(this.previewSlide.slides.length);
      // const arr = this.clearSafeUrl();
      // localStorage.setItem('ipDataPre', JSON.stringify(arr));
      this.utility.isAbort = false;
      this.s3Upload(this.previewSlide.vidPlayer.recordedData, ippage);
    });
    this.setCss();
  }

  playPauseVid(): void {
    this.previewSlide.isRecording = !this.previewSlide.isRecording;
    if (this.previewSlide.isRecording) {
      this.previewSlide.vidPlayer.record().pause();
    } else {
      this.previewSlide.vidPlayer.record().resume();
    }
  }

  flipCamera(): void {
    this.previewSlide.vidPlayer.record().stopDevice();
    this.previewSlide.vidPlayer.record().destroy();
    const element = document.getElementById('vid-parent');
    if (element) {
      element.innerHTML +=
        '<video id="myvideo" class="video-js vjs-default-skin parent-audio"></video>';
    }
    this.previewSlide.front = !this.previewSlide.front;
    this.previewSlide.vidPlayer.off('finishRecord');
    this.previewSlide.options = undefined;
    this.previewSlide.vidPlayer = undefined;
    this.recordVid();
  }

  exitRecord(): void {
    this.utility.isFullscreen = false;
    this.previewSlide.vidPlayer.record().stopDevice();
    this.previewSlide.vidPlayer.record().destroy();
    const element = document.getElementById('vid-parent');
    if (element) {
      element.innerHTML +=
        '<video id="myvideo" class="video-js vjs-default-skin parent-audio"></video>';
    }
    this.previewSlide.vidPlayer.off('finishRecord');
    this.previewSlide.options = undefined;
    this.previewSlide.vidPlayer = undefined;
    this.previewSlide.showVideoRec = false;
  }

  startRecord(): void {
    console.log('heyyy');
    this.previewSlide.vidPlayer.record().start();
    this.previewSlide.startRecording = true;
  }

  stopRecord(): void {
    this.utility.isFullscreen = false;
    this.previewSlide.vidPlayer.record().stop();
  }

  getOS(): void {
    const platform = window.navigator.platform;
    const iosPlatforms = ['iPhone', 'iPad', 'iPod'];
    if (iosPlatforms.indexOf(platform) !== -1) {
      this.previewSlide.isIos = true;
    }
  }

  setCss(): void {
    const audio = document.getElementsByClassName('parent-audio');
    if (audio.length) {
      for (let i = 0; i < audio.length; i++) {
        audio[i].setAttribute('style', 'background-color: ' + '#000000');
      }
    }
    const controls = document.getElementsByClassName('vjs-control-bar');
    if (controls.length) {
      for (let i = 0; i < controls.length; i++) {
        controls[i].setAttribute(
          'style',
          'display: flex;background-color: ' + 'grey'
        );
      }
    }
    const elements = document.getElementsByClassName('vjs-control');
    if (elements.length) {
      for (let i = 0; i < elements.length; i++) {
        elements[i].setAttribute('style', 'color: ' + '#ffffff');
      }
    }
    const controlsbar = document.getElementsByClassName('vjs-time-control');
    if (controlsbar.length) {
      for (let i = 0; i < controlsbar.length; i++) {
        controlsbar[i].setAttribute(
          'style',
          'display: flex;color:' + '#ffffff'
        );
      }
    }
  }

  showIntro(): void {
    const intro = localStorage.getItem('showIntro');
    if (!intro) {
      setTimeout(() => {
        const introArr = [];
        this.translateSevice.get('intro.AddSound').subscribe((res) => {
          const obj = {
            element: '#addSound',
            intro: res,
          };
          introArr.push(obj);
        });
        // this.translateSevice.get('intro.AddSurvey').subscribe(res => {
        //   const obj = {
        //     element: '#addSurvey',
        //     intro: res
        //   };
        //   introArr.push(obj);
        // });
        this.translateSevice.get('intro.Edit').subscribe((res) => {
          const obj = {
            element: '#edit',
            intro: res,
          };
          introArr.push(obj);
        });
        this.translateSevice.get('intro.AddInteraction').subscribe((res) => {
          const obj = {
            element: '#addInteraction',
            intro: res,
          };
          introArr.push(obj);
        });
        this.translateSevice.get('intro.Remove').subscribe((res) => {
          const obj = {
            element: '#remove',
            intro: res,
          };
          introArr.push(obj);
        });
        const introobj = {
          steps: introArr,
          nextLabel: 'Next',
          prevLabel: 'Back',
          skipLabel: 'Skip',
          doneLabel: 'Done',
          showStepNumbers: false,
        };
        this.translateSevice
          .get('previewImages.gesture.Next')
          .subscribe((res) => {
            introobj.nextLabel = res;
          });
        this.translateSevice.get('builder.BackButton').subscribe((res) => {
          introobj.prevLabel = res;
        });
        this.translateSevice.get('intro.Skip').subscribe((res) => {
          introobj.skipLabel = res;
        });
        this.translateSevice
          .get('previewImages.gesture.Done')
          .subscribe((res) => {
            introobj.doneLabel = res;
          });
        this.previewSlide.introJS.setOptions(introobj).start();
        localStorage.setItem('showIntro', 'true');
      }, 2000);
    }
  }
}
