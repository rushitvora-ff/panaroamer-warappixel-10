import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NgxImageCompressService} from 'ngx-image-compress';
import { BuilderRoutingModule } from './builder-routing.module';
import { BuilderComponent } from './builder/builder.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { PreviewSlideComponent } from './preview-slide/preview-slide.component';
import { DemoMaterialModule } from '../demo-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SharedModule } from '../shared/shared.module';
import {WebcamModule} from 'ngx-webcam';

@NgModule({
  declarations: [BuilderComponent, PreviewSlideComponent],
  imports: [
    CommonModule,
    BuilderRoutingModule,
    DemoMaterialModule,
    FlexLayoutModule,
    FormsModule,
    WebcamModule,
    DragDropModule,
    SharedModule,
    ReactiveFormsModule
  ],
  providers: [NgxImageCompressService]
})
export class BuilderModule { }
