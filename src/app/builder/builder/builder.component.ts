import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { NgxImageCompressService } from 'ngx-image-compress';
import { MatDialog } from '@angular/material/dialog';
import { UtilityService } from '../../services/utility.service';
import { EditImageDialogComponent } from 'src/app/shared/dialogs/edit-image-dialog/edit-image-dialog.component';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { Ip, makeIp } from '@oneb/ip-api-clients-models';
import { IpPage, makeIpPage } from '@oneb/ip-api-clients-models';
import { makePage } from '@oneb/ip-api-clients-models';
import { PageTypeEnum_Image } from '@oneb/ip-api-clients-models';
import { MapDialogComponent } from 'src/app/shared/dialogs/map-dialog/map-dialog.component';
import { StepperSelectionEvent } from '@angular/cdk/stepper';
import * as AWS from 'aws-sdk';
import { BuilderInterface } from '../../interfaces/builderModule';
import { PlatformLocation } from '@angular/common';
import { environment } from '../../../environments/environment';
import { IpPreviewService } from '../../services/ip-preview.service';
import { clientResponseSuccess, clientResponseHasErrors } from '@rainmaker2/rainmaker2-codegen-api-clients-common';

@Component({
  selector: 'app-builder',
  templateUrl: './builder.component.html',
  styleUrls: ['./builder.component.scss']
})

export class BuilderComponent implements OnInit, OnDestroy {
  builder: BuilderInterface = {
      openPanel: true,
      openCoverpagePanel: true,
      openPreviewPanel: true,
      firstFormGroup: Object.create(null),
      secondFormGroup: Object.create(null),
      ipPages: [],
      s3: new AWS.S3({
        accessKeyId: environment.accessKeyId,
        secretAccessKey: environment.secretAccessKey,
        region: environment.region,
      })
  };
  // openPanel: true;
  // openCoverpagePanel = true;
  // openPreviewPanel = true;
  // firstFormGroup: FormGroup = Object.create(null);
  // secondFormGroup: FormGroup = Object.create(null);
  // slidesLength: number;
  // ip: Ip;
  // abortParams;
  // isS3Loader;
  // s3 = new AWS.S3({
  //   accessKeyId: environment.accessKeyId,
  //   secretAccessKey: environment.secretAccessKey,
  //   region: environment.region,
  // });
  // ipPages: IpPage[] = [];
  // latlong;
  constructor(
    private location: PlatformLocation,
    public dialog: MatDialog,
    public ipPreviewService: IpPreviewService,
    private formBuilder: FormBuilder,
    private router: Router,
    private toast: ToastrService,
    private translateSevice: TranslateService,
    public utility: UtilityService,
    private imageCompress: NgxImageCompressService) {
      console.log(this.builder);
      const latlong = localStorage.getItem('latlong');
      this.builder.latlong = latlong;
      console.log('latlongcontructor', this.builder.latlong);
      this.location.onPopState(() => {
        if (this.builder.isS3Loader == true) {
          this.utility.setLoader(false);
          this.builder.isS3Loader = false;
          this.builder.s3.abortMultipartUpload(this.builder.abortParams, (err, data) => {
            if (err) {
              console.log(err, err.stack);
            } // an error occurred
            else {
              console.log(data);
            } // successful response
          });
        }
      });
    }

  ngOnInit(): void {
    const ip = localStorage.getItem('contentImage');
    if (ip) {
      this.utility.createdIP = JSON.parse(ip);
      localStorage.removeItem('contentImage');
    }
    if (this.utility.createdIP) {
      this.builder.ip = this.utility.createdIP;
      if (this.builder.ip.mainPages && this.builder.ip.mainPages.length) {
        this.builder.ipPages = this.builder.ip.mainPages;
      }
      if (this.builder.ip.frontCoverPages.length) {
        this.utility.coverImageUrl = this.builder.ip.frontCoverPages[0].page.dataUrl;
      }
    } else {
      this.builder.ip = makeIp({}, true);
    }
    this.builder.firstFormGroup = this.formBuilder.group({
      title: ['', Validators.required],
      description: [''],
      category: [''],
      price: ['']
    });
    this.builder.secondFormGroup = this.formBuilder.group({
      images: ['', Validators.required]
    });
    const info = JSON.parse(localStorage.getItem('info'));
    if (info) {
      this.builder.firstFormGroup.patchValue({
        title: info.title,
        description: info.description,
        category: info.category ? info.category : 'Default'
      });
    }
  }

  ngOnDestroy(): void {
    this.utility.createdIP = this.builder.ip;
    localStorage.setItem('contentImage', JSON.stringify(this.utility.createdIP) );
  }

  open(event): void{
    if (event.selectedIndex == 0){
      this.builder.openPanel = true;
    }
    else if (event.selectedIndex == 1){
       this.builder.openCoverpagePanel = true;
    }
    else if (event.selectedIndex == 2){
       this.builder.openPreviewPanel = true;
    }
  }

  addedPage(ipPages: IpPage[]): void {
    this.builder.ipPages = ipPages;
    this.builder.ip.mainPages = this.builder.ipPages;
  }

  getS3upload(event): void {
    console.log(event);
    console.log(this.builder.ipPages);
    for (let events = 0; events < event.length; events++) {
      for (let pages = 0; pages < this.builder.ipPages.length; pages++) {
        if (this.builder.ipPages[pages].page.pageId == event[events].id) {
          this.builder.ipPages[pages].page.dataUrl = event[events].data;
          this.builder.ipPages[pages].page.soundTrackUrl = event[events].sound;
          break;
        }
      }
    }
    console.log('updated', this.builder.ipPages);
  }

  getSlidesLength(event): void {
    this.builder.slidesLength = event;
    if (event > 0) {
      this.builder.secondFormGroup.patchValue({images: 'test'});
    } else {
      this.builder.secondFormGroup.reset();
    }
  }

  setInfo(): void {
    const info = JSON.parse(localStorage.getItem('info'));
    const obj = this.builder.firstFormGroup.value;
    if (info && info.coverImage) {
      obj.coverImage = info.coverImage;
    }
    localStorage.setItem('info', JSON.stringify(obj));
  }

  openMap(): void{
    this.dialog.open(MapDialogComponent, {
      panelClass: 'app-full-bleed-dialog',
      width: '500px',
      data: {
        latlong: this.builder.latlong
      }
    });
  }

  addCoverImage(files: File[]): void {
    history.pushState(null, null, location.href);
    if (files.length === 0) {
      return;
    }
    // this.utility.setLoader(true);
    const reader = new FileReader();
    reader.readAsDataURL(files[0]); // read file as data url
    reader.onload = (event: any) => { // called once readAsDataURL is completed
      this.imageCompress.compressFile(event.target.result, -2, 40, 40).then(
        result => {
          try {
            const dialogRef = this.dialog.open(EditImageDialogComponent, {
              panelClass: this.utility.dark ? 'dark' : 'edit-image-dialog',
              maxWidth: '500px',
              maxHeight: '90vh',
              data: {
                imageFile: result,
                selectedItem: this.utility.coverFilter
              },
              disableClose: false
            });
            dialogRef.afterClosed().subscribe(res => {
              if (res && res.data) {
                // this.utility.coverImageUrl = res.data;
                if (!this.utility.coverImageUrl) {
                  this.utility.coverFilter = res.type;
                  const page = makePage({}, false);
                  page.type = PageTypeEnum_Image;
                  // page.dataUrl = res.data;
                  this.builder.ip.frontCoverPages = [];
                  const ippage = makeIpPage({}, false);
                  ippage.setPage(page);
                  this.builder.ip.frontCoverPages.push(ippage);
                  console.log(this.builder.ip);
                }
                // const str = localStorage.getItem('info');
                // if (str) {
                //   const info = JSON.parse(str);
                //   info.coverImage = res.data;
                //   localStorage.setItem('info', JSON.stringify(info));
                // } else {
                //   localStorage.setItem('info', JSON.stringify({ coverImage: res.data }));
                // }
                const blob = this.b64toBlob(res.data);
                blob.name = `${new Date().getTime()}`;
                console.log(blob);
                this.s3Upload(blob);
              }
            });
          } catch (err) {
            // this.utility.setLoader(false);
            // this.utility.logMessage('ERR:' + err);
            throw err;
          }
        }
      );
    };
  }

  updateCoverImage(): void {
    const dialogRef = this.dialog.open(EditImageDialogComponent, {
      panelClass: this.utility.dark ? 'dark' : 'edit-image-dialog',
      maxWidth: '500px',
      maxHeight: '90vh',
      data: {
        imageFile: this.utility.coverImageUrl,
        selectedItem: this.utility.coverFilter
      },
      disableClose: false
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res && res.data) {
        // this.utility.coverImageUrl = res.data;
        this.utility.coverFilter = res.type;
        // this.ip.frontCoverPages[0].page.dataUrl = res.data;
        // const str = localStorage.getItem('info');
        // if (str) {
        //   const info = JSON.parse(str);
        //   info.coverImage = res.data;
        //   localStorage.setItem('info', JSON.stringify(info));
        // } else {
        //   localStorage.setItem('info', JSON.stringify({ coverImage: res.data }));
        // }
        const blob = this.b64toBlob(res.data);
        blob.name = `${new Date().getTime()}`;
        console.log(blob);
        this.s3Upload(blob);
      }
    });
  }

  b64toBlob(dataURI: string): any {
    const byteString = atob(dataURI.split(',')[1]);
    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);

    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ab], { type: 'png' });
  }

  s3Upload(files: any): void {
    const fileType = files.type.split('/').pop();
    const id = Math.random().toString().substring(2);
    console.log(fileType);
    this.utility.setLoader(true);
    this.builder.isS3Loader = true;
    let uploadID;
    const BucketName = environment.BucketName;
    const s3Key = `${id}.${fileType}`;
    const params = {
      Bucket: BucketName,
      Key: s3Key,
    };

    const chunkSize = 5242880;
    const chunkArray = [];
    const uploadedChunks = [];
    let i = 0;
    for (let offset = 0; offset < files.size; offset += chunkSize) {
      const chunk = files.slice(offset, offset + chunkSize);
      chunk.name = `${new Date().getTime()}`;
      chunkArray.push({ blob: chunk, partNumber: i + 1 });
      console.log(chunkArray);
      i++;
    }
    const s = this;
    if (this.builder.isS3Loader){
      this.builder.s3.createMultipartUpload(params, (err, data) => {
        if (err){
          this.builder.isS3Loader = false;
          this.utility.setLoader(false);
        } else {
          console.log(s);
          console.log(data);
          uploadID = data.UploadId;
          this.builder.abortParams = {
            Bucket: environment.BucketName,
            Key: s3Key,
            UploadId: data.UploadId
          };
          let part;
          for (let j = 0; j < chunkArray.length + 1; j++) {
            if (j < chunkArray.length) {
              part = s.uploadPart(s, chunkArray, uploadedChunks, j, s3Key, uploadID);
            }
          }
          console.log(part);
        }
      });
    }
  }

  uploadPart(s: any, chunkArray: Array<any>, uploadedChunks: Array<any>, j: number, s3Key: string, uploadID: string): void{
    try {
      this.builder.s3.uploadPart(
        {
          Body: chunkArray[j].blob,
          Bucket: environment.BucketName,
          Key: s3Key,
          PartNumber: chunkArray[j].partNumber,
          UploadId: uploadID,
          ContentLength: chunkArray[j].size,
        }, (err, data) => {
          if (err){
            console.log(err);
          } else {
            console.log(data, j);
            uploadedChunks.push({
              ETag: JSON.parse(data.ETag),
              PartNumber: chunkArray[j].partNumber,
            });
            console.log(uploadedChunks);
            let sortedPartArray;
            let result;
            if (uploadedChunks.length == chunkArray.length) {
              sortedPartArray = {
                Parts: uploadedChunks.sort((a, b) => {
                  return a.PartNumber - b.PartNumber;
                }),
              };
              if (this.builder.isS3Loader){
                result = s.MultiPart(s, sortedPartArray, uploadID, s3Key);
              }
            }
          }
        }
      );
    } catch (err) {
      console.log(err);
    }
  }
  MultiPart(s: any, sortedPartArray: object, uploadID: string, s3Key: string): void {
    console.log(s);
    try {
      this.builder.s3.completeMultipartUpload(
        {
          Bucket: environment.BucketName,
          Key: s3Key,
          MultipartUpload: sortedPartArray,
          UploadId: uploadID,
        }, (err, response) => {
          s.utility.setLoader(false);
          this.builder.isS3Loader = false;
          s.toast.success('Successfully Uploaded');
          s.utility.coverImageUrl = response.Location;
          s.builder.ip.frontCoverPages[0].page.dataUrl = response.Location;
          localStorage.setItem('info', JSON.stringify({ coverImage: response.Location }));
          console.log('Final Data', response);
          console.log('Final Err', err);
        }
      );
    } catch (err) {
      s.utility.setLoader(false);
      s.toast.error('Upload Failed');
      console.log(err);
    }
  }

  async createIpApi(): Promise<any> {
    const createRsp = await this.ipPreviewService.createIp(this.utility.createdIP);
    if (clientResponseSuccess(createRsp)) {
      console.log('createdIp', createRsp);
    }
    else if (clientResponseHasErrors(createRsp)) {
      console.log('createdIpError', createRsp);
    }
  }

  showPreview(): void {
    console.log(this.builder.slidesLength);
    const FinalIpPages = [];
    for (let b = 0; b < this.builder.ipPages.length; b++) {
      if (this.builder.ipPages[b].page.dataUrl != '') {
        FinalIpPages.push(this.builder.ipPages[b]);
      }
    }
    this.builder.ipPages = FinalIpPages;
    console.log(FinalIpPages);
    this.builder.ip.name = this.builder.firstFormGroup.value.title;
    this.builder.ip.mainPages = this.builder.ipPages;
    // TODO: Rushit, if imgs can be null, then this.urls.length will cause a problem
    if (this.builder.slidesLength && this.utility.coverImageUrl) {
      this.utility.createdIP = this.builder.ip;
      console.log(this.utility.createdIP);
      // this.createIpApi();
      this.utility.setUrl('preview');
      this.router.navigate(['/preview']);
    } else {
      if (!this.builder.slidesLength) {
        this.translateSevice.get('previewImages.addImagesOrVideo').subscribe(res => {
          this.toast.info(res, '', {
            timeOut: 3000
          });
        });
      } else if (!this.utility.coverImageUrl) {
        this.translateSevice.get('previewImages.setCoverPhoto').subscribe(res => {
          this.toast.info(res, '', {
            timeOut: 3000
          });
        });
      }
    }
  }
}

