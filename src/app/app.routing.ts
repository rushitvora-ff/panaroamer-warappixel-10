import { Routes } from '@angular/router';
import { FullComponent } from './layouts/full/full.component';
import { AppBlankComponent } from './layouts/blank/blank.component';
import { HomeComponent } from './home/home.component';
import { PaymentsComponent } from './components/payments/payments.component';
import { PaymentSuccessComponent } from './components/payments/payment-success/payment-success.component';
import { PlayComponent } from './components/play/play.component';
import { ChatComponent } from './components/chat/chat.component';
import { GroupsComponent } from './components/groups/groups.component';
import { ContactsComponent } from './components/contacts/contacts.component';
import { IpScheduleCalendarComponent } from './components/ip-schedule-calendar/ip-schedule-calendar.component';
import { PaymentFailureComponent } from './components/payments/payment-failure/payment-failure.component';
import { PreInteractionsComponent } from './components/pre-interactions/pre-interactions.component';
import { SurveyComponent } from './components/survey/survey.component';

export const AppRoutes: Routes = [
    {
        path: '',
        component: AppBlankComponent,
        children: [
          {
              path: '',
              redirectTo: '/home',
              pathMatch: 'full'
          },
          {
              path: 'home',
              component: HomeComponent
          },
          {
                path: 'authentication',
                loadChildren:
                    () => import('./authentication/authentication.module').then(m => m.AuthenticationModule)
            }
        ]
    },
    {
        path: '',
        component: FullComponent,
        children: [
            {
                path: '',
                redirectTo: '/builder',
                pathMatch: 'full'
            },
            {
                path: 'material',
                loadChildren: () => import('./material-component/material.module').then(m => m.MaterialComponentsModule)
            },
            {
                path: 'builder',
                loadChildren: () => import('./builder/builder.module').then(m => m.BuilderModule)
            },
            {
                path: 'gallery',
                loadChildren: () => import('./components/gallery/gallery.module').then(m => m.GalleryModule)
            },
            {
                path: 'map',
                loadChildren: () => import('./components/map/map.module').then(m => m.MapModule)
            },
            {
                path: 'qr-scanner',
                loadChildren: () => import('./components/qr-scanner/qr-scanner.module').then(m => m.QrScannerModule)
            },
            {
                path: 'interactions',
                component: PreInteractionsComponent
            },
            {
                path: 'survey',
                component: SurveyComponent
            },
            {
                path: 'play',
                component: PlayComponent
            },
            {
                path: 'preview',
                component: PlayComponent
            },
            {
                path: 'payments',
                component: PaymentsComponent
            },
            {
              path: 'payments/success',
              component: PaymentSuccessComponent
            },
            {
                path: 'payments/failure',
                component: PaymentFailureComponent
            },
            {
              path: 'chat',
              component: ChatComponent
            },
            {
              path: 'contacts',
              component: ContactsComponent
            },
            {
              path: 'groups',
              component: GroupsComponent
            },
            {
              path: 'ip-schedule',
              component: IpScheduleCalendarComponent
            }
        ]
    }
];
