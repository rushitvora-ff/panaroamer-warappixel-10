import { FormGroup } from '@angular/forms';
import { Ip } from '@oneb/ip-api-clients-models';
import { IpPage, Page } from '@oneb/ip-api-clients-models';

export interface BuilderInterface {
  openPanel?: boolean;
  openCoverpagePanel?: boolean;
  openPreviewPanel?: boolean;
  firstFormGroup?: FormGroup;
  secondFormGroup?: FormGroup;
  slidesLength?: number;
  ip?: Ip;
  abortParams?: any;
  isS3Loader?: boolean;
  s3?: any;
  ipPages?: IpPage[];
  categories?: Array<string>;
  latlong?: string;
}

export interface PreviewSlideInterface {
  surveyItems?: {
        id: number,
        name: string,
        description: string,
        isActive: boolean,
      } [];
  s3?: any;
  slides?: Array<any>;
  selectSoundsFrom?: Array<any>;
  selectItemsFrom?: Array<any>;
  completeIndex?: number;
  id?: string;
  options?: any;
  showVideoRec?: boolean;
  isRecording?: boolean;
  startRecording?: boolean;
  vidPlayer?: any;
  isIos?: boolean;
  finalData?: Array<object>;
  localData?: string;
  isOpenMenu?: boolean;
  front?: boolean;
  temporarySlide?: any[];
  isS3Loader?: boolean;
  isRecorder?: boolean;
  introJS?: any;
  abortParams?: any;
  ipPages?: IpPage[];
  webcamImage?: string;
}
