export interface SidebarInterface {
  mobileQuery?: MediaQueryList;
  status?: boolean;
  itemSelect?: number[];
  parentIndex?: number;
  childIndex?: number;
}

export interface HeaderInterface {
  notifications?: {
    round: string,
    icon: string,
    title: string,
    subject: string,
    time: string
  } [];
  mymessages?: {
    useravatar: string,
    status: string,
    from: string,
    subject: string,
    time: string
  } [];
  isLogin?: boolean;
}

export interface FullInterface {
  mobileQuery?: MediaQueryList;
  url?: string;
  sidebarOpened?: boolean;
  status?: boolean;
  isVisible?: boolean;
  isLoad?: boolean;
  selectedLanguage?: any;
  languageArr?: Array<any>;
  artifacts?: any;
  displayPanel?: boolean;
}
