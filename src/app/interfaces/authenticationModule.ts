import { FormGroup } from '@angular/forms';

export interface LoginInterface {
    form?: FormGroup;
    hide?: boolean;
    loader?: boolean;
    isDestroy?: boolean;
}

export interface RegisterInterface {
    form?: FormGroup;
    hide?: boolean;
    hideConfirm?: boolean;
    confirmSignUp?: boolean;
    isSubmit?: boolean;
    confirmCode?: any;
}
