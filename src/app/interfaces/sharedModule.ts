import {WebcamImage} from 'ngx-webcam';
import {Subject} from 'rxjs';
import { ImageTransform } from 'ngx-image-cropper';
import { FormControl } from '@angular/forms';
import { Interaction } from '@oneb/interactions-api-clients-models';
import { Activation } from '@oneb/interactions-api-clients-models';
import { Effect } from '@oneb/interactions-api-clients-models';
import { Observable } from 'rxjs';
import { CalendarView } from 'angular-calendar';

export interface AssetsInterface {
    items?: {
        id: number,
        name: string,
        description: string,
        isActive: boolean,
        url: string,
        link: string
    }[];
    selected?: any;
    activeId?: any;
    activeTab?: number;
    tabs?: string[];
    openPanel?: boolean;
    activeIndex?: number;
}

export interface CaptureImageInterface {
    webcam?: boolean;
    webcamImage?: WebcamImage;
    trigger?: Subject<void>;
}

export interface EditImageInterface {
    imageBase64?: any;
    croppedImage?: any;
    canvasRotation?: any;
    rotation?: any;
    imageUrl?: any;
    scale?: number;
    showCropper?: boolean;
    containWithinAspectRatio?: boolean;
    transform?: ImageTransform;
    filter?: string;
    filterOptions?: Array<any>;
}

export interface InteractionInterface {
    selectedItem?: any;
    selectedAction?: string;
    gestArr?: any[];
    selectMode?: string;
    interaction?: Interaction;
    effect?: Effect;
    coordinates?: any;
    step?: number;
    emoji?: any;
    gesture?: any;
    buyActionText?: any;
    visitActionText?: any;
    activation?: Activation;
    mediaType?: string;
    mediaTime?: number;
    videoUpload?: any;
    userAction?: FormControl;
    userSelectedActions?: string[];
    phoneNumber?: number;
    smsNumber?: number;
    Text?: string;
    fileUrl?: string;
    gesFilterType?: string;
    showRecorder?: boolean;
    startGesRecord?: boolean;
    actionView?: boolean;
    selectedGesture?: any;
    isRecording?: boolean;
    showVideoRec?: boolean;
    isIos?: boolean;
    front?: boolean;
    startRecording?: boolean;
    vidPlayer?: any;
    abortParams?: any;
    isRecorder?: boolean;
    options?: any;
    isS3Loader?: boolean;
    player?: any;
    displayedColumns?: string[];
    survey?: any;
    userActions?:
      {
          label: string,
          value: string
      }[];
    surveys?:
      {
        id: number,
        name: string,
        description: string,
        isActive: boolean,
        url: string,
        link: string
    }[];
    s3?: any;
}

export interface ShareDialogInterface {
    optionsMsg?: string[];
    filteredOptions?: Observable<string[]>;
    myControl?: FormControl;
}

export interface PreviewScheduleInterface {
    view?: CalendarView;
    viewDate?: Date;
    events?: [];
}

export interface MapInterface {
  markers?: any[];
  latlong?: object;
  newLat?: number;
  newLong?: number;
  polyLines?: any;
  map?: any;
  polyLinesGroup?: Array<any>;
  long?: number;
  drawPath?: boolean;
  lat?: number;
  options?: any;
  center?: any;
}

export interface IpPreviewInterface {
  artifactsArr?: Array<any>;
  content?: any;
  timeout?: any;
  isOpen?: boolean;
  progressTimeOut?: any;
  navTimeout?: any;
  audioTimeout?: any;
  reveal?: any;
  isLoadScript?: boolean;
  selectedGesture?: any;
  idx?: number;
  tapregion?: any;
  longTapregion?: any;
  panRegion?: any;
  gestureTimeout?: any;
  regionArr?: any;
  mode?: string;
  selectedIndex?: number;
}

