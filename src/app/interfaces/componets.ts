import { FormGroup } from '@angular/forms';
import { CalendarView } from 'angular-calendar';
import { OwlOptions } from 'ngx-owl-carousel-o';

export interface ChatInterface {
    messages?: [];
    ipUrl?: any;
    labels?: any;
    senderProfile?: {
      name: string,
      avatar: string,
    };
}

export interface IpScheduleInterface {
    view?: CalendarView;
    viewDate?: Date;
    events?: [];
}

export interface ContactsInterface {
  forPersonProfile?: {
    contactMechanism: Array<any>,
    address: Array<any>,
    contactMechanismsIds: Array<any>,
    addressesIds: Array<any>
  };
  contacts?: any;
  labels?: any;
  isLoading?: boolean;
  isLoggedIn?: string;
}

export interface GalleryInterface {
  searchStr?: any;
  location?: any;
  trendingArr?: Array<any>;
  starredArr?: Array<any>;
  yoursArr?: Array<any>;
  sharedArr?: Array<any>;
  filTrendingArr?: Array<any>;
  filStarredArr?: Array<any>;
  filYoursArr?: Array<any>;
  filSharedArr?: Array<any>;
  isLoggedIn?: string;
  activeTab?: number;
  type?: string;
}

export interface MapComponentInterface {
  landscape?: boolean;
  options?: any;
  scrWidth?: number;
  starred?: Array<any>;
  optionsMsg?: string[];
  isParams?: boolean;
  isFullscreen?: boolean;
  center?: any;
  data?: Array<any>;
  selectedItem?: any;
  currentData?: any;
  optionsSpec?: any;
  categories?: string[];
  markers?: any[];
  map?: any;
  timeoutVal?: any;
  timeoutMapVal?: any;
  latlng?: any;
  myForm?: FormGroup;
  selectPath?: boolean;
  showEachPath?: boolean;
  polyLines?: any;
  polyLinesGroup?: Array<any>;
  isLoggedIn?: string;
}

export interface PlayInterface {
  selectedItem?: any;
  lng?: number;
  lat?: number;
  display?: boolean;
  priceList?: Array<any>;
  productList?: Array<any>;
  starred?: Array<any>;
  title?: string;
  showPreview?: boolean;
  landscape?: boolean;
  content?: any;
  baseConversionCount?: number;
  blobArr?: Array<any>;
  latlong?: any;
  myform?: any;
  object?: any;
  value?: string;
  btnText?: boolean;
  previewButton?: boolean;
  disable?: boolean;
}

export interface PreInteractionInterface {
  tabs?: string[];
}

export interface InfiniteGrid {
  throttle?: number;
  scrollDistance?: number;
  scrollUpDistance?: number;
  customOptions?: OwlOptions;
}