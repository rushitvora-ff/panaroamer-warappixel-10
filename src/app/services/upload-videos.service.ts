import { Injectable } from '@angular/core';
import * as AWS from 'aws-sdk';

@Injectable({
  providedIn: 'root'
})
export class UploadVideosService {

  s3Key = `pwa-images-${new Date().toISOString()}.webm`;
  params = {
    Bucket: 'pwa-images',
    Key: this.s3Key,
  };

   s3 = new AWS.S3({
    accessKeyId: 'AKIA6EPPMC4DTY7YXIWU',
    secretAccessKey: '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',
    region: 'us-east-1',
  });

  constructor() {

   }

   createMultiPart() : any {
     return this.s3.createMultipartUpload(this.params);
   }

   uploadParts(body,partNumber,uploadId,contentLength) : any {
    return this.s3.uploadPart(
      {
        Body: body,
        Bucket: 'pwa-images',
        Key: this.s3Key,
        PartNumber: partNumber,
        UploadId: uploadId,
        ContentLength: contentLength,
      }
    );
   }

   completeMultipart(multipart , uploadId): any {
     return this.s3.completeMultipartUpload({
      Bucket: 'pwa-images',
      Key: this.s3Key,
      MultipartUpload: multipart,
      UploadId: uploadId,
     });
   }
}
