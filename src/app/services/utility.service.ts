import { Injectable } from '@angular/core';
import * as awsamplify from '../../aws-exports';
import { AuthenticationConnector } from '@oneb/authentication-connector-api';
import { AuthenticationAwsCognitoUserPoolConnector } from '@oneb/authentication-aws-cognito-user-pool-connector';
import { GlobalAuthenticationContext } from '@oneb/authentication-connector-api';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { tileLayer, marker, icon } from 'leaflet';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Ip } from '@oneb/ip-api-clients-models';

interface Coordinates {
  latitude: number;
  longitude: number;
}

export declare type Permission = 'denied' | 'granted' | 'default';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {
  private authenticationConnector: AuthenticationConnector;
  coverFilter = '';
  isExpanded = false;
  shouldShow = false;
  cancelUpload;
  polyLines = [];
  pathMarkers = [];
  createdIP: Ip;
  isAbort;
  interactionsArray = [
    {
      name: 'Dubai',
      author: 'Aaren Johnson',
      streetAddress: '1163 Centre City Parkway, Escondido, CA 92026',
      url: 'https://ippub.panaroamer.com/dubai/index.html',
      category: 'Infrastructure',
      rating: 2,
      views: 2,
      shared: 3,
      location: {
        lat: 33.1361053,
        lng: -117.096036
      },
      published: '10.01.2019, 10:21 AM',
      expiration: '15.02.2020 10:21 AM'
    },
    {
      name: 'New York',
      author: 'Sam John',
      streetAddress: '302 W El Norte Pkwy, Escondido, CA 92026',
      url: 'https://ippub.panaroamer.com/new-york/index.html',
      category: 'Infrastructure',
      rating: 3,
      views: 32,
      shared: 12,
      price: 50,
      location: {
        lat: 33.1404684,
        lng: -117.0959901,
      },
      published: '10.10.2019, 10:21 AM',
      expiration: '10.10.2020 10:21 AM'
    },
    {
      name: 'Singapore',
      author: 'David Arnold',
      streetAddress: '121 W Lincoln Ave, Escondido, CA 92026',
      url: 'https://ippub.panaroamer.com/singapore/index.html',
      category: 'Infrastructure',
      rating: 4,
      views: 32,
      shared: 12,
      location: {
        lat: 33.1333261,
        lng: -117.08921,
      },
      published: '23.12.2019, 10:21 AM',
      expiration: '12.04.2020 10:21 AM'
    },
    {
      name: 'Melbourne',
      author: 'Michal Smith',
      streetAddress: '725 Center Dr, San Marcos, CA 92069',
      url: 'https://ippub.panaroamer.com/melbourne/index.html',
      category: 'Infrastructure',
      rating: 5,
      views: 12,
      shared: 4,
      location: {
        lat: 33.1358356,
        lng: -117.1275982,
      },
      published: '22.06.2019, 05:21 AM',
      expiration: '12.07.2020 10:09 PM'
    },
    {
      name: 'Las Vegas',
      author: 'Michal Clerk',
      streetAddress: '163 Technology DrIrvine, CA 92618',
      url: 'https://ippub.panaroamer.com/las-vegas/index.html',
      category: 'Infrastructure',
      rating: 1,
      views: 33,
      shared: 23,
      location: {
        lat: 33.6630765,
        lng: -117.7498852,
      },
      published: '12.12.2019, 04:21 AM',
      expiration: '04.10.2020 10:35 AM'
    },
    {
      name: 'Senic-beauty',
      author: 'Jhon Doe',
      streetAddress: '732 Center Dr, San Marcos, CA 92069',
      url: 'https://ippub.panaroamer.com/Senic-beauty/index.html',
      category: 'Nature',
      rating: 2,
      views: 45,
      shared: 2,
      location: {
        lat: 33.13578,
        lng: -117.1255441,
      },
      published: '23.10.2019, 10:21 AM',
      expiration: '02.10.2020 10:09 PM'
    },
    {
      name: 'River',
      author: 'Alex Stanley',
      streetAddress: '18000 Spectrum, Irvine, CA 92618',
      url: 'https://ippub.panaroamer.com/river/index.html',
      category: 'Nature',
      rating: 3,
      views: 44,
      shared: 21,
      location: {
        lat: 33.6537044,
        lng: -117.757363,
      },
      published: '10.10.2019, 10:21 AM',
      expiration: '10.10.2020 10:21 AM'
    },
    {
      name: 'Ocean',
      author: 'David Simon',
      streetAddress: '900 Spectrum Center Dr, Irvine, CA 92618',
      url: 'https://ippub.panaroamer.com/ocean/index.html',
      category: 'Nature',
      rating: 4,
      views: 54,
      shared: 23,
      location: {
        lat: 33.6490039,
        lng: -117.7460399,
      },
      published: '10.10.2019, 10:21 AM',
      expiration: '10.10.2020 10:21 AM'
    },
    {
      name: 'Animals',
      author: 'Harry Leonard',
      streetAddress: '18051 Von Karman Ave, Irvine, CA 92612',
      url: 'https://ippub.panaroamer.com/animals/index.html',
      category: 'Nature',
      rating: 5,
      views: 14,
      shared: 17,
      location: {
        lat: 33.6799624,
        lng: -117.8549068,
      },
      published: '10.10.2019, 10:21 AM',
      expiration: '10.10.2020 10:21 AM'
    },
    {
      name: 'Birds',
      author: 'Alex Robert',
      streetAddress: '175 Technology DrIrvine, CA 92618',
      url: 'https://ippub.panaroamer.com/birds/index.html',
      category: 'Nature',
      rating: 1,
      views: 12,
      shared: 5,
      location: {
        lat: 33.6646932,
        lng: -117.7498852,
      },
      published: '10.10.2019, 10:21 AM',
      expiration: '10.10.2020 10:21 AM'
    },
    {
      name: 'Happy Faces',
      author: 'Dennis Hilson',
      streetAddress: '2272 Michelson Dr, Irvine, CA 92612',
      url: 'https://ippub.panaroamer.com/happy-faces/index.html',
      category: 'Nature',
      rating: 2,
      views: 30,
      shared: 21,
      location: {
        lat: 33.676295,
        lng: -117.8568336,
      },
      published: '10.10.2019, 10:21 AM',
      expiration: '10.10.2020 10:21 AM'
    },
    {
      name: 'Liqour',
      author: 'Alvarez Woods',
      streetAddress: '18400 Von Karman Ave, Irvine, CA 92612',
      url: 'https://ippub.panaroamer.com/liqour/index.html',
      category: 'Store',
      rating: 3,
      views: 23,
      shared: 16,
      location: {
        lat: 33.6769747,
        lng: -117.8542624,
      },
      published: '10.10.2019, 10:21 AM',
      expiration: '10.10.2020 10:21 AM'
    },
    {
      name: 'Toy',
      author: 'Oconnor Fischer',
      streetAddress: '18500 Von Karman Ave, Irvine, CA 92612',
      url: 'https://ippub.panaroamer.com/toy/index.html',
      category: 'Store',
      rating: 4,
      views: 45,
      shared: 21,
      location: {
        lat: 33.6763347,
        lng: -117.8538961,
      },
      published: '10.10.2019, 10:21 AM',
      expiration: '10.10.2020 10:21 AM'
    },
    {
      name: 'Sports',
      author: 'Chen Lee',
      streetAddress: '59 Technology Dr W, Irvine, CA 92618',
      url: 'https://ippub.panaroamer.com/sports/index.html',
      category: 'Store',
      rating: 5,
      views: 32,
      shared: 24,
      location: {
        lat: 33.6582855,
        lng: -117.7436317,
      },
      published: '10.10.2019, 10:21 AM',
      expiration: '10.10.2020 10:21 AM'
    },
    {
      name: 'Mobile',
      author: 'Andrews Symonds',
      streetAddress: '123 Technology Dr, Irvine, CA 92618',
      url: 'https://ippub.panaroamer.com/mobile/index.html',
      category: 'Electronics',
      rating: 1,
      views: 43,
      shared: 14,
      location: {
        lat: 33.6625026,
        lng: -117.7469532,
      },
      published: '10.10.2019, 10:21 AM',
      expiration: '10.10.2020 10:21 AM'
    },
    {
      name: 'Camera',
      author: 'Jason Samthon',
      streetAddress: '115 Technology Dr, Irvine, CA 92618',
      url: 'https://ippub.panaroamer.com/camera/index.html',
      category: 'Electronics',
      rating: 2,
      views: 2,
      shared: 3,
      location: {
        lat: 33.6605057,
        lng: -117.7472276,
      },
      published: '10.10.2019, 10:21 AM',
      expiration: '10.10.2020 10:21 AM'
    },
    {
      name: 'Laptops',
      author: 'Steve Rogers',
      streetAddress: '71 Technology Dr, Irvine, CA 92618',
      url: 'https://ippub.panaroamer.com/laptops/index.html',
      category: 'Electronics',
      rating: 3,
      views: 2,
      shared: 3,
      location: {
        lat: 33.656893,
        lng: -117.7438084,
      },
      published: '10.10.2019, 10:21 AM',
      expiration: '10.10.2020 10:21 AM'
    },
    {
      name: 'Machines',
      author: 'Tony Stark',
      streetAddress: '196 Technology Dr K, Irvine, CA 92618',
      url: 'https://ippub.panaroamer.com/machines/index.html',
      category: 'Electronics',
      rating: 4,
      views: 2,
      shared: 3,
      location: {
        lat: 33.6663452,
        lng: -117.7494592,
      },
      published: '10.10.2019, 10:21 AM',
      expiration: '10.10.2020 10:21 AM'
    },
    {
      name: 'Indian',
      author: 'Headth Ledger',
      streetAddress: '125 Technology DrIrvine, CA 92618',
      url: 'https://ippub.panaroamer.com/indian/index.html',
      category: 'Food',
      rating: 5,
      views: 2,
      shared: 3,
      location: {
        lat: 33.6622584,
        lng: -117.7475254,
      },
      published: '10.10.2019, 10:21 AM',
      expiration: '10.10.2020 10:21 AM'
    },
    {
      name: 'Italian',
      author: 'Manning Fleming',
      streetAddress: '1800 Von Karman Ave, Irvine, CA 92614',
      url: 'https://ippub.panaroamer.com/italian/index.html',
      category: 'Food',
      rating: 1,
      views: 2,
      shared: 3,
      location: {
        lat: 33.6814276,
        lng: -117.8522928,
      },
      published: '10.10.2019, 10:21 AM',
      expiration: '10.10.2020 10:21 AM'
    },
    {
      name: 'Thai',
      author: 'Fitzgerald Wilkins',
      streetAddress: '40 Pacifica Irvine CA 92618',
      url: 'https://ippub.panaroamer.com/thai/index.html',
      category: 'Food',
      rating: 2,
      views: 2,
      shared: 3,
      location: {
        lat: 33.6506498,
        lng: -117.7539167,
      },
      published: '10.10.2019, 10:21 AM',
      expiration: '10.10.2020 10:21 AM'
    },
    {
      name: 'Mexican',
      author: 'Jason Statham',
      streetAddress: '153 Technology DrIrvine, CA 92618',
      url: 'https://ippub.panaroamer.com/mexican/index.html',
      category: 'Food',
      rating: 3,
      views: 2,
      shared: 3,
      location: {
        lat: 33.6630968,
        lng: -117.74861,
      },
      published: '10.10.2019, 10:21 AM',
      expiration: '10.10.2020 10:21 AM'
    },
    {
      name: 'Scenic',
      author: 'Fitzgerald Johnson',
      streetAddress: 'New Town, 128 00 Prague 2, Czechia',
      url: 'https://ippub.panaroamer.com/scenic/index.html',
      category: 'Nature',
      rating: 27,
      views: 19,
      shared: 3,
      location: {
        lat: 50.073658,
        lng: 14.418540,
      },
      published: '10.10.2019, 10:21 AM',
      expiration: '10.10.2020 10:21 AM'
    },
    {
      name: 'small-technology',
      author: 'James Mac',
      streetAddress: 'U Nemocnice 1563/3, 128 00 Nové Město, Czechia',
      url: 'https://ippub.panaroamer.com/small-technology/index.html',
      category: 'Electronics',
      rating: 4,
      views: 34,
      shared: 3,
      location: {
        lat: 50.0736614,
        lng: 14.4163513,
      },
      published: '10.10.2019, 01:28 AM',
      expiration: '10.10.2020 10:25 AM'
    },
    {
      name: 'Survey-XXX',
      author: 'Fitz Johnson',
      rating: 3,
      views: 12,
      shared: 3,
      streetAddress: '18000 Von Karman  Ave Irvine CA',
      url: 'https://ippub.panaroamer.com/survey-xxx/index.html',
      category: 'Survey',
      published: '10.10.2019, 10:21 AM',
      expiration: '10.10.2020 10:21 AM'
    },
    {
      name: 'Survey-YYY',
      author: 'Ronald Johnson',
      rating: 3,
      views: 32,
      shared: 32,
      streetAddress: '2272 Michelson Dr, Irvine, CA 92612',
      url: 'https://ippub.panaroamer.com/survey-yyy/index.html',
      category: 'Survey',
      published: '10.10.2019, 10:21 AM',
      expiration: '10.10.2020 10:21 AM'
    },
    {
      name: 'Survey-ZZZ',
      author: 'Jimmy Johnson',
      rating: 5,
      views: 23,
      shared: 12,
      streetAddress: '18000 Von Karman  Ave Irvine CA',
      url: 'https://ippub.panaroamer.com/survey-zzz/index.html',
      category: 'Survey',
      published: '10.10.2019, 10:21 AM',
      expiration: '10.10.2020 10:21 AM'
    }
  ];

  initialThreadsArr = [
    {
      id: Math.random().toString(36).substring(2),
      name: 'John Rosemary',
      profileUrl: 'assets/icons/Ellipse 9.png',
      timeStamp: '9:00PM',
      messages: [{ text: 'This is thread from her', emoji: null }, { text: 'Thread details', emoji: null }],
      type: 'receive',
      contactId: 1, files: [],
      sendingTime: null,
      video: [],
      presentation: [],
      audio: []
    },
    {
      id: Math.random().toString(36).substring(2),
      name: 'You',
      profileUrl: 'assets/icons/Ellipse 22.png',
      timeStamp: '9:02PM',
      messages: [{ text: 'This is thread from me', emoji: null }, { text: 'Thread details', emoji: null }],
      type: 'send',
      contactId: 3, files: [],
      sendingTime: null,
      presentation: [],
      video: [],
      audio: []

    }
  ];

  intialMessagesArr: any = [
    {
      id: Math.random().toString(36).substring(2),
      name: 'Alex',
      avatar: 'assets/images/users/1.jpg',
      lastMessageTime: '11:00 AM',
      messagesArr: [
        {
          id: Math.random().toString(36).substring(2),
          name: 'John Doe',
          profileUrl: 'assets/images/users/1.jpg',
          timeStamp: '9:00PM',
          messages: [
            { text: 'Hi', emoji: null, threadArr: [] },
            {
              text:
                `I wish I and her and more time together to spend she is gone now and
              I miss her all my life I wanted to be with her all my life.`,
              emoji: null, threadArr: []
            }
          ],
          type: 'receive',
          files: [],
          sendingTime: null,
          video: [],
          presentation: [],
          audio: []
        },
        {
          id: Math.random().toString(36).substring(2),
          name: 'You',
          profileUrl: 'https://panaroamer.com/assets/icons/Ellipse 22.png',
          timeStamp: '9:02PM',
          messages: [
            {
              text: 'Hope you are not at office?', emoji: null,
              threadArr: [
                {
                  id: Math.random().toString(36).substring(2),
                  name: 'John Doe',
                  profileUrl: 'https://panaroamer.com/assets/images/avatar1.png',
                  timeStamp: '9:00PM',
                  messages: [{ text: 'This is thread from her', emoji: null }, { text: 'Thread details', emoji: null }],
                  type: 'receive',
                  files: [],
                  sendingTime: null,
                  video: [],
                  presentation: [],
                  audio: []
                },
                {
                  id: Math.random().toString(36).substring(2),
                  name: 'You',
                  profileUrl: 'https://panaroamer.com/assets/icons/Ellipse 22.png',
                  timeStamp: '9:02PM',
                  messages: [{ text: 'This is thread from me', emoji: null }, { text: 'Thread details', emoji: null }],
                  type: 'send',
                  files: [],
                  sendingTime: null,
                  video: [],
                  presentation: [],
                  audio: []
                }
              ]
            }
          ],
          type: 'send',
          files: [],
          sendingTime: null,
          video: [],
          presentation: [],
          audio: []
        },
        {
          id: Math.random().toString(36).substring(2),
          name: 'John Doe',
          profileUrl: 'assets/images/users/1.jpg',
          timeStamp: '9:04PM',
          messages: [{ text: 'No I am not at office', emoji: null, threadArr: [] }],
          type: 'receive',
          files: [],
          sendingTime: null,
          video: [],
          presentation: [],
          audio: []
        },
        {
          id: Math.random().toString(36).substring(2),
          name: 'You',
          profileUrl: 'https://panaroamer.com/assets/icons/Ellipse 22.png',
          timeStamp: '9:06PM',
          messages: [{ text: 'No I am not at office', emoji: null, threadArr: [] }],
          type: 'send',
          files: [],
          sendingTime: null,
          video: [],
          presentation: [],
          audio: []
        }
      ]
    },
    {
      id: Math.random().toString(36).substring(2),
      name: 'John Doe',
      avatar: 'https://panaroamer.com/assets/images/avatar1.png',
      lastMessageTime: '11:00 AM',
      messagesArr: [
        {
          id: Math.random().toString(36).substring(2),
          name: 'John Doe',
          profileUrl: 'https://panaroamer.com/assets/images/avatar1.png',
          timeStamp: '9:00PM',
          messages: [
            { text: 'Hi', emoji: null, threadArr: [] },
            {
              text:
                `I wish I and her and more time together to spend she is gone now and
              I miss her all my life I wanted to be with her all my life.`,
              emoji: null, threadArr: []
            }
          ],
          type: 'receive',
          files: [],
          sendingTime: null,
          video: [],
          presentation: [],
          audio: []
        }]
      }
  ];

  contacts = [
    {
      id: '6mkxugw7szw',
      name: 'John Rosemary',
      address: '18000 Von Karman  Ave Irvine CA',
      notes: 'Notes are here',
      avatar: 'https://panaroamer.com/assets/icons/Ellipse 9.png',
      phoneNumbers: [
        { type: 'home', number: +2134780945 }
      ],
      emails: [
        { type: 'home', email: 'john@example.com' }
      ],
      websites: [
        { type: 'home', site: 'example.com' }
      ],
      instagram: '',
      twitter: '',
      linkedin: '',
      snapchat: '',
      tinder: '',
      panaroamer: '',
      following: 1,
      group: '123abc'
    },
    {
      id: 'ffyoujtc0rf',
      name: 'Nikkie Davis',
      address: '157 Technology Dr W, Irvine, CA 92618',
      notes: 'Notes are here',
      avatar: 'https://panaroamer.com/assets/icons/Ellipse 23.png',
      phoneNumbers: [
        { type: 'home', number: +2134780945 }
      ],
      emails: [
        { type: 'home', email: 'john@example.com' }
      ],
      websites: [
        { type: 'home', site: 'example.com' }
      ],
      instagram: '',
      twitter: '',
      linkedin: '',
      snapchat: '',
      tinder: '',
      panaroamer: '',
      following: 1,
      group: '123abc'
    },
    {
      id: 'n9uqmemd9wr',
      name: 'Phil Daley',
      address: '153 Technology DrIrvine, CA 92618',
      notes: 'Notes are here',
      avatar: 'https://panaroamer.com/assets/icons/Ellipse 22.png',
      phoneNumbers: [
        { type: 'home', number: +2134780945 }
      ],
      emails: [
        { type: 'home', email: 'john@example.com' }
      ],
      websites: [
        { type: 'home', site: 'example.com' }
      ],
      instagram: '',
      twitter: '',
      linkedin: '',
      snapchat: '',
      tinder: '',
      panaroamer: '',
      following: 1,
      group: '123abc'
    },
    {
      id: 'hzrplukl0q',
      name: 'Jim Majewski',
      address: '196 Technology Dr K, Irvine, CA 92618',
      notes: 'Notes are here',
      avatar: 'https://panaroamer.com/assets/images/avatar1.png',
      phoneNumbers: [
        { type: 'home', number: +2134780945 }
      ],
      emails: [
        { type: 'home', email: 'john@example.com' }
      ],
      websites: [
        { type: 'home', site: 'example.com' }
      ],
      instagram: '',
      twitter: '',
      linkedin: '',
      snapchat: '',
      tinder: '',
      following: 1,
      panaroamer: ''
    },
    {
      id: 'gs6d0rvqrhv',
      name: 'Lisa Black',
      address: '18023 Von Karman  Ave Irvine CA',
      notes: 'Notes are here',
      avatar: 'https://panaroamer.com/assets/images/avatar2.png',
      phoneNumbers: [
        { type: 'home', number: +2134780945 }
      ],
      emails: [
        { type: 'home', email: 'john@example.com' }
      ],
      websites: [
        { type: 'home', site: 'example.com' }
      ],
      instagram: '',
      twitter: '',
      linkedin: '',
      snapchat: '',
      tinder: '',
      panaroamer: '',
      following: 2,
      group: 'xyz123'
    },
    {
      id: 'l7ifrrhbyw',
      name: 'Inge Lowe',
      address: '725 Center Dr, San Marcos, CA 92069',
      notes: 'Notes are here',
      avatar: 'https://panaroamer.com/assets/images/avatar3.png',
      phoneNumbers: [
        { type: 'home', number: +2134780945 }
      ],
      emails: [
        { type: 'home', email: 'john@example.com' }
      ],
      websites: [
        { type: 'home', site: 'example.com' }
      ],
      instagram: '',
      twitter: '',
      linkedin: '',
      snapchat: '',
      tinder: '',
      panaroamer: '',
      following: 2,
      group: 'xyz123'
    },
    {
      id: 'zrt3nfomljk',
      name: 'Moira Flower',
      address: '71 Technology Dr, Irvine, CA 92618',
      notes: 'Notes are here',
      avatar: 'https://panaroamer.com/assets/images/avatar4.png',
      phoneNumbers: [
        { type: 'home', number: +2134780945 }
      ],
      emails: [
        { type: 'home', email: 'john@example.com' }
      ],
      websites: [
        { type: 'home', site: 'example.com' }
      ],
      instagram: '',
      twitter: '',
      linkedin: '',
      snapchat: '',
      tinder: '',
      following: 2,
      panaroamer: ''
    },
    {
      id: 'zqvbddwhxir',
      name: 'Lisa Daniels',
      address: '115 Technology Dr, Irvine, CA 92618',
      notes: 'Notes are here',
      avatar: 'https://panaroamer.com/assets/images/avatar5.png',
      phoneNumbers: [
        { type: 'home', number: +2134780945 }
      ],
      emails: [
        { type: 'home', email: 'john@example.com' }
      ],
      websites: [
        { type: 'home', site: 'example.com' }
      ],
      instagram: '',
      twitter: '',
      linkedin: '',
      snapchat: '',
      tinder: '',
      panaroamer: '',
      following: 2,
      group: 'xyz123'
    },
    {
      id: 'bspl2rxcvks',
      name: 'Vivian Thyme',
      address: '125 Technology DrIrvine, CA 92618',
      notes: 'Notes are here',
      avatar: 'https://panaroamer.com/assets/images/avatar6.png',
      phoneNumbers: [
        { type: 'home', number: +2134780945 }
      ],
      emails: [
        { type: 'home', email: 'john@example.com' }
      ],
      websites: [
        { type: 'home', site: 'example.com' }
      ],
      instagram: '',
      twitter: '',
      linkedin: '',
      snapchat: '',
      tinder: '',
      following: 3,
      panaroamer: ''
    },
    {
      id: 'xrnwfz9wylk',
      name: 'Hila Brown',
      address: '18500 Von Karman Ave, Irvine, CA 92612',
      notes: 'Notes are here',
      avatar: 'https://panaroamer.com/assets/images/avatar7.png',
      phoneNumbers: [
        { type: 'home', number: +2134780945 }
      ],
      emails: [
        { type: 'home', email: 'john@example.com' }
      ],
      websites: [
        { type: 'home', site: 'example.com' }
      ],
      instagram: '',
      twitter: '',
      linkedin: '',
      snapchat: '',
      tinder: '',
      following: 3,
      panaroamer: ''
    },
    {
      id: '3ix1v60xs2h',
      name: 'Aiden Plow',
      address: '18003 Von Karman  Ave Irvine CA',
      notes: 'Notes are here',
      avatar: 'https://panaroamer.com/assets/images/avatar8.png',
      phoneNumbers: [
        { type: 'home', number: +2134780945 }
      ],
      emails: [
        { type: 'home', email: 'john@example.com' }
      ],
      websites: [
        { type: 'home', site: 'example.com' }
      ],
      instagram: '',
      twitter: '',
      linkedin: '',
      snapchat: '',
      tinder: '',
      panaroamer: '',
      following: 3,
      group: 'xyz123'
    },
    {
      id: 'ajqd6xaa1fw',
      name: 'Frank Sting',
      address: '59 Technology Dr W, Irvine, CA 92618',
      notes: 'Notes are here',
      avatar: 'https://panaroamer.com/assets/images/avatar9.png',
      phoneNumbers: [
        { type: 'home', number: +2134780945 }
      ],
      emails: [
        { type: 'home', email: 'john@example.com' }
      ],
      websites: [
        { type: 'home', site: 'example.com' }
      ],
      instagram: '',
      twitter: '',
      linkedin: '',
      snapchat: '',
      tinder: '',
      following: 3,
      panaroamer: ''
    }
  ];

  groups = [
    {
      id: '123abc',
      name: 'Group 1',
      description: 'This is Group 1',
      contacts: ['6mkxugw7szw', 'ffyoujtc0rf', 'n9uqmemd9wr']
    },
    {
      id: 'xyz123',
      name: 'Group 2',
      description: 'This is Group 2',
      contacts: ['zqvbddwhxir', 'ajqd6xaa1fw', 'bspl2rxcvks', '3ix1v60xs2h']
    }
  ];

  public permission: Permission;
  public loader = new BehaviorSubject<boolean>(false);
  private showChatPanel = new BehaviorSubject<boolean>(false);
  private coordinates = new BehaviorSubject<Coordinates>({ latitude: null, longitude: null });
  private iFrameUrl = new BehaviorSubject<any>(null);
  private surveyUrl = new BehaviorSubject<any>(null);
  loader$ = this.loader.asObservable();
  conversationObj: any = {};
  conIframeUrl;
  changeRoute = true;
  selectedMapObj: any;
  isFullscreen = false;
  iframeUrlArr = [];
  content = [];
  landscape = false;
  preview = false;
  mostOld = 0;
  coverImageUrl = '';
  recurrenceData;
  dark = false;
  dir = 'ltr';
  gestArray = [];
  dirValue;
  green = false;
  blue = false;
  minisidebar = false;
  boxed = false;
  danger = false;
  saved = [];

  constructor(private router: Router, private http: HttpClient) {
    this.authenticationConnector = new AuthenticationAwsCognitoUserPoolConnector(
      awsamplify.awsmobile.aws_cognito_region,
      awsamplify.awsmobile.aws_user_pools_id,
      'cognito-idp.us-west-2.amazonaws.com/' + awsamplify.awsmobile.aws_user_pools_id,
      awsamplify.awsmobile.aws_user_pools_web_client_id,
      awsamplify.awsmobile.aws_cognito_identity_pool_id,
      'arn:aws:iam::081847204825:role/OnebApiIdentityPoolUnauth');
    this.permission = this.isSupported() ? 'default' : 'denied';
  }

  isSupported(): boolean {
    return 'Notification' in window;
  }

  logOut(): void {
    this.setLoader(true);
    this.getAuthenticator().signOut(GlobalAuthenticationContext.getSignedInUserDetails().username).then(() => {
      this.getAuthenticator().signInGuest().then(() => {
        this.setLoader(false);
        this.router.navigate(['/home']);
      });
    });
  }

  setLoader(value): void {
    this.loader.next(value);
  }

  getAuthenticator(): any {
    return this.authenticationConnector;
  }

  getLeafletMapOptions(): any {
    const optionsSpec = {
      layers: [
        {
          url: `${environment.leafletMapUrl}?key=${environment.leafletApiKey}`,
          attribution: 'Open Street Map',
        }],
      zoom: 15,
      center: [46.879966, -121.726909],
    };
    return JSON.parse(JSON.stringify(optionsSpec));
  }

  setLeafletMapOptions(maxZoom, centerPoint, url): any {
    const options = {
      layers: [
        tileLayer(url)],
      zoom: maxZoom,
      center: centerPoint,
    };
    return options;
  }

  setCurrentLocMarker(lat, long): any {
    const newMarker = marker(
      [lat, long],
      {
        icon: icon({
          iconUrl: '../../assets/images/self-map-marker.png',
          iconSize: [50, 50],
          iconAnchor: [12, 41],
          popupAnchor: [1, -34],
          shadowSize: [41, 41],
        }),
      },
    );
    return newMarker;
  }

  setLeafletMapMarker(lat, long, url?: string, id?: string): any {
    const newMarker = marker(
      [lat, long],
      {
        icon: icon({
          iconUrl: '../../assets/images/marker-icon-2x-red.png',
          shadowUrl: '../../assets/images/marker-shadow.png',
          iconSize: [25, 41],
          iconAnchor: [12, 41],
          popupAnchor: [1, -34],
          shadowSize: [41, 41],
        }),
      },
    );
    return newMarker;
  }

  setCoordinates(data): void {
    const obj = { latitude: data.latitude, longitude: data.longitude, type: data.type };
    this.coordinates.next(obj);
  }

  getCoordinates(): any {
    return this.coordinates.asObservable();
  }

  getProducts(): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', 'Bearer sk_test_IQW4G8IUtXeNKODLAuOMgpo200hpVnAfhb');
    return this.http.get<any>('https://api.stripe.com/v1/products', { headers });
  }

  getPrices(): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', 'Bearer sk_test_IQW4G8IUtXeNKODLAuOMgpo200hpVnAfhb');
    return this.http.get<any>('https://api.stripe.com/v1/prices', { headers });
  }

  getUrl(): Observable<any> {
    return this.iFrameUrl.asObservable();
  }

  setUrl(url): void {
    this.iFrameUrl.next(url);
  }

  getSurvey(): Observable<any> {
    return this.surveyUrl.asObservable();
  }

  setSurvey(survey): void {
    this.surveyUrl.next(survey);
  }

  getChatPanel(): Observable<any> {
    return this.showChatPanel.asObservable();
  }

  setChatPanel(val): void {
    this.showChatPanel.next(val);
  }

  getJSON(custName?: any): Observable<any> {
    return this.http.get<any>('/assets/config.json');
    return this.http.get<any>('https://panaroamer-theming-test.s3.us-east-2.amazonaws.com/' + custName + '/config.json');
  }

  generateIPDs(): void {

  }
}
