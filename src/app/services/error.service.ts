import { Injectable, ErrorHandler, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import * as Sentry from '@sentry/browser';
Sentry.init({
  dsn: 'https://7b34e9f653f849ae9531fa2c1aa41634@o497009.ingest.sentry.io/5572777',
});

@Injectable({
  providedIn: 'root'
})
export class SentryErrorHandler implements ErrorHandler {
  constructor(private injector: Injector) { }
  handleError(error: any) { 
    const router = this.injector.get(Router);
    const eventId = Sentry.captureException(error.originalError || error);
    if (Error instanceof HttpErrorResponse) {
    console.log(error.status);
    }
    else {
    console.error('an error occured here mate');
    // Sentry.showReportDialog({ eventId });
    }
    // router.navigate(['error']);
  }
}