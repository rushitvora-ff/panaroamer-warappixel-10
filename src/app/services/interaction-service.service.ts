import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { ClientLibAxios } from '@rainmaker2/rainmaker2-codegen-api-clients-axios';
import { InteractionsFinderClientDefault } from '@oneb/interactions-api-clients';
import { InteractionsManagerClientDefault } from '@oneb/interactions-api-clients';
// import { ContactsFinderClientLocal } from '../../api-local-poc/ContactsFinderClientLocal';
import { from, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class InteractionServiceService {

  clientLibAxios = new ClientLibAxios(environment.onebApi.interactions.service.serviceUrl);
  finder = new InteractionsFinderClientDefault(this.clientLibAxios);
  manager = new InteractionsManagerClientDefault(this.clientLibAxios);

  constructor() {
    this.finder.setTenancy({ appId: 'rushit', dataId: 'vora'});
    this.manager.setTenancy({ appId: 'rushit', dataId: 'vora'});
  }

  createInteractions(interactions): Promise<any> {
    const createReq = {
      interaction : interactions
      // interaction
    };
    return this.manager.createInteraction(createReq);
  }

  createEffects(effects): Promise<any> {
    const createReq = {
      effect : effects
      // interaction
    };
    return this.manager.createEffect(createReq);
  }

  updateInteraction(interactionId , interactions) : Promise<any> {
    const createReq = {
      filter: {
        interactionId: {eq : interactionId}
    },
    interaction: interactions
    }
    return this.manager.updateInteractionsByInteractionId(createReq);
  }

  getInteractions(interactionId) : Promise<any> {
    // let callToActionId = id;
    let findReq = { filter : { interactionId : interactionId }};
    return this.finder.findInteractionsByInteractionId(findReq);
  }

  deleteSingleInteraction(interactionId) : Promise<any> {
    const findReq = { filter: { effectsId: interactionId }};
    return this.manager.deleteInteractionsByEffectsId(findReq);
  }

  // updateContact(updatedContact): Promise<any> {
  //   const createReq = {
  //     filter: {
  //       contactId: { eq: updatedContact.contactId }
  //     },
  //     contact: updatedContact
  //   };
  //   return this.manager.updateContactsByContactId(createReq);
  // }

  // getContacts(): Promise<any> {
  //   const findReq = { filter: { }};
  //   return this.finder.findContacts(findReq);
  // }

  // getContactById(id): Observable<any> {
  //   const findReq = { filter: { contactId: id }};
  //   return from(this.finder.findOneContactByContactId(findReq));
  // }

  // getContactMechanism(id): Observable<any> {
  //   const findReq = { filter: { contactMechanismId: id }};
  //   return from(this.finder.findOneContactMechanismByContactMechanismId(findReq));
  // }

  // updateContactMechanism(data, id): Observable<any> {
  //   const findReq = { filter: { contactMechanismId: id }, contactMechanism: data};
  //   return from(this.manager.updateOneContactMechanismByContactMechanismId(findReq));
  // }

  // deleteContactMechanism(id): Observable<any> {
  //   const findReq = { filter: { contactMechanismId: id }};
  //   return from(this.manager.deleteContactMechanismsByContactMechanismId(findReq));
  // }

  // deleteContact(id): Promise<any> {
  //   const findReq = { filter: { contactId: {eq : id} }};
  //   return this.manager.deleteContactsByContactId(findReq);
  // }
}
