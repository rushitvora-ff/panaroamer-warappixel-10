import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { ClientLibAxios } from '@rainmaker2/rainmaker2-codegen-api-clients-axios';
import { IpsFinderClientDefault } from '@oneb/ip-api-clients';
import { IpsManagerClientDefault } from '@oneb/ip-api-clients';

@Injectable({
  providedIn: 'root'
})
export class IpPreviewService {

  clientLibAxios = new ClientLibAxios(environment.onebApi.ip.service.serviceUrl);
  finder = new IpsFinderClientDefault(this.clientLibAxios);
  manager = new IpsManagerClientDefault(this.clientLibAxios);

  constructor() {
    this.finder.setTenancy({ appId: 'rushit', dataId: 'vora'});
    this.manager.setTenancy({ appId: 'rushit', dataId: 'vora'});
  }

  createIp(ips): Promise<any> {
    const createReq = {
        ip : ips
    };
    return this.manager.createIp(createReq);
  }
}
