import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-calender-scheduling',
  templateUrl: './calender-scheduling.component.html',
  styleUrls: ['./calender-scheduling.component.scss'],
})
export class CalenderSchedulingComponent implements OnInit {
  title;

  constructor(
    public dialogRef: MatDialogRef<CalenderSchedulingComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {
    this.title = data.presentationTitle;
    dialogRef.disableClose = true;
  }

  ngOnInit(): void {
    console.log(this.title);
    if (isPlatformBrowser(this.platformId)) {
      history.pushState(
        {},
        document.getElementsByTagName('title')[0].innerHTML,
        window.location.href
      );
    }
  }

  closeModal(): void {
    this.dialogRef.close();
  }
}
