import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-ip-info-dialog',
  templateUrl: './ip-info-dialog.component.html',
  styleUrls: ['./ip-info-dialog.component.scss']
})
export class IpInfoDialogComponent implements OnInit {
  selectedItem;

  constructor(
    public dialogRef: MatDialogRef<IpInfoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.selectedItem = data.selectedItem;
    }

  ngOnInit(): void {
  }

  closeModal(): void {
    this.dialogRef.close();
  }

  convertDMS(lat, lng): string {
    const latitude = this.toDegreesMinutesAndSeconds(lat);
    const latitudeCardinal = lat >= 0 ? 'N' : 'S';

    const longitude = this.toDegreesMinutesAndSeconds(lng);
    const longitudeCardinal = lng >= 0 ? 'E' : 'W';

    return latitude + '"' + latitudeCardinal + ' ' + longitude + '"' + longitudeCardinal;
  }

  toDegreesMinutesAndSeconds(coordinate): string {
    const absolute = Math.abs(coordinate);
    const degrees = Math.floor(absolute);
    const minutesNotTruncated = (absolute - degrees) * 60;
    const minutes = Math.floor(minutesNotTruncated);
    const seconds = Math.floor((minutesNotTruncated - minutes) * 60);
    return degrees + '°' + minutes + `'` + seconds;
  }

}
