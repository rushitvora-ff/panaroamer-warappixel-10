import { Component, OnInit, Inject, Optional,PLATFORM_ID } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { isPlatformBrowser } from '@angular/common';
import { AssetsInterface } from '../../../interfaces/sharedModule';
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-assets-dialog',
  templateUrl: './assets-dialog.component.html',
  styleUrls: ['./assets-dialog.component.scss']
})
export class AssetsDialogComponent implements OnInit {
  assets: AssetsInterface = {
    items: [
      {
        id: 1, name: 'Survey-XXX', description: 'This is very good survey',
        isActive: false, url: 'survey-xyz', link: 'https://ippub.panaroamer.com/animals/index.html'
      },
      {
        id: 2, name: 'Survey-YYY', description: 'This is very good survey',
        isActive: false, url: 'survey-abc', link: 'https://ippub.panaroamer.com/birds/index.html'
      },
      {
        id: 3, name: 'Survey-ZZZ', description: 'This is very good survey',
        isActive: false, url: 'survey-def', link: 'https://ippub.panaroamer.com/camera/index.html'
      }
    ],
    selected: [],
    activeId: ''
  };

  constructor(
    private router: Router,
    public dialogRef: MatDialogRef<AssetsDialogComponent>,
    public utility: UtilityService,
    @Inject(PLATFORM_ID) private platformId: Object,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    if (this.data) {
      const prevIdx = localStorage.getItem('selectedIdx');
      const asstArr = localStorage.getItem('assetsArray');
      if (asstArr) {
        const arr = JSON.parse(asstArr);
        for (const item of arr) {
          if (item.selectedItemIdx === prevIdx) {
            this.assets.activeId = item.selectedRow;
          }
        }
      }
      for (const data of this.assets.items) {
        if (data.id == this.assets.activeId) {
          this.assets.selected = [data];
        }
      }
    }
    if (isPlatformBrowser(this.platformId)) {
      history.pushState(
        {},
        document.getElementsByTagName('title')[0].innerHTML,
        window.location.href
      );
    }
  }

  onSelect({ selected }): void {
    if (this.data) {
      this.selectedRow(selected[0]);
    } else {
      this.dialogRef.close(selected[0]); 
    }
  }

  selectedRow(row): void {
    const images = JSON.parse(localStorage.getItem('ipDataPre'));
    const prevIdx = localStorage.getItem('selectedIdx');
    const obj = {selectedItemIdx: prevIdx, selectedRow: row.id, url: row.link};
    let asstArr = localStorage.getItem('assetsArray');
    if (asstArr) {
      if (this.getIncludes(JSON.parse(asstArr), prevIdx)) {
        const arr = JSON.parse(asstArr);
        for (const item of arr) {
          if (item.selectedItemIdx === prevIdx) {
            item.selectedRow = row.id;
          }
        }
        asstArr = arr;
      } else {
        const arr = JSON.parse(asstArr);
        arr.push(obj);
        asstArr = arr;
      }
      localStorage.setItem('assetsArray', JSON.stringify(asstArr));
    } else {
      localStorage.setItem('assetsArray', JSON.stringify([obj]));
    }
    for (const image of images) {
      if (image.id === obj.selectedItemIdx) {
        const dummArr = row.link.split('/');
        dummArr[dummArr.length - 1] = 'presentations/media/BEFORE_ALL.json';
        image.surveyUrl = dummArr.join('/');
      }
    }
    localStorage.setItem('ipDataPre', JSON.stringify(images));
    this.dialogRef.close();
  }

  getIncludes(data, idx): boolean {
    for (const item of data) { 
      if (item.selectedItemIdx === idx) {
        return true;
      }
    }
  }

  redirectTo(row): void {
    localStorage.setItem('isSurvey', 'true');
    this.dialogRef.close();
    this.router.navigate(['/play'], { queryParams: { name: row.name, url: row.link } });
  }

}
