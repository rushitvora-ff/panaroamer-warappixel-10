import { Component, Inject, OnInit, PLATFORM_ID, NgZone } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as L from 'leaflet';
import { UtilityService } from 'src/app/services/utility.service';
import { latLng, Layer, marker, icon, Popup } from 'leaflet';
import { isPlatformBrowser } from '@angular/common';
import { MapInterface } from 'src/app/interfaces/sharedModule';

@Component({
  selector: 'app-map-dialog',
  templateUrl: './map-dialog.component.html',
  styleUrls: ['./map-dialog.component.scss']
})

export class MapDialogComponent implements OnInit {
  mapInterface: MapInterface = {
    markers: [],
    polyLinesGroup: [],
    drawPath: false
  };

  // markers: any[] = [];
  // latlong: object;
  // newLat;
  // newLong;
  // polyLines;
  // map;
  // polyLinesGroup = [];
  // long;
  // drawPath = false;
  // lat;
  // options;
  // center;

  constructor(public ngZone: NgZone, public utility: UtilityService, @Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<MapDialogComponent>, @Inject(PLATFORM_ID) private platformId: Object) {
    if (isPlatformBrowser(this.platformId)) {
      history.pushState(
        {},
        document.getElementsByTagName('title')[0].innerHTML,
        window.location.href
      );
    }
  }

  ngOnInit(): void {
    if(this.data.latlong){
    this.initMap();
    }
    else{
      this.getCurrentLocation();
    }
  }

  initMap(): void {
    this.mapInterface.latlong = JSON.parse(this.data.latlong);
    this.mapInterface.long = this.mapInterface.latlong['lng'];
    this.mapInterface.lat = this.mapInterface.latlong['lat'];
    const optionsSpec = this.utility.getLeafletMapOptions();
    this.mapInterface.center = latLng(this.mapInterface.lat, this.mapInterface.long);
    this.mapInterface.options = this.utility.setLeafletMapOptions(
      optionsSpec.zoom, this.mapInterface.center, optionsSpec.layers[0].url);

    const newMarker = marker(
      [this.mapInterface.lat, this.mapInterface.long],
      {
        icon: icon({
          iconUrl: '../../assets/images/marker-icon-2x-blue.png',
          shadowUrl: '../../assets/images/marker-shadow.png',
          iconSize: [25, 41],
          iconAnchor: [12, 41],
          popupAnchor: [1, -34],
          shadowSize: [41, 41]
        }),
      },
    );
    this.mapInterface.markers.push(newMarker);
  }

  getCurrentLocation(){
    navigator.geolocation.getCurrentPosition((position) => {
      this.mapInterface.center = latLng(position.coords.latitude, position.coords.longitude);
      console.log('Lat: ' + position.coords.latitude + ' Long: ' + position.coords.longitude);
      this.mapInterface.lat = position.coords.latitude;
      this.mapInterface.long = position.coords.longitude;
      const newMarker = marker(
        [this.mapInterface.lat, this.mapInterface.long],
        {
          icon: icon({
            iconUrl: '../../assets/images/marker-icon-2x-blue.png',
            shadowUrl: '../../assets/images/marker-shadow.png',
            iconSize: [25, 41],
            iconAnchor: [12, 41],
            popupAnchor: [1, -34],
            shadowSize: [41, 41]
          }),
        },
      );
      this.mapInterface.markers.push(newMarker);
    });
    const optionsSpec = this.utility.getLeafletMapOptions();
    this.mapInterface.options = this.utility.setLeafletMapOptions(
    optionsSpec.zoom, this.mapInterface.center, optionsSpec.layers[0].url);
  }

  onMapClick(eve){
    if(this.mapInterface.drawPath == true){
    if (!this.mapInterface.polyLines) {
      this.mapInterface.polyLines = {
        origin: { lat: this.mapInterface.markers[this.mapInterface.markers.length - 1]._latlng.lat, lng: this.mapInterface.markers[this.mapInterface.markers.length - 1]._latlng.lng },
        path: [
          [this.mapInterface.markers[this.mapInterface.markers.length - 1]._latlng.lat, this.mapInterface.markers[this.mapInterface.markers.length - 1]._latlng.lng],
          [eve.latlng.lat, eve.latlng.lng]
        ]
      };
    } else {
      this.mapInterface.polyLines.path.push([eve.latlng.lat, eve.latlng.lng]);
    }
    const idx = this.utility.polyLines.findIndex(x => x.origin.lat === this.mapInterface.polyLines.origin.lat &&
      x.origin.lng === this.mapInterface.polyLines.origin.lng);
    if (idx !== -1) {
      this.utility.polyLines[idx] = this.mapInterface.polyLines;
    } else {
      this.utility.polyLines.push(this.mapInterface.polyLines);
    }
    const newMarker = marker(
      [eve.latlng.lat, eve.latlng.lng],
      {
        icon: icon({
          iconUrl: '../../assets/images/marker-icon-2x-green.png',
          shadowUrl: '../../assets/images/marker-shadow.png',
          iconSize: [25, 41],
          iconAnchor: [12, 41],
          popupAnchor: [1, -34],
          shadowSize: [41, 41]
        }),
      },
      ).on('click', () => {
        this.ngZone.run(() => {
          this.removePath(eve.latlng.lat, eve.latlng.lng, newMarker);
        });
      });
    this.mapInterface.markers.push(newMarker);
    const polyline = L.polyline(this.mapInterface.polyLines.path).addTo(this.mapInterface.map);
    this.mapInterface.polyLinesGroup.push(polyline);
    }
    else{
      this.mapInterface.newLat = eve.latlng.lat;
      this.mapInterface.newLong = eve.latlng.lng;
      console.log(this.mapInterface.newLat);
      const Marker = marker(
        [this.mapInterface.newLat, this.mapInterface.newLong],
        {
          icon: icon({
            iconUrl: '../../assets/images/marker-icon-2x-red.png',
            shadowUrl: '../../assets/images/marker-shadow.png',
            iconSize: [25, 41],
            iconAnchor: [12, 41],
            popupAnchor: [1, -34],
            shadowSize: [41, 41]
          }),
        },
      )
      if(this.mapInterface.markers.length == 2){
        this.mapInterface.markers.splice(1 , 1);
        this.mapInterface.markers.push(Marker);
      }
      else{
        this.mapInterface.markers.push(Marker);
      }
    }
  }

  removePath(lat, lng, newMarker): void {
    if (this.mapInterface.polyLines.origin.lat !== lat && this.mapInterface.polyLines.origin.lng !== lng) {
      for (let i = 0; i < this.mapInterface.polyLinesGroup.length; i++) {
        for (const line of this.mapInterface.polyLinesGroup[i]._latlngs) {
          if (line.lat === lat && line.lng === lng) {
            this.mapInterface.polyLinesGroup[i].remove(this.mapInterface.map);
            this.mapInterface.polyLinesGroup.splice(i, 1);
            i = i - 1;
          }
        }
      }
      for (let i = 0; i < this.mapInterface.polyLines.path.length; i++) {
        if (this.mapInterface.polyLines.path[i][0] === lat && this.mapInterface.polyLines.path[i][1] === lng) {
          this.mapInterface.polyLines.path.splice(i, 1);
        }
      }
      const indx = this.utility.polyLines.findIndex(x => x.origin.lat === this.mapInterface.polyLines.origin.lat &&
        x.origin.lng === this.mapInterface.polyLines.origin.lng);
      if (indx !== -1) {
        this.utility.polyLines[indx] = this.mapInterface.polyLines;
      }
      newMarker.remove();
      const pline = L.polyline(this.mapInterface.polyLines.path).addTo(this.mapInterface.map);
      this.mapInterface.polyLinesGroup.push(pline);
    }
  }

  showPath(){
    this.mapInterface.drawPath = true;
    if (this.utility.polyLines){
    this.mapInterface.polyLinesGroup = [];
    for (const item of this.utility.polyLines) {
      for (const latlng of item.path) {
        let marUrl;
        if (latlng[0] === item.origin.lat && latlng[1] === item.origin.lng) {
          marUrl = '../../assets/images/marker-icon-2x-blue.png';
        } else {
          marUrl = '../../assets/images/marker-icon-2x-green.png';
        }
        const newMarker = marker(
          [latlng[0], latlng[1]],
          {
            icon: icon({
              iconUrl: marUrl,
              shadowUrl: '../../assets/images/marker-shadow.png',
              iconSize: [25, 41],
              iconAnchor: [12, 41],
              popupAnchor: [1, -34],
              shadowSize: [41, 41]
            }),
          },
        );
        this.mapInterface.markers.push(newMarker);
      }
      const polyline = L.polyline(item.path).addTo(this.mapInterface.map);
      this.mapInterface.polyLinesGroup.push(polyline);
    }
  }
  }

  onMapReady(map: L.Map){
    this.mapInterface.map = map;
  }

  pathCreated(){
    this.mapInterface.drawPath = false;
    this.mapInterface.markers = [];
    this.mapInterface.polyLinesGroup.forEach(item => {
      item.remove(this.mapInterface.map);
    });
    this.mapInterface.polyLines = undefined;
    this.mapInterface.polyLinesGroup = [];
    this.ngOnInit();
  }

  closeDialog(){
    history.back();
    this.dialogRef.close();
  }

   closeModal(): void {
    history.back();
    let Obj = {
       lat: this.mapInterface.newLat,
       lng: this.mapInterface.newLong
     }
    this.dialogRef.close(Obj);
  }
}
