import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-interaction-selection',
  templateUrl: './interaction-selection.component.html',
  styleUrls: ['./interaction-selection.component.scss']
})
export class InteractionSelectionComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<InteractionSelectionComponent>, private router: Router) { }

  ngOnInit(): void {
  }
  rediretTo(): void {
    this.router.navigate(['/interactions']);
    this.dialogRef.close();
  }
}
