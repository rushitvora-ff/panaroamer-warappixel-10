import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { startWith, map } from 'rxjs/operators';
import { ShareDialogInterface } from '../../../interfaces/sharedModule';

@Component({
  selector: 'app-share-dialog',
  templateUrl: './share-dialog.component.html',
  styleUrls: ['./share-dialog.component.scss']
})
export class ShareDialogComponent implements OnInit {
  shareDialogInterface: ShareDialogInterface = {
    optionsMsg: [
      'Cool! Check it out!',
      'This is woke! You have to see it!',
      'Wow! Take a look!',
      'Let me know what you think!',
      'Do you like it?'
    ],
    myControl: new FormControl('Cool! Check it out!')
  };

  constructor(
    private translateSevice: TranslateService,
    public dialogRef: MatDialogRef<ShareDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.translateSevice.get(
        ['shareIp.CoolCheckItOut', 'shareIp.ThisIsWoke', 'shareIp.WowTakeLook', 'shareIp.LetMeKnow', 'shareIp.DoYouLikeIt']
      ).subscribe((res: any[]) => {
        this.shareDialogInterface.optionsMsg = Object.keys(res).map((key) => res[key]);
      });
  }

  ngOnInit(): void {
    this.shareDialogInterface.filteredOptions = this.shareDialogInterface.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.shareDialogInterface.optionsMsg.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  onSubmit(): void {
    const url = this.data.selectedItem.url.replace('index.html', 'pwa.html');
    const nav: any = navigator;
    if (nav.share !== undefined) {
      nav.share({
        title: '',
        text: this.shareDialogInterface.myControl.value + '\n',
        url
      });
    } else {
      // let formattedBody = url + '\n' + '<img src="' + this.data.selectedItem.imgSrc + '">';
      const mailToLink = 'mailto:?Subject=' + this.shareDialogInterface.myControl.value + '&body=' + encodeURIComponent(url);
      window.location.href = mailToLink;
    }
  }

  closeModal(): void {
    this.dialogRef.close();
  }

}
