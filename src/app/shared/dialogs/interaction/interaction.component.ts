import { Component, OnInit, Inject, ChangeDetectorRef, ViewChild, ElementRef, OnDestroy, PLATFORM_ID } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import { MatSelectChange } from '@angular/material/select';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { NgxImageCompressService } from 'ngx-image-compress';
import { DomSanitizer } from '@angular/platform-browser';
import { UtilityService } from 'src/app/services/utility.service';
import { EditImageDialogComponent } from '../edit-image-dialog/edit-image-dialog.component';
import { ToastrService } from 'ngx-toastr';
import { Interaction, makeInteraction } from '@oneb/interactions-api-clients-models';
import { Activation, makeActivation } from '@oneb/interactions-api-clients-models';
import { makeMarker, Marker } from '@oneb/interactions-api-clients-models';
import { Emoji, makeEmoji } from '@oneb/interactions-api-clients-models';
import { Gesture, makeGesture } from '@oneb/interactions-api-clients-models';
import { Effect, makeEffect } from '@oneb/interactions-api-clients-models';
import { GestureTypeEnum_LongTap, GestureTypeEnum_Rub, GestureTypeEnum_Tap } from '@oneb/interactions-api-clients-models';
import { EffectActivation, makeEffectActivation } from '@oneb/interactions-api-clients-models';
import { EffectTypeEnum_Audio, EffectTypeEnum_Image, EffectTypeEnum_Survey, EffectTypeEnum_Text, EffectTypeEnum_Video } from '@oneb/interactions-api-clients-models';
import { ActivationModeEnum_All, ActivationModeEnum_Single } from '@oneb/interactions-api-clients-models';
import { makeCallToAction } from '@oneb/interactions-api-clients-models';
import { CaptureImageComponent } from 'src/app/shared/dialogs/capture-image/capture-image.component';
import { PlatformLocation } from '@angular/common';
import { InteractionServiceService } from '../../../services/interaction-service.service';
import { environment } from '../../../../environments/environment';
import { clientResponseSuccess, clientResponseHasErrors } from '@rainmaker2/rainmaker2-codegen-api-clients-common';
import { CallToActionTypeEnum_Buy, CallToActionTypeEnum_Call, CallToActionTypeEnum_Chat, CallToActionTypeEnum_Visit } from '@oneb/interactions-api-clients-models';
import * as AWS from 'aws-sdk';
import { isPlatformBrowser } from '@angular/common';
import { InteractionInterface } from '../../../interfaces/sharedModule';
import { InteractionSelectionComponent } from '../interaction-selection/interaction-selection.component';

declare var WaveSurfer: any;
declare var videojs: any;
declare var RecordRTC: any;
declare var StereoAudioRecorder: any;

@Component({
  selector: 'app-interaction',
  templateUrl: './interaction.component.html',
  styleUrls: ['./interaction.component.scss']
})
export class InteractionComponent implements OnInit, OnDestroy {

  interacrtionInterface: InteractionInterface = {
    gestArr: [],
    selectMode: 'All',
    coordinates: '',
    step: 0,
    gesture: '',
    buyActionText: '',
    visitActionText: '',
    mediaType: '',
    mediaTime: 5,
    userAction: new FormControl(),
    userSelectedActions: [],
    Text: '',
    fileUrl: '',
    gesFilterType: '',
    showRecorder: false,
    startGesRecord: false,
    actionView: false,
    selectedGesture: '',
    isRecording: false,
    showVideoRec: false,
    isIos: false,
    front: true,
    startRecording: false,
    isRecorder: false,
    isS3Loader: false,
    displayedColumns: ['gesture', 'x', 'y'],
    userActions: [
      { label: 'Buy', value: 'buy' },
      { label: 'Visit', value: 'visit' },
      { label: 'Call', value: 'call' },
      { label: 'Text', value: 'text' }
    ],
    surveys: [
      {
        id: 1, name: 'Survey-XXX', description: 'This is very good survey',
        isActive: false, url: 'survey-xyz', link: 'https://ippub.panaroamer.com/animals/index.html'
      },
      {
        id: 2, name: 'Survey-YYY', description: 'This is very good survey',
        isActive: false, url: 'survey-abc', link: 'https://ippub.panaroamer.com/birds/index.html'
      },
      {
        id: 3, name: 'Survey-ZZZ', description: 'This is very good survey',
        isActive: false, url: 'survey-def', link: 'https://ippub.panaroamer.com/camera/index.html'
      }
    ],
    s3: new AWS.S3({
      accessKeyId: environment.accessKeyId,
      secretAccessKey: environment.secretAccessKey,
      region: environment.region,
    })
  };

  @ViewChild('addImage', { read: ElementRef }) addImage: ElementRef;
  @ViewChild('forVideo', { read: ElementRef }) video: ElementRef;

  get isMobile(): any {
    return navigator.userAgent.match(/(iPhone)|(android)|(webOS)/i);
  }

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    @Inject(PLATFORM_ID) private platformId: Object,
    private location: PlatformLocation,
    private translate: TranslateService,
    private interactionService: InteractionServiceService,
    private imageCompress: NgxImageCompressService,
    public dialog: MatDialog,
    private router: Router,
    private utility: UtilityService,
    private toast: ToastrService,
    private cd: ChangeDetectorRef,
    public sanitizer: DomSanitizer,
    public dialogRef: MatDialogRef<InteractionComponent>) {
      this.interacrtionInterface.selectedItem = data.selectedItem;
      console.log(data);
      this.getOS();
      const saved = localStorage.getItem('interactionSaved');
      if (saved) {
        this.utility.saved = JSON.parse(saved);
        localStorage.removeItem('interactionSaved');
      }
  }

  ngOnInit(): void {
    if (this.data.interaction && !Array.isArray(this.data.interaction)) {
      this.interacrtionInterface.interaction = this.data.interaction;
    } else {
      this.interacrtionInterface.interaction = makeInteraction({}, true);
      this.interacrtionInterface.interaction.activationMode = ActivationModeEnum_All;
    }
    this.interacrtionInterface.effect = makeEffect({}, true);
    if (this.data.coordinates != 'false'){
      console.log(this.interacrtionInterface.activation);
      console.log(this.data.coordinates);
      this.interacrtionInterface.activation = makeActivation({}, true);
      this.interacrtionInterface.activation.x = this.data.coordinates.x;
      this.interacrtionInterface.activation.y = this.data.coordinates.y;
      this.interacrtionInterface.coordinates = { x: this.data.coordinates.x, y: this.data.coordinates.y };
      this.interacrtionInterface.step = 1;
      console.log(this.interacrtionInterface.step);
      if (this.interacrtionInterface.selectedItem.effects && this.interacrtionInterface.selectedItem.effects.length) {
        this.interacrtionInterface.gestArr = this.interacrtionInterface.selectedItem.effects ? JSON.parse(JSON.stringify(this.interacrtionInterface.selectedItem.effects)) : [];
        this.interacrtionInterface.selectMode = this.interacrtionInterface.selectedItem.mode ? JSON.parse(JSON.stringify(this.interacrtionInterface.selectedItem.mode)) : 'All';
      }
    }
    else{
      if (this.interacrtionInterface.selectedItem.effects && this.interacrtionInterface.selectedItem.effects.length) {
        this.interacrtionInterface.gestArr = this.interacrtionInterface.selectedItem.effects ? JSON.parse(JSON.stringify(this.interacrtionInterface.selectedItem.effects)) : [];
        this.interacrtionInterface.selectMode = this.interacrtionInterface.selectedItem.mode ? JSON.parse(JSON.stringify(this.interacrtionInterface.selectedItem.mode)) : 'All';
        this.interacrtionInterface.step = -1;
      }
    }
    if (isPlatformBrowser(this.platformId)) {
      history.pushState(
        {},
        document.getElementsByTagName('title')[0].innerHTML,
        window.location.href
      );

      this.location.onPopState(() => {

        if(this.interacrtionInterface.isRecorder == true){
          this.interacrtionInterface.isRecording = false;
          this.interacrtionInterface.startRecording = false;
          this.interacrtionInterface.vidPlayer.record().stop();
          this.exitRecord();
        }

        if (this.interacrtionInterface.isS3Loader == true) {
          this.utility.setLoader(false);
          this.interacrtionInterface.isS3Loader = false;
          if (this.interacrtionInterface.videoUpload == true) {
            this.video.nativeElement.value = '';
            this.interacrtionInterface.videoUpload = false;
          }
          this.interacrtionInterface.s3.abortMultipartUpload(this.interacrtionInterface.abortParams, function (err, data) {
            if (err){
              console.log('interactionAbortedError', err, err.stack); // an error occurred
            }
            else{
              console.log('interactionAborted', data);           // successful response
            }
          });
          this.dialogRef.disableClose = true;
        }
        else {
          this.dialogRef.close();
        }
    });
  };
}

  ngOnDestroy(): void {
    this.utility.isFullscreen = false;
  }

  getOS(): void {
    const platform = window.navigator.platform;
    const iosPlatforms = ['iPhone', 'iPad', 'iPod'];

    if (iosPlatforms.indexOf(platform) !== -1) {
      this.interacrtionInterface.isIos = true;
    }
  }

  onSelect({ selected }): void {
    this.interacrtionInterface.survey = selected[0];
  }

  showCoords(event: any): void {
    const x = event.offsetX;
    const y = event.offsetY;
    this.interacrtionInterface.coordinates = { x: Math.round((x * 100) / event.target.clientWidth), y: Math.round((y * 100) / event.target.clientHeight) };
    this.next();
    this.interacrtionInterface.activation = makeActivation({}, true);
    this.interacrtionInterface.activation.x = this.interacrtionInterface.coordinates.x;
    this.interacrtionInterface.activation.y = this.interacrtionInterface.coordinates.y;
    console.log(this.interacrtionInterface.activation.x);
    console.log(this.interacrtionInterface.activation.y);
  }

  select(event: any): void {
    this.interacrtionInterface.emoji = event.emoji;
    let marker: Marker = makeMarker({}, true);
    let emoji: Emoji = makeEmoji({}, true);
    emoji.name = this.interacrtionInterface.emoji.id;
    emoji.data = this.interacrtionInterface.emoji.sheet;
    marker.emoji = emoji;

    this.interacrtionInterface.activation.marker = marker;

    this.next();
  }

  onChange(value: string): void {
    if (value && this.interacrtionInterface.mediaType && this.interacrtionInterface.gesture) {
      const gesture: Gesture = makeGesture({}, true);
      if (this.interacrtionInterface.gesture === 'tap') {
        gesture.type = GestureTypeEnum_Tap;
      } else if (this.interacrtionInterface.gesture === 'long-tap') {
        gesture.type = GestureTypeEnum_LongTap;
      } else {
        gesture.type = GestureTypeEnum_Rub;
      }

      this.interacrtionInterface.activation.gesture = gesture;
      this.interacrtionInterface.activation.timeInMs = this.interacrtionInterface.mediaTime;
      const effectActivation: EffectActivation = makeEffectActivation({}, true);
      effectActivation.activation = this.interacrtionInterface.activation;

      this.interacrtionInterface.effect.activations = [];
      this.interacrtionInterface.effect.activations.push(effectActivation);

      if (this.interacrtionInterface.mediaType === 'survey') {
        this.dialogRef.updateSize('700px');
      }
      this.next();
    }
  }

  onChangeUserAction(event: MatSelectChange): void {
    this.interacrtionInterface.userSelectedActions = event.value;
  }

  preInteractions(): void{
    this.dialogRef.close({
      x: this.interacrtionInterface.activation.x,
      y: this.interacrtionInterface.activation.y,
      interactions: this.interacrtionInterface.interaction
    });
    this.router.navigate(['/interactions']);
  }

  createNewInteraction(): void {
    this.next();
  }

  next(): void {
    if (this.interacrtionInterface.step === 0) {
      if (this.interacrtionInterface.coordinates) {
        this.interacrtionInterface.step++;
      }
    } else if (this.interacrtionInterface.step === 1) {
      // if (this.interacrtionInterface.emoji) {
          this.interacrtionInterface.step++;
      // }
    }else if (this.interacrtionInterface.step === 2){
      if (this.interacrtionInterface.emoji) {
        this.interacrtionInterface.step++;
      }
    }
     else if (this.interacrtionInterface.step === 3) {
      if (this.interacrtionInterface.mediaType && this.interacrtionInterface.gesture && this.interacrtionInterface.mediaTime) {
        this.interacrtionInterface.step++;
      }
    } else if (this.interacrtionInterface.step === 4) {
      this.interacrtionInterface.step++;
      setTimeout(() => {
        this.openSelection();
      }, 1000);
    } else {
      if (this.interacrtionInterface.step === -1) {
        this.interacrtionInterface.mediaTime = 5;
      }
      this.interacrtionInterface.step++;
    }
  }

  deleteEventItem(item: any): void {
    // this.utility.setLoader(true);
    // this.interactionService.deleteSingleInteraction(item.effectId).then((res) => {
    //       if(res) {
            const idx = this.interacrtionInterface.gestArr.findIndex((x: any) => x.id === item.id);
            if (idx !== -1) {
              this.interacrtionInterface.gestArr.splice(idx, 1);
              this.interacrtionInterface.interaction.effects.splice(idx, 1);
            }
            // this.interacrtionInterface.selectedItem.effects = this.interacrtionInterface.gestArr;
            if (!this.interacrtionInterface.gestArr.length) {
              this.interacrtionInterface.selectMode = 'All';
              this.interacrtionInterface.step = 0;
            }
            this.cd.detectChanges();
            localStorage.setItem('ipDataPre', JSON.stringify(this.clearSafeUrl()));
            this.utility.setLoader(false);
            this.toast.success('Successfully Deleted');
          // }
          // else{
          //   this.utility.setLoader(false);
          //   this.toast.error('Something went Wrong.');
          // }
    // });
  }

  submitAction(): void {
    const arr = [];
    const callToAction = makeCallToAction({}, true);
    if ((this.interacrtionInterface.userSelectedActions.includes('buy') || this.interacrtionInterface.selectedAction === 'buy') && this.interacrtionInterface.buyActionText) {
      arr.push({action: 'buy', value: this.interacrtionInterface.buyActionText});
      callToAction.data = this.interacrtionInterface.buyActionText;
      callToAction.type = CallToActionTypeEnum_Buy;
    }
    if ((this.interacrtionInterface.userSelectedActions.includes('visit') || this.interacrtionInterface.selectedAction === 'visit') &&  this.interacrtionInterface.visitActionText) {
      arr.push({action: 'visit', value: this.interacrtionInterface.visitActionText});
      callToAction.data = this.interacrtionInterface.visitActionText;
      callToAction.type = CallToActionTypeEnum_Visit;
    }
    if ((this.interacrtionInterface.userSelectedActions.includes('call') || this.interacrtionInterface.selectedAction === 'call') && this.interacrtionInterface.phoneNumber) {
      arr.push({action: 'call', value: this.interacrtionInterface.phoneNumber});
      callToAction.data = this.interacrtionInterface.phoneNumber.toString();
      callToAction.type = CallToActionTypeEnum_Call;
    }
    if ((this.interacrtionInterface.userSelectedActions.includes('text') || this.interacrtionInterface.selectedAction === 'text') && this.interacrtionInterface.smsNumber) {
      arr.push({action: 'text', value: this.interacrtionInterface.smsNumber});
      callToAction.data = this.interacrtionInterface.smsNumber.toString();
      callToAction.type = CallToActionTypeEnum_Chat;
    }
    const idx = this.interacrtionInterface.gestArr.findIndex((x: any) => x.id === this.interacrtionInterface.selectedGesture.id);
    if (idx !== -1) {
     this.interacrtionInterface.gestArr[idx].userActions = arr;
    }
    const idxa = this.interacrtionInterface.selectedItem.effects.findIndex((x: any) => x.id === this.interacrtionInterface.selectedGesture.id);
    if (idx !== -1) {
      this.interacrtionInterface.selectedItem.effects[idxa].userActions = arr;
    }

    const idxDs = this.interacrtionInterface.interaction.effects.findIndex((x: any) => x.effectId === this.interacrtionInterface.selectedGesture.effectId);
    if (idxDs !== -1) {
      this.interacrtionInterface.interaction.effects[idxDs].callToActions.push(callToAction);
    }

    this.interacrtionInterface.step = -1;

    this.interacrtionInterface.fileUrl = '';
    this.interacrtionInterface.gesture = '';
    this.interacrtionInterface.mediaType = '';
    this.interacrtionInterface.mediaTime = 5;
    this.interacrtionInterface.coordinates = '';
    this.interacrtionInterface.step = -1;
    this.interacrtionInterface.Text = '';
    this.interacrtionInterface.userSelectedActions = [];
    this.interacrtionInterface.buyActionText = null;
    this.interacrtionInterface.visitActionText = null;
    this.interacrtionInterface.phoneNumber = null;
    this.interacrtionInterface.smsNumber = null;
    this.interacrtionInterface.gesFilterType = 'normal';
    this.interacrtionInterface.actionView = false;

    this.interacrtionInterface.userAction.patchValue('');

    localStorage.setItem('ipDataPre', JSON.stringify(this.clearSafeUrl()));
  }

  getActions(element): string[] {
    return element.userActions ? element.userActions.map(act => act.action) : [];
  }

  deleteAction(type, element): void {
    const idx =  this.interacrtionInterface.gestArr.findIndex((x: any) => x.id === element.id);
    if (idx !== -1) {
     const actIdx =  this.interacrtionInterface.gestArr[idx].userActions.findIndex(x => x.action === type);
     if (actIdx !== -1) {
      this.interacrtionInterface.gestArr[idx].userActions.splice(actIdx, 1);
      this.interacrtionInterface.interaction.effects[idx].callToActions.splice(actIdx, 1);
     }
     localStorage.setItem('ipDataPre', JSON.stringify(this.clearSafeUrl()));
    }
  }

  addAction(type, element): void {
    this.interacrtionInterface.selectedAction = type;
    if (element.userActions && element.userActions.length) {
      this.interacrtionInterface.userSelectedActions = element.userActions.map(act => act.action);
      for (const act of element.userActions) {
        if (act.action === 'buy') {
          this.interacrtionInterface.buyActionText = act.value;
        }  else if (act.action === 'visit') {
          this.interacrtionInterface.visitActionText = act.value;
        } else if (act.action === 'call') {
          this.interacrtionInterface.phoneNumber = act.value;
        } else if (act.action === 'text') {
          this.interacrtionInterface.smsNumber = act.value;
        }
      }
      this.interacrtionInterface.step = 10;
      this.interacrtionInterface.actionView = true;
      this.interacrtionInterface.selectedGesture = element;
    } else {
      this.interacrtionInterface.step = 10;
      this.interacrtionInterface.actionView = true;
      this.interacrtionInterface.selectedGesture = element;
    }
  }

  submitObj(): void {
    if (( this.interacrtionInterface.mediaType === 'text' &&  this.interacrtionInterface.Text) || ( this.interacrtionInterface.mediaType !== 'text' && ( this.interacrtionInterface.fileUrl ||  this.interacrtionInterface.survey))) {
      this.dialogRef.updateSize('500px');
      const obj: any = Object.assign({},  this.interacrtionInterface.coordinates);
      obj.time =  this.interacrtionInterface.mediaTime;
      obj.gesture =  this.interacrtionInterface.gesture;
      obj.type =  this.interacrtionInterface.mediaType;
      obj.emoji =  this.interacrtionInterface.emoji.id;
      obj.emojiObj =  this.interacrtionInterface.emoji.sheet;
      let preMsg = '';
      if ( this.interacrtionInterface.gesture === 'tap') {
        preMsg = 'Tap here to ';
      } else if ( this.interacrtionInterface.gesture === 'long-tap') {
        preMsg = 'Long tap here to ';
      } else {
        preMsg = 'Rub here to ';
      }

      if ( this.interacrtionInterface.mediaType === 'text') {
        obj.message = preMsg + 'see text';
        obj.text =  this.interacrtionInterface.Text;
        this.interacrtionInterface.effect.type = EffectTypeEnum_Text;
        this.interacrtionInterface.effect.dataUrl =  this.interacrtionInterface.Text;
      } else if ( this.interacrtionInterface.mediaType === 'image') {
        obj.message = preMsg + 'see Image';
        obj.text =  this.interacrtionInterface.fileUrl;
        this.interacrtionInterface.effect.type = EffectTypeEnum_Image;
        this.interacrtionInterface.effect.dataUrl =  this.interacrtionInterface.fileUrl;
        obj.gesFilterType =  this.interacrtionInterface.gesFilterType;
      } else if ( this.interacrtionInterface.mediaType === 'audio') {
        obj.message = preMsg + 'listen Audio';
        obj.text =  this.interacrtionInterface.fileUrl;
        this.interacrtionInterface.effect.type = EffectTypeEnum_Audio;
        this.interacrtionInterface.effect.dataUrl =  this.interacrtionInterface.fileUrl;
      } else if ( this.interacrtionInterface.mediaType === 'video') {
        obj.message = preMsg + 'see Video';
        this.interacrtionInterface.effect.type = EffectTypeEnum_Video;
        this.interacrtionInterface.effect.dataUrl =  this.interacrtionInterface.fileUrl;
        obj.text =  this.interacrtionInterface.fileUrl;
      } else if ( this.interacrtionInterface.mediaType === 'survey') {
        obj.message = preMsg + 'see Survey';
        this.interacrtionInterface.effect.type = EffectTypeEnum_Survey;
        this.interacrtionInterface.effect.dataUrl =  this.interacrtionInterface.survey.link;
        obj.text =  this.interacrtionInterface.survey.link;
      }
      obj.id = Math.random().toString(36).substring(2);
      obj.effectId =  this.interacrtionInterface.effect.effectId;
      this.interacrtionInterface.gestArr.push(obj);
      console.log(this.interacrtionInterface.gestArr);
      this.utility.saved.push(false);
      this.interacrtionInterface.interaction.effects.push( this.interacrtionInterface.effect);
      this.interacrtionInterface.effect = makeEffect({}, true);
      this.utility.gestArray = this.interacrtionInterface.gestArr;
      // this.interacrtionInterface.selectedItem.effects =  this.interacrtionInterface.gestArr;
      localStorage.setItem('ipDataPre', JSON.stringify(this.clearSafeUrl()));
      console.log(this.interacrtionInterface.selectedItem);

      this.interacrtionInterface.fileUrl = '';
      this.interacrtionInterface.gesture = '';
      this.interacrtionInterface.mediaType = '';
      this.interacrtionInterface.mediaTime = 5;
      this.interacrtionInterface.coordinates = '';
      this.interacrtionInterface.step = -1;
      this.interacrtionInterface.Text = '';
      this.interacrtionInterface.userSelectedActions = [];
      this.interacrtionInterface.phoneNumber = null;
      this.interacrtionInterface.smsNumber = null;
      this.interacrtionInterface.gesFilterType = 'normal';
    }

    // this.createEffectsApi();
  }

  async createInteractionApi() {
    let createRsp = await this.interactionService.createInteractions( this.interacrtionInterface.interaction);
    if (clientResponseSuccess(createRsp)) {
      console.log('createdInteraction', createRsp);
    }
    else if (clientResponseHasErrors(createRsp)) {
      console.log('createInteractionError', createRsp);
    }
  }

  async createEffectsApi() {
    let createRsp = await this.interactionService.createEffects( this.interacrtionInterface.effect[0]);
    if (clientResponseSuccess(createRsp)) {
      console.log('createdEffects', createRsp);
    }
    else if (clientResponseHasErrors(createRsp)) {
      console.log('createdEffectsError', createRsp);
    }
  }

  setInteractionData() {
    this.dialogRef.close(this.interacrtionInterface.interaction);
  }

  onSelectMode(event): void {
    this.interacrtionInterface.selectMode = event.value;
    this.interacrtionInterface.selectedItem.mode =  this.interacrtionInterface.selectMode;
    if( this.interacrtionInterface.selectMode === 'All') {
      this.interacrtionInterface.interaction.activationMode = ActivationModeEnum_All;

    } else {
      this.interacrtionInterface.interaction.activationMode = ActivationModeEnum_Single;
    }
    localStorage.setItem('ipDataPre', JSON.stringify(this.clearSafeUrl()));
  }
  b64toBlob(dataURI: string): any {
    const byteString = atob(dataURI.split(',')[1]);
    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);

    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ab], { type: this.base64MimeType(dataURI) });
  }

  base64MimeType(encoded: any): any {
    let result = null;
    if (typeof encoded !== 'string') {
      return result;
    }
    const mime = encoded.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);
    if (mime && mime.length) {
      result = mime[1];
    }
    return result;
  }
  setImage(eve): void {
    const files = eve.target.files;
    if (files.length === 0) {
      return;
    }
    const reader = new FileReader();
    reader.onload = (event) => {
      this.imageCompress.compressFile(event.target.result, -2, 40, 40).then(
        result => {
          try {
            eve.target.value = '';
            const dialogRef = this.dialog.open(EditImageDialogComponent, {
              panelClass: 'edit-image-dialog',
              maxWidth: '500px',
              maxHeight: '90vh',
              data: {
                imageFile: result
              },
              closeOnNavigation: true
            });
            dialogRef.afterClosed().subscribe(res => {
              if (res) {
                let blob = this.b64toBlob(res.data);
                blob.name = `${new Date().getTime()}`;
                this.s3Upload(blob);
                // this.fileUrl = res.data;
                this.interacrtionInterface.gesFilterType = res.type;
                // this.utility.setLoader(false);
              }
            });
          } catch (err) {
            throw err;
          }
        }
      );
    };
    reader.readAsDataURL(files[0]);
  }

  updateImage(): void {
    const dialogRef = this.dialog.open(EditImageDialogComponent, {
      panelClass: 'edit-image-dialog',
      maxWidth: '500px',
      maxHeight: '90vh',
      data: {
        imageFile:  this.interacrtionInterface.fileUrl,
        selectedItem:  this.interacrtionInterface.gesFilterType
      },
      disableClose: false
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        let blob = this.b64toBlob(res.data);
        blob.name = `${new Date().getTime()}`;
        this.s3Upload(blob);
        // this.fileUrl = res.data;
        this.interacrtionInterface.gesFilterType = res.type;
        // this.utility.setLoader(false);
      }
    });
  }

  selectGesMp3(event): void {
    const files = event.target.files;
    if (files.length) {
      // this.fileUrl = window.URL.createObjectURL(files[0]);
      const reader = new FileReader();
      reader.readAsDataURL(files[0]);
      reader.onload = (evet: any) => {
        let blob = this.b64toBlob(evet.target.result);
        blob.name = `${new Date().getTime()}`;
        console.log(blob);
        this.s3Upload(blob);
        // this.fileUrl = evet.target.result;
      };
    }
    event.target.value = '';
  }

  openSelection(): void {
    if ( this.interacrtionInterface.mediaType === 'image') {
      const el: any = this.addImage.nativeElement;
      if (el) {
        el.click();
      }
    }
  }

  clearSafeUrl(): any[] {
    const arr = JSON.parse(localStorage.getItem('AddEffects'));
    const idx = arr.findIndex(x => x.id ==  this.interacrtionInterface.selectedItem.id);
    if (idx !== -1) {
      arr[idx].id =  this.interacrtionInterface.selectedItem.id;
      arr[idx].type =  this.interacrtionInterface.selectedItem.type;
      arr[idx].url =  this.interacrtionInterface.selectedItem.url;
      arr[idx].music =  this.interacrtionInterface.selectedItem.music;
      arr[idx].item =  this.interacrtionInterface.selectedItem.item;
      arr[idx].surveyUrl =  this.interacrtionInterface.selectedItem.surveyUrl;
      arr[idx].effects =  this.interacrtionInterface.selectedItem.effects;
      arr[idx].mode =  this.interacrtionInterface.selectedItem.mode || 'All';
      arr[idx].filterType =  this.interacrtionInterface.selectedItem.filterType;
    }
    return arr;
  }

  captureImage(): void{
    const dialogRef = this.dialog.open(CaptureImageComponent, {
      panelClass: 'app-full-screen-dialog',
      maxWidth: '100vw',
      maxHeight: '100vh',
      height: '100%',
      width: '100%',
      closeOnNavigation: true,
    });
    dialogRef.afterClosed().subscribe(res => {
      console.log(res);
      if (res) {
        let blob = this.b64toBlob(res._imageAsDataUrl);
        blob.name = `${new Date().getTime()}`;
        this.s3Upload(blob);
        this.interacrtionInterface.gesFilterType = res.type;
        this.utility.setLoader(false);
      }
    });
  }

  recordAudio(): void {
    this.interacrtionInterface.showRecorder = true;
    this.interacrtionInterface.fileUrl = undefined;
    this.interacrtionInterface.options = {
      controls: true,
      width: 600,
      height: 75,
      fluid: false,
      controlBar: {
        // hide fullscreen and volume controls
        fullscreenToggle: false,
        volumePanel: false
      },
      plugins: {
        wavesurfer: {
          src: 'live',
          waveColor: '#ffffff',
          progressColor: '#7def68',
          debug: true,
          cursorWidth: 1,
          msDisplayMax: 20,
          hideScrollbar: true
        },
        record: {
          audio: true,
          video: false,
          maxLength: 20,
          debug: true
        }
      }
    };
    setTimeout(() => {

      // apply audio workarounds for certain browsers
      this.applyAudioWorkaround();
      // create player
      this.interacrtionInterface.player = videojs('myAudio',  this.interacrtionInterface.options, () => {
        // print version information at startup
        const msg = 'Using video.js ' + videojs.VERSION +
          ' with videojs-record ' + videojs.getPluginVersion('record') +
          ', videojs-wavesurfer ' + videojs.getPluginVersion('wavesurfer') +
          ', wavesurfer.js ' + WaveSurfer.VERSION + ' and recordrtc ' +
          RecordRTC.version;
        videojs.log(msg);
      });

      setTimeout(() => {
        this.interacrtionInterface.player.record().getDevice();
      }, 300);

      // error handgling
      this.interacrtionInterface.player.on('deviceError', () => {
        console.log('device error:',  this.interacrtionInterface.player.deviceErrorCode);
        this.translate.get('toast.Device not found!').subscribe(response => {
          this.toast.error(response);
        });
      });

      this.interacrtionInterface.player.on('error', (element, error) => {
        console.error(error);
      });

      // user clicked the record button and started recording
      this.interacrtionInterface.player.on('startRecord', () => {
        console.log('started recording!');
        this.interacrtionInterface.startGesRecord = true;
      });

      // user completed recording and stream is available
      this.interacrtionInterface.player.on('finishRecord', () => {
        // the blob object contains the recorded data that
        // can be downloaded by the user, stored on server etc
        setTimeout(() => {
          this.cd.detectChanges();
        }, 500);
        console.log('finished recording: ',  this.interacrtionInterface.player.recordedData);
        // this.fileUrl = window.URL.createObjectURL(this.player.recordedData);
        const reader = new FileReader();
        reader.readAsDataURL( this.interacrtionInterface.player.recordedData);
        reader.onload = (evet: any) => {
          let blob = this.b64toBlob(evet.target.result);
          blob.name = `${new Date().getTime()}`;
          console.log(blob);
          this.s3Upload(blob);
          // this.fileUrl = evet.target.result;
          console.log(evet.target.result);
        };
        this.interacrtionInterface.player.record().stopDevice();
        this.interacrtionInterface.startGesRecord = false;
        this.exitAudioRecord();
      });

      this.setCss();
    }, 100);
  }

   s3Upload(files: any) {
    const fileType = files.type.split('/').pop();
    const id = Math.random().toString().substring(2);

    const BucketName = environment.BucketName;
    const s3Key = `${id}.${fileType}`;
    const params = {
      Bucket: BucketName,
      Key: s3Key,
    };

    this.utility.setLoader(true);
    this.interacrtionInterface.isS3Loader = true;
    let uploadID;

    const chunkSize = 5242880;
    let chunkArray = [];
    let i = 0;
    for (let offset = 0; offset < files.size; offset += chunkSize) {
      const chunk = files.slice(offset, offset + chunkSize);
      chunk.name = `${new Date().getTime()}`;
      chunkArray.push({ blob: chunk, partNumber: i + 1 });
      i++;
    }
    const s = this;
    if( this.interacrtionInterface.isS3Loader){
      this.interacrtionInterface.s3.createMultipartUpload(params, (err, data) => {
      if (err) {
        s.utility.setLoader(false);
      } else {
        console.log(s);
        console.log(data);
        uploadID = data.UploadId;
        this.interacrtionInterface.abortParams = {
          Bucket: environment.BucketName,
          Key: s3Key,
          UploadId: data.UploadId
        }
        let part;
        for (let j = 0; j < chunkArray.length + 1; j++) {
          if (j < chunkArray.length &&  this.interacrtionInterface.isS3Loader) {
            part = s.uploadPart(s, chunkArray, j, uploadID, s3Key);
          }
        }
        console.log(part);
      }
    });
  }
  }

  uploadPart(s: any , chunkArray: Array<any>, j: number, uploadID: string, s3Key: string ) {
    try {
      this.interacrtionInterface.s3.uploadPart(
        {
          Body: chunkArray[j].blob,
          Bucket: environment.BucketName,
          Key: s3Key,
          PartNumber: chunkArray[j].partNumber,
          UploadId: uploadID,
          ContentLength: chunkArray[j].size,
        },
        (err, data) => {
          if(err){
            console.log(err)
          }
          else{
          let uploadedChunks = [];
          console.log(data, j);
          uploadedChunks.push({
            ETag: JSON.parse(data.ETag),
            PartNumber: chunkArray[j].partNumber,
          });
          let sortedPartArray;
          let result;
          if (uploadedChunks.length == chunkArray.length) {
            sortedPartArray = {
              Parts: uploadedChunks.sort(function (a, b) {
                return a.PartNumber - b.PartNumber;
              }),
            };
            if( this.interacrtionInterface.isS3Loader){
            result = s.MultiPart(s, sortedPartArray, uploadID, s3Key);
           }
          }
        }
      }
      );
    } catch (err) {
      s.utility.setLoader(false);
      s.toast.error('Upload Failed');
      console.log(err);
    }
  }
  MultiPart(s: any, sortedPartArray: any, uploadID: string, s3Key: string) {
    console.log(s);
    try {
      this.interacrtionInterface.s3.completeMultipartUpload(
        {
          Bucket: environment.BucketName,
          Key: s3Key,
          MultipartUpload: sortedPartArray,
          UploadId: uploadID,
        },
        (err, response) => {
          s.interacrtionInterface.fileUrl = response.Location;
          s.utility.setLoader(false);
          s.toast.success('Successfully Uploaded');
          s.submitObj();
          console.log('fileUrl',  this.interacrtionInterface.fileUrl);
          console.log('Final Data', response);
          console.log('Final Err', err);
        }
      );
    } catch (err) {
      s.utility.setLoader(false);
      s.toast.error('Upload Failed');
    }
  }

  applyAudioWorkaround(): void {
    const WINDOW: any = window;
    const isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
    const isEdge = /Edge/.test(navigator.userAgent);
    if (isSafari || isEdge) {
      if (isSafari && WINDOW.MediaRecorder !== undefined) {
        // this version of Safari has MediaRecorder
        return;
      }

      // support recording in safari 11/12
      // see https://github.com/collab-project/videojs-record/issues/295
      this.interacrtionInterface.options.plugins.record.audioRecorderType = StereoAudioRecorder;
      this.interacrtionInterface.options.plugins.record.audioSampleRate = 44100;
      this.interacrtionInterface.options.plugins.record.audioBufferSize = 4096;
      this.interacrtionInterface.options.plugins.record.audioChannels = 2;

      console.log('applied audio workarounds for this browser');
    }
  }

  exitAudioRecord(): void {
    if ( this.interacrtionInterface.player) {
      this.interacrtionInterface.player.record().destroy();
      document.getElementById('waves').innerHTML += '<audio id="myAudio" class="video-js vjs-default-skin parent-audio"></audio>';
      this.interacrtionInterface.player.off('finishRecord');
    }
    this.interacrtionInterface.options = undefined;
    this.interacrtionInterface.player = undefined;
    this.interacrtionInterface.showRecorder = false;
  }

  setCss(): void {
    const audio = document.getElementsByClassName('parent-audio');
    if (audio.length) {
      for (let i = 0; i < audio.length; i++) {
        audio[i].setAttribute('style', 'background-color: ' + '#000000');
      }
    }
    const controls = document.getElementsByClassName('vjs-control-bar');
    if (controls.length) {
      for (let i = 0; i < controls.length; i++) {
        controls[i].setAttribute('style', 'display: flex;background-color: ' +
          'grey');
      }
    }
    const elements = document.getElementsByClassName('vjs-control');
    if (elements.length) {
      for (let i = 0; i < elements.length; i++) {
        elements[i].setAttribute('style', 'color: ' + '#ffffff');
      }
    }
    const controlsbar = document.getElementsByClassName('vjs-time-control');
    if (controlsbar.length) {
      for (let i = 0; i < controlsbar.length; i++) {
        controlsbar[i].setAttribute('style', 'display: flex;color:' + '#ffffff');
      }
    }
  }

  playPauseAudio(): void {
    this.interacrtionInterface.isRecording = ! this.interacrtionInterface.isRecording;
    if ( this.interacrtionInterface.isRecording) {
      this.interacrtionInterface.player.record().pause();
    } else {
      this.interacrtionInterface.player.record().resume();
    }
  }

  addGesVideos(evnt): void {
    this.interacrtionInterface.videoUpload = true;
    const files = evnt.target.files;
    if (files.length === 0) {
      return;
    }
    const data = [];
    const mimeType = files[0].type;
    if (mimeType.match(/video\/*/) == null) {
      alert('Only videos are supported.');
      return;
    }
    const reader = new FileReader();
    reader.onload = (event: any) => {
      // this.fileUrl = window.URL.createObjectURL(this.b64toBlob(event.target.result));
      let blob = this.b64toBlob(event.target.result);
      blob.name = `${new Date().getTime()}`;
      console.log(blob);
      this.s3Upload(blob);
      // this.fileUrl = event.target.result;
      // this.utility.setLoader(false);
    };
    reader.readAsDataURL(files[0]);
  }

  recordVid(): void {
    this.interacrtionInterface.isRecorder = true;
    this.utility.isFullscreen = true;
    this.interacrtionInterface.showVideoRec = true;
    if ( this.interacrtionInterface.front) {
      this.interacrtionInterface.options = {
        controls: true,
        width: 400,
        height: 200,
        fluid: false,
        controlBar: {
          // hide fullscreen and volume controls
          fullscreenToggle: false,
          volumePanel: false,
          pipToggle: false
        },
        plugins: {
          record: {
            audio: true,
            video: {
              facingMode: 'user'
            },
            maxLength: 20,
            debug: true,
            pip: false
          }
        }
      };
    } else {
      this.interacrtionInterface.options = {
        controls: true,
        width: 400,
        height: 200,
        fluid: false,
        controlBar: {
          // hide fullscreen and volume controls
          fullscreenToggle: false,
          volumePanel: false,
          pipToggle: false
        },
        plugins: {
          record: {
            audio: true,
            video: {
              facingMode: 'environment'
            },
            maxLength: 20,
            debug: true,
            pip: false
          }

        }
      };
    }

    // apply audio workarounds for certain browsers
    this.applyAudioWorkaround();
    // create player
    this.interacrtionInterface.vidPlayer = videojs('myvideo1',  this.interacrtionInterface.options, () => {
      // print version information at startup
      const msg = 'Using video.js ' + videojs.VERSION +
        ' with videojs-record ' + videojs.getPluginVersion('record') +
        ', videojs-wavesurfer ' + videojs.getPluginVersion('wavesurfer') +
        ', wavesurfer.js ' + WaveSurfer.VERSION + ' and recordrtc ' +
        RecordRTC.version;
      videojs.log(msg);
    });
    this.interacrtionInterface.vidPlayer.record().getDevice();

    // error handgling
    this.interacrtionInterface.vidPlayer.on('deviceError', () => {
      console.log('-----device-error-----');
      this.interacrtionInterface.isIos = true;
      this.utility.isFullscreen = false;
      // this.uploadVid();
      // console.log('device error:', this.vidPlayer.deviceErrorCode);
      // this.translateSevice.get('toast.Device not found!').subscribe(res => {
      //   this.toast.error(res);
      // });
      this.interacrtionInterface.showVideoRec = false;
    });

    this.interacrtionInterface.vidPlayer.on('error', (element, error) => {
      console.log('-----error-----');
      this.interacrtionInterface.isIos = true;
      this.utility.isFullscreen = false;
      // this.uploadVid();
      // console.log('device error:', this.vidPlayer.deviceErrorCode);
      // this.translateSevice.get('toast.webrtcerror').subscribe(res => {
      //   this.toast.error(res);
      // });
      this.interacrtionInterface.showVideoRec = false;
    });

    // user completed recording and stream is available
    this.interacrtionInterface.vidPlayer.on('finishRecord', () => {
      console.log('-----finishRecord-----');
      const data = [];
      this.utility.isFullscreen = false;
      this.interacrtionInterface.showVideoRec = false;
      this.interacrtionInterface.vidPlayer.off('finishRecord');
      this.interacrtionInterface.vidPlayer.record().stopDevice();
      this.interacrtionInterface.isRecording = false;

      this.interacrtionInterface.startRecording = false;

      // this.fileUrl = window.URL.createObjectURL(this.vidPlayer.recordedData);
      const reader = new FileReader();
      reader.onload = (evet: any) => {
        let blob = this.b64toBlob('data:video/mp4;base64,' + evet.target.result.split(',')[2]);
        blob.name = `${new Date().getTime()}`;
        console.log(blob);
        this.s3Upload(blob);
        // this.fileUrl = 'data:video/mp4;base64,' + evet.target.result.split(',')[2];
      };
      reader.readAsDataURL( this.interacrtionInterface.vidPlayer.recordedData);
    });
    this.setCss();
  }

  flipCamera(): void {
    this.interacrtionInterface.vidPlayer.record().stopDevice();
    this.interacrtionInterface.vidPlayer.record().destroy();
    document.getElementById('vid-parent1').innerHTML += '<video id="myvideo1" class="video-js vjs-default-skin parent-audio"></video>';
    this.interacrtionInterface.front = !this.interacrtionInterface.front;
    this.interacrtionInterface.vidPlayer.off('finishRecord');
    this.interacrtionInterface.options = undefined;
    this.interacrtionInterface.vidPlayer = undefined;
    this.recordVid();
  }

  playPauseVid(): void {
    this.interacrtionInterface.isRecording = !this.interacrtionInterface.isRecording;
    if ( this.interacrtionInterface.isRecording) {
      this.interacrtionInterface.vidPlayer.record().pause();
    } else {
      this.interacrtionInterface.vidPlayer.record().resume();
    }
  }

  startRecord(): void {
    this.interacrtionInterface.vidPlayer.record().start();
    this.interacrtionInterface.startRecording = true;
  }

  stopRecord(): void {
    this.utility.isFullscreen = false;
    this.interacrtionInterface.vidPlayer.record().stop();
  }

  exitRecord(): void {
    this.utility.isFullscreen = false;
    this.interacrtionInterface.vidPlayer.record().stopDevice();
    this.interacrtionInterface.vidPlayer.record().destroy();
    document.getElementById('vid-parent1').innerHTML += '<video id="myvideo1" class="video-js vjs-default-skin parent-audio"></video>';
    this.interacrtionInterface.vidPlayer.off('finishRecord');
    this.interacrtionInterface.options = undefined;
    this.interacrtionInterface.vidPlayer = undefined;
    this.interacrtionInterface.showVideoRec = false;
  }

   addToAPI(element, i): void {
    // this.interacrtionInterface.step = 6;
    // this.interacrtionInterface.interactionDialog = true;
    // this.createEffectsApi();
    this.utility.saved[i] = true;
    const dialogRef = this.dialog.open(InteractionSelectionComponent, {
      panelClass: this.utility.dark ? ['dark', 'app-full-bleed-dialog-interaction'] : 'app-full-bleed-dialog-interaction',
      minWidth: '250px',
      minHeight: '110px',
      // data: {
      //   imageFile: result
      // },
      closeOnNavigation: true
    }).afterClosed().subscribe(res => {
      console.log(this.router.url);
      this.dialogRef.close();
    }) ;
  }
}
