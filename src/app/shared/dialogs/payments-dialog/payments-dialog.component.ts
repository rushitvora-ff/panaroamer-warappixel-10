import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-payments-dialog',
  templateUrl: './payments-dialog.component.html',
  styleUrls: ['./payments-dialog.component.scss']
})
export class PaymentsDialogComponent implements OnInit {
  startDate;
  endDate;

  constructor(
    private toast: ToastrService, private translateSevice: TranslateService,
    private router: Router,
    public dialogRef: MatDialogRef<PaymentsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
  }

  makePayment(): void {
    if (this.startDate && this.endDate) {
      this.closeModal();
      this.router.navigate(['/payments'], {queryParams: {id: '13635'}});
    } else {
      this.translateSevice.get('toast.Please fill all the fields.').subscribe(res => {
        this.toast.warning(res);
      });
    }
  }

  closeModal(): void {
    this.dialogRef.close();
  }

}
