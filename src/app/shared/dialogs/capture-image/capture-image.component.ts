import { Component, OnInit,Inject, PLATFORM_ID } from '@angular/core';
import {WebcamImage} from 'ngx-webcam'; 
import {Subject, Observable} from 'rxjs'; 
import { MatDialogRef } from '@angular/material/dialog';
import { isPlatformBrowser } from '@angular/common';
import { CaptureImageInterface } from '../../../interfaces/sharedModule';

@Component({
  selector: 'app-capture-image',
  templateUrl: './capture-image.component.html',
  styleUrls: ['./capture-image.component.scss']
})
export class CaptureImageComponent implements OnInit {
  captureImageInterface: CaptureImageInterface = {
  webcam: false,
  webcamImage: null,
  trigger: new Subject<void>()
}

  constructor(
    public dialogRef: MatDialogRef<CaptureImageComponent>,
    @Inject(PLATFORM_ID) private platformId: Object,
  ) {}

  ngOnInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      history.pushState(
        {},
        document.getElementsByTagName('title')[0].innerHTML,
        window.location.href
      );
    }
  }

  triggerSnapshot(): void {
    this.captureImageInterface.trigger.next();
   }

   handleImage(webcamImage: WebcamImage): void {
    this.captureImageInterface.webcamImage = webcamImage;
    this.dialogRef.close(this.captureImageInterface.webcamImage);
   }

   public get triggerObservable(): Observable<void> {
    return this.captureImageInterface.trigger.asObservable();
   }

   closeModal(): void {
    this.dialogRef.close();
  }

}
