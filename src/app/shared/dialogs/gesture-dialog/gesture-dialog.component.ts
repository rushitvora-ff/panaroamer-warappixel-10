import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Inject, AfterViewInit,PLATFORM_ID} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { isPlatformBrowser } from '@angular/common';
declare var Survey: any;
declare var $: any;

@Component({
  selector: 'app-gesture-dialog',
  templateUrl: './gesture-dialog.component.html',
  styleUrls: ['./gesture-dialog.component.scss']
})
export class GestureDialogComponent implements OnInit, AfterViewInit {
  selectedGesture;
  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    public dialogRef: MatDialogRef<GestureDialogComponent>,
    public sanitizer: DomSanitizer, private http: HttpClient,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.selectedGesture = data.selectedItem;
    }

  ngOnInit(): void {
    console.log(this.data);
    if (isPlatformBrowser(this.platformId)) {
      history.pushState(
        {},
        document.getElementsByTagName('title')[0].innerHTML,
        window.location.href
      );
    }
  }

  ngAfterViewInit(): void {
    if (this.selectedGesture.type === 'survey') {
      Survey
        .StylesManager
        .applyTheme('modern');

      Survey
        .Serializer
        .addProperty('page', {
          name: 'navigationTitle:string',
          isLocalizable: true
        });
      Survey
        .Serializer
        .addProperty('page', {
          name: 'navigationDescription:string',
          isLocalizable: true
        });
      const dummArr = this.selectedGesture.text.split('/');
      dummArr[dummArr.length - 1] = 'presentations/media/BEFORE_ALL.json';
      const url =  dummArr.join('/');
      this.http.get<any>(url).subscribe(json => {
        const survey = new Survey.Model(json);
        $('.survey').Survey({
          model: survey
        });
      });
    }
  }

  closeModal(): void {
    this.dialogRef.close();
  }

}
