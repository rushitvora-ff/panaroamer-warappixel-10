import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-schedule-ip',
  templateUrl: './schedule-ip.component.html',
  styleUrls: ['./schedule-ip.component.scss']
})
export class ScheduleIpComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ScheduleIpComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
  }

  rruledata(event): void {
    this.dialogRef.close(event);
  }

  cancel(event): void {
    if (event) {
      this.closeModal();
    }
  }

  closeModal(): void {
    this.dialogRef.close();
  }

}
