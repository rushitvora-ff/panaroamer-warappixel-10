import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ImageCroppedEvent, ImageTransform, Dimensions } from 'ngx-image-cropper';
import { EditImageInterface } from '../../../interfaces/sharedModule'
 
@Component({
  selector: 'app-edit-image-dialog',
  templateUrl: './edit-image-dialog.component.html',
  styleUrls: ['./edit-image-dialog.component.scss']
})
export class EditImageDialogComponent implements OnInit {
 editImageInterface: EditImageInterface = {
  imageBase64: '',
  croppedImage: '',
  canvasRotation: 0,
  rotation: 0,
  imageUrl: '',
  scale: 1,
  showCropper: false,
  containWithinAspectRatio: false,
  transform: {},
  filter: 'normal',
  filterOptions: ['Normal', '1977', 'Aden', 'Amaro', 'Ashby', 'Brannan', 'Brooklyn', 'Charmes', 'Clarendon', 'Crema', 'Dogpatch',
                    'Earlybird', 'Gingham', 'Ginza', 'Hefe' , 'Helena', 'Hudson', 'Inkwell', 'Kelvin', 'Juno', 'Lark', 'Lo-Fi',
                  'Ludwig', 'Maven', 'Mayfair', 'Moon', 'Nashville', 'Perpetua', 'Poprocket', 'Reyes', 'Rise', 'Sierra', 'Skyline',
                'Slumber', 'Stinson', 'Sutro', 'Toaster', 'Valencia', 'Vesper', 'Walden', 'Willow', 'X-Pro-ii']
 }

  constructor(
    public dialogRef: MatDialogRef<EditImageDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      if (data.selectedItem) {
        this.editImageInterface.filter = data.selectedItem;
      }
  }

  ngOnInit(): void {
    if(this.data.imageFile.substring(0, 4) == 'http'){
      this.editImageInterface.imageUrl = this.data.imageFile;
    }
    else{
        this.editImageInterface.imageBase64 = this.data.imageFile;
    }
  }

  closeModal(): void {
    this.dialogRef.close();
  }

  triggerEvent(): void {
    const obj = { data: this.editImageInterface.croppedImage, type: this.editImageInterface.filter };
    this.dialogRef.close(obj);
  }

  imageCropped(event: ImageCroppedEvent): void {
    this.editImageInterface.croppedImage = event.base64;
  }

  imageLoaded(): void {
      this.editImageInterface.showCropper = true;
  }

  cropperReady(sourceImageDimensions: Dimensions): void {
      // console.log('Cropper ready', sourceImageDimensions);
  }

  loadImageFailed(): void {
      console.log('Load failed');
  }

  rotateLeft(): void {
      this.editImageInterface.canvasRotation--;
      this.flipAfterRotate();
  }

  rotateRight(): void {
      this.editImageInterface.canvasRotation++;
      this.flipAfterRotate();
  }

  private flipAfterRotate(): void {
      const flippedH = this.editImageInterface.transform.flipH;
      const flippedV = this.editImageInterface.transform.flipV;
      this.editImageInterface.transform = {
          ...this.editImageInterface.transform,
          flipH: flippedV,
          flipV: flippedH
      };
  }

  flipHorizontal(): void {
      this.editImageInterface.transform = {
          ...this.editImageInterface.transform,
          flipH: !this.editImageInterface.transform.flipH
      };
  }

  flipVertical(): void {
      this.editImageInterface.transform = {
          ...this.editImageInterface.transform,
          flipV: !this.editImageInterface.transform.flipV
      };
  }

  resetImage(): void {
      this.editImageInterface.scale = 1;
      this.editImageInterface.rotation = 0;
      this.editImageInterface.canvasRotation = 0;
      this.editImageInterface.transform = {};
  }

  zoomOut(): void {
      this.editImageInterface.scale -= .1;
      this.editImageInterface.transform = {
          ...this.editImageInterface.transform,
          scale: this.editImageInterface.scale
      };
  }

  zoomIn(): void {
      this.editImageInterface.scale += .1;
      this.editImageInterface.transform = {
          ...this.editImageInterface.transform,
          scale: this.editImageInterface.scale
      };
  }

}
