import { Component, Inject, OnInit, Optional, PLATFORM_ID } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent {

  constructor(
    public dialogRef: MatDialogRef<ConfirmationComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    @Inject(PLATFORM_ID) private platformId: Object) {
      if (isPlatformBrowser(this.platformId)) {
        history.pushState(
          {},
          document.getElementsByTagName('title')[0].innerHTML,
          window.location.href
        );
      }
  }

  doAction(): void {
    this.dialogRef.close(true);
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

}
