import { Component, OnInit, Input, HostListener, OnDestroy, AfterViewInit } from '@angular/core';
import { Theme, Labels, ChatObject } from '@oneb-angular/angular-chat';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-chat-panel',
  templateUrl: './chat-panel.component.html',
  styleUrls: ['./chat-panel.component.scss']
})
export class ChatPanelComponent implements OnInit, OnDestroy, AfterViewInit {

  @Input() isExpanded;
  @Input() set mapData(value) {
    if (value) {
      this.MAP_DATA = value;
    }
  }

  landscape;
  isFullscrn: boolean;
  shouldShow = false;
  iconshow = false;
  panelHeight;
  MAP_DATA;

  get mapData() {
    return this.MAP_DATA;
  }

  ipUrl;

  labels: Labels = {
    placeholder: {
      search: 'search',
      message: 'Type here...'
    },
    media: {
      upload: 'Upload',
      record: 'Record',
      cancel: 'Cancel'
    },
    ip: {
      new: 'New IP',
      existing: 'Existing IP'
    },
    message: {
      actions: 'Actions',
      reaction: 'Reaction',
      startCoversation: 'Tap here to send message'
    }
  };

  chatTheme: Theme;

  @HostListener('window:orientationchange', ['$event'])
  onOrientationChange(event) {
    setTimeout(() => {
      if ((window.innerWidth > window.innerHeight) && window.innerHeight < 767) {
        this.landscape = true;
      } else {
        this.landscape = false;
      }
    }, 100);
  }

  constructor(private router: Router, public utilitySer: UtilityService, private route: ActivatedRoute) {
    // if (Object.keys(this.utilitySer.conversationObj).length) {
    // }
    if ((window.innerWidth > window.innerHeight) && window.innerHeight < 500) {
      this.landscape = true;
    }
  }

  ngOnInit() {

    if (Object.keys(this.utilitySer.conversationObj).length) {
      this.isExpanded = this.utilitySer.isExpanded;
      if (this.isExpanded) {
        this.iconshow = true;
        this.shouldShow = this.utilitySer.shouldShow;
        this.utilitySer.setChatPanel(this.shouldShow);
        this.utilitySer.conversationObj = {};
      }
    } else if (!this.utilitySer.changeRoute) {
      this.iconshow = this.utilitySer.isExpanded;
      this.shouldShow = this.utilitySer.shouldShow;
      this.utilitySer.setChatPanel(this.shouldShow);
    } else {
      setTimeout(() => {
        this.utilitySer.isExpanded = false;
        this.utilitySer.shouldShow = false;
      }, 100);
    }
    this.utilitySer.getChatPanel().subscribe(response => {
      this.isFullscrn = response;
      this.panelHeight = this.getHeight();
    });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      if (this.utilitySer.conIframeUrl) {
        this.ipUrl = this.utilitySer.conIframeUrl;
        this.utilitySer.conIframeUrl = undefined;
        setTimeout(() => {
          this.ipUrl = '';
        }, 5000);
      }
      this.utilitySer.conversationObj = {};
    }, 500);
  }

  showIcon() {
    this.iconshow = !this.iconshow;
    this.utilitySer.isExpanded = this.iconshow;
  }

  onSelectIp(event) {
    console.log(event);
    if (event.existingIp) {
      let queryparams: any;
      this.route.queryParams.subscribe(params => {
        if (params) {
          console.log(params);
          queryparams = params;
        }
      });
      this.utilitySer.conversationObj = {
        isChat: false,
        redirectTo: queryparams ? this.router.url.split('?')[0] : this.router.url,
        queryParams: queryparams
      };
      if (this.mapData) {
        this.utilitySer.conversationObj.mapData = this.mapData;
      }
      this.router.navigate(['/gallery'], { queryParams: { type: 'attachment' } });
    } else {
      this.router.navigate(['/builder'], { queryParams: { type: 'attachment' } });
    }
  }

  showAccordion(event) {
    this.shouldShow = !this.shouldShow;
    event.stopPropagation();
    this.utilitySer.setChatPanel(this.shouldShow);
    this.utilitySer.shouldShow = this.shouldShow;
  }

  onCloseAcc() {
    this.shouldShow = false;
    this.utilitySer.setChatPanel(this.shouldShow);
  }

  ngOnDestroy() {
    this.utilitySer.shouldShow = this.shouldShow;
  }

  isOver(): boolean {
    return window.matchMedia(`(max-width: 960px)`).matches;
  }

  getHeight() {
    if (this.isFullscrn) {
      console.log(this.isOver());
      return 'calc(100vh - 350px)';
    } else {
      return 'calc(45vh - ' + (this.isOver() ? '140px' : '200px') + ')';
    }
  }

}
