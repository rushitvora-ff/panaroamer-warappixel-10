import { Component, OnInit } from '@angular/core';
import { UtilityService } from 'src/app/services/utility.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-set-language',
  templateUrl: './set-language.component.html',
  styleUrls: ['./set-language.component.scss']
})
export class SetLanguageComponent implements OnInit {

  languageArr = [];
  selectedLanguage: any;

  constructor( public translate: TranslateService,
    public utility: UtilityService) {
    let getLang = localStorage.getItem('language');
    this.utility.getJSON('Panaroamer').subscribe(response => {
      this.languageArr = response.internationalization;
      if (this.languageArr) {
        for(let i = 0 ; i < this.languageArr.length; i++){
          if(getLang == this.languageArr[i].lang){
            this.selectedLanguage = this.languageArr[i];
            break;
          }
        }
      }
    });
   }

  ngOnInit(): void {
  }

  changeLanguage() {
    this.translate.use(this.selectedLanguage.lang);
    localStorage.setItem('language' , this.selectedLanguage.lang);
    console.log(this.selectedLanguage);
  }

}
