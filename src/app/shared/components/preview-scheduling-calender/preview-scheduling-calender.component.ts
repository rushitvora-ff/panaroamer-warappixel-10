import { Component, OnInit, Input } from '@angular/core';
import { CalendarView } from 'angular-calendar';
import { UtilityService } from 'src/app/services/utility.service';
import { PreviewScheduleInterface } from '../../../interfaces/sharedModule'

@Component({
  selector: 'app-preview-scheduling-calender',
  templateUrl: './preview-scheduling-calender.component.html',
  styleUrls: ['./preview-scheduling-calender.component.scss']
})
export class PreviewSchedulingCalenderComponent implements OnInit {
  
  @Input() title: string;

  previewScheduleInterface: PreviewScheduleInterface = {
    view: CalendarView.Month,
    viewDate: new Date(),
    events: [],
  };

  constructor(public utility: UtilityService) { }

  ngOnInit(): void {
  }

}
