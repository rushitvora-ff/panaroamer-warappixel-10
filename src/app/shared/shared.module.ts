import { NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { MenuItems } from './menu-items/menu-items';
import {
  AccordionAnchorDirective,
  AccordionLinkDirective,
  AccordionDirective
} from './accordion';
import { EditImageDialogComponent } from './dialogs/edit-image-dialog/edit-image-dialog.component';
import { DemoMaterialModule } from '../demo-material-module';
import { CommonModule } from '@angular/common';
import { ImageCropperModule } from 'ngx-image-cropper';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { PickerModule, PreviewComponent } from '@ctrl/ngx-emoji-mart';
import { EmojiModule } from '@ctrl/ngx-emoji-mart/ngx-emoji';
import { InteractionComponent } from './dialogs/interaction/interaction.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AssetsDialogComponent } from './dialogs/assets-dialog/assets-dialog.component';
import { IpInfoDialogComponent } from './dialogs/ip-info-dialog/ip-info-dialog.component';
import { StarRatingModule, StarRatingConfigService } from 'angular-star-rating';
import { ShareDialogComponent } from './dialogs/share-dialog/share-dialog.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { PaymentsDialogComponent } from './dialogs/payments-dialog/payments-dialog.component';
import { IpPreviewComponent } from './ip-preview/ip-preview.component';
import { GestureDialogComponent } from './dialogs/gesture-dialog/gesture-dialog.component';
import { ChatPanelComponent } from './chat-panel/chat-panel.component';
import { ChatPanelModule } from '@oneb-angular/angular-chat';
import { SetLanguageComponent } from './set-language/set-language.component';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { ConfirmationComponent } from './dialogs/confirmation/confirmation.component';
import { ScheduleIpComponent } from './dialogs/schedule-ip/schedule-ip.component';
import { AngularCalendarRecurrenceModule } from '@oneb-angular/angular-calendar-recurrence';
import { NgxMasonryModule } from 'ngx-masonry';
import { MapDialogComponent } from './dialogs/map-dialog/map-dialog.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { CalenderSchedulingComponent } from './dialogs/calender-scheduling/calender-scheduling.component';
import { PreviewSchedulingCalenderComponent } from './components/preview-scheduling-calender/preview-scheduling-calender.component';
import { AngularSchedulingModule } from '@oneb-angular/angular-scheduling';
import {WebcamModule} from 'ngx-webcam';
import { CaptureImageComponent } from './dialogs/capture-image/capture-image.component';
import { InteractionSelectionComponent } from './dialogs/interaction-selection/interaction-selection.component';
// import { ChatPanelModule } from 'chat-panel';

@NgModule({
  declarations: [
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
    EditImageDialogComponent,
    InteractionComponent,
    AssetsDialogComponent,
    IpInfoDialogComponent,
    ShareDialogComponent,
    IpPreviewComponent,
    ChatPanelComponent,
    PaymentsDialogComponent,
    GestureDialogComponent,
    SetLanguageComponent,
    ConfirmationComponent,
    ScheduleIpComponent,
    MapDialogComponent,
    CalenderSchedulingComponent,
    PreviewSchedulingCalenderComponent,
    CaptureImageComponent,
    InteractionSelectionComponent
  ],
  imports: [
    CommonModule,
    ChatPanelModule,
    ImageCropperModule,
    DemoMaterialModule,
    AngularCalendarRecurrenceModule,
    FormsModule,
    NgxDatatableModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    StarRatingModule,
    ReactiveFormsModule,
    PickerModule,
    WebcamModule,
    NgxMasonryModule,
    EmojiModule,
    TranslateModule,
    FlexLayoutModule,
    FormsModule,
    MatCardModule,
    MatSelectModule,
    ReactiveFormsModule,
    LeafletModule,
    AngularSchedulingModule
  ],
  exports: [
    EditImageDialogComponent,
    ChatPanelComponent,
    IpPreviewComponent,
    AccordionAnchorDirective,
    AccordionLinkDirective,
    TranslateModule,
    ChatPanelModule,
    PickerModule,
    FormsModule,
    AngularCalendarRecurrenceModule,
    EmojiModule,
    ScheduleIpComponent,
    ReactiveFormsModule,
    AccordionDirective,
    SetLanguageComponent,
    PreviewSchedulingCalenderComponent,
    InteractionSelectionComponent
  ],
  providers: [MenuItems, StarRatingConfigService],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class SharedModule {}
