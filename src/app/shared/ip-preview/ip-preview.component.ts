import {
  Component, OnInit, OnDestroy,
  ChangeDetectorRef, Input, AfterViewInit, ViewChild, ElementRef
} from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import { forkJoin } from 'rxjs';
import { UtilityService } from 'src/app/services/utility.service';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { GestureDialogComponent } from '../dialogs/gesture-dialog/gesture-dialog.component';
import { Ip } from '@oneb/ip-api-clients-models';
import { PageTypeEnum_Image, PageTypeEnum_Survey, PageTypeEnum_Video } from '@oneb/ip-api-clients-models';
import { ActivationModeEnum_All } from '@oneb/interactions-api-clients-models';
import { GestureTypeEnum_LongTap, GestureTypeEnum_Rub, GestureTypeEnum_Tap } from '@oneb/interactions-api-clients-models';
import { EffectTypeEnum_Audio, EffectTypeEnum_Image, EffectTypeEnum_Survey, EffectTypeEnum_Text, EffectTypeEnum_Video } from '@oneb/interactions-api-clients-models';
import { CallToActionTypeEnum_Buy, CallToActionTypeEnum_Call, CallToActionTypeEnum_Chat, CallToActionTypeEnum_Visit } from '@oneb/interactions-api-clients-models';
import { IpPreviewInterface } from 'src/app/interfaces/sharedModule';
declare var Reveal: any;
declare var Survey: any;
declare var $: any;
declare var ZingTouch: any;

@Component({
  selector: 'app-ip-preview',
  templateUrl: './ip-preview.component.html',
  styleUrls: ['./ip-preview.component.css'],
})
export class IpPreviewComponent implements OnInit, OnDestroy, AfterViewInit {
  ipPreview: IpPreviewInterface = {
    artifactsArr: [],
    isOpen: true,
    isLoadScript: false,
    idx: 0,
    tapregion: [],
    longTapregion: [],
    panRegion: [],
    regionArr: [],
    mode: 'Single',
    selectedIndex: 0,
  };

  dialogRef: MatDialogRef<GestureDialogComponent>;

  @Input() Ip: Ip;
  @Input() artifacts;
  // artifactsArr = [];
  // content;
  // timeout;
  // isOpen = true;
  // progressTimeOut;
  // navTimeout;
  // audioTimeout;
  // surveys = [];
  // reveal;
  // isLoadScript = false;
  // selectedGesture: any;
  // idx = 0;
  // tapregion: any = [];
  // longTapregion: any = [];
  // panRegion: any = [];
  // gestureTimeout;
  // regionArr = [];
  // mode = 'Single';
  // selectedIndex = 0;
  // masonryOptions = {
  //   fitWidth: true
  // };

  constructor(
    private changedet: ChangeDetectorRef,
    public sanitizer: DomSanitizer,
    public dialog: MatDialog,
    public utility: UtilityService,
    private http: HttpClient) {
    this.utility.setLoader(true);
  }

  ngOnInit(): void {
    this.loadScripts();
    const ips = [];
    ips.push( {
      id: this.Ip.frontCoverPages[0].ipPageId,
      url:   this.Ip.frontCoverPages[0].page.dataUrl,
      type: 'image'
    });

    for (const ipPage of this.Ip.mainPages) {
      const ipData: any = {
        id: ipPage.ipPageId,
        url: ipPage.page.dataUrl,
        effects: []
      };
      if (ipPage.page.soundTrackUrl) {
        ipData.music = ipPage.page.soundTrackUrl;
      }
      if (ipPage.page.type === PageTypeEnum_Image) {
        ipData.type = 'image';
      } else if (ipPage.page.type === PageTypeEnum_Video) {
        ipData.type = 'video';
      } else if (ipPage.page.type === PageTypeEnum_Survey) {
        ipData.type = 'survey';
        const dummArr = ipData.url.split('/');
        dummArr[dummArr.length - 1] = 'presentations/media/BEFORE_ALL.json';
        ipData.surveyUrl = dummArr.join('/');
      }
      if (ipPage.page.interactions && ipPage.page.interactions['activationMode'] === ActivationModeEnum_All) {
        ipData.mode = 'All';
      } else {
        ipData.mode = 'Single';
      }
      if (ipPage.page.interactions && ipPage.page.interactions['effects'] && ipPage.page.interactions['effects'].length) {
        for (const effect of ipPage.page.interactions['effects']) {
          const activation = effect.activations[0].activation;
          const eff: any = {
            x: activation.x,
            y: activation.y,
            time: activation.timeInMs,
            emoji: activation.marker.emoji.name,
            emojiObj: activation.marker.emoji.data
          };
          if (activation.gesture.type === GestureTypeEnum_Tap) {
            eff.gesture = 'tap';
          } else if (activation.gesture.type === GestureTypeEnum_Rub) {
            eff.gesture = 'rub';
          } else if (activation.gesture.type === GestureTypeEnum_LongTap) {
            eff.gesture = 'long-tap';
          }
          if (effect.dataUrl) {
            eff.text = effect.dataUrl;
          }
          if (effect.callToActions && effect.callToActions.length) {
            const userActions = [];
            for (const action of effect.callToActions) {
              if (action.type === CallToActionTypeEnum_Buy) {
                userActions.push({
                  action: 'buy', value: 'https://www.google.com'
                });
              } else if (action.type === CallToActionTypeEnum_Visit) {
                userActions.push({
                  action: 'visit', value: 'https://www.google.com'
                });
              } else if (action.type === CallToActionTypeEnum_Call) {
                userActions.push({
                  action: 'call', value: action.data
                });
              } else if (action.type === CallToActionTypeEnum_Chat) {
                userActions.push({
                  action: 'text', value: action.data
                });
              }
            }
            eff.userActions = userActions;
          }
          if (effect.type === EffectTypeEnum_Text) {
            eff.type = 'text';
          } else if (effect.type === EffectTypeEnum_Audio) {
            eff.type = 'audio';
          } else if (effect.type === EffectTypeEnum_Image) {
            eff.type = 'image';
          } else if (effect.type === EffectTypeEnum_Video) {
            eff.type = 'video';
          } else if (effect.type === EffectTypeEnum_Survey) {
            eff.type = 'survey';
          }
          ipData.effects.push(eff);
        }
      }
      ips.push(ipData);
    }
    console.log(ips);
    this.artifacts = ips;
    if (this.artifacts) {
      this.ipPreview.artifactsArr = JSON.parse(JSON.stringify(this.artifacts));
      for (let i = 0; i < this.ipPreview.artifactsArr.length; i++) {
        if (this.ipPreview.artifactsArr[i].surveyUrl && this.ipPreview.artifactsArr[i].type !== 'survey') {
          const obj = JSON.parse(JSON.stringify(this.ipPreview.artifactsArr[i]));
          obj.type = 'survey';
          this.ipPreview.artifactsArr.splice(i, 0, obj);
          i++;
        }
      }
      this.handleArtifacts();
      this.initializeReveal();
    }
  }

  handleArtifacts(): void {
    this.ipPreview.content = {
      pages: [],
      streetAddress: '157 Technology Dr W, Irvine, CA 92618',
      name: 'previewPresent',
      lat: 33.663717,
      long: -117.7469075,
      category: 'Electronics'
    };
    let j = 0;


    let prevRow;
    let prevCol;

    const dummyPage = { rows: [], music: '' };
    const row = { cols: [] };

    dummyPage.rows.push(row);

    const pages = dummyPage;

    if (pages.rows[0].cols.length) {
      this.ipPreview.content.pages.push(pages);
    }


    if (this.artifacts.length === 1) {
      prevRow = 1;
      prevCol = 1;
    } else if (this.artifacts.length === 2) {
      prevRow = 1;
      prevCol = 2;
    } else if (this.artifacts.length === 3) {
      prevRow = 2;
      prevCol = 2;
    } else if (this.artifacts.length === 4) {
      prevRow = 2;
      prevCol = 2;
    } else if (this.artifacts.length === 5) {
      prevRow = 3;
      prevCol = 2;
    } else if (this.artifacts.length === 6) {
      prevRow = 3;
      prevCol = 2;
    } else if (this.artifacts.length === 7) {
      prevRow = 3;
      prevCol = 3;
    } else if (this.artifacts.length === 8) {
      prevRow = 3;
      prevCol = 3;
    } else if (this.artifacts.length === 9) {
      prevRow = 3;
      prevCol = 3;
    } else if (this.artifacts.length === 10) {
      prevRow = 4;
      prevCol = 3;
    } else if (this.artifacts.length === 11) {
      prevRow = 4;
      prevCol = 3;
    } else if (this.artifacts.length === 12) {
      prevRow = 4;
      prevCol = 3;
    } else if (this.artifacts.length === 13) {
      prevRow = 4;
      prevCol = 4;
    } else if (this.artifacts.length === 14) {
      prevRow = 4;
      prevCol = 4;
    } else if (this.artifacts.length === 15) {
      prevRow = 4;
      prevCol = 4;
    } else if (this.artifacts.length === 16) {
      prevRow = 4;
      prevCol = 4;
    } else if (this.artifacts.length === 17) {
      prevRow = 5;
      prevCol = 4;
    }
    else if (this.artifacts.length === 18) {
      prevRow = 5;
      prevCol = 4;
    }
    else if (this.artifacts.length === 19) {
      prevRow = 5;
      prevCol = 4;
    }
    else if (this.artifacts.length === 20) {
      prevRow = 5;
      prevCol = 4;
    }
    else if (this.artifacts.length === 21) {
      prevRow = 5;
      prevCol = 5;
    }
    else if (this.artifacts.length === 22) {
      prevRow = 5;
      prevCol = 5;
    }
    else if (this.artifacts.length === 23) {
      prevRow = 5;
      prevCol = 5;
    }
    else if (this.artifacts.length === 24) {
      prevRow = 5;
      prevCol = 5;
    }
    else if (this.artifacts.length === 25) {
      prevRow = 5;
      prevCol = 5;
    }
    else if (this.artifacts.length === 26) {
      prevRow = 6;
      prevCol = 5;
    }
    else if (this.artifacts.length === 27) {
      prevRow = 6;
      prevCol = 5;
    }
    else if (this.artifacts.length === 28) {
      prevRow = 6;
      prevCol = 5;
    }
    else if (this.artifacts.length === 29) {
      prevRow = 6;
      prevCol = 5;
    }
    else if (this.artifacts.length === 30) {
      prevRow = 6;
      prevCol = 5;
    }
    else if (this.artifacts.length === 31) {
      prevRow = 6;
      prevCol = 6;
    }
    else if (this.artifacts.length === 32) {
      prevRow = 6;
      prevCol = 6;
    }
    else if (this.artifacts.length === 33) {
      prevRow = 6;
      prevCol = 6;
    }
    else if (this.artifacts.length === 34) {
      prevRow = 6;
      prevCol = 6;
    }
    else if (this.artifacts.length === 35) {
      prevRow = 6;
      prevCol = 6;
    }
    else if (this.artifacts.length === 36) {
      prevRow = 6;
      prevCol = 6;
    }

    const page = { rows: [], music: '' };

    for (let l = 0; l < prevRow; l++) {
      const rowNew = { cols: [] };
      for (let k = 0; k < prevCol; k++) {
        if (j === this.artifacts.length) {
          break;
        }
        if (this.artifacts[j].type === 'image') {
          rowNew.cols.push({
            type: 'image',
            url: this.artifacts[j].url,
            filterType: this.artifacts[j].filterType
          });
        } else if (this.artifacts[j].type === 'video') {
          rowNew.cols.push({
            type: 'video',
            url: this.getSafeUrl(this.artifacts[j].url),
            normalUrl: this.artifacts[j].url
          });
        } else if (this.artifacts[j].type === 'survey') {
          rowNew.cols.push({
            type: 'image',
            url: 'assets/images/surveyPlaceholderHeader.png'
          });
        }

        j++;
      }
      page.rows.push(rowNew);
    }
    if (page.rows[0].cols.length) {
      this.ipPreview.content.pages.push(page);
    }


    j = 0;
    while (j < this.artifacts.length) {
      let rows;
      let cols;
      do {
        rows = 1;
        cols = 1;
      }
      while ((rows * cols) + j > this.artifacts.length);

      let pageNew: any = { rows: [], music: null };
      for (let l = 0; l < rows; l++) {
        const rowNew = { cols: [] };
        for (let k = 0; k < cols; k++) {
          if (this.artifacts[j].type === 'image') {
            if (this.artifacts[j].music) {
              pageNew.music = this.sanitizer.bypassSecurityTrustUrl(this.artifacts[j].music);
              pageNew.normalUrl = this.artifacts[j].music;
            }
            rowNew.cols.push({
              type: 'image',
              url: this.artifacts[j].url,
              mode: this.artifacts[j].mode,
              effects: this.artifacts[j].effects,
              filterType: this.artifacts[j].filterType
            });
          } else if (this.artifacts[j].type === 'video') {
            if (this.artifacts[j].music) {
              pageNew.music = this.sanitizer.bypassSecurityTrustUrl(this.artifacts[j].music);
              pageNew.normalUrl = this.artifacts[j].music;
            }
            rowNew.cols.push({
              type: 'video',
              url: this.getSafeUrl(this.artifacts[j].url),
              mode: this.artifacts[j].mode,
              effects: this.artifacts[j].effects,
              normalUrl: this.artifacts[j].url
            });
          }
        }

        pageNew.rows.push(rowNew);

      }
      if (pageNew.rows[0].cols.length) {
        this.ipPreview.content.pages.push(pageNew);
      }
      if (this.artifacts[j].surveyUrl) {
        pageNew = { rows: [], music: null };
        for (let l = 0; l < rows; l++) {
          const rowNew = { cols: [] };
          for (let k = 0; k < cols; k++) {
            if (this.artifacts[j].music) {
              pageNew.music = this.sanitizer.bypassSecurityTrustUrl(this.artifacts[j].music);
              pageNew.normalUrl = this.artifacts[j].music;
            }
            rowNew.cols.push({
              type: 'survey',
              url: this.artifacts[j].surveyUrl,
              mode: this.artifacts[j].mode,
              effects: this.artifacts[j].effects
            });
          }
          pageNew.rows.push(rowNew);
        }

        this.ipPreview.content.pages.push(pageNew);
      }

      j++;
    }
    this.traverseJson(this.ipPreview.content.pages, null);
    this.utility.content = this.ipPreview.content.pages;
    console.log(this.ipPreview.content.pages);
  }

  traverseJson(pages, urlFolder): void {
    const surveyPromise = [];
    for (let i = 0; i < pages.length; i++) {
      if (pages[i].backgroundImageUrl) {
        if (urlFolder) {
          pages[i].backgroundImageUrl = './presentations/' + urlFolder + '/' + pages[i].backgroundImageUrl;
        } else {
          pages[i].backgroundImageUrl = './presentations/' + pages[i].backgroundImageUrl;
        }
      }

      for (const item of pages[i].rows) {
        item.style = { height: (100 / pages[i].rows.length) + '%' };
        for (const col of item.cols) {
          col.style = { width: (100 / item.cols.length) + '%' };
          if ((col.type === 'image' || col.type === 'video') &&
            item.cols.length === 1) {
            col.class = col.class + ' text-center';
          }

          if (col.type === 'survey') {

            const surveyJson = col.url;
            col.name = 'Survey' + i;

            surveyPromise.push(this.http.get<any>(surveyJson));
            if (pages[i].rows.length >= 1) {
              col.class = col.class + ' full-widheight-survey overflow-auto-y';
            } else {
              if (pages[i].rows.length === 1 && col.length > 1) {
                col.class = col.class + ' full-widheight-survey overflow-auto-y';
              }
            }
          }
        }
      }
    }


    forkJoin(surveyPromise).subscribe((response) => {
      let j = 0;
      for (const page of pages) {
        if (page.rows[0].cols[0].type === 'survey') {
          page.rows[0].cols[0].surveyData = response[j];
          j++;
        }

      }
    });
  }

  focusControl(): void {
    if (this.ipPreview.audioTimeout) {
      clearTimeout(this.ipPreview.audioTimeout);
    }
  }

  resetTimer(): void {
    this.ipPreview.audioTimeout = setTimeout(() => {
      const audio = document.getElementsByClassName('reveal-audio');
      if (audio.length) {
        for (let i = 0; i < audio.length; i++) {
          if (audio[i].hasAttribute('controls')) {
            audio[i].removeAttribute('controls');
          }
        }
      }
    }, 5000);
  }

  sendDataToServer(survey): void {

  }

  initializeReveal(): void {
    setTimeout(() => {
      Reveal.initialize({
        loop: false,
        width: '100%',
        height: '100%',
        margin: 0,
        minScale: 0.2,
        maxScale: 1.5,
        touch: true
      });
      this.onClick();
      Reveal.addEventListener('slidechanged', (event) => {
        this.ipPreview.selectedIndex = event.indexh;
        this.ipPreview.isOpen = false;
        clearTimeout(this.ipPreview.gestureTimeout);
        this.ipPreview.idx = 0;
        const gesElement = document.getElementsByClassName('region-box');
        for (let i = 0; i < gesElement.length; i++) {
          this.ipPreview.tapregion.forEach(elem => {
            elem.unbind(gesElement[i]);
          });
          this.ipPreview.longTapregion.forEach(elem => {
            elem.unbind(gesElement[i]);
          });
          this.ipPreview.panRegion.forEach(elem => {
            elem.unbind(gesElement[i]);
          });
        }
        if (
            this.ipPreview.artifactsArr && this.ipPreview.artifactsArr.length && event.indexh > 0 &&
            this.ipPreview.artifactsArr[event.indexh].effects && this.ipPreview.artifactsArr[event.indexh].effects.length
          ){
          this.ipPreview.regionArr = this.ipPreview.artifactsArr[event.indexh].effects;
          this.ipPreview.mode = this.ipPreview.artifactsArr[event.indexh].mode;
        } else {
          this.ipPreview.selectedGesture = undefined;
          this.ipPreview.regionArr = [];
        }
        if (this.ipPreview.regionArr && this.ipPreview.regionArr.length) {
          this.ipPreview.isOpen = true;
          setTimeout(() => {
            this.applyGesture();
          }, 300);
        }
        if (this.utility.content[event.indexh + 1].rows[0].cols[0].type === 'survey') {
          Survey
            .StylesManager
            .applyTheme('modern');

          Survey
            .Serializer
            .addProperty('page', {
              name: 'navigationTitle:string',
              isLocalizable: true
            });
          Survey
            .Serializer
            .addProperty('page', {
              name: 'navigationDescription:string',
              isLocalizable: true
            });
          const survey = new Survey.Model(this.utility.content[event.indexh + 1].rows[0].cols[0].surveyData);

          $('.Survey' + (event.indexh + 1)).Survey({
            model: survey
          });
        }
        let allImgs;
        clearTimeout(this.ipPreview.timeout);
        clearTimeout(this.ipPreview.progressTimeOut);
        clearTimeout(this.ipPreview.navTimeout);
        this.onClick();
        const element = document.getElementsByClassName('progress') as HTMLCollectionOf<HTMLElement>;
        element[0].style.transition = 'opacity 0.5s linear';
        element[0].style.opacity = '1';
        allImgs = [].slice.call(document.getElementsByClassName('enabled'));
        allImgs.map((eachArrow) => {
          eachArrow.style.opacity = 1;
          eachArrow.style.cursor = 'pointer';
        });
        this.ipPreview.progressTimeOut = setTimeout(() => {
          element[0].style.opacity = '0';
        }, 2 * 1000);
        this.ipPreview.navTimeout = setTimeout(() => {
          allImgs = [].slice.call(document.getElementsByClassName('enabled'));
          allImgs.map((eachArrow) => {
            eachArrow.style.opacity = 0;
            eachArrow.style.cursor = 'auto';
            eachArrow.style.transition = 'opacity 0.5s linear';
          });
        }, 5000);
      });
      this.utility.setLoader(false);
      this.ipPreview.reveal = true;
    }, 4000);
  }

  onClick(): void {
    clearTimeout(this.ipPreview.timeout);
    let allImgs = [].slice.call(document.getElementsByClassName('enabled'));
    allImgs.map((eachArrow) => {
      eachArrow.style.opacity = 1;
      eachArrow.style.cursor = 'pointer';
    });
    const audio = document.getElementsByClassName('reveal-audio');
    if (audio.length) {
      for (let i = 0; i < audio.length; i++) {
        audio[i].setAttribute('controls', 'controls');
      }
    }
    this.ipPreview.timeout = setTimeout(() => {
      allImgs = [].slice.call(document.getElementsByClassName('enabled'));
      allImgs.map((eachArrow) => {
        eachArrow.style.opacity = 0;
        eachArrow.style.cursor = 'auto';
        eachArrow.style.transition = 'opacity 0.5s linear';
      });
    }, 5000);
    this.focusControl();
    this.resetTimer();
  }

  getSafeUrl(url): SafeUrl {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

  counter(i: number): any[] {
    return new Array(i);
  }

  navigateSlide(idx): void {
    // this.selectedIndex = idx;
    Reveal.slide(idx);
  }

  loadScripts(): void {
    const dynamicScripts = [
      'https://unpkg.com/jquery@3.5.1/dist/jquery.min.js',
      '../../../assets/js/reveal.js',
      'https://surveyjs.azureedge.net/1.8.4/survey.jquery.js'
    ];
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.id = 'reveal-js' + i;
      document.getElementsByTagName('head')[0].appendChild(node);
    }
    this.loadStyle();
  }

  loadStyle(): void {
    const head = document.getElementsByTagName('head')[0];


    const style = document.createElement('link');
    style.id = 'client-theme';
    style.rel = 'stylesheet';
    style.href = `https://surveyjs.azureedge.net/1.8.4/modern.css`;

    head.appendChild(style);
    this.changedet.detectChanges();
    this.ipPreview.isLoadScript = true;

  }

  ngAfterViewInit(): void {
    // setTimeout(() => {
    //   this.applyGesture();
    // }, 4000);
  }

  applyGesture(): void {
    if (this.ipPreview.mode === 'Single') {
      this.ipPreview.idx++;
      if (this.ipPreview.idx <= this.ipPreview.regionArr.length) {
        this.ipPreview.selectedGesture = this.ipPreview.regionArr[this.ipPreview.idx - 1];
        if (this.ipPreview.selectedGesture.gesture === 'tap') {
          const tapElement = document.getElementsByClassName('region-box');
          for (let i = 0; i < tapElement.length; i++) {
            const tapregion = new ZingTouch.Region(tapElement[i]);
            tapregion.bind(tapElement[i], 'tap', (e) => {
              if (!this.dialogRef) {
                this.openDialog();
              }
            });
            this.ipPreview.tapregion.push(tapregion);
          }
          this.ipPreview.gestureTimeout = setTimeout(() => {
            for (let i = 0; i < tapElement.length; i++) {
              this.ipPreview.tapregion.forEach(element => {
                element.unbind(tapElement[i], 'tap');
              });
            }
            if (this.dialogRef) {
              this.dialogRef.close();
              this.dialogRef = undefined;
            }
            setTimeout(() => {
              this.applyGesture();
            }, 400);
          }, this.ipPreview.selectedGesture.time * 1000);
        } else if (this.ipPreview.selectedGesture.gesture === 'long-tap') {
          const longTapElement = document.getElementsByClassName('region-box');
          for (let i = 0; i < longTapElement.length; i++) {
            const longTapregion = new ZingTouch.Region(longTapElement[i]);
            const longtap = new ZingTouch.Tap({
              maxDelay: 4000,
              numInputs: 1,
              tolerance: 1000
            });
            longTapregion.bind(longTapElement[i], longtap, (e) => {
              if (e && e.detail.interval > 300) {
                if (!this.dialogRef) {
                  this.openDialog();
                }
              }
            });
            this.ipPreview.longTapregion.push(longTapregion);
          }
          this.ipPreview.gestureTimeout = setTimeout(() => {
            for (let i = 0; i < longTapElement.length; i++) {
              this.ipPreview.longTapregion.forEach(element => {
                element.unbind(longTapElement[i]);
              });
            }
            if (this.dialogRef) {
              this.dialogRef.close();
              this.dialogRef = undefined;
            }
            setTimeout(() => {
              this.applyGesture();
            }, 400);
          }, this.ipPreview.selectedGesture.time * 1000);
        } else {
          const panElement = document.getElementsByClassName('region-box');
          for (let i = 0; i < panElement.length; i++) {
            const panRegion = new ZingTouch.Region(panElement[i]);
            const pan = new ZingTouch.Pan();
            panRegion.bind(panElement[i], pan, (e) => {
              if (!this.dialogRef) {
                this.openDialog();
              }
            });
            this.ipPreview.panRegion.push(panRegion);
          }
          this.ipPreview.gestureTimeout = setTimeout(() => {
            for (let i = 0; i < panElement.length; i++) {
              this.ipPreview.panRegion.forEach(element => {
                element.unbind(panElement[i]);
              });
            }
            if (this.dialogRef) {
              this.dialogRef.close();
              this.dialogRef = undefined;
            }
            setTimeout(() => {
              this.applyGesture();
            }, 400);
          }, this.ipPreview.selectedGesture.time * 1000);
        }
      } else {
        if (this.dialogRef) {
          this.dialogRef.close();
          this.dialogRef = undefined;
        }
        this.ipPreview.idx = 0;
        setTimeout(() => {
          this.applyGesture();
        }, 400);
      }
    } else if (this.ipPreview.mode === 'All') {
      for (let i = 0; i < this.ipPreview.regionArr.length; i++) {
        if (this.ipPreview.regionArr[i].gesture === 'tap') {
          const tapElement = document.getElementsByClassName('region-box' + i);
          for (let j = 0; j < tapElement.length; j++) {
            const tapregion = new ZingTouch.Region(tapElement[j]);
            tapregion.bind(tapElement[j], 'tap', (e) => {
              if (e) {
                let idx;
                const str = e.target.className.split(' ');
                for (const cls of str) {
                  if (cls.includes('region-box')) {
                    idx = JSON.parse(cls.split('box')[1]);
                  }
                }
                this.ipPreview.selectedGesture = this.ipPreview.regionArr[idx];
                if (!this.dialogRef) {
                  this.openDialog();
                }
              }
            });
            this.ipPreview.tapregion.push(tapregion);
          }
        } else if (this.ipPreview.regionArr[i].gesture === 'long-tap') {
          const longTapElement = document.getElementsByClassName('region-box' + i);
          for (let j = 0; j < longTapElement.length; j++) {
            const longTapregion = new ZingTouch.Region(longTapElement[j]);
            const longtap = new ZingTouch.Tap({
              maxDelay: 4000,
              numInputs: 1,
              tolerance: 1000
            });
            longTapregion.bind(longTapElement[j], longtap, (e) => {
              if (e && e.detail.interval > 300) {
                let idx;
                const str = e.target.className.split(' ');
                for (const cls of str) {
                  if (cls.includes('region-box')) {
                    idx = JSON.parse(cls.split('box')[1]);
                  }
                }
                this.ipPreview.selectedGesture = this.ipPreview.regionArr[idx];
                if (!this.dialogRef) {
                  this.openDialog();
                }
              }
            });
            this.ipPreview.longTapregion.push(longTapregion);
          }
        } else {
          const panElement = document.getElementsByClassName('region-box' + i);
          for (let j = 0; j < panElement.length; j++) {
            const panRegion = new ZingTouch.Region(panElement[j]);
            const pan = new ZingTouch.Pan();
            panRegion.bind(panElement[j], pan, (e) => {
              let idx;
              const str = e.target.className.split(' ');
              for (const cls of str) {
                if (cls.includes('region-box')) {
                  idx = JSON.parse(cls.split('box')[1]);
                }
              }
              this.ipPreview.selectedGesture = this.ipPreview.regionArr[idx];
              if (!this.dialogRef) {
                this.openDialog();
              }
            });
            this.ipPreview.panRegion.push(panRegion);
          }
        }
      }
    }
  }

  openDialog(): void {
    let className = 'gesture-dialog';
    className += this.ipPreview.selectedGesture.type === 'survey' ? '-survey' : '';
    this.dialogRef = this.dialog.open(GestureDialogComponent, {
      panelClass: this.utility.dark ? 'dark' : className,
      maxHeight: '90vh',
      disableClose: true,
      data: {
        selectedItem: this.ipPreview.selectedGesture
      }
    });
    this.dialogRef.afterClosed().subscribe(() => this.dialogRef = undefined);
  }

  removeElement(id): any {
    const elem = document.getElementById(id);
    return elem ? elem.parentNode.removeChild(elem) : null;
  }

  ngOnDestroy(): void {
    clearTimeout(this.ipPreview.timeout);
    clearTimeout(this.ipPreview.progressTimeOut);
    clearTimeout(this.ipPreview.navTimeout);
    clearTimeout(this.ipPreview.audioTimeout);
    this.removeElement('reveal-js0');
    this.removeElement('reveal-js1');
    this.removeElement('reveal-js2');
    this.removeElement('client-theme');
    this.removeElement('surveyjs-styles');
    this.removeElement('survey-content-jquery');
    this.utility.setLoader(false);
  }


}
