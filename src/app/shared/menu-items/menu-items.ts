import { Injectable } from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}
export interface Saperator {
  name: string;
  type?: string;
}
export interface SubChildren {
  state: string;
  name: string;
  type?: string;
}
export interface ChildrenItems {
  state: string;
  name: string;
  type?: string;
  child?: SubChildren[];
}

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  saperator?: Saperator[];
  children?: ChildrenItems[];
}

const MENUITEMS = [
  {
    state: 'builder',
    name: 'Designer',
    type: 'link',
    icon: 'content_copy'
  },
  {
    state: 'gallery',
    name: 'Gallery',
    type: 'link',
    icon: 'collections'
  },
  {
    state: 'qr-scanner',
    name: 'QR',
    type: 'link',
    icon: 'qr_code_scanner'
  },
  {
    state: 'map',
    name: 'Map',
    type: 'link',
    icon: 'map'
  },
  {
    state: 'chat',
    name: 'Chat',
    type: 'link',
    icon: 'message'
  },
  {
    state: 'contacts',
    name: 'Contacts',
    type: 'link',
    icon: 'perm_contact_calendar'
  },
  {
    state: 'ip-schedule',
    name: 'IpSchedule',
    type: 'link',
    icon: 'schedule'
  }
];

@Injectable()
export class MenuItems {
  getMenuitem(): Menu[] {
    return MENUITEMS;
  }
}
