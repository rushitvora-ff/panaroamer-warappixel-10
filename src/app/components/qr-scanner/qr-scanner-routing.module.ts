import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QrScannerComponent } from './qr-scanner.component';


const routes: Routes = [
  {
    path: '',
    component: QrScannerComponent
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QrScannerRoutingModule { }
