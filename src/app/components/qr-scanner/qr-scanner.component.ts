import { Component, OnInit, ViewChild } from '@angular/core';
import { ZXingScannerComponent } from '@zxing/ngx-scanner';
import { UtilityService } from 'src/app/services/utility.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-qr-scanner',
  templateUrl: './qr-scanner.component.html',
  styleUrls: ['./qr-scanner.component.scss']
})
export class QrScannerComponent implements OnInit {
  // @ViewChild('scanner', {static: true}) scanner: ZXingScannerComponent;

  constructor(
    private router: Router,
    private utilitySer: UtilityService) { }

  ngOnInit(): void {
  }

  scanSuccessHandler(event: any): any {
    for (const item of this.utilitySer.interactionsArray) {
      if (item.name.toLowerCase() === event.toLowerCase()) {
        this.router.navigate(['/home']);
      }
    }
  }

}
