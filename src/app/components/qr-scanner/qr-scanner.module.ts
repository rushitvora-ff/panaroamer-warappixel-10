import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QrScannerRoutingModule } from './qr-scanner-routing.module';
import { DemoMaterialModule } from '../../demo-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SharedModule } from '../../shared/shared.module';
import { QrScannerComponent } from './qr-scanner.component';
import { ZXingScannerModule } from '@zxing/ngx-scanner';

@NgModule({
  declarations: [QrScannerComponent],
  imports: [
    CommonModule,
    DemoMaterialModule,
    FlexLayoutModule,
    ZXingScannerModule,
    SharedModule,
    QrScannerRoutingModule
  ]
})
export class QrScannerModule { }
