import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss']
})
export class GroupsComponent implements OnInit {
  labels;

  constructor(public utility: UtilityService, private translateSevice: TranslateService) {
    this.loadTranslation();
    this.translateSevice.onLangChange.subscribe(() => {
      this.loadTranslation();
    });
  }

  ngOnInit(): void {
  }

  loadTranslation(): void {
    this.translateSevice.get(['addGroup.NamePlaceHolder', 'addGroup.DescriptionPlaceHolder', 'addGroup.SaveButton',
    'addGroup.UpdateButton', 'contacts.SearchPlaceHolder', 'addGroup.add', 'addGroup.addMembers',
    'contactDetails.AddButton', 'contacts.CancelContactButton', 'addGroup.update']).subscribe((res: any[]) => {
      res = Object.keys(res).map((key) => res[key]);
      this.labels = {
        group: {
          create: {
            name: res[0],
            description: res[1],
            save: res[2],
            update: res[3],
            add: res[5],
            cancel: res[8]
          },
          list: {
            search: res[4],
            add: res[5],
            update: res[9],
            addMembers: res[6],
            addToGroup: res[7]
          }
        }
      };
    });
  }

}
