import { Platform } from '@angular/cdk/platform';
import { AfterViewInit, Component, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { InfiniteGrid } from 'src/app/interfaces/componets';
import { AssetsInterface } from 'src/app/interfaces/sharedModule';
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.scss']
})
export class SurveyComponent implements OnInit, AfterViewInit {
  survey: AssetsInterface = {
    items: [
      {
        id: 1, name: 'Survey-XXX', description: 'This is very good survey',
        isActive: false, url: 'survey-xyz', link: 'https://ippub.panaroamer.com/animals/index.html'
      },
      {
        id: 2, name: 'Survey-YYY', description: 'This is very good survey',
        isActive: false, url: 'survey-abc', link: 'https://ippub.panaroamer.com/birds/index.html'
      },
      {
        id: 3, name: 'Survey-ZZZ', description: 'This is very good survey',
        isActive: false, url: 'survey-def', link: 'https://ippub.panaroamer.com/camera/index.html'
      },
      {
        id: 1, name: 'Survey-XXX', description: 'This is very good survey',
        isActive: false, url: 'survey-xyz', link: 'https://ippub.panaroamer.com/animals/index.html'
      },
      {
        id: 2, name: 'Survey-YYY', description: 'This is very good survey',
        isActive: false, url: 'survey-abc', link: 'https://ippub.panaroamer.com/birds/index.html'
      },
      {
        id: 3, name: 'Survey-ZZZ', description: 'This is very good survey',
        isActive: false, url: 'survey-def', link: 'https://ippub.panaroamer.com/camera/index.html'
      }
    ],
    selected: [],
    activeId: '',
    tabs: ['Category 1', 'Category 2', 'Category 3'],
    openPanel: true
  };
  infiniteGrid: InfiniteGrid = {
    throttle: 300,
    scrollDistance: 1,
    scrollUpDistance: 2,
    customOptions: {
      loop: false,
      margin: 12,
      mouseDrag: false,
      touchDrag: false,
      pullDrag: false,
      dots: false,
      navSpeed: 400,
      slideBy: 'page',
      navText: [
        '<i class="fa fa-chevron-left" aria-hidden="true"></i>',
        '<i class="fa fa-chevron-right" aria-hidden="true"></i>'
      ],
      responsive: {
        0: {
          items: 1,
          stagePadding: 0,
        },
        400: {
          items: 2,
        },
        500: {
          items: 3,
          stagePadding: 50,
        },
        700: {
          items: 3,
          stagePadding: 50,
        },
        768: {
          items: 4,
        },
        800: {
          items: 4,
        },
        820: {
          items: 4,
        },
        1200: {
          items: 5
        }
      },
      nav: true
    }
  };

  constructor(private router: Router, public utility: UtilityService) {
   }

  ngOnInit(): void {
    console.log(this.survey.items[0]);
  }

  // onSelect({ selected }): void {
  //   console.log(selected[0]);
  //   this.router.navigateByUrl('/builder');
  //   this.utility.setSurvey(selected[0]);
  // }
  onSelect(survey): void {
    console.log(survey);
    this.router.navigateByUrl('/builder');
    this.utility.setSurvey(survey);
  }

  ngAfterViewInit(): void {
    window.dispatchEvent(new Event('resize'));
  }

  onTabChanged(event): any {
    window.dispatchEvent(new Event('resize'));
    this.survey.openPanel = true;
    // console.log(event.index);
    // if (event.index !== this.survey.activeTab) {
    //   this.survey.activeTab = event.index;
    //   if (event.index == 1) {
    //     this.survey.items = [
    //       {
    //         id: 2, name: 'Survey-YYY', description: 'This is very good survey',
    //         isActive: false, url: 'survey-abc', link: 'https://ippub.panaroamer.com/birds/index.html'
    //       }
    //     ];
    //   } else if (event.index == 2) {
    //     this.survey.items = [
    //       {
    //         id: 3, name: 'Survey-ZZZ', description: 'This is very good survey',
    //         isActive: false, url: 'survey-def', link: 'https://ippub.panaroamer.com/camera/index.html'
    //       }
    //     ];
    //   } else {
    //     this.survey.items = [
    //       {
    //         id: 1, name: 'Survey-XXX', description: 'This is very good survey',
    //         isActive: false, url: 'survey-xyz', link: 'https://ippub.panaroamer.com/animals/index.html'
    //       }
    //     ];
    //   }
    // }
  }

  redirectTo(row): void {
    localStorage.setItem('isSurvey', 'true');
    this.router.navigate(['/play'], { queryParams: { name: row.name, url: row.link } });
  }
}
