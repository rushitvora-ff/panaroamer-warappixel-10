import { Component, OnInit, Inject, OnDestroy, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilityService } from 'src/app/services/utility.service';
import { GlobalAuthenticationContext } from '@oneb/authentication-connector-api';
import { MatDialog } from '@angular/material/dialog';
import { ShareDialogComponent } from 'src/app/shared/dialogs/share-dialog/share-dialog.component';
import { IpInfoDialogComponent } from 'src/app/shared/dialogs/ip-info-dialog/ip-info-dialog.component';
import { DOCUMENT } from '@angular/common';
import { forkJoin } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ScheduleIpComponent } from 'src/app/shared/dialogs/schedule-ip/schedule-ip.component';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { ScheduleService } from '../ip-schedule-calendar/schedule.service';
import { MapDialogComponent } from 'src/app/shared/dialogs/map-dialog/map-dialog.component';
import { CalenderSchedulingComponent } from '../../shared/dialogs/calender-scheduling/calender-scheduling.component';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PlayInterface } from 'src/app/interfaces/componets';

declare var stripe: any;

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.scss']
})
export class PlayComponent implements OnInit, OnDestroy, AfterViewInit {
  play: PlayInterface = {
    display: false,
    priceList: [],
    productList: [],
    starred: [],
    showPreview: false,
    landscape: false,
    baseConversionCount: 0,
    btnText: false,
    disable: false
  };
  // selectedItem;
  // lng;
  // lat;
  // display = false;
  // priceList = [];
  // productList = [];
  // starred = [];
  // title;
  // showPreview = false;
  // landscape = false;
  // content: any;
  // baseConversionCount = 0;
  // blobArr: Array<any>;
  // latlong;
  // displayIfBuilder;
  // myform;
  // object;
  // value;
  // btnText = false;
  // previewButton;
  // openLandscape = false;
  // disable = false;
  // isLoggedIn = GlobalAuthenticationContext.hasSignedInUserDetails() && GlobalAuthenticationContext.getSignedInUserDetails().email;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient,
    public dialog: MatDialog,
    private scheduleSer: ScheduleService,
    private translate: TranslateService,
    private toast: ToastrService,
    @Inject(DOCUMENT) private document: Document,
    public utility: UtilityService,
    public sanitizer: DomSanitizer) {
    if (localStorage.getItem('starred') != null) {
      this.play.starred = JSON.parse(localStorage.getItem('starred'));
    }
    if (localStorage.getItem('latlong')){
      const latlangCoordinated = localStorage.getItem('latlong');
      if (latlangCoordinated){
        localStorage.setItem('coordinates', latlangCoordinated);
      }
    }
    if (localStorage.getItem('previewFull')){
        this.play.previewButton = true;
        localStorage.removeItem('previewFull');
    }
    if (localStorage.getItem('coordinates')) {
      this.play.display = true;
      const latlong = localStorage.getItem('coordinates');
      this.play.latlong = latlong;
      const lastlongHTML = JSON.parse(localStorage.getItem('coordinates'));
      this.play.lng = lastlongHTML.lng;
      this.play.lat = lastlongHTML.lat;
      localStorage.removeItem('coordinates');
    } else{
      // navigator.geolocation.getCurrentPosition((position) => {
      //   console.log('Lat: ' + position.coords.latitude + ' Long: ' + position.coords.longitude);
      //   this.lat = position.coords.latitude;
      //   this.lng = position.coords.longitude
      //   console.log(this.lat);
      // });

      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((position) => {
          this.utility.setCoordinates(position.coords);
          this.play.lat = position.coords.latitude;
          this.play.lng = position.coords.longitude;
          console.log('Lat: ' + position.coords.latitude + ' Long: ' + position.coords.longitude);
        });
      } else {
        alert('Geolocation is not supported by this browser.');
      }
    }
    console.log('latlongcontructor', this.play.latlong);
    if (localStorage.getItem('success') == 'true'){
        this.play.disable = true;
    }
    if (localStorage.getItem('success') == 'false'){
      this.play.disable = false;
    }
    localStorage.removeItem('display');
    // if (localStorage.getItem('latlong') != null){
    //   const latlong = localStorage.getItem('latlong');
    //   this.latlong = latlong;
    //   console.log('latlongcontructor', this.latlong);
    // }
    this.route.queryParams.subscribe(params => {
      if (params.name || params.url) {
        for (const item of this.utility.interactionsArray) {
          if (params.name) {
            if (item.name === params.name) {
              this.play.selectedItem = item;
            }
          } else {
            if (params.url) {
              if (params.url.indexOf(item.name.split(' ').join('-').toLowerCase()) !== -1 || params.url.indexOf(item.name) !== -1) {
                this.play.selectedItem = item;
              }
            }
          }
        }
      }
    });
  }

  ngOnInit(): void {
    if (localStorage.getItem('publishBtn')){
        this.play.btnText = true;
    }
    console.log(this.router.url);
    this.getProducts();

    this.play.myform = new FormGroup({
      title: new FormControl(),
      category: new FormControl(),
      street1: new FormControl(),
      street2: new FormControl(),
      city: new FormControl(),
      zipCode: new FormControl(),
      country: new FormControl(),
    });
    if (localStorage.getItem('Object')) {
      this.play.value = localStorage.getItem('Object');
      this.play.object = JSON.parse(this.play.value);
      console.log(this.play.object.title);
      localStorage.removeItem('Object');
      this.play.myform.setValue({
        title: this.play.object.title ? this.play.object.title : '',
        category: this.play.object.category ? this.play.object.category : '',
        street1: this.play.object.street1 ? this.play.object.street1 : '',
        street2: this.play.object.street2 ? this.play.object.street2 : '',
        city: this.play.object.city ? this.play.object.city : '',
        zipCode: this.play.object.zipCode ? this.play.object.zipCode : '',
        country: this.play.object.country ? this.play.object.country : '',
      });
    }
    if (this.utility.coverImageUrl !== '' || this.play.previewButton == true){
      this.play.showPreview = true;
    }
  }

  ngOnDestroy(): void {
    this.utility.isFullscreen = false;
    this.utility.preview = false;
  }
  getProducts(): void {
    this.utility.getProducts().subscribe(res => {
    if (res) {
    this.getPrices(res.data);
    }
    });
}

  getPrices(products): void {
    this.utility.getPrices().subscribe(res => {
    if (res) {
    const prices = res.data;
    this.play.productList = products.map(item => {
    const obj = prices.find(o => o.product === item.id);
    return { ...item, ...obj };
    });
    this.play.priceList = products.map(item => {
    const obj = prices.find(o => o.product === item.id);
    return { ...item, ...obj };
    });
    this.play.productList.push(this.play.productList[0]);
    }
    });
  }
  pay(product): void {
    console.log(product.recurring);
    localStorage.setItem('Object', JSON.stringify(this.play.myform.value));
    localStorage.setItem('image', JSON.stringify(this.utility.createdIP));
    localStorage.setItem('contentImage', JSON.stringify(this.utility.createdIP));
    localStorage.setItem('previewFull', 'true');
    localStorage.setItem('interactionSaved', JSON.stringify(this.utility.saved));
    stripe.redirectToCheckout({
      lineItems: [
        // Replace with the ID of your price
        { price: product.id, quantity: 1 }
      ],
      mode: product.recurring ? 'subscription' : 'payment',
      successUrl: 'http://' + window.location.host + '/payments/success',
      cancelUrl: 'http://' + window.location.host + '/payments/failure',
    }).then((result) => {
      console.log(result);
      // If `redirectToCheckout` fails due to a browser or network
      // error, display the localized error message to your customer
      // using `result.error.message`.
    });
  }
  scheduleIp(): void {
    const dialogRef = this.dialog.open(ScheduleIpComponent, {
      autoFocus: false,
      maxWidth: '95vw',
      panelClass: this.utility.dark ? 'dark' : 'recurrence-dialog',
      maxHeight: '90vh'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result && result.rruleAll) {
        this.utility.recurrenceData = result;
        const events = [];
        result.rruleAll.forEach(date => {
          events.push({
            dateTime: date,
            name: result.title
          });
        });
        this.utility.setLoader(true);
        this.scheduleSer.createEvent(events).then(resp => {
          this.utility.setLoader(false);
          this.toast.success(this.translate.instant('toast.Ip Scheduled successfully'));
          setTimeout(() => {
            this.router.navigate(['/ip-schedule']);
          }, 500);
        });
      }
    });
  }

  ngAfterViewInit(): void {
    if (this.play.selectedItem) {
      setTimeout(() => {
        const obj = this.utility.iframeUrlArr.find(item => item.url === this.play.selectedItem.url);
        if (!obj) {
          if (this.utility.iframeUrlArr.length === 10) {
            this.utility.iframeUrlArr[this.utility.mostOld].url = this.play.selectedItem.url;
            this.utility.iframeUrlArr[this.utility.mostOld].isVisible = true;
            this.utility.setUrl(this.utility.mostOld);
            this.utility.mostOld++;
            if (this.utility.mostOld === 10) {
              this.utility.mostOld = 0;
            }
          } else {
            this.utility.iframeUrlArr.push({
              url: this.play.selectedItem.url,
              isVisible: true,
              typeSafeUrl: this.sanitizer.bypassSecurityTrustResourceUrl(this.play.selectedItem.url)
            });
          }
          this.utility.iframeUrlArr.filter(x => x.url !== this.play.selectedItem.url).map(item => item.isVisible = false);
        } else {
          for (const item of this.utility.iframeUrlArr) {
            item.url === this.play.selectedItem.url ? item.isVisible = true : item.isVisible = false;
          }
        }
      }, 1);
    }
  }

  publish(): void {
    if (localStorage.getItem('success')){
      localStorage.removeItem('success');
    }
  }

  redirectMap(): void {
    if (this.play.selectedItem.location) {
      this.router.navigate(['/map'],
        {
          queryParams: { lat: this.play.selectedItem.location.lat, long: this.play.selectedItem.location.lng, type: this.play.selectedItem.name }
        });
    }
  }

  accept(): void{
    localStorage.removeItem('publishBtn');
    localStorage.setItem('fullIp', JSON.stringify(this.utility.createdIP));
    localStorage.setItem('Object', JSON.stringify(this.play.myform.value));
    this.router.navigate(['/contacts']);
  }

  goTODesigner(): void {
    localStorage.setItem('Object', JSON.stringify(this.play.myform.value));
    this.router.navigateByUrl('/builder');
  }

  addtoStar(): void {
    let arr = [];
    if (localStorage.getItem('starred') == null) {
      if (this.play.selectedItem) {
        arr.push(this.play.selectedItem);
        this.play.starred = arr;
        localStorage.setItem('starred', JSON.stringify(arr));
      }
    } else {
      arr = JSON.parse(localStorage.getItem('starred'));
      if (!this.checkArr(arr)) {
        if (this.play.selectedItem) {
          arr.push(this.play.selectedItem);
          this.play.starred = arr;
          localStorage.setItem('starred', JSON.stringify(arr));
        }
      }
    }
  }

  getLocation(event): void {
    if (event === true) {
      if (this.play.selectedItem && this.play.selectedItem.location) {
        this.utility.setCoordinates(
          { latitude: this.play.selectedItem.location.lat, longitude: this.play.selectedItem.location.lng, type: this.play.selectedItem.name }
        );
      }
    }
  }

  removeStarred(): void {
    const arr = JSON.parse(localStorage.getItem('starred'));
    const index = arr.findIndex(i => i.name === this.play.selectedItem.name);
    arr.splice(index, 1);
    this.play.starred = arr;
    localStorage.setItem('starred', JSON.stringify(arr));
  }

  checkArr(data): boolean {
    if (this.play.selectedItem) {
      for (const item of data) {
        if (item.name === this.play.selectedItem.name) {
          return true;
        }
      }
    }
  }
  openCalenderScheduling(): void {
    this.dialog.open(CalenderSchedulingComponent, {
      panelClass: this.utility.dark ? ['dark' , 'app-full-screen-dialog'] : 'app-full-screen-dialog',
      maxWidth: '100vw',
      maxHeight: '100vh',
      height: '100%',
      width: '100%',
      data : {presentationTitle : this.play.title }
    }).backdropClick().subscribe(e => history.back());
  }

  openModal(): void{
    this.dialog.open(ShareDialogComponent, {
      panelClass: this.utility.dark ? 'dark' : 'edit-image-dialog',
      maxHeight: '90vh',
      autoFocus: false,
      data: {
        selectedItem: this.play.selectedItem
      }
    });
  }
  openMap(): void{
    navigator.permissions.query({ name: 'geolocation' })
    .then(res => {
      if (this.play.lat || res.state === 'granted'){
        navigator.geolocation.getCurrentPosition((position) => {
          this.utility.setCoordinates(position.coords);
          this.play.lat = position.coords.latitude;
          this.play.lng = position.coords.longitude;
          console.log('Lat: ' + position.coords.latitude + ' Long: ' + position.coords.longitude);
        });
        const dialogRef = this.dialog.open(MapDialogComponent, {
          disableClose: true ,
          panelClass: this.utility.dark ? ['dark' , 'app-full-screen-dialog'] : 'app-full-screen-dialog',
          maxWidth: '100vw',
          maxHeight: '100vh',
          height: '100%',
          width: '100%',
          data: {
            latlong: this.play.latlong
          }
        });
        dialogRef.afterClosed().subscribe(res => {
          console.log(res);
          if (res){
            if (res.lat){
                localStorage.setItem('coordinates', JSON.stringify(res));
                this.play.latlong = JSON.stringify(res);
                this.play.lat = res.lat;
                this.play.lng = res.lng;
            }
          }
        });
      }
      else{
        this.toast.error('Please turn on your location.');
      }
    //   console.log(res);
    //  console.log('dbhfjbjdf',res.state);
    });
    // if(this.display){
  }
  openModalMore(): void{
    this.dialog.open(IpInfoDialogComponent, {
      panelClass: this.utility.dark ? 'dark' : 'edit-image-dialog',
      maxHeight: '90vh',
      data: {
        selectedItem: this.play.selectedItem
      }
    });
  }

  openFullscreen(): void {
    this.play.landscape = true;
    this.utility.landscape = true;
    this.utility.isFullscreen = true;
  }

  closeFullscreen(): void {
    this.play.landscape = false;
    this.utility.landscape = false;
    this.utility.isFullscreen = false;
    if (document.fullscreenElement) {
      if (this.document.exitFullscreen) {
        this.document.exitFullscreen();
      }
    }
  }

  publishIp(): any {
    const ipHtml = `

    <!DOCTYPE html>
    <html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <meta property="og:title" content="Senic-beauty | Jhon Doe" />
        <meta property="og:type" content="website" />
        <meta property="og:image" id="ogMeta" content="http://ippub.panaroamer.com/fifth/presentations/media/thumbnail.jpg" />
        <meta property="og:description"
            content="Presentation related to Senic-beauty" />
        <title>Senic-beauty | Jhon Doe</title>
        <link rel="stylesheet" href="https://ippub.panaroamer.com/italian/css/reset.css">
        <link rel="stylesheet" href="https://ippub.panaroamer.com/italian/css/reveal.css">
        <link rel="icon" href="data:;base64,iVBORwOKGO=" />
        <link rel="stylesheet" href="https://ippub.panaroamer.com/italian/css/main.css">
        <link rel="stylesheet" href="https://ippub.panaroamer.com/italian/css/theme/black.css">
        <link rel="shortcut icon" type="image/png" href="presentations/media/thumbnail.jpg"/>
        <!-- Theme used for syntax highlighting of code -->
        <link rel="stylesheet" href="https://ippub.panaroamer.com/italian/lib/css/monokai.css">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
            integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <style>
          div#timeline {
            position: absolute;
            display: block;
            height: 40px;
            bottom: 0;
            z-index: 100;
            text-align: center !important;
          }
          div#timeline ul {
            padding: 0;
          }
          div#timeline ul li {
            display: inline-block;
            margin: 0 20px 0 0;
          }
          div#timeline ul li a {
            display: block;
            width: 9px;
            height: 9px;
            background: url('/assets/icons/timeBullet.png') 0 0 no-repeat;
          }
          div#timeline ul li a.active {
            background: url('/assets/icons/timeBullet.png') 0 -9px no-repeat;
          }
        </style>
    </head>

    <body ng-app="myApp">
    <div class="reveal">
      <div class="slides">` +
      this.getSlideStr(this.utility.content)
      + `</div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/reveal.js/3.6.0/js/reveal.min.js"></script>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/reveal.js/3.6.0/lib/js/head.min.js"></script>
    <script>
        var timeOut;
        var progressTimeOut;
        var navTimeout;
        var audioTimeout;
        var audio = document.getElementsByTagName('audio');
        for (let i = 0; i < audio.length; i++) {
            audio[i].setAttribute('controls', 'controls');
        }

        progressTimeOut = setTimeout(function () {
            document.getElementsByClassName('progress')[0].style.transition =  "opacity 0.5s linear";
            document.getElementsByClassName('progress')[0].style.opacity = 0;
        }, ` + 2 * 1000 + `);

        navTimeout = setTimeout(function () {
            allImgs = [].slice.call(document.getElementsByClassName('enabled'));
            allImgs.map((eachArrow) => {
                    eachArrow.style.opacity = 0;
                    eachArrow.style.cursor = 'auto';
                    eachArrow.style.transition =  "opacity 0.5s linear";
                });
        }, 5000);

        function focusControl() {
            if(audioTimeout) {
                clearTimeout(audioTimeout);
            }
        }

        function resetTimer() {
            audioTimeout = setTimeout(function () {
            var audio = document.getElementsByTagName('audio')
            for (let i = 0; i < audio.length; i++) {
                if (audio[i].hasAttribute('controls')) {
                    audio[i].removeAttribute('controls');
                }
            }
            }, 5000);
        }

        function showControl() {
            if(audioTimeout) {
                clearTimeout(audioTimeout);
            }

            if(navTimeout) {
                clearTimeout(navTimeout);
            }

            var audio = document.getElementsByTagName('audio')
            for (let i = 0; i < audio.length; i++) {
                audio[i].setAttribute('controls', 'controls');
            }
            audioTimeout = setTimeout(function () {
                var audio = document.getElementsByTagName('audio')
                    for (let i = 0; i < audio.length; i++) {
                        if (audio[i].hasAttribute('controls')) {
                            audio[i].removeAttribute('controls');
                        }
                    }
            }, 5000);

            allImgs = [].slice.call(document.getElementsByClassName('enabled'));
            allImgs.map((eachArrow) => {
                eachArrow.style.opacity = 1;
                eachArrow.style.cursor = 'pointer';
            });

            navTimeout = setTimeout(function () {
                allImgs = [].slice.call(document.getElementsByClassName('enabled'));
                allImgs.map((eachArrow) => {
                    eachArrow.style.opacity = 0;
                    eachArrow.style.cursor = 'auto';
                    eachArrow.style.transition =  "opacity 0.5s linear";
                });
            }, 5000);
        }

        Reveal.initialize({
            dependencies: [
                { src: '' },
            ],
            loop: false,
            width: "100%",
            height: "100%",
            margin: 0,
            minScale: 0.2,
            maxScale: 1.5,
            touch: true
        });

        Reveal.addEventListener( 'slidechanged', function( event ) {
            clearTimeout(timeOut);
            clearTimeout(progressTimeOut);
            clearTimeout(navTimeout);
            document.getElementsByClassName('progress')[0].style.transition =  "opacity 0.5s linear";

            document.getElementsByClassName('progress')[0].style.opacity = 1;
            allImgs = [].slice.call(document.getElementsByClassName('enabled'));
            allImgs.map((eachArrow) => {
                eachArrow.style.opacity = 1;
                eachArrow.style.cursor = 'pointer';
            });
            progressTimeOut = setTimeout(function () {
                document.getElementsByClassName('progress')[0].style.opacity = 0;
              }, ` + 2 * 1000 + `);
            navTimeout = setTimeout(function () {
                allImgs = [].slice.call(document.getElementsByClassName('enabled'));
                allImgs.map((eachArrow) => {
                    eachArrow.style.opacity = 0;
                    eachArrow.style.cursor = 'auto';
                    eachArrow.style.transition =  "opacity 0.5s linear";
                });
            }, 5000);
            if(audioTimeout) {
                clearTimeout(audioTimeout);
            }
            var audio = document.getElementsByTagName('audio')
            for (let i = 0; i < audio.length; i++) {
              audio[i].setAttribute('controls', 'controls');
            }

            audioTimeout = setTimeout(function () {
                var audio = document.getElementsByTagName('audio')
                    for (let i = 0; i < audio.length; i++) {
                        if (audio[i].hasAttribute('controls')) {
                            audio[i].removeAttribute('controls');
                        }
                    }
            }, 5000);
        });
    </script>

    </body>

    </html>
    `;
  }

  getSlideStr(content): any {
    this.play.content = content;
    const requestArray = [];

    for (const slide of content) {
      if (slide.music) {
        requestArray.push(this.http.get<any>(slide.normalUrl, { responseType: 'blob' as 'json' }));
      }
      for (const row of slide.rows) {
        for (const col of row.cols) {
          if (col.type === 'video') {
            requestArray.push(this.http.get<any>(col.normalUrl, { responseType: 'blob' as 'json' }));
          }
        }
      }
    }

    if (requestArray.length === 0) {
      this.getHTML();
      return;
    }

    forkJoin(requestArray).subscribe(results => {
      console.log(results);
      this.play.blobArr = results;
      let j = 0;
      while (j < results.length) {
        for (const slide of content) {
          if (slide.music) {
            slide.base64Url = this.getBase64(results[j], j);
            j++;
          }
          for (const row of slide.rows) {
            for (const col of row.cols) {
              if (col.type === 'video') {
                col.base64Url = this.getBase64(results[j], j);
                j++;
              }
            }
          }
        }
      }
    });
  }

  getBase64(blob, idx): any {
    const reader = new FileReader();
    reader.readAsDataURL(blob);
    reader.onloadend = ((event: any) => {
      const base64data: any = reader.result;
      this.play.blobArr[idx] = reader.result;
      this.play.baseConversionCount++;
      if (this.play.baseConversionCount === this.play.blobArr.length) {
        let j = 0;
        for (const slide of this.play.content) {
          if (slide.music) {
            slide.base64Url = this.play.blobArr[j];
            j++;
          }
          for (const row of slide.rows) {
            for (const col of row.cols) {
              if (col.type === 'video') {
                col.base64Url = this.play.blobArr[j];
                j++;
              }
            }
          }
        }
        console.log(this.play.content);
        this.getHTML();
      }

      return null;
    });
  }

  getHTML(): any {
    const info = JSON.parse(localStorage.getItem('info'));
    const title = info.name ? info.name : 'Senic-beauty | Jhon Doe';
    const description = info.description ? info.description : 'Presentation related to Senic-beauty';
    const coverImage = info.coverImage ? info.coverImage : 'http://ippub.panaroamer.com/fifth/presentations/media/thumbnail.jpg';
    let ipHtml = `

    <!DOCTYPE html>
    <html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <meta property="og:title" content="${title}" />
        <meta property="og:type" content="website" />
        <meta property="og:image" id="ogMeta" content="${coverImage}" />
        <meta property="og:description"
            content="${description}" />
        <title>${title}</title>
        <link rel="stylesheet" href="https://ippub.panaroamer.com/italian/css/reset.css">
        <link rel="stylesheet" href="https://ippub.panaroamer.com/italian/css/reveal.css">
        <link rel="icon" href="${coverImage}" />
        <link rel="stylesheet" href="https://ippub.panaroamer.com/italian/css/main.css">
        <link rel="stylesheet" href="https://ippub.panaroamer.com/italian/css/theme/black.css">
        <link rel="shortcut icon" type="image/png" href="presentations/media/thumbnail.jpg"/>
        <!-- Theme used for syntax highlighting of code -->
        <link rel="stylesheet" href="https://ippub.panaroamer.com/italian/lib/css/monokai.css">
        <link rel="stylesheet" href="https://picturepan2.github.io/instagram.css/dist/instagram.min.css">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
        <style>
          .region-box {
              width: 200px;
              height: 200px;
          }
          .region-box, .region-all {
              display: flex;
              justify-content: center;
              align-items: center;
          }
          .box, .region-box, .region-all {
              position: fixed;
              z-index: 999999;
              height: 100%;
              top: 0;
          }
          div#timeline {
            position: absolute;
            display: block;
            height: 10px;
            bottom: 30px;
            width: 100%;
            z-index: 100;
            text-align: center !important;
          }
          div#timeline ul {
            padding: 0;
          }
          div#timeline ul li {
            display: inline-block;
            margin: 0 20px 0 0;
          }
          div#timeline ul li a {
            display: block;
            width: 9px;
            height: 9px;
            border-radius: 100%;
            opacity: 0.4;
            background: #ffffff;
          }
          div#timeline ul li a.active {
            opacity: 1;
          }
          .reveal aside.controls {
            display: none !important;
          }
          .modal-body .selected-img {
            width: 100%;
            max-width: 100%;
            height: 100%;
          }
          .modal-content {
            border-radius: 0;
          }
          .modal-body h2 {
            font-size: 30px;
            text-transform: unset;
            font-weight: 500;
            text-align: left;
          }
          .modal-body {
            position: relative;
            padding: 15px;
          }
          .modal-body .head .close{
            color: #ffffff;
            opacity: 1;
            font-size: 20px;
            text-shadow: none;
            float: none;
            font-weight: 500;
          }
          .modal-body .body-content audio {
            position: unset !important;
          }
          .modal-body .body-content .content a:hover img {
            box-shadow: none !important;
          }
          .modal-body .body-content .content a img {
            width: 25px;
            height: auto;
            margin-right: 10px !important;
          }
          .modal-body .head {
            cursor: pointer;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 20px;
            background: black;
            position: absolute;
            right: -10px;
            top: -11px;
            width: 20px;
            border-radius: 50%;
            text-align: center;
          }
          .audio-wrapper audio {
            bottom: 25px;
          }
          body .modal-dialog {
            width: 500px;
            display: flex;
          }
          .reveal img, .reveal video, .reveal iframe {
            max-height: 100%;
          }
          .img-size {
            height: 100%;
          }
          @media only screen and (max-width: 600px) {
            body .modal-dialog {
              width: auto !important;
            }
          }
          .modal {
            z-index: -1;
            display: flex !important;
            justify-content: center;
            align-items: center;
          }
          .modal-open .modal {
             z-index: 1050;
          }
          .modal-dialog video {
            width: 100%;
            height: 100%;
          }
          figure {
            margin: 0;
            height: 100%;
          }
          .emoji-mart-emoji {
            position: relative;
            display: inline-block;
            font-size: 0;
            margin: 0;
            padding: 0;
            border: none;
            background: none;
            box-shadow: none;
          }
          .icons {
            position: absolute !important;
          }
          .icons, .icons.hide {
            display: none;
          }
          .icons.show {
            display: block;
          }
        </style>
    </head>

    <body ng-app="myApp">
    <div class="reveal">
      <div class="slides">`;

    let ipStr = '';
    for (const slide of this.play.content) {
      let str = `
        <section class="full-widheight" style="padding: 2px !important" onclick="showControl()">`;

      if (slide.music) {
        console.log(slide.base64Url);
        str = str + `
            <div class="audio-wrapper">
              <audio id="audio" class="audio-play" onmouseover="focusControl(this)"
              onmouseleave="resetTimer(this)" controlsList="nodownload">
                  <source src="` + slide.base64Url + `" type="audio/mpeg">
              </audio>
              </div>
            `;
      }
      for (const row of slide.rows) {
        str = str + `
            <div class="row full-width ml-0 slide-row" style="height:` + row.style.height + `;">`;
        for (const col of row.cols) {
          str = str + `
                <div class="full-widheight text-center remove-padd ` + col.class + `" style="width:` + col.style.width + `;">`;

          if (col.type === 'image') {
            str = str + `<figure class="filter-` + col.filterType + `">`;
            str = str + `<img src="` + col.url + `" class="img-size" /></figure>`;
          }
          if (col.type === 'video') {
            console.log(col.base64Url);
            str = str + `
              <video  class="img-size" style="border: 4px solid #fff;
              box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);" controls>
                <source type="video/webm" src="` + col.base64Url + `">
              </video>`;
          }
          if (col.mode === 'Single' && col.effects) {
            if (col.effects.length) {
              str = str + `<div id="single-gesture${col.effects[0].id}"></div>`;
            }
          }

          if (col.mode === 'All' && col.effects) {
            if (col.effects.length) {
              for (let i = 0; i < col.effects.length; i++) {
                str = str + `<div class="region-box${i} region-all" id="my-div"
                    style="width: 100px;
                    height: 100px;
                    top: calc(${col.effects[i].y}% - 50px); left: calc(${col.effects[i].x}% - 50px);">
                    <span class="emoji-mart-emoji" title="">
                    <span style="width: 18px; height: 18px; display: inline-block;
                    background-image: url(&quot;https://unpkg.com/emoji-datasource-apple@5.0.1/img/apple/sheets-256/32.png&quot;);
                    background-size: 5700% 5700%;
                    background-position: ${this.getPosition(col.effects[i].emojiObj)};">
                    </span></span>
                </div>`;
              }
            }
          }
          if (col.type === 'iframe') {
            str = str + `
                    <iframe class="img-size" src="` + col.url + `" allow="autoplay" *ngIf="col.type == 'iframe'">
                    </iframe>`;
          }
          if (col.type === 'survey') {
            str = str + `<div class="` + col.name + `"></div>`;
          }
          str = str + '</div>';
        }
      }
      str = str + '</section>';
      ipStr = ipStr + str;
    }
    ipStr = ipStr + `
    <div class="icons bottom-icons hide">
        <img class="share-icon icn" onclick="share()" width="20" src="https://ippub.panaroamer.com/new-york/assets/share.png" />`;

    if (localStorage.getItem('latlong')) {
      const latlng = JSON.parse(localStorage.getItem('latlong'));
      ipStr = ipStr +
        `<a href="https://panaroamer.com/map?lat=${latlng.lat}&long=${latlng.lng}" id="location-url" target="_blank" class="home">`;
    } else {
      ipStr = ipStr + `<a href="javascript:;" id="location-url" target="_blank" class="home">`;
    }

    ipStr = ipStr + `
            <img class="home-icon" width="20" src="https://ippub.panaroamer.com/new-york/assets/location1.png" />
        </a>
        <a href="https://panaroamer.com/" target="_blank" class="home">
            <img class="home-icon" width="20" src="https://ippub.panaroamer.com/new-york/assets/home.png" />
        </a>
    </div>`;
    ipHtml = ipHtml + ipStr;
    ipHtml = ipHtml + `
    <section class="promotional-slide">
        <p>
            Share your presentation url to others:
        </p>
        <a id="lastSlide" target="_top" class="slide-url"></a>
        <img class="share-icon" onclick="share()" width="20"
            src="https://ippub.panaroamer.com/dubai/assets/share.png"
        <p>
            <a
            href="https://ippub.panaroamer.com/${title.split(' ').join('-').toLowerCase()}/assets/pdf.png"
            class="panaroamer" target="_blank">
                <img class = "pdf-img" src="https://ippub.panaroamer.com/new-york/assets/pdf.png" title="Export to Pdf" />
            </a>
        </p>
        <p>
            <a href="https://panaroamer.com/" class="panaroamer" target="_blank">https://panaroamer.com</a>
        </p>
    </section>
    `;

    const data = [];
    for (const item of this.utility.content) {
      data.push(item.rows[0].cols[0].surveyData);
    }

    ipHtml = ipHtml + `</div><div id="timeline"><ul>`;
    for (let i = 0; i <= this.play.content.length; i++) {
      ipHtml = ipHtml + `<li>
                  <a href="javascript:;" onclick="navigateSlide(${i})"></a>
              </li>`;
    }

    ipHtml = ipHtml + `
            </ul>
        </div>
        <button data-toggle="modal" data-target="#myModal" id="bsModal" style="opacity: 0;"></button>
        <div class="modal fade" id="myModal" role="dialog">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
              <div class="modal-body">
                <div class="head" style="'background-color': ${`#000000`};">
                    <div class="close" data-dismiss="modal"
                      style="'color': ${`#ffffff`};">
                        <span class="mr-0" aria-hidden="true">&times;</span>
                    </div>
                </div>
                <div class="body-content"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <script src="https://code.jquery.com/jquery-3.4.1.min.js" type="text/javascript" id="reveal-js1"></script>
      <script src="https://unpkg.com/survey-jquery@0.12.10/survey.jquery.min.js" type="text/javascript" id="reveal-js2"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
      integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/reveal.js/3.6.0/js/reveal.min.js"></script>
      <script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/374756/zingtouch-1.0.0.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/reveal.js/3.6.0/lib/js/head.min.js"></script>
      <script>
          var timeOut;
          var progressTimeOut;
          var navTimeout;
          var audioTimeout;
          var selectedGesture;
          var mode;
          var singleId;
          var gesIdx = 0;
          var tapregion = [];
          var longTapregion = [];
          var panRegion = [];
          var gestureTimeout;
          var showIcon;
          var regionArr = [];
          var content = getEffectArr(${JSON.stringify(this.play.content)});

          var ele = document.getElementById('lastSlide');
          ele.href = 'https://ippub.panaroamer.com/' + getTitle() + '/pwa.html';
          ele.innerHTML = 'https://ippub.panaroamer.com/' + getTitle() + '/pwa.html';

          var audio = document.getElementsByClassName('audio-play');
          for (let i = 0; i < audio.length; i++) {
              audio[i].setAttribute('controls', 'controls');
          }

          if (window != window.top) {
            showIcon = false;
            var elements = document.getElementsByClassName('bottom-icons');
            for (let i=0; i < elements.length; i++) {
              elements[i].classList.remove('show');
              elements[i].classList.add('hide');
            }
          }
          else {
              timeOut = setTimeout(function () {
                showIcon = true;
                var elements = document.getElementsByClassName('bottom-icons');
                for (let i=0; i < elements.length; i++) {
                  elements[i].classList.remove('hide');
                  elements[i].classList.add('show');
                }
              }, 3000);
          }

          var elems = document.querySelectorAll("#timeline a");
          elems[0].className = "active";

          progressTimeOut = setTimeout(function () {
              document.getElementsByClassName('progress')[0].style.transition =  "opacity 0.5s linear";
              document.getElementsByClassName('progress')[0].style.opacity = 0;
          }, ` + 2 * 1000 + `);

          navTimeout = setTimeout(function () {
              allImgs = [].slice.call(document.getElementsByClassName('enabled'));
              allImgs.map((eachArrow) => {
                      eachArrow.style.opacity = 0;
                      eachArrow.style.cursor = 'auto';
                      eachArrow.style.transition =  "opacity 0.5s linear";
                  });
          }, 5000);

          function focusControl() {
              if(audioTimeout) {
                  clearTimeout(audioTimeout);
              }
          }
          window.addEventListener("message", receiveMessage);

          function receiveMessage(event) {

              if(event.data.color) {
                var elements = document.getElementsByClassName('img-size');
                for (let i = 0; i < elements.length; i++) {
                  elements[i].setAttribute("style", "border: 4px solid " + event.data.color + ";");
                }
              }
          }

          function getTitle() {
            return '${title.split(' ').join('-').toLowerCase()}';
          }

          function isOpenModal() {
            var elem = document.getElementById('myModal');
            return elem.classList.contains('show');
          }

          function navigateSlide(idx) {
            var elems = document.querySelectorAll("#timeline a");
            [].forEach.call(elems, function(el) {
              el.classList.remove("active");
            });
            elems[idx].className = "active";
            Reveal.slide(idx);
          }

          function share() {
            if (window.navigator.share) {
              window.navigator.share({
                  title: 'Interactive Presentation',
                  text: 'Check out this great interactive presentation',
                  url: window.location.href
              })
            }
          }

          function resetTimer() {
              audioTimeout = setTimeout(function () {
              var audio = document.getElementsByClassName('audio-play')
              for (let i = 0; i < audio.length; i++) {
                  if (audio[i].hasAttribute('controls')) {
                      audio[i].removeAttribute('controls');
                  }
              }
              }, 5000);
          }

          function showControl() {
              if(audioTimeout) {
                  clearTimeout(audioTimeout);
              }

              if(navTimeout) {
                  clearTimeout(navTimeout);
              }

              var audio = document.getElementsByClassName('audio-play')
              for (let i = 0; i < audio.length; i++) {
                  audio[i].setAttribute('controls', 'controls');
              }

              audioTimeout = setTimeout(function () {
                  var audio = document.getElementsByClassName('audio-play')
                      for (let i = 0; i < audio.length; i++) {
                          if (audio[i].hasAttribute('controls')) {
                              audio[i].removeAttribute('controls');
                          }
                      }
              }, 5000);

              allImgs = [].slice.call(document.getElementsByClassName('enabled'));
              allImgs.map((eachArrow) => {
                  eachArrow.style.opacity = 1;
                  eachArrow.style.cursor = 'pointer';
              });

              navTimeout = setTimeout(function () {
                  allImgs = [].slice.call(document.getElementsByClassName('enabled'));
                  allImgs.map((eachArrow) => {
                      eachArrow.style.opacity = 0;
                      eachArrow.style.cursor = 'auto';
                      eachArrow.style.transition =  "opacity 0.5s linear";
                  });
              }, 5000);
          }
          var data = ` + JSON.stringify(data) + `;
          Reveal.initialize({
              dependencies: [
                  { src: '' },
              ],
              loop: false,
              width: "100%",
              height: "100%",
              margin: 0,
              minScale: 0.2,
              maxScale: 1.5,
              touch: true
          });

          Reveal.addEventListener( 'slidechanged', function( event ) {
              clearTimeout(gestureTimeout);
              gesIdx = 0;
              selectedGesture = undefined;
              const gesElement = document.getElementsByClassName('region-all');
              for (let i=0; i < gesElement.length; i++) {
                tapregion.forEach(elem => {
                  elem.unbind(gesElement[i]);
                });
                longTapregion.forEach(elem => {
                  elem.unbind(gesElement[i]);
                });
                panRegion.forEach(elem => {
                  elem.unbind(gesElement[i]);
                });
              }
              tapregion = [];
              longTapregion = [];
              panRegion = [];
              if (content && content.length && event.indexh > 0 && event.indexh <= content.length) {
                  if (content[event.indexh - 1].type !== 'survey') {
                    regionArr = content[event.indexh - 1].effects;
                    mode = content[event.indexh - 1].mode;
                    if (mode === 'Single' && content[event.indexh - 1].effects && content[event.indexh - 1].effects.length) {
                      singleId = content[event.indexh - 1].effects[0].id;
                    }
                  }
              } else {
                selectedGesture = undefined;
                regionArr = [];
              }
              if (regionArr && regionArr.length) {
                setTimeout(function() {
                  applyGesture();
                }, 300);
              }

              if(data[event.indexh]) {
                Survey.defaultBootstrapCss.navigationButton = "btn btn-green";
                Survey.defaultBootstrapCss.matrixdynamic.button = "btn btn-green";
                Survey.Survey.cssType = "bootstrap";
                const survey = new Survey.Model(data[event.indexh]);

                $('.Survey' + event.indexh).Survey({
                    model: survey
                });
              }

              navigateSlide(event.indexh);
              clearTimeout(timeOut);
              clearTimeout(progressTimeOut);
              clearTimeout(navTimeout);

              document.getElementsByClassName('progress')[0].style.transition =  "opacity 0.5s linear";

              document.getElementsByClassName('progress')[0].style.opacity = 1;
              allImgs = [].slice.call(document.getElementsByClassName('enabled'));
              allImgs.map((eachArrow) => {
                  eachArrow.style.opacity = 1;
                  eachArrow.style.cursor = 'pointer';
              });
              progressTimeOut = setTimeout(function () {
                  document.getElementsByClassName('progress')[0].style.opacity = 0;
              }, ` + 2 * 1000 + `);
              navTimeout = setTimeout(function () {
                  allImgs = [].slice.call(document.getElementsByClassName('enabled'));
                  allImgs.map((eachArrow) => {
                      eachArrow.style.opacity = 0;
                      eachArrow.style.cursor = 'auto';
                      eachArrow.style.transition =  "opacity 0.5s linear";
                  });
              }, 5000);

              if (window != window.top) {
                showIcon = false;
                var elements = document.getElementsByClassName('bottom-icons');
                for (let i=0; i < elements.length; i++) {
                  elements[i].classList.remove('show');
				          elements[i].classList.add('hide');
                }
              }
              else {
                var elements = document.getElementsByClassName('bottom-icons');
                for (let i=0; i < elements.length; i++) {
                  elements[i].classList.remove('show');
                  elements[i].classList.add('hide');
                }
                if (event.indexh != content.length + 1) {
                  timeOut = setTimeout(function () {
                    showIcon = true;
                    for (let i=0; i < elements.length; i++) {
                      elements[i].classList.remove('hide');
                      elements[i].classList.add('show');
                    }
                  }, 3000);
                }
              }

              if(audioTimeout) {
                  clearTimeout(audioTimeout);
              }

              var audio = document.getElementsByClassName('audio-play')
              for (let i = 0; i < audio.length; i++) {
                      audio[i].setAttribute('controls', 'controls');

              }

              audioTimeout = setTimeout(function () {
                  var audio = document.getElementsByClassName('audio-play')
                      for (let i = 0; i < audio.length; i++) {
                          if (audio[i].hasAttribute('controls')) {
                              audio[i].removeAttribute('controls');
                          }
                      }
              }, 5000);
          });

          function getEffectArr(arr) {
            var myArr = [];
            for (let i = 1; i < arr.length; i++ ){
              var obj = {};
              obj = arr[i].rows[0].cols[0];
              myArr.push(obj);
            }
            return myArr;
          }

          function getPosition(emojiObj) {
            const obj = {};
            obj.x = (emojiObj[0] * 25) / 14;
            obj.y = (emojiObj[1] * 16.0714) / 9;
            return obj.x.toFixed(4) + '% ' + obj.y.toFixed(4) + '%';
          }

          function applyGesture() {
            if (mode === 'Single') {
              gesIdx++;
              if (gesIdx <= regionArr.length) {
                selectedGesture = regionArr[gesIdx - 1];
                document.getElementById("single-gesture" + singleId).innerHTML =
                '<div class="region-box region-all" ` +
      `style="width: 100px;` +
      `height: 100px;` +
      `top: calc(' + selectedGesture.y + '% - 50px); left: calc(' + selectedGesture.x + '% - 50px);">` +
      `<span class="emoji-mart-emoji" title="">` +
      `<span style="width: 18px; height: 18px; display: inline-block;` +
      `background-image: url(&quot;https://unpkg.com/emoji-datasource-apple@5.0.1/img/apple/sheets-256/32.png&quot;);` +
      `background-size: 5700% 5700%;` +
      `background-position: ' + getPosition(selectedGesture.emojiObj) + ';">` +
      `</span></span>` +
      `</div>';
                if (selectedGesture.gesture === 'tap') {
                  const tapElement = document.getElementsByClassName('region-box');
                  for (let i=0; i < tapElement.length; i++) {
                    const tapRgn = new ZingTouch.Region(tapElement[i]);
                    tapRgn.bind(tapElement[i], 'tap', function(e) {
                      var elem = document.getElementsByClassName('body-content')[0];
                      elem.innerHTML = '';
                      if (selectedGesture.type == 'text') {
                        var modalHtml = '<div class="content text-center">' +
                        '<h2 style="color: ${`#000000`};text-transform: unset;" class="m-0">'
                        + selectedGesture.text +'</h2>';
                        if (selectedGesture.userActions.length) {
                          modalHtml += '<div class="text-center mt-2">';
                          for (var act of selectedGesture.userActions) {
                            if (act.action == 'buy') {
                              modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                '<img title="Buy" src="https://panaroamer.com/assets/icons/iconmonstr-checkout-9-48.png" alt=""></a>';
                            }
                            if (act.action == 'visit') {
                              modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                '<img title="Visit" src="https://panaroamer.com/assets/icons/backend-website-10.png" alt=""></a>';
                            }
                            if (act.action == 'call') {
                              modalHtml += '<a href="tel:' + act.value + '" class="mr-2">' +
                                  '<img title="Call" src="https://panaroamer.com/assets/icons/iconmonstr-phone-6-48.png" alt=""></a>';
                            }
                            if (act.action == 'text') {
                              modalHtml += '<a href="sms:' + act.value + '" class="mr-2">' +
                                  '<img title="Text" src="https://panaroamer.com/assets/icons/iconmonstr-sms-3-48.png" alt=""></a>';
                            }
                          }
                          modalHtml += '</div>';
                        }
                        modalHtml += '</div>';
                        elem.innerHTML = modalHtml;
                      } else if (selectedGesture.type == 'image') {
                          var modalHtml = '<div class="content text-center">' +
                          '<figure class="filter-' + selectedGesture.gesFilterType + '">' +
                            '<img class="selected-img" src="' + selectedGesture.text + '" alt="">' +
                          '</figure>';
                          if (selectedGesture.userActions.length) {
                            modalHtml += '<div class="text-center mt-2">';
                            for (var act of selectedGesture.userActions) {
                              if (act.action == 'buy') {
                                modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                  '<img title="Buy" src="https://panaroamer.com/assets/icons/iconmonstr-checkout-9-48.png" alt=""></a>';
                              }
                              if (act.action == 'visit') {
                                modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                  '<img title="Visit" src="https://panaroamer.com/assets/icons/backend-website-10.png" alt=""></a>';
                              }
                              if (act.action == 'call') {
                                modalHtml += '<a href="tel:' + act.value + '" class="mr-2">' +
                                    '<img title="Call" src="https://panaroamer.com/assets/icons/iconmonstr-phone-6-48.png" alt=""></a>';
                              }
                              if (act.action == 'text') {
                                modalHtml += '<a href="sms:' + act.value + '" class="mr-2">' +
                                    '<img title="Text" src="https://panaroamer.com/assets/icons/iconmonstr-sms-3-48.png" alt=""></a>';
                              }
                            }
                            modalHtml += '</div>';
                          }
                          modalHtml += '</div>';
                          elem.innerHTML = modalHtml;
                      } else if(selectedGesture.type == 'video') {
                        var modalHtml = '<div class="content text-center">' +
                        '<video width="400" controls>' +
                          '<source src="' + selectedGesture.text + '" type="video/mp4">' +
                          'Your browser does not support HTML video.' +
                        '</video>';
                        if (selectedGesture.userActions.length) {
                          modalHtml += '<div class="text-center mt-2">';
                          for (var act of selectedGesture.userActions) {
                            if (act.action == 'buy') {
                              modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                '<img title="Buy" src="https://panaroamer.com/assets/icons/iconmonstr-checkout-9-48.png" alt=""></a>';
                            }
                            if (act.action == 'visit') {
                              modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                '<img title="Visit" src="https://panaroamer.com/assets/icons/backend-website-10.png" alt=""></a>';
                            }
                            if (act.action == 'call') {
                              modalHtml += '<a href="tel:' + act.value + '" class="mr-2">' +
                                  '<img title="Call" src="https://panaroamer.com/assets/icons/iconmonstr-phone-6-48.png" alt=""></a>';
                            }
                            if (act.action == 'text') {
                              modalHtml += '<a href="sms:' + act.value + '" class="mr-2">' +
                                  '<img title="Text" src="https://panaroamer.com/assets/icons/iconmonstr-sms-3-48.png" alt=""></a>';
                            }
                          }
                          modalHtml += '</div>';
                        }
                        modalHtml += '</div>';
                        elem.innerHTML = modalHtml;
                      } else if(selectedGesture.type == 'audio') {
                        var modalHtml = '<div class="content audio text-center">' +
                        '<audio controls>' +
                          '<source src="' + selectedGesture.text + '" type="audio/ogg">' +
                          '<source src="' + selectedGesture.text + '" type="audio/mpeg">' +
                          'Your browser does not support the audio element.' +
                        '</audio>';
                        if (selectedGesture.userActions.length) {
                          modalHtml += '<div class="text-center mt-2">';
                          for (var act of selectedGesture.userActions) {
                            if (act.action == 'buy') {
                              modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                '<img title="Buy" src="https://panaroamer.com/assets/icons/iconmonstr-checkout-9-48.png" alt=""></a>';
                            }
                            if (act.action == 'visit') {
                              modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                '<img title="Visit" src="https://panaroamer.com/assets/icons/backend-website-10.png" alt=""></a>';
                            }
                            if (act.action == 'call') {
                              modalHtml += '<a href="tel:' + act.value + '" class="mr-2">' +
                                  '<img title="Call" src="https://panaroamer.com/assets/icons/iconmonstr-phone-6-48.png" alt=""></a>';
                            }
                            if (act.action == 'text') {
                              modalHtml += '<a href="sms:' + act.value + '" class="mr-2">' +
                                  '<img title="Text" src="https://panaroamer.com/assets/icons/iconmonstr-sms-3-48.png" alt=""></a>';
                            }
                          }
                          modalHtml += '</div>';
                        }
                        modalHtml += '</div>';
                        elem.innerHTML = modalHtml;
                      }
                      var btn = document.getElementById("bsModal");
                      btn.click();
                    });
                    tapregion.push(tapRgn);
                  }
                  gestureTimeout = setTimeout(function() {
                    var closeBtn = document.getElementsByClassName("close");
                    closeBtn[0].click();
                    for (let i=0; i < tapElement.length; i++) {
                      tapregion.forEach(element => {
                        element.unbind(tapElement[i], 'tap');
                      });
                    }
                    setTimeout(function() {
                      applyGesture();
                    }, 400);
                  }, selectedGesture.time * 1000);
                } else if (selectedGesture.gesture === 'long-tap') {
                  const longTapElement = document.getElementsByClassName('region-box');
                  for (let i=0; i < longTapElement.length; i++) {
                    const longTapRgn = new ZingTouch.Region(longTapElement[i]);
                    const longtap = new ZingTouch.Tap({
                      maxDelay: 4000,
                      numInputs: 1,
                      tolerance: 1000
                    });
                    longTapRgn.bind(longTapElement[i], longtap, function(e) {
                      if (e && e.detail.interval > 300) {
                        var elem = document.getElementsByClassName('body-content')[0];
                        elem.innerHTML = '';
                        if (selectedGesture.type == 'text') {
                          var modalHtml = '<div class="content text-center">' +
                          '<h2 style="color: ${`#000000`};text-transform: unset;" class="m-0">'
                          + selectedGesture.text +'</h2>';
                          if (selectedGesture.userActions.length) {
                            modalHtml += '<div class="text-center mt-2">';
                            for (var act of selectedGesture.userActions) {
                              if (act.action == 'buy') {
                                modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                  '<img title="Buy" src="https://panaroamer.com/assets/icons/iconmonstr-checkout-9-48.png" alt=""></a>';
                              }
                              if (act.action == 'visit') {
                                modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                  '<img title="Visit" src="https://panaroamer.com/assets/icons/backend-website-10.png" alt=""></a>';
                              }
                              if (act.action == 'call') {
                                modalHtml += '<a href="tel:' + act.value + '" class="mr-2">' +
                                    '<img title="Call" src="https://panaroamer.com/assets/icons/iconmonstr-phone-6-48.png" alt=""></a>';
                              }
                              if (act.action == 'text') {
                                modalHtml += '<a href="sms:' + act.value + '" class="mr-2">' +
                                    '<img title="Text" src="https://panaroamer.com/assets/icons/iconmonstr-sms-3-48.png" alt=""></a>';
                              }
                            }
                            modalHtml += '</div>';
                          }
                          modalHtml += '</div>';
                          elem.innerHTML = modalHtml;
                        } else if (selectedGesture.type == 'image') {
                          var modalHtml = '<div class="content text-center">' +
                          '<figure class="filter-' + selectedGesture.gesFilterType + '">' +
                            '<img class="selected-img" src="' + selectedGesture.text + '" alt="">' +
                          '</figure>';
                          if (selectedGesture.userActions.length) {
                            modalHtml += '<div class="text-center mt-2">';
                            for (var act of selectedGesture.userActions) {
                              if (act.action == 'buy') {
                                modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                  '<img title="Buy" src="https://panaroamer.com/assets/icons/iconmonstr-checkout-9-48.png" alt=""></a>';
                              }
                              if (act.action == 'visit') {
                                modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                  '<img title="Visit" src="https://panaroamer.com/assets/icons/backend-website-10.png" alt=""></a>';
                              }
                              if (act.action == 'call') {
                                modalHtml += '<a href="tel:' + act.value + '" class="mr-2">' +
                                    '<img title="Call" src="https://panaroamer.com/assets/icons/iconmonstr-phone-6-48.png" alt=""></a>';
                              }
                              if (act.action == 'text') {
                                modalHtml += '<a href="sms:' + act.value + '" class="mr-2">' +
                                    '<img title="Text" src="https://panaroamer.com/assets/icons/iconmonstr-sms-3-48.png" alt=""></a>';
                              }
                            }
                            modalHtml += '</div>';
                          }
                          modalHtml += '</div>';
                          elem.innerHTML = modalHtml;
                        } else if(selectedGesture.type == 'video') {
                          var modalHtml = '<div class="content text-center">' +
                          '<video width="400" controls>' +
                            '<source src="' + selectedGesture.text + '" type="video/mp4">' +
                            'Your browser does not support HTML video.' +
                          '</video>';
                          if (selectedGesture.userActions.length) {
                            modalHtml += '<div class="text-center mt-2">';
                            for (var act of selectedGesture.userActions) {
                              if (act.action == 'buy') {
                                modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                  '<img title="Buy" src="https://panaroamer.com/assets/icons/iconmonstr-checkout-9-48.png" alt=""></a>';
                              }
                              if (act.action == 'visit') {
                                modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                  '<img title="Visit" src="https://panaroamer.com/assets/icons/backend-website-10.png" alt=""></a>';
                              }
                              if (act.action == 'call') {
                                modalHtml += '<a href="tel:' + act.value + '" class="mr-2">' +
                                    '<img title="Call" src="https://panaroamer.com/assets/icons/iconmonstr-phone-6-48.png" alt=""></a>';
                              }
                              if (act.action == 'text') {
                                modalHtml += '<a href="sms:' + act.value + '" class="mr-2">' +
                                    '<img title="Text" src="https://panaroamer.com/assets/icons/iconmonstr-sms-3-48.png" alt=""></a>';
                              }
                            }
                            modalHtml += '</div>';
                          }
                          modalHtml += '</div>';
                          elem.innerHTML = modalHtml;
                        } else if(selectedGesture.type == 'audio') {
                          var modalHtml = '<div class="content audio text-center">' +
                          '<audio controls>' +
                            '<source src="' + selectedGesture.text + '" type="audio/ogg">' +
                            '<source src="' + selectedGesture.text + '" type="audio/mpeg">' +
                            'Your browser does not support the audio element.' +
                          '</audio>';
                          if (selectedGesture.userActions.length) {
                            modalHtml += '<div class="text-center mt-2">';
                            for (var act of selectedGesture.userActions) {
                              if (act.action == 'buy') {
                                modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                  '<img title="Buy" src="https://panaroamer.com/assets/icons/iconmonstr-checkout-9-48.png" alt=""></a>';
                              }
                              if (act.action == 'visit') {
                                modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                  '<img title="Visit" src="https://panaroamer.com/assets/icons/backend-website-10.png" alt=""></a>';
                              }
                              if (act.action == 'call') {
                                modalHtml += '<a href="tel:' + act.value + '" class="mr-2">' +
                                    '<img title="Call" src="https://panaroamer.com/assets/icons/iconmonstr-phone-6-48.png" alt=""></a>';
                              }
                              if (act.action == 'text') {
                                modalHtml += '<a href="sms:' + act.value + '" class="mr-2">' +
                                    '<img title="Text" src="https://panaroamer.com/assets/icons/iconmonstr-sms-3-48.png" alt=""></a>';
                              }
                            }
                            modalHtml += '</div>';
                          }
                          modalHtml += '</div>';
                          elem.innerHTML = modalHtml;
                        }
                        var btn = document.getElementById("bsModal");
                        btn.click();
                      }
                    });
                    longTapregion.push(longTapRgn);
                  }
                  gestureTimeout = setTimeout(function() {
                    var closeBtn = document.getElementsByClassName("close");
                    closeBtn[0].click();
                    for (let i=0; i < longTapElement.length; i++) {
                      longTapregion.forEach(element => {
                        element.unbind(longTapElement[i]);
                      });
                    }
                    setTimeout(function() {
                      applyGesture();
                    }, 400);
                  }, selectedGesture.time * 1000);
                } else {
                  const panElement = document.getElementsByClassName('region-box');
                  for (let i=0; i < panElement.length; i++) {
                    const panRgn = new ZingTouch.Region(panElement[i]);
                    const pan = new ZingTouch.Pan({
                      threshold: 8
                    });
                    panRgn.bind(panElement[i], pan, function(e) {
                      var elem = document.getElementsByClassName('body-content')[0];
                      elem.innerHTML = '';
                      if (selectedGesture.type == 'text') {
                        var modalHtml = '<div class="content text-center">' +
                        '<h2 style="color: ${`#000000`};text-transform: unset;" class="m-0">'
                        + selectedGesture.text +'</h2>';
                        if (selectedGesture.userActions.length) {
                          modalHtml += '<div class="text-center mt-2">';
                          for (var act of selectedGesture.userActions) {
                            if (act.action == 'buy') {
                              modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                '<img title="Buy" src="https://panaroamer.com/assets/icons/iconmonstr-checkout-9-48.png" alt=""></a>';
                            }
                            if (act.action == 'visit') {
                              modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                '<img title="Visit" src="https://panaroamer.com/assets/icons/backend-website-10.png" alt=""></a>';
                            }
                            if (act.action == 'call') {
                              modalHtml += '<a href="tel:' + act.value + '" class="mr-2">' +
                                  '<img title="Call" src="https://panaroamer.com/assets/icons/iconmonstr-phone-6-48.png" alt=""></a>';
                            }
                            if (act.action == 'text') {
                              modalHtml += '<a href="sms:' + act.value + '" class="mr-2">' +
                                  '<img title="Text" src="https://panaroamer.com/assets/icons/iconmonstr-sms-3-48.png" alt=""></a>';
                            }
                          }
                          modalHtml += '</div>';
                        }
                        modalHtml += '</div>';
                        elem.innerHTML = modalHtml;
                      } else if (selectedGesture.type == 'image') {
                        var modalHtml = '<div class="content text-center">' +
                        '<figure class="filter-' + selectedGesture.gesFilterType + '">' +
                          '<img class="selected-img" src="' + selectedGesture.text + '" alt="">' +
                        '</figure>';
                        if (selectedGesture.userActions.length) {
                          modalHtml += '<div class="text-center mt-2">';
                          for (var act of selectedGesture.userActions) {
                            if (act.action == 'buy') {
                              modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                '<img title="Buy" src="https://panaroamer.com/assets/icons/iconmonstr-checkout-9-48.png" alt=""></a>';
                            }
                            if (act.action == 'visit') {
                              modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                '<img title="Visit" src="https://panaroamer.com/assets/icons/backend-website-10.png" alt=""></a>';
                            }
                            if (act.action == 'call') {
                              modalHtml += '<a href="tel:' + act.value + '" class="mr-2">' +
                                  '<img title="Call" src="https://panaroamer.com/assets/icons/iconmonstr-phone-6-48.png" alt=""></a>';
                            }
                            if (act.action == 'text') {
                              modalHtml += '<a href="sms:' + act.value + '" class="mr-2">' +
                                  '<img title="Text" src="https://panaroamer.com/assets/icons/iconmonstr-sms-3-48.png" alt=""></a>';
                            }
                          }
                          modalHtml += '</div>';
                        }
                        modalHtml += '</div>';
                        elem.innerHTML = modalHtml;
                      } else if(selectedGesture.type == 'video') {
                        var modalHtml = '<div class="content text-center">' +
                        '<video width="400" controls>' +
                          '<source src="' + selectedGesture.text + '" type="video/mp4">' +
                          'Your browser does not support HTML video.' +
                        '</video>';
                        if (selectedGesture.userActions.length) {
                          modalHtml += '<div class="text-center mt-2">';
                          for (var act of selectedGesture.userActions) {
                            if (act.action == 'buy') {
                              modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                '<img title="Buy" src="https://panaroamer.com/assets/icons/iconmonstr-checkout-9-48.png" alt=""></a>';
                            }
                            if (act.action == 'visit') {
                              modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                '<img title="Visit" src="https://panaroamer.com/assets/icons/backend-website-10.png" alt=""></a>';
                            }
                            if (act.action == 'call') {
                              modalHtml += '<a href="tel:' + act.value + '" class="mr-2">' +
                                  '<img title="Call" src="https://panaroamer.com/assets/icons/iconmonstr-phone-6-48.png" alt=""></a>';
                            }
                            if (act.action == 'text') {
                              modalHtml += '<a href="sms:' + act.value + '" class="mr-2">' +
                                  '<img title="Text" src="https://panaroamer.com/assets/icons/iconmonstr-sms-3-48.png" alt=""></a>';
                            }
                          }
                          modalHtml += '</div>';
                        }
                        modalHtml += '</div>';
                        elem.innerHTML = modalHtml;
                      } else if(selectedGesture.type == 'audio') {
                        var modalHtml = '<div class="content audio text-center">' +
                        '<audio controls>' +
                          '<source src="' + selectedGesture.text + '" type="audio/ogg">' +
                          '<source src="' + selectedGesture.text + '" type="audio/mpeg">' +
                          'Your browser does not support the audio element.' +
                        '</audio>';
                        if (selectedGesture.userActions.length) {
                          modalHtml += '<div class="text-center mt-2">';
                          for (var act of selectedGesture.userActions) {
                            if (act.action == 'buy') {
                              modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                '<img title="Buy" src="https://panaroamer.com/assets/icons/iconmonstr-checkout-9-48.png" alt=""></a>';
                            }
                            if (act.action == 'visit') {
                              modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                '<img title="Visit" src="https://panaroamer.com/assets/icons/backend-website-10.png" alt=""></a>';
                            }
                            if (act.action == 'call') {
                              modalHtml += '<a href="tel:' + act.value + '" class="mr-2">' +
                                  '<img title="Call" src="https://panaroamer.com/assets/icons/iconmonstr-phone-6-48.png" alt=""></a>';
                            }
                            if (act.action == 'text') {
                              modalHtml += '<a href="sms:' + act.value + '" class="mr-2">' +
                                  '<img title="Text" src="https://panaroamer.com/assets/icons/iconmonstr-sms-3-48.png" alt=""></a>';
                            }
                          }
                          modalHtml += '</div>';
                        }
                        modalHtml += '</div>';
                        elem.innerHTML = modalHtml;
                      }
                      var btn = document.getElementById("bsModal");
                      btn.click();
                    });
                    panRegion.push(panRgn);
                  }
                  gestureTimeout = setTimeout(function() {
                    var closeBtn = document.getElementsByClassName("close");
                    closeBtn[0].click();
                    for (let i=0; i < panElement.length; i++) {
                      panRegion.forEach(element => {
                        element.unbind(panElement[i]);
                      });
                    }
                    setTimeout(function() {
                      applyGesture();
                    }, 400);
                  }, selectedGesture.time * 1000);
                }
              } else {
                gesIdx = 0;
                setTimeout(() => {
                  applyGesture();
                }, 400);
              }
            } else if(mode === 'All') {
              for (let i = 0; i < regionArr.length; i++) {
                if (regionArr[i].gesture === 'tap') {
                  const tapElement = document.getElementsByClassName('region-box' + i);
                  for (let j=0; j < tapElement.length; j++) {
                    const tapRgn = new ZingTouch.Region(tapElement[j]);
                    tapRgn.bind(tapElement[j], 'tap', function(e) {
                      if (e) {
                        let idx;
                        const str = e.target.className.split(' ');
                        for (const cls of str) {
                          if (cls.includes('region-box')) {
                            idx = JSON.parse(cls.split('box')[1]);
                          }
                        }
                        // $(document).on('click','#btnPrepend',function(){ });
                        selectedGesture = regionArr[idx];
                        var elem = document.getElementsByClassName('body-content')[0];
                        elem.innerHTML = '';
                        if (selectedGesture.type == 'text') {
                          var modalHtml = '<div class="content text-center">' +
                          '<h2 style="color: ${`#000000`};text-transform: unset;" class="m-0">'
                          + selectedGesture.text +'</h2>';
                          if (selectedGesture.userActions.length) {
                            modalHtml += '<div class="text-center mt-2">';
                            for (var act of selectedGesture.userActions) {
                              if (act.action == 'buy') {
                                modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                  '<img title="Buy" src="https://panaroamer.com/assets/icons/iconmonstr-checkout-9-48.png" alt=""></a>';
                              }
                              if (act.action == 'visit') {
                                modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                  '<img title="Visit" src="https://panaroamer.com/assets/icons/backend-website-10.png" alt=""></a>';
                              }
                              if (act.action == 'call') {
                                modalHtml += '<a href="tel:' + act.value + '" class="mr-2">' +
                                    '<img title="Call" src="https://panaroamer.com/assets/icons/iconmonstr-phone-6-48.png" alt=""></a>';
                              }
                              if (act.action == 'text') {
                                modalHtml += '<a href="sms:' + act.value + '" class="mr-2">' +
                                    '<img title="Text" src="https://panaroamer.com/assets/icons/iconmonstr-sms-3-48.png" alt=""></a>';
                              }
                            }
                            modalHtml += '</div>';
                          }
                          modalHtml += '</div>';
                          elem.innerHTML = modalHtml;
                        } else if (selectedGesture.type == 'image') {
                          var modalHtml = '<div class="content text-center">' +
                          '<figure class="filter-' + selectedGesture.gesFilterType + '">' +
                            '<img class="selected-img" src="' + selectedGesture.text + '" alt="">' +
                          '</figure>';
                          if (selectedGesture.userActions.length) {
                            modalHtml += '<div class="text-center mt-2">';
                            for (var act of selectedGesture.userActions) {
                              if (act.action == 'buy') {
                                modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                  '<img title="Buy" src="https://panaroamer.com/assets/icons/iconmonstr-checkout-9-48.png" alt=""></a>';
                              }
                              if (act.action == 'visit') {
                                modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                  '<img title="Visit" src="https://panaroamer.com/assets/icons/backend-website-10.png" alt=""></a>';
                              }
                              if (act.action == 'call') {
                                modalHtml += '<a href="tel:' + act.value + '" class="mr-2">' +
                                    '<img title="Call" src="https://panaroamer.com/assets/icons/iconmonstr-phone-6-48.png" alt=""></a>';
                              }
                              if (act.action == 'text') {
                                modalHtml += '<a href="sms:' + act.value + '" class="mr-2">' +
                                    '<img title="Text" src="https://panaroamer.com/assets/icons/iconmonstr-sms-3-48.png" alt=""></a>';
                              }
                            }
                            modalHtml += '</div>';
                          }
                          modalHtml += '</div>';
                          elem.innerHTML = modalHtml;
                        } else if(selectedGesture.type == 'video') {
                          var modalHtml = '<div class="content text-center">' +
                          '<video width="400" controls>' +
                            '<source src="' + selectedGesture.text + '" type="video/mp4">' +
                            'Your browser does not support HTML video.' +
                          '</video>';
                          if (selectedGesture.userActions.length) {
                            modalHtml += '<div class="text-center mt-2">';
                            for (var act of selectedGesture.userActions) {
                              if (act.action == 'buy') {
                                modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                  '<img title="Buy" src="https://panaroamer.com/assets/icons/iconmonstr-checkout-9-48.png" alt=""></a>';
                              }
                              if (act.action == 'visit') {
                                modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                  '<img title="Visit" src="https://panaroamer.com/assets/icons/backend-website-10.png" alt=""></a>';
                              }
                              if (act.action == 'call') {
                                modalHtml += '<a href="tel:' + act.value + '" class="mr-2">' +
                                    '<img title="Call" src="https://panaroamer.com/assets/icons/iconmonstr-phone-6-48.png" alt=""></a>';
                              }
                              if (act.action == 'text') {
                                modalHtml += '<a href="sms:' + act.value + '" class="mr-2">' +
                                    '<img title="Text" src="https://panaroamer.com/assets/icons/iconmonstr-sms-3-48.png" alt=""></a>';
                              }
                            }
                            modalHtml += '</div>';
                          }
                          modalHtml += '</div>';
                          elem.innerHTML = modalHtml;
                        } else if(selectedGesture.type == 'audio') {
                          var modalHtml = '<div class="content audio text-center">' +
                          '<audio controls>' +
                            '<source src="' + selectedGesture.text + '" type="audio/ogg">' +
                            '<source src="' + selectedGesture.text + '" type="audio/mpeg">' +
                            'Your browser does not support the audio element.' +
                          '</audio>';
                          if (selectedGesture.userActions.length) {
                            modalHtml += '<div class="text-center mt-2">';
                            for (var act of selectedGesture.userActions) {
                              if (act.action == 'buy') {
                                modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                  '<img title="Buy" src="https://panaroamer.com/assets/icons/iconmonstr-checkout-9-48.png" alt=""></a>';
                              }
                              if (act.action == 'visit') {
                                modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                  '<img title="Visit" src="https://panaroamer.com/assets/icons/backend-website-10.png" alt=""></a>';
                              }
                              if (act.action == 'call') {
                                modalHtml += '<a href="tel:' + act.value + '" class="mr-2">' +
                                    '<img title="Call" src="https://panaroamer.com/assets/icons/iconmonstr-phone-6-48.png" alt=""></a>';
                              }
                              if (act.action == 'text') {
                                modalHtml += '<a href="sms:' + act.value + '" class="mr-2">' +
                                    '<img title="Text" src="https://panaroamer.com/assets/icons/iconmonstr-sms-3-48.png" alt=""></a>';
                              }
                            }
                            modalHtml += '</div>';
                          }
                          modalHtml += '</div>';
                          elem.innerHTML = modalHtml;
                        }
                        var btn = document.getElementById("bsModal");
                        btn.click();
                      }
                    });
                    tapregion.push(tapRgn);
                  }
                } else if (regionArr[i].gesture === 'long-tap') {
                  const longTapElement = document.getElementsByClassName('region-box' + i);
                  for (let j=0; j < longTapElement.length; j++) {
                    const longTapRgn = new ZingTouch.Region(longTapElement[j]);
                    const longtap = new ZingTouch.Tap({
                      maxDelay: 4000,
                      numInputs: 1,
                      tolerance: 1000
                    });
                    longTapRgn.bind(longTapElement[j], longtap, function(e) {
                      if (e && e.detail.interval > 300) {
                        let idx;
                        const str = e.target.className.split(' ');
                        for (const cls of str) {
                          if (cls.includes('region-box')) {
                            idx = JSON.parse(cls.split('box')[1]);
                          }
                        }
                        selectedGesture = regionArr[idx];
                        var elem = document.getElementsByClassName('body-content')[0];
                        elem.innerHTML = '';
                        if (selectedGesture.type == 'text') {
                          var modalHtml = '<div class="content text-center">' +
                          '<h2 style="color: ${`#000000`};text-transform: unset;" class="m-0">'
                          + selectedGesture.text +'</h2>';
                          if (selectedGesture.userActions.length) {
                            modalHtml += '<div class="text-center mt-2">';
                            for (var act of selectedGesture.userActions) {
                              if (act.action == 'buy') {
                                modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                  '<img title="Buy" src="https://panaroamer.com/assets/icons/iconmonstr-checkout-9-48.png" alt=""></a>';
                              }
                              if (act.action == 'visit') {
                                modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                  '<img title="Visit" src="https://panaroamer.com/assets/icons/backend-website-10.png" alt=""></a>';
                              }
                              if (act.action == 'call') {
                                modalHtml += '<a href="tel:' + act.value + '" class="mr-2">' +
                                    '<img title="Call" src="https://panaroamer.com/assets/icons/iconmonstr-phone-6-48.png" alt=""></a>';
                              }
                              if (act.action == 'text') {
                                modalHtml += '<a href="sms:' + act.value + '" class="mr-2">' +
                                    '<img title="Text" src="https://panaroamer.com/assets/icons/iconmonstr-sms-3-48.png" alt=""></a>';
                              }
                            }
                            modalHtml += '</div>';
                          }
                          modalHtml += '</div>';
                          elem.innerHTML = modalHtml;
                        } else if (selectedGesture.type == 'image') {
                          var modalHtml = '<div class="content text-center">' +
                          '<figure class="filter-' + selectedGesture.gesFilterType + '">' +
                            '<img class="selected-img" src="' + selectedGesture.text + '" alt="">' +
                          '</figure>';
                          if (selectedGesture.userActions.length) {
                            modalHtml += '<div class="text-center mt-2">';
                            for (var act of selectedGesture.userActions) {
                              if (act.action == 'buy') {
                                modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                  '<img title="Buy" src="https://panaroamer.com/assets/icons/iconmonstr-checkout-9-48.png" alt=""></a>';
                              }
                              if (act.action == 'visit') {
                                modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                  '<img title="Visit" src="https://panaroamer.com/assets/icons/backend-website-10.png" alt=""></a>';
                              }
                              if (act.action == 'call') {
                                modalHtml += '<a href="tel:' + act.value + '" class="mr-2">' +
                                    '<img title="Call" src="https://panaroamer.com/assets/icons/iconmonstr-phone-6-48.png" alt=""></a>';
                              }
                              if (act.action == 'text') {
                                modalHtml += '<a href="sms:' + act.value + '" class="mr-2">' +
                                    '<img title="Text" src="https://panaroamer.com/assets/icons/iconmonstr-sms-3-48.png" alt=""></a>';
                              }
                            }
                            modalHtml += '</div>';
                          }
                          modalHtml += '</div>';
                          elem.innerHTML = modalHtml;
                        } else if(selectedGesture.type == 'video') {
                          var modalHtml = '<div class="content text-center">' +
                          '<video width="400" controls>' +
                            '<source src="' + selectedGesture.text + '" type="video/mp4">' +
                            'Your browser does not support HTML video.' +
                          '</video>';
                          if (selectedGesture.userActions.length) {
                            modalHtml += '<div class="text-center mt-2">';
                            for (var act of selectedGesture.userActions) {
                              if (act.action == 'buy') {
                                modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                  '<img title="Buy" src="https://panaroamer.com/assets/icons/iconmonstr-checkout-9-48.png" alt=""></a>';
                              }
                              if (act.action == 'visit') {
                                modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                  '<img title="Visit" src="https://panaroamer.com/assets/icons/backend-website-10.png" alt=""></a>';
                              }
                              if (act.action == 'call') {
                                modalHtml += '<a href="tel:' + act.value + '" class="mr-2">' +
                                    '<img title="Call" src="https://panaroamer.com/assets/icons/iconmonstr-phone-6-48.png" alt=""></a>';
                              }
                              if (act.action == 'text') {
                                modalHtml += '<a href="sms:' + act.value + '" class="mr-2">' +
                                    '<img title="Text" src="https://panaroamer.com/assets/icons/iconmonstr-sms-3-48.png" alt=""></a>';
                              }
                            }
                            modalHtml += '</div>';
                          }
                          modalHtml += '</div>';
                          elem.innerHTML = modalHtml;
                        } else if(selectedGesture.type == 'audio') {
                          var modalHtml = '<div class="content audio text-center">' +
                          '<audio controls>' +
                            '<source src="' + selectedGesture.text + '" type="audio/ogg">' +
                            '<source src="' + selectedGesture.text + '" type="audio/mpeg">' +
                            'Your browser does not support the audio element.' +
                          '</audio>';
                          if (selectedGesture.userActions.length) {
                            modalHtml += '<div class="text-center mt-2">';
                            for (var act of selectedGesture.userActions) {
                              if (act.action == 'buy') {
                                modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                  '<img title="Buy" src="https://panaroamer.com/assets/icons/iconmonstr-checkout-9-48.png" alt=""></a>';
                              }
                              if (act.action == 'visit') {
                                modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                  '<img title="Visit" src="https://panaroamer.com/assets/icons/backend-website-10.png" alt=""></a>';
                              }
                              if (act.action == 'call') {
                                modalHtml += '<a href="tel:' + act.value + '" class="mr-2">' +
                                    '<img title="Call" src="https://panaroamer.com/assets/icons/iconmonstr-phone-6-48.png" alt=""></a>';
                              }
                              if (act.action == 'text') {
                                modalHtml += '<a href="sms:' + act.value + '" class="mr-2">' +
                                    '<img title="Text" src="https://panaroamer.com/assets/icons/iconmonstr-sms-3-48.png" alt=""></a>';
                              }
                            }
                            modalHtml += '</div>';
                          }
                          modalHtml += '</div>';
                          elem.innerHTML = modalHtml;
                        }
                        var btn = document.getElementById("bsModal");
                        btn.click();
                      }
                    });
                    longTapregion.push(longTapRgn);
                  }
                } else {
                  const panElement = document.getElementsByClassName('region-box' + i);
                  for (let j=0; j < panElement.length; j++) {
                    const panRgn = new ZingTouch.Region(panElement[j]);
                    const pan = new ZingTouch.Pan({
                      threshold: 8
                    });
                    panRgn.bind(panElement[j], pan, function(e) {
                      let idx;
                      const str = e.target.className.split(' ');
                      for (const cls of str) {
                        if (cls.includes('region-box')) {
                          idx = JSON.parse(cls.split('box')[1]);
                        }
                      }
                      selectedGesture = regionArr[idx];
                      var elem = document.getElementsByClassName('body-content')[0];
                      elem.innerHTML = '';
                      if (selectedGesture.type == 'text') {
                        var modalHtml = '<div class="content text-center">' +
                        '<h2 style="color: ${`#000000`};text-transform: unset;" class="m-0">'
                        + selectedGesture.text +'</h2>';
                        if (selectedGesture.userActions.length) {
                          modalHtml += '<div class="text-center mt-2">';
                          for (var act of selectedGesture.userActions) {
                            if (act.action == 'buy') {
                              modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                '<img title="Buy" src="https://panaroamer.com/assets/icons/iconmonstr-checkout-9-48.png" alt=""></a>';
                            }
                            if (act.action == 'visit') {
                              modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                '<img title="Visit" src="https://panaroamer.com/assets/icons/backend-website-10.png" alt=""></a>';
                            }
                            if (act.action == 'call') {
                              modalHtml += '<a href="tel:' + act.value + '" class="mr-2">' +
                                  '<img title="Call" src="https://panaroamer.com/assets/icons/iconmonstr-phone-6-48.png" alt=""></a>';
                            }
                            if (act.action == 'text') {
                              modalHtml += '<a href="sms:' + act.value + '" class="mr-2">' +
                                  '<img title="Text" src="https://panaroamer.com/assets/icons/iconmonstr-sms-3-48.png" alt=""></a>';
                            }
                          }
                          modalHtml += '</div>';
                        }
                        modalHtml += '</div>';
                        elem.innerHTML = modalHtml;
                      } else if (selectedGesture.type == 'image') {
                        var modalHtml = '<div class="content text-center">' +
                        '<figure class="filter-' + selectedGesture.gesFilterType + '">' +
                          '<img class="selected-img" src="' + selectedGesture.text + '" alt="">' +
                        '</figure>';
                        if (selectedGesture.userActions.length) {
                          modalHtml += '<div class="text-center mt-2">';
                          for (var act of selectedGesture.userActions) {
                            if (act.action == 'buy') {
                              modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                '<img title="Buy" src="https://panaroamer.com/assets/icons/iconmonstr-checkout-9-48.png" alt=""></a>';
                            }
                            if (act.action == 'visit') {
                              modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                '<img title="Visit" src="https://panaroamer.com/assets/icons/backend-website-10.png" alt=""></a>';
                            }
                            if (act.action == 'call') {
                              modalHtml += '<a href="tel:' + act.value + '" class="mr-2">' +
                                  '<img title="Call" src="https://panaroamer.com/assets/icons/iconmonstr-phone-6-48.png" alt=""></a>';
                            }
                            if (act.action == 'text') {
                              modalHtml += '<a href="sms:' + act.value + '" class="mr-2">' +
                                  '<img title="Text" src="https://panaroamer.com/assets/icons/iconmonstr-sms-3-48.png" alt=""></a>';
                            }
                          }
                          modalHtml += '</div>';
                        }
                        modalHtml += '</div>';
                        elem.innerHTML = modalHtml;
                      } else if(selectedGesture.type == 'video') {
                        var modalHtml = '<div class="content text-center">' +
                        '<video width="400" controls>' +
                          '<source src="' + selectedGesture.text + '" type="video/mp4">' +
                          'Your browser does not support HTML video.' +
                        '</video>';
                        if (selectedGesture.userActions.length) {
                          modalHtml += '<div class="text-center mt-2">';
                          for (var act of selectedGesture.userActions) {
                            if (act.action == 'buy') {
                              modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                '<img title="Buy" src="https://panaroamer.com/assets/icons/iconmonstr-checkout-9-48.png" alt=""></a>';
                            }
                            if (act.action == 'visit') {
                              modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                '<img title="Visit" src="https://panaroamer.com/assets/icons/backend-website-10.png" alt=""></a>';
                            }
                            if (act.action == 'call') {
                              modalHtml += '<a href="tel:' + act.value + '" class="mr-2">' +
                                  '<img title="Call" src="https://panaroamer.com/assets/icons/iconmonstr-phone-6-48.png" alt=""></a>';
                            }
                            if (act.action == 'text') {
                              modalHtml += '<a href="sms:' + act.value + '" class="mr-2">' +
                                  '<img title="Text" src="https://panaroamer.com/assets/icons/iconmonstr-sms-3-48.png" alt=""></a>';
                            }
                          }
                          modalHtml += '</div>';
                        }
                        modalHtml += '</div>';
                        elem.innerHTML = modalHtml;
                      } else if(selectedGesture.type == 'audio') {
                          var modalHtml = '<div class="content audio text-center">' +
                          '<audio controls>' +
                            '<source src="' + selectedGesture.text + '" type="audio/ogg">' +
                            '<source src="' + selectedGesture.text + '" type="audio/mpeg">' +
                            'Your browser does not support the audio element.' +
                          '</audio>';
                          if (selectedGesture.userActions.length) {
                            modalHtml += '<div class="text-center mt-2">';
                            for (var act of selectedGesture.userActions) {
                              if (act.action == 'buy') {
                                modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                  '<img title="Buy" src="https://panaroamer.com/assets/icons/iconmonstr-checkout-9-48.png" alt=""></a>';
                              }
                              if (act.action == 'visit') {
                                modalHtml += '<a href="' + act.value + '" class="mr-2" target="_blank">' +
                                  '<img title="Visit" src="https://panaroamer.com/assets/icons/backend-website-10.png" alt=""></a>';
                              }
                              if (act.action == 'call') {
                                modalHtml += '<a href="tel:' + act.value + '" class="mr-2">' +
                                    '<img title="Call" src="https://panaroamer.com/assets/icons/iconmonstr-phone-6-48.png" alt=""></a>';
                              }
                              if (act.action == 'text') {
                                modalHtml += '<a href="sms:' + act.value + '" class="mr-2">' +
                                    '<img title="Text" src="https://panaroamer.com/assets/icons/iconmonstr-sms-3-48.png" alt=""></a>';
                              }
                            }
                            modalHtml += '</div>';
                          }
                          modalHtml += '</div>';
                          elem.innerHTML = modalHtml;
                      }
                      var btn = document.getElementById("bsModal");
                      btn.click();
                    });
                    panRegion.push(panRgn);
                  }
                }

              }
            }
          }
      </script>

      </body>

      </html>

      `;

    localStorage.removeItem('latlong');
    this.createPwaHtml();
    console.log(ipHtml);
  }

  createPwaHtml(): any {
    const info = JSON.parse(localStorage.getItem('info'));
    const title = info.name ? info.name : 'Senic-beauty | Jhon Doe';
    const description = info.description ? info.description : 'Presentation related to Senic-beauty';
    const coverImage = info.coverImage ? info.coverImage : 'http://ippub.panaroamer.com/fifth/presentations/media/thumbnail.jpg';
    const pwaHtml = `

    <!DOCTYPE html>
    <html>
        <head>
        <meta property="og:title" content="${title} | Jhon Doe" />
        <meta property="og:image" content="${coverImage}"/>
        <link rel="shortcut icon" type="image/png" href="presentations/media/thumbnail.jpg">
        <meta property="og:description"
            content="${description}" />
        <title>${title} | Jhon Doe</title>
        </head>
        <body>
            <p>Loading...</p>

                <script>
                    window.location = "https://panaroamer.com/play?url=http://ippub.panaroamer.com/${title}/index.html";
                </script>
             </body>
    </html>

    `;

    // console.log(pwaHtml);
  }

  getPosition(emojiObj): any {
    const obj: any = {};
    obj.x = (emojiObj[0] * 25) / 14;
    obj.y = (emojiObj[1] * 16.0714) / 9;
    console.log(obj);
    return obj.x.toFixed(4) + '% ' + obj.y.toFixed(4) + '%';
  }

}
