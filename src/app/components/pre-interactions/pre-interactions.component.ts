import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { CarouselComponent } from 'ngx-owl-carousel-o';
import { InfiniteGrid, PreInteractionInterface } from 'src/app/interfaces/componets';
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-pre-interactions',
  templateUrl: './pre-interactions.component.html',
  styleUrls: ['./pre-interactions.component.scss']
})
export class PreInteractionsComponent implements OnInit, AfterViewInit {

  preInteractions: PreInteractionInterface = {
    tabs: ['Category A', 'Category B', 'Category C']
  };
  infiniteGrid: InfiniteGrid = {
    throttle: 300,
    scrollDistance: 1,
    scrollUpDistance: 2,
    customOptions: {
      loop: false,
      margin: 12,
      mouseDrag: false,
      touchDrag: false,
      pullDrag: false,
      dots: false,
      navSpeed: 400,
      slideBy: 'page',
      navText: [
        '<i class="fa fa-chevron-left" aria-hidden="true"></i>',
        '<i class="fa fa-chevron-right" aria-hidden="true"></i>'
      ],
      responsive: {
        0: {
          items: 1,
          stagePadding: 0,
        },
        400: {
          items: 2,
        },
        500: {
          items: 3,
          stagePadding: 50,
        },
        700: {
          items: 3,
          stagePadding: 50,
        },
        768: {
          items: 4,
        },
        800: {
          items: 4,
        },
        820: {
          items: 4,
        },
        1200: {
          items: 5
        }
      },
      nav: true
    }
  };

  folderAA = [{
    interactions: ['interaction AA1', 'interaction AA2', 'interaction AA3', 'interaction AA4', 'interaction AA5', 'interaction AA6', 'interaction AA7', 'interaction AA8', 'interaction AA9', 'interaction AA10']
  }];
  folderBB = [{
    interactions: ['interaction BB1', 'interaction BB2', 'interaction BB3', 'interaction BB4', 'interaction BB5', 'interaction BB6', 'interaction BB7', 'interaction BB8', 'interaction BB9', 'interaction BB10']
  }];
  folderA = [{
    interactions: ['interaction A1', 'interaction A2', 'interaction A3', 'interaction A4', 'interaction A5', 'interaction A6', 'interaction A7', 'interaction A8', 'interaction A9', 'interaction A10'],
    folders: [{ name: 'folderAA', data: this.folderAA }]
  }];
  folderB = [{
    interactions: ['interaction B1', 'interaction B2', 'interaction B3', 'interaction B4', 'interaction B5', 'interaction B6', 'interaction B7', 'interaction B8', 'interaction B9', 'interaction B10'],
    folders: [{ name: 'folderBB', data: this.folderBB }]
  }];
  root = {
    interactions: ['interaction 1', 'interaction 2', 'interaction 3', 'interaction 4', 'interaction 5', 'interaction 6', 'interaction 7', 'interaction 8', 'interaction 9', 'interaction 10'],
    folders: [{ name: 'folderA', data: this.folderA }, { name: 'folderB', data: this.folderB }]
  };

  @ViewChild('owlElement', { static: true }) owlElement: CarouselComponent;

  constructor(private router: Router, private route: ActivatedRoute, public utility: UtilityService) {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    window.dispatchEvent(new Event('resize'));
  }

  onTabChanged(): void {
    window.dispatchEvent(new Event('resize'));
  }

}


