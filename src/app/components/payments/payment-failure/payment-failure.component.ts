import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-payment-failure',
  templateUrl: './payment-failure.component.html',
  styleUrls: ['./payment-failure.component.scss']
})
export class PaymentFailureComponent implements OnInit {

  constructor(private toast: ToastrService, public router: Router) { }

  ngOnInit(): void {
    this.toast.error('Payment Failed');
    this.router.navigate(['/preview']);
  }
}
