import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-payment-success',
  templateUrl: './payment-success.component.html',
  styleUrls: ['./payment-success.component.css']
})
export class PaymentSuccessComponent implements OnInit {

  constructor(private toast: ToastrService, public router: Router, private translateSevice: TranslateService) { }

  ngOnInit(): void {
    this.translateSevice.get('payment.PaymentSuccess').subscribe(res => {
      this.toast.success(res, '');
    });
    this.router.navigate(['/preview']);
  }
}
