import { Component, OnInit } from '@angular/core';
import { UtilityService } from 'src/app/services/utility.service';
import { Router } from '@angular/router';
declare var stripe: any;

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent implements OnInit {
  productList = [];
  constructor(
    public utility: UtilityService,
    public router: Router) { }

  ngOnInit(): void {
    this.getProducts();
  }

  pay(product): void {
    stripe.redirectToCheckout({
      lineItems: [
        // Replace with the ID of your price
        {price: product.id, quantity: 1}
      ],
      mode: product.recurring ? 'subscription' : 'payment',
      successUrl: 'http://' + window.location.host + '/payments/success',
      cancelUrl: 'http://' + window.location.host + '/payments/failure',
    }).then((result) => {
      console.log(result);
      // If `redirectToCheckout` fails due to a browser or network
      // error, display the localized error message to your customer
      // using `result.error.message`.
    });
  }

  getProducts(): void {
    this.utility.setLoader(true);
    this.utility.getProducts().subscribe(res => {
      if (res) {
        this.getPrices(res.data);
      }
    });
  }

  getPrices(products): void {
    this.utility.getPrices().subscribe(res => {
      if (res) {
        const prices = res.data;
        this.productList = products.map(item => {
          const obj = prices.find(o => o.product === item.id);
          return { ...item, ...obj };
        });
        this.utility.setLoader(false);
      }
    });
  }

  cancel() {
    this.router.navigate(['/gallery']);
  }
}
