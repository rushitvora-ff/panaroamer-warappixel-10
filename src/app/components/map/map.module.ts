import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MapRoutingModule } from './map-routing.module';
import { MapComponent } from './map.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DemoMaterialModule } from 'src/app/demo-material-module';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { MatExpansionModule } from '@angular/material/expansion';

@NgModule({
  declarations: [MapComponent],
  imports: [
    CommonModule,
    DemoMaterialModule,
    FlexLayoutModule,
    SharedModule,
    LeafletModule.forRoot(),
    MapRoutingModule,
    MatExpansionModule
  ]
})
export class MapModule { }
