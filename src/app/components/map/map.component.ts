import { Component, OnInit, Inject, HostListener, NgZone, ElementRef, OnDestroy } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { UtilityService } from 'src/app/services/utility.service';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { latLng, Layer, marker, icon, Popup } from 'leaflet';
import { FormGroup, FormBuilder, FormControl, FormArray } from '@angular/forms';
import { GlobalAuthenticationContext } from '@oneb/authentication-connector-api';
import * as L from 'leaflet';
declare module 'leaflet' {
  namespace control {
    function fullscreen(v: any): any;
  }
}
import '../../../../node_modules/leaflet.fullscreen/Control.FullScreen.js';
import { MatDialog } from '@angular/material/dialog';
import { IpInfoDialogComponent } from 'src/app/shared/dialogs/ip-info-dialog/ip-info-dialog.component.js';
import { ShareDialogComponent } from 'src/app/shared/dialogs/share-dialog/share-dialog.component.js';
import { MapComponentInterface } from 'src/app/interfaces/componets.js';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, OnDestroy {
  mapInterface: MapComponentInterface = {
    landscape: false,
    starred: [],
    optionsMsg: [
      'Cool! Check it out!',
      'This is woke! You have to see it!',
      'Wow! Take a look!',
      'Let me know what you think!',
      'Do you like it?'
    ],
    isParams: false,
    isFullscreen: false,
    data: this.utility.interactionsArray,
    categories: ['Default', 'Infrastructure', 'Nature', 'Happy Faces', 'Store', 'Electronics', 'Food'],
    markers: [],
    selectPath: false,
    showEachPath: false,
    polyLinesGroup: [],
    isLoggedIn: GlobalAuthenticationContext.hasSignedInUserDetails() && GlobalAuthenticationContext.getSignedInUserDetails().email
  };
  // landscape = false;
  // options: any;
  // scrWidth: number;
  // starred = [];
  // optionsMsg = [
  //   'Cool! Check it out!',
  //   'This is woke! You have to see it!',
  //   'Wow! Take a look!',
  //   'Let me know what you think!',
  //   'Do you like it?'
  // ];
  // isParams = false;
  // isFullscreen = false;
  // shouldShow = false;
  // center;
  // data = this.utility.interactionsArray;
  // selectedItem;
  // currentData;
  // optionsSpec;
  // categories = ['Default', 'Infrastructure', 'Nature', 'Happy Faces', 'Store', 'Electronics', 'Food'];
  // markers: any[] = [];
  // map;
  // timeoutVal;
  // timeoutMapVal;
  // latlng: any;
  // myForm: FormGroup;
  // selectPath = false;
  // showEachPath = false;
  // polyLines;
  // polyLinesGroup = [];
  // isLoggedIn = GlobalAuthenticationContext.hasSignedInUserDetails() && GlobalAuthenticationContext.getSignedInUserDetails().email;

  @HostListener('window:resize', ['$event'])
  getScreenSize(): void {
    this.mapInterface.scrWidth = window.innerWidth;
  }

  constructor(
    public ngZone: NgZone,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog,
    private elementRef: ElementRef,
    public utility: UtilityService,
    private translateSevice: TranslateService,
    @Inject(DOCUMENT) private document: Document,
  ) {
    this.getScreenSize();
    if (localStorage.getItem('starred') != null) {
      this.mapInterface.starred = JSON.parse(localStorage.getItem('starred'));
    }
    this.translateSevice.get(
      ['shareIp.CoolCheckItOut', 'shareIp.ThisIsWoke', 'shareIp.WowTakeLook', 'shareIp.LetMeKnow', 'shareIp.DoYouLikeIt']
    ).subscribe((res: any[]) => {
      this.mapInterface.optionsMsg = Object.keys(res).map((key) => res[key]);
    });
    this.route.queryParams.subscribe(params => {
      if (params.lat && params.long) {
        this.mapInterface.isParams = true;
        const optionsSpec = this.utility.getLeafletMapOptions();
        this.mapInterface.center = latLng(params.lat, params.long);
        this.mapInterface.options = this.utility.setLeafletMapOptions(
          optionsSpec.zoom, this.mapInterface.center, optionsSpec.layers[0].url);
        for (const item of this.mapInterface.data) {
          if (item.name === params.type) {
            this.mapInterface.selectedItem = item;
          }
        }
      }
    });
  }

  ngOnInit(isSetPath?: boolean): void {
    this.mapInterface.myForm = this.fb.group({
      categories: this.fb.array([])
    });

    this.utility.getCoordinates().subscribe((data: any) => {
      this.mapInterface.currentData = data;
      this.mapInterface.optionsSpec = this.utility.getLeafletMapOptions();
      this.onSelectAll(true);

      if (this.utility.conversationObj.mapData) {
        const optionsSpec = this.utility.getLeafletMapOptions();
        if (!isSetPath) {
          this.mapInterface.center = latLng(this.utility.conversationObj.mapData.location.lat, this.utility.conversationObj.mapData.location.lng);
        }
        this.mapInterface.options = this.utility.setLeafletMapOptions(
          optionsSpec.zoom, this.mapInterface.center, optionsSpec.layers[0].url);
        for (const item of this.mapInterface.data) {
          if (item.name === this.utility.conversationObj.mapData.name) {
            this.mapInterface.selectedItem = item;
          }
        }
        for (const category of this.mapInterface.categories) {
          this.onChange(category, true);
        }
      } else if (!this.utility.changeRoute) {
        const optionsSpec = this.utility.getLeafletMapOptions();
        if (!isSetPath) {
          this.mapInterface.center = latLng(this.utility.selectedMapObj.location.lat, this.utility.selectedMapObj.location.lng);
        }
        this.mapInterface.options = this.utility.setLeafletMapOptions(
          optionsSpec.zoom, this.mapInterface.center, optionsSpec.layers[0].url);
        for (const item of this.mapInterface.data) {
          if (item.name === this.utility.selectedMapObj.name) {
            this.mapInterface.selectedItem = item;
          }
        }
        for (const category of this.mapInterface.categories) {
          this.onChange(category, true);
        }
        this.mapInterface.markers.push(this.utility.setCurrentLocMarker(data.latitude, data.longitude));
      } else if (data.latitude !== null && data.longitude !== null) {
        if (data.type) {
          // this.redirectMap(data);
          this.mapInterface.isParams = true;
          const optionsSpec = this.utility.getLeafletMapOptions();
          if (!isSetPath) {
            this.mapInterface.center = latLng(data.latitude, data.longitude);
          }
          this.mapInterface.options = this.utility.setLeafletMapOptions(
            optionsSpec.zoom, this.mapInterface.center, optionsSpec.layers[0].url);
          for (const item of this.mapInterface.data) {
            if (item.name === data.type) {
              this.mapInterface.selectedItem = item;
              for (const category of this.mapInterface.categories) {
                this.onChange(category, true);
              }
            }
          }
        } else {
          this.mapInterface.latlng = { lat: data.latitude, lng: data.longitude };
          if (!isSetPath) {
            this.mapInterface.center = latLng(this.mapInterface.latlng);
          }
          if (!this.mapInterface.isParams) {
            this.mapInterface.options = this.utility.setLeafletMapOptions(
              this.mapInterface.optionsSpec.zoom, this.mapInterface.center, this.mapInterface.optionsSpec.layers[0].url);
          }
          this.mapInterface.markers.push(this.utility.setCurrentLocMarker(data.latitude, data.longitude));
        }
      } else {
        this.mapInterface.latlng = { lat: 40.717079, lng: -74.008026 };
        if (!this.mapInterface.isParams) {
          this.mapInterface.center = latLng(40.717079, -74.008026);
          this.mapInterface.options = this.utility.setLeafletMapOptions(
            this.mapInterface.optionsSpec.zoom, this.mapInterface.center, this.mapInterface.optionsSpec.layers[0].url);
        }
      }
    });
  }

  ngOnDestroy(): void {
    this.utility.isFullscreen = false;
  }

  onSelectAll(checked): void {
    const catFormArray = this.mapInterface.myForm.controls.categories as FormArray;
    this.clearFormArray(catFormArray);
    for (const cat of this.mapInterface.categories) {
      this.onChange(cat, checked);
    }
  }

  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  }

  onChange(category: string, isChecked): void {
    const catFormArray = this.mapInterface.myForm.controls.categories as FormArray;
    if (isChecked) {
      catFormArray.push(new FormControl(category));
      for (let i = 0; i < this.mapInterface.data.length; i++) {
        if (this.mapInterface.data[i].category === category && this.mapInterface.data[i].location) {
          const lat = this.mapInterface.data[i].location.lat;
          const long = this.mapInterface.data[i].location.lng;
          const idx = this.mapInterface.markers.findIndex(mark => mark._latlng.lat === lat && mark._latlng.lng === long);
          if (idx === -1) {
            this.mapInterface.markers.push(
              this.setLeafletMapMarker(this.mapInterface.data[i].location.lat,
                this.mapInterface.data[i].location.lng, this.mapInterface.data[i].name,
                this.mapInterface.data[i].url, 'map' + i
              )
            );
          }
        }
      }
    } else {
      const index = catFormArray.controls.findIndex(x => x.value === category);
      catFormArray.removeAt(index);
      for (const item of this.mapInterface.data) {
        if (item.category === category) {
          if (this.mapInterface.markers.length) {
            for (let i = 0; i < this.mapInterface.markers.length; i++) {
              const lat = item.location.lat;
              const long = item.location.lng;
              if (this.mapInterface.markers[i]._latlng.lat === lat && this.mapInterface.markers[i]._latlng.lng === long) {
                this.mapInterface.markers.splice(i, 1);
              }
            }
          }
        }
      }
    }
  }

  setLeafletMapMarker(lat, long, name?: string, url?: string, id?: string): any {
    const nm = name.replace(' ', '-');
    let src;
    if (this.mapInterface.isLoggedIn) {
      src = 'https://panaroamer-theming-test.s3.us-east-2.amazonaws.com/Panaroamer/assets/Default/heart-normal.png';
    } else {
      // src = "assets/icons/heart-disabled.png";
      src = 'https://panaroamer-theming-test.s3.us-east-2.amazonaws.com/Panaroamer/assets/Default/heart-disabled.png';
    }
    if (this.mapInterface.starred.length) {
      for (const item of this.mapInterface.starred) {
        if (item.name === name) {
          src = 'https://panaroamer-theming-test.s3.us-east-2.amazonaws.com/Panaroamer/assets/Default/heart-fill.png';
        }
      }
    }
    const html = `<a id ="` + id + `"><img  src="` +
      'https://panaroamer-theming-test.s3.us-east-2.amazonaws.com/Panaroamer/assets/Default/play.png' +
      `" class="play-icn"></a><a id="` + nm +
      `"><img style="margin-left:5px; width:25px; height:25px" src="` +
      'https://panaroamer-theming-test.s3.us-east-2.amazonaws.com/Panaroamer/assets/Default/iconmonstr-share.png' + `" class="share play-icn"></a><img id ="` + nm + id +
      `" style="margin-left:8px; margin-bottom:3px; width:25px; height:25px" src="` + src + `" class="play-icn">
                `;

    let icnSrc;
    if (this.mapInterface.selectedItem && name === this.mapInterface.selectedItem.name) {
      icnSrc = '../../assets/images/marker-icon-2x-blue.png';
    } else {
      icnSrc = '../../assets/images/marker-icon-2x-red.png';
    }

    const newMarker = marker(
      [lat, long],
      {
        icon: icon({
          iconUrl: icnSrc,
          shadowUrl: '../../assets/images/marker-shadow.png',
          iconSize: [25, 41],
          iconAnchor: [12, 41],
          popupAnchor: [1, -34],
          shadowSize: [41, 41],
          className: id
        }),
      },
    ).on('click', () => {
      this.ngZone.run(() => {
        if (this.mapInterface.selectPath) {
          return;
        }
        for (let i = 0; i < this.mapInterface.data.length; i++) {
          if (this.mapInterface.data[i].name === name) {
            this.mapInterface.selectedItem = this.mapInterface.data[i];
          }
          if (id === 'map' + i || id === 'map') {
            this.elementRef.nativeElement.querySelector('.leaflet-marker-pane .' + id).src = '../../assets/images/marker-icon-2x-blue.png';
          } else {
            const catFormArray = this.mapInterface.myForm.controls.categories as FormArray;
            if (catFormArray.value.includes(this.mapInterface.data[i].category)) {
              this.elementRef.nativeElement
                .querySelector('.leaflet-marker-pane .map' + i).src = '../../assets/images/marker-icon-2x-red.png';
            }
          }
        }
        let favSrc;
        if (this.mapInterface.isLoggedIn) {
          favSrc = 'https://panaroamer-theming-test.s3.us-east-2.amazonaws.com/Panaroamer/assets/Default/heart-normal.png';
          if (this.mapInterface.starred.length) {
            for (const item of this.mapInterface.starred) {
              if (item.name === name) {
                favSrc = 'https://panaroamer-theming-test.s3.us-east-2.amazonaws.com/Panaroamer/assets/Default/heart-fill.png';
              }
            }
          }
        } else {
          favSrc = 'https://panaroamer-theming-test.s3.us-east-2.amazonaws.com/Panaroamer/assets/Default/heart-disabled.png';
        }
        const htmlFav = `<a id="` + id + `"><img src="` + 'https://panaroamer-theming-test.s3.us-east-2.amazonaws.com/Panaroamer/assets/Default/play.png' +
          `" style='width:30px; height:30px; margin-right:8px' class='play-icn'></a><a id = "` + nm +
          `"><img style=' margin-right:9px; width:25px; height:25px' src="` + 'https://panaroamer-theming-test.s3.us-east-2.amazonaws.com/Panaroamer/assets/Default/iconmonstr-share.png' +
          `" class='share play-icn'></a><a id ='fav'><img id = "` + nm + id +
          `" style='margin-right:9px; width:25px; height:25px' src="` +
          src + `" class='play-icn'></a><a id = 'info'><img  src="` + 'https://panaroamer-theming-test.s3.us-east-2.amazonaws.com/Panaroamer/assets/Default/arrow.png' +
          `" style='margin-right:9px; width:25px; height:25px' class='play-icn'></a><a id = 'map-path'><img  src="` + 'assets/icons/map-path.png' +
          `" style='margin-right:9px; width:25px; height:25px' class='play-icn'></a>`;
        newMarker.getPopup().setContent(htmlFav);
      });
    }).bindPopup(html).on('popupopen', () => {
      this.initInfoWindowEventListner(name, url, id, lat, long, newMarker);
      clearTimeout(this.mapInterface.timeoutVal);
      this.mapInterface.timeoutVal = setTimeout(() => {
        newMarker.closePopup();
      }, 5000);
      this.setPopupCss();
    }).openPopup();
    return newMarker;
  }

  setPopupCss(): void {
    const elements = document.getElementsByClassName('leaflet-popup-content-wrapper');
    for (let i = 0; i < elements.length; i++) {
      elements[i].setAttribute('style', 'background-color: ' + '#ffffff');
    }
    const tooltips = document.getElementsByClassName('leaflet-popup-tip');
    for (let i = 0; i < tooltips.length; i++) {
      tooltips[i].setAttribute('style', 'background-color: ' + '#ffffff');
    }
    const closeBtn = document.getElementsByClassName('leaflet-popup-close-button');
    for (let i = 0; i < closeBtn.length; i++) {
      closeBtn[i].setAttribute('style', 'color: ' + '#c3c3c3');
    }
  }

  initInfoWindowEventListner(name, url, id, lat, long, mapMarker?: any): void {
    this.elementRef.nativeElement.querySelector('#' + id)
      .addEventListener('click', (e: any) => {
        this.ngZone.run(() => {
          this.router.navigate(['/play'], { queryParams: { name, url, lat, lng: long } });
        });
      });

    this.elementRef.nativeElement.querySelector('#info')
    .addEventListener('click', (e: any) => {
      this.ngZone.run(() => {
        this.openModalMore();
      });
    });

    this.elementRef.nativeElement.querySelector('#map-path')
    .addEventListener('click', (e: any) => {
      this.ngZone.run(() => {
        this.mapInterface.selectPath = true;
        mapMarker.closePopup();
        this.mapInterface.markers = [];
        const idx = this.utility.polyLines.findIndex(x => x.origin.lat === lat && x.origin.lng === long);
        if (idx !== -1) {
          this.mapInterface.polyLines = this.utility.polyLines[idx];
          for (const latlng of this.mapInterface.polyLines.path) {
            let marUrl;
            if (latlng[0] === this.mapInterface.polyLines.origin.lat && latlng[1] === this.mapInterface.polyLines.origin.lng) {
              marUrl = '../../assets/images/marker-icon-2x-blue.png';
            } else {
              marUrl = '../../assets/images/marker-icon-2x-green.png';
            }
            const newMarker = marker(
              [latlng[0], latlng[1]],
              {
                icon: icon({
                  iconUrl: marUrl,
                  shadowUrl: '../../assets/images/marker-shadow.png',
                  iconSize: [25, 41],
                  iconAnchor: [12, 41],
                  popupAnchor: [1, -34],
                  shadowSize: [41, 41]
                }),
              },
            ).on('click', () => {
              this.ngZone.run(() => {
                this.removePath(latlng[0], latlng[1], newMarker);
              });
            });
            this.mapInterface.markers.push(newMarker);
          }
          const polyline = L.polyline(this.mapInterface.polyLines.path).addTo(this.mapInterface.map);
          this.mapInterface.polyLinesGroup.push(polyline);
        } else {
          const newMarker = marker(
            [lat, long],
            {
              icon: icon({
                iconUrl: '../../assets/images/marker-icon-2x-blue.png',
                shadowUrl: '../../assets/images/marker-shadow.png',
                iconSize: [25, 41],
                iconAnchor: [12, 41],
                popupAnchor: [1, -34],
                shadowSize: [41, 41]
              }),
            },
          ).on('click', () => {
            this.ngZone.run(() => {
              this.removePath(lat, long, newMarker);
            });
          });
          this.mapInterface.markers.push(newMarker);
        }
      });
    });

    const nm = name.replace(' ', '-');

    this.elementRef.nativeElement.querySelector('#' + nm)
      .addEventListener('click', (e: any) => {
        this.ngZone.run(() => {
          this.translateSevice.get(
            ['shareIp.CoolCheckItOut', 'shareIp.ThisIsWoke', 'shareIp.WowTakeLook', 'shareIp.LetMeKnow', 'shareIp.DoYouLikeIt']
          ).subscribe((res: any[]) => {
            this.mapInterface.optionsMsg = Object.keys(res).map((key) => res[key]);
            this.openModal();
          });
        });
      });

    this.elementRef.nativeElement.querySelector('#' + nm + id)
      .addEventListener('click', (e: any) => {
        this.ngZone.run(() => {
          if (this.mapInterface.isLoggedIn) {
            if (this.checkArr(this.mapInterface.starred)) {
              this.removeStarred();
              // this.elementRef.nativeElement.querySelector('#' + nm + id).src = "assets/icons/heart-normal.png";
              this.elementRef.nativeElement.querySelector('#' + nm + id).src = 'https://panaroamer-theming-test.s3.us-east-2.amazonaws.com/Panaroamer/assets/Default/heart-normal.png';
            } else {
              this.addtoStar();
              // this.elementRef.nativeElement.querySelector('#' + nm + id).src = "assets/icons/heart-fill.png";
              this.elementRef.nativeElement.querySelector('#' + nm + id).src = 'https://panaroamer-theming-test.s3.us-east-2.amazonaws.com/Panaroamer/assets/Default/heart-fill.png';
            }
          }
        });
      });
  }

  setPath(): void {
    this.mapInterface.showEachPath = false;
    this.mapInterface.selectPath = false;
    this.mapInterface.markers = [];
    this.mapInterface.polyLinesGroup.forEach(item => {
      item.remove(this.mapInterface.map);
    });
    this.mapInterface.polyLines = undefined;
    this.mapInterface.polyLinesGroup = [];
    this.ngOnInit(true);
  }

  showAllPath(): void {
    this.mapInterface.showEachPath = true;
    this.mapInterface.polyLinesGroup = [];
    for (const item of this.utility.polyLines) {
      for (const latlng of item.path) {
        let marUrl;
        if (latlng[0] === item.origin.lat && latlng[1] === item.origin.lng) {
          marUrl = '../../assets/images/marker-icon-2x-blue.png';
        } else {
          marUrl = '../../assets/images/marker-icon-2x-green.png';
        }
        const newMarker = marker(
          [latlng[0], latlng[1]],
          {
            icon: icon({
              iconUrl: marUrl,
              shadowUrl: '../../assets/images/marker-shadow.png',
              iconSize: [25, 41],
              iconAnchor: [12, 41],
              popupAnchor: [1, -34],
              shadowSize: [41, 41]
            }),
          },
        );
        this.mapInterface.markers.push(newMarker);
      }
      const polyline = L.polyline(item.path).addTo(this.mapInterface.map);
      this.mapInterface.polyLinesGroup.push(polyline);
    }
  }

  addtoStar(): void {
    let arr = [];
    if (localStorage.getItem('starred') == null) {
      arr.push(this.mapInterface.selectedItem);
      this.mapInterface.starred = arr;
      localStorage.setItem('starred', JSON.stringify(arr));
    } else {
      arr = JSON.parse(localStorage.getItem('starred'));
      if (!this.checkArr(arr)) {
        arr.push(this.mapInterface.selectedItem);
        this.mapInterface.starred = arr;
        localStorage.setItem('starred', JSON.stringify(arr));
      }
    }
  }

  removeStarred(): void {
    const arr = JSON.parse(localStorage.getItem('starred'));
    const index = arr.findIndex(i => i.name === this.mapInterface.selectedItem.name);
    arr.splice(index, 1);
    this.mapInterface.starred = arr;
    localStorage.setItem('starred', JSON.stringify(arr));
  }

  checkArr(data): boolean {
    for (const item of data) {
      if (item.name === this.mapInterface.selectedItem.name) {
        return true;
      }
    }
  }

  onMapClick(e): void {
    if (!this.mapInterface.selectPath) {
      const popLocation = e.latlng;
      const html = `<a id ='quick-build'><img style='width:25px; height:25px' src="` + 'https://panaroamer-theming-test.s3.us-east-2.amazonaws.com/Panaroamer/assets/Default/iconmonstr-plus-circle-thin.png' + `"
                    class='play-icn'></a><a id='gallery'><img style='margin-left:8px; width:25px; height:25px'
                    src="` + 'https://panaroamer-theming-test.s3.us-east-2.amazonaws.com/Panaroamer/assets/Default/iconmonstr-archive-box-thin.png' + `" class='play-icn'></a>`;
      clearTimeout(this.mapInterface.timeoutMapVal);
      const popup = L.popup()
        .setLatLng(popLocation)
        .setContent(html)
        .openOn(this.mapInterface.map);

      this.mapInterface.timeoutMapVal = setTimeout(() => {
        this.mapInterface.map.closePopup();
      }, 5000);

      this.elementRef.nativeElement.querySelector('#quick-build')
        .addEventListener('click', () => {
          this.ngZone.run(() => {
            const obj = { lat: e.latlng.lat, lng: e.latlng.lng };
            localStorage.setItem('latlong', JSON.stringify(obj));
            this.router.navigate(['/builder']);
            localStorage.setItem('display', 'yes');
          });

        });

      this.elementRef.nativeElement.querySelector('#gallery')
        .addEventListener('click', () => {
          this.ngZone.run(() => {
            this.router.navigate(['/gallery'], { queryParams: { lat: popLocation.lat, lng: popLocation.lng } });
          });
        });
      this.setPopupCss();
    } else {
      if (!this.mapInterface.polyLines) {
        this.mapInterface.polyLines = {
          origin: { lat: this.mapInterface.markers[0]._latlng.lat, lng: this.mapInterface.markers[0]._latlng.lng },
          path: [
            [this.mapInterface.markers[0]._latlng.lat, this.mapInterface.markers[0]._latlng.lng],
            [e.latlng.lat, e.latlng.lng]
          ]
        };
      } else {
        this.mapInterface.polyLines.path.push([e.latlng.lat, e.latlng.lng]);
      }
      const idx = this.utility.polyLines.findIndex(x => x.origin.lat === this.mapInterface.polyLines.origin.lat &&
        x.origin.lng === this.mapInterface.polyLines.origin.lng);
      if (idx !== -1) {
        this.utility.polyLines[idx] = this.mapInterface.polyLines;
      } else {
        this.utility.polyLines.push(this.mapInterface.polyLines);
      }
      const newMarker = marker(
        [e.latlng.lat, e.latlng.lng],
        {
          icon: icon({
            iconUrl: '../../assets/images/marker-icon-2x-green.png',
            shadowUrl: '../../assets/images/marker-shadow.png',
            iconSize: [25, 41],
            iconAnchor: [12, 41],
            popupAnchor: [1, -34],
            shadowSize: [41, 41]
          }),
        },
        ).on('click', () => {
          this.ngZone.run(() => {
            this.removePath(e.latlng.lat, e.latlng.lng, newMarker);
          });
        });
      this.mapInterface.markers.push(newMarker);
      const polyline = L.polyline(this.mapInterface.polyLines.path).addTo(this.mapInterface.map);
      this.mapInterface.polyLinesGroup.push(polyline);
    }
  }

  removePath(lat, lng, newMarker): void {
    if (this.mapInterface.polyLines.origin.lat !== lat && this.mapInterface.polyLines.origin.lng !== lng) {
      for (let i = 0; i < this.mapInterface.polyLinesGroup.length; i++) {
        for (const line of this.mapInterface.polyLinesGroup[i]._latlngs) {
          if (line.lat === lat && line.lng === lng) {
            this.mapInterface.polyLinesGroup[i].remove(this.mapInterface.map);
            this.mapInterface.polyLinesGroup.splice(i, 1);
            i = i - 1;
          }
        }
      }
      for (let i = 0; i < this.mapInterface.polyLines.path.length; i++) {
        if (this.mapInterface.polyLines.path[i][0] === lat && this.mapInterface.polyLines.path[i][1] === lng) {
          this.mapInterface.polyLines.path.splice(i, 1);
        }
      }
      const indx = this.utility.polyLines.findIndex(x => x.origin.lat === this.mapInterface.polyLines.origin.lat &&
        x.origin.lng === this.mapInterface.polyLines.origin.lng);
      if (indx !== -1) {
        this.utility.polyLines[indx] = this.mapInterface.polyLines;
      }
      newMarker.remove();
      const pline = L.polyline(this.mapInterface.polyLines.path).addTo(this.mapInterface.map);
      this.mapInterface.polyLinesGroup.push(pline);
    }
  }

  onMapReady(map: L.Map): void {
    // Do stuff with map
    this.mapInterface.map = map;
    L.control.fullscreen({
      position: 'topleft', // change the position of the button can be topleft, topright, bottomright or bottomleft, defaut topleft
      title: 'Show me the fullscreen !', // change the title of the button, default Full Screen
      titleCancel: 'Exit fullscreen mode', // change the title of the button when fullscreen is on, default Exit Full Screen
      content: null, // change the content of the button, can be HTML, default null
      forceSeparateButton: true, // force seperate button to detach from zoom buttons, default false
      forcePseudoFullscreen: true, // force use of pseudo full screen even if full screen API is available, default false
      fullscreenElement: false // Dom element to render in full screen, false by default, fallback to map._container
    }).addTo(this.mapInterface.map);

    this.mapInterface.map.on('enterFullscreen', () => {
      this.mapInterface.map.invalidateSize();
      this.mapInterface.isFullscreen = true;
      this.utility.isFullscreen = true;
    });
    this.mapInterface.map.on('exitFullscreen', () => {
      this.mapInterface.isFullscreen = false;
      this.utility.isFullscreen = false;
      this.mapInterface.map.invalidateSize();
    });
  }

  getCurrent(): void {
    this.utility.getCoordinates().subscribe(data => {
      this.mapInterface.currentData = data;
      this.mapInterface.optionsSpec = this.utility.getLeafletMapOptions();
      if (data.latitude !== null && data.longitude !== null) {
        this.mapInterface.latlng = { lat: data.latitude, lng: data.longitude };
        this.mapInterface.center = latLng(this.mapInterface.latlng);
      }
    });
  }

  closeFullscreen(): void {
    this.mapInterface.landscape = false;
    if (this.document.exitFullscreen) {
      this.document.exitFullscreen();
    }
  }

  openModalMore(): void{
    this.dialog.open(IpInfoDialogComponent, {
      panelClass: 'edit-image-dialog',
      maxHeight: '90vh',
      data: {
        selectedItem: this.mapInterface.selectedItem
      }
    });
  }

  openModal(): void{
    this.dialog.open(ShareDialogComponent, {
      panelClass: 'edit-image-dialog',
      maxHeight: '90vh',
      autoFocus: false,
      data: {
        selectedItem: this.mapInterface.selectedItem
      }
    });
  }

}
