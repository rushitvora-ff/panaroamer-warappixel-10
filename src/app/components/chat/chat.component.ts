import { Component, OnInit, AfterViewInit } from '@angular/core';
import { UtilityService } from 'src/app/services/utility.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { ChatInterface } from '../../interfaces/componets';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, AfterViewInit {
  chatInterface: ChatInterface = {
    messages: [],
    senderProfile: {
      name: 'Luke',
      avatar: 'assets/images/users/8.jpg',
    }
  };
  // messages = [];
  // ipUrl;
  // labels;
  // senderProfile = {
  //   name: 'Luke',
  //   avatar: 'assets/images/users/8.jpg',
  // };
  constructor(
    public utility: UtilityService,
    private router: Router,
    private translateSevice: TranslateService,
    private route: ActivatedRoute) {
    this.loadTranslation();
    this.translateSevice.onLangChange.subscribe((event: LangChangeEvent) => {
      this.loadTranslation();
    });
  }

  ngOnInit(): void {
    this.chatInterface.messages = this.utility.intialMessagesArr;
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      if (this.utility.conIframeUrl) {
        this.chatInterface.ipUrl = this.utility.conIframeUrl;
        this.utility.conIframeUrl = undefined;
        setTimeout(() => {
          this.chatInterface.ipUrl = '';
        }, 5000);
      }
      this.utility.conversationObj = {};
    }, 500);
  }

  onSelectIp(event): void {
    if (event.existingIp) {
      let queryparams: any;
      this.route.queryParams.subscribe(params => {
        if (params) {
          queryparams = params;
        }
      });
      this.utility.conversationObj = {
        isChat: false,
        redirectTo: queryparams ? this.router.url.split('?')[0] : this.router.url,
        queryParams: queryparams
      };
      // if (this.mapData) {
      //   this.utilitySer.conversationObj.mapData = this.mapData;
      // }
      this.router.navigate(['/gallery'], { queryParams: { type: 'attachment' } });
    } else {
      this.router.navigate(['/builder'], { queryParams: { type: 'attachment' } });
    }
  }

  loadTranslation(): void {
    this.translateSevice.get(['chat.SearchPlaceHolder', 'chatScreen.Typehere', 'previewImages.sound.Upload',
    'previewImages.sound.Record', 'DeleteModal.CancelButton', 'conversation.New IP', 'conversation.Exisiting IP',
    'chatScreen.Actions', 'chatScreen.Reaction', 'chat.startCoversation']).subscribe((res: any[]) => {
      res = Object.keys(res).map((key) => res[key]);
      this.chatInterface.labels = undefined;
      this.chatInterface.labels = {
        placeholder: {
          search: res[0],
          message: res[1]
        },
        media: {
          upload: res[2],
          record: res[3],
          cancel: res[4]
        },
        ip: {
          new: res[5],
          existing: res[6]
        },
        message: {
          actions: res[7],
          reaction: res[8],
          startCoversation: res[9]
        }
      };
    });
  }

}
