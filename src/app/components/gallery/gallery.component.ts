import { Component, OnInit } from '@angular/core';
import { UtilityService } from 'src/app/services/utility.service';
import { IpInfoDialogComponent } from 'src/app/shared/dialogs/ip-info-dialog/ip-info-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { ShareDialogComponent } from 'src/app/shared/dialogs/share-dialog/share-dialog.component';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { PaymentsDialogComponent } from 'src/app/shared/dialogs/payments-dialog/payments-dialog.component';
import { GlobalAuthenticationContext } from '@oneb/authentication-connector-api';
import { GalleryInterface } from 'src/app/interfaces/componets';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {
  gallery: GalleryInterface = {
    searchStr: '',
    trendingArr: [],
    starredArr: [],
    yoursArr: [],
    sharedArr: [],
    filTrendingArr: [],
    filStarredArr: [],
    filYoursArr: [],
    filSharedArr: [],
    isLoggedIn: GlobalAuthenticationContext.hasSignedInUserDetails() && GlobalAuthenticationContext.getSignedInUserDetails().email,
    activeTab: 3,
    type: ''
  };

  // interactions = [];
  // searchStr = '';
  // location;
  // trendingArr = [];
  // starredArr = [];
  // yoursArr = [];
  // sharedArr = [];
  // filTrendingArr = [];
  // filStarredArr = [];
  // filYoursArr = [];
  // filSharedArr = [];
  // selectedItem;
  // isLoggedIn = GlobalAuthenticationContext.hasSignedInUserDetails() && GlobalAuthenticationContext.getSignedInUserDetails().email;
  // activeTab = 3;
  // type = '';

  constructor(
    public dialog: MatDialog,
    private translateSevice: TranslateService,
    private toast: ToastrService,
    private router: Router,
    route: ActivatedRoute,
    public utility: UtilityService) {
      route.queryParams.subscribe(params => {
        if (params.lat && params.lng) {
          this.gallery.location = {
            lat: params.lat,
            lng: params.lng
          };
          translateSevice.get('toast.Please click on presentation to select one!!').subscribe(res => {
            this.toast.info(res, '');
          });
        } else if (params.type && params.type === 'attachment') {
          this.gallery.type = 'attachment';
          translateSevice.get('toast.Please click on presentation to select one!!').subscribe(res => {
            this.toast.info(res, '');
          });
        }
      });
      if (this.gallery.isLoggedIn) {
        this.gallery.activeTab = parseInt(localStorage.getItem('activeTab'), 0);
      }
      console.log(GlobalAuthenticationContext.hasSignedInUserDetails());
    }

  ngOnInit(): void {
    if (localStorage.getItem('starred') !== null) {
      const dataArr = JSON.parse(localStorage.getItem('starred'));
      this.mapArray(dataArr);
      this.gallery.starredArr = dataArr;
      this.gallery.filStarredArr = this.gallery.starredArr;
    }
    this.gallery.trendingArr = this.mapArray(this.utility.interactionsArray.slice(0, 11));
    this.gallery.yoursArr = this.mapArray(this.utility.interactionsArray.slice(11, 17));
    this.gallery.sharedArr = this.mapArray(this.utility.interactionsArray.slice(17, this.utility.interactionsArray.length - 3));
    this.gallery.filTrendingArr = this.gallery.trendingArr;
    this.gallery.filYoursArr = this.gallery.yoursArr;
    this.gallery.filSharedArr = this.gallery.sharedArr;
  }

  mapArray(data: any): any {
    for (const item of data) {
      const theArr = item.url.split('/');
      theArr.pop();
      item.imgSrc = theArr.join('/') + '/presentations/media/thumbnail.jpg';
    }
    return data;
  }

  onSelectIP(item): void {
    if (this.gallery.location) {
      const obj = item;
      obj.location = this.gallery.location;
      this.utility.interactionsArray.splice(0, 0, obj);
      this.router.navigate(['/map'], { queryParams: { lat: this.gallery.location.lat, long: this.gallery.location.lng, type: obj.name } });
    } else if (this.gallery.type === 'attachment') {
      if (this.utility.conversationObj.queryParams) {
        this.router.navigate([this.utility.conversationObj.redirectTo], { queryParams: this.utility.conversationObj.queryParams });
      } else {
        this.router.navigate([this.utility.conversationObj.redirectTo]);
      }
      this.utility.conIframeUrl = item.url;
      // this.utility.setConvUrl(item.url);
    }
  }

  redirectTo(item): void {
    if (this.gallery.location || this.gallery.type === 'attachment') {
      this.onSelectIP(item);
    } else if (item.price) {
      this.openPaymentsDialog(item);
    } else {
      this.router.navigate(['/play'], { queryParams: { perName: item.author, name: item.name, url: item.url } });
    }
  }

  redirectMap(item): void {
    this.router.navigate(['/map'], { queryParams: { lat: item.location.lat, long: item.location.lng, type: item.name } });
  }

  openModalMore(info): void{
    this.dialog.open(IpInfoDialogComponent, {
      panelClass: this.utility.dark ? 'dark' : 'edit-image-dialog',
      maxHeight: '90vh',
      data: {
        selectedItem: info
      }
    });
  }

  openPaymentsDialog(item): void {
    this.dialog.open(PaymentsDialogComponent, {
      panelClass: this.utility.dark ? 'dark' : 'edit-image-dialog',
      maxHeight: '90vh',
      data: {
        selectedItem: item
      }
    });
  }

  openModal(info): void{
    this.dialog.open(ShareDialogComponent, {
      panelClass: this.utility.dark ? 'dark' : 'edit-image-dialog',
      maxHeight: '90vh',
      autoFocus: false,
      data: {
        selectedItem: info
      }
    });
  }

  onTabChanged(event): any {
    if (event.index !== this.gallery.activeTab) {
      this.gallery.filStarredArr = this.gallery.starredArr;
      this.gallery.filTrendingArr = this.gallery.trendingArr;
      this.gallery.filYoursArr = this.gallery.yoursArr;
      this.gallery.filSharedArr = this.gallery.sharedArr;
      this.gallery.searchStr = undefined;
      this.gallery.activeTab = event.index;

    }
    localStorage.setItem('activeTab', this.gallery.activeTab.toString());
  }

  search(): void {
    if (this.gallery.searchStr) {
      let items = [];
      if (this.gallery.activeTab === 0) {
        items = this.gallery.yoursArr.filter(item => {
          return item.name.toLowerCase().indexOf(this.gallery.searchStr.toLowerCase()) !== -1;
        });
        this.gallery.filYoursArr = JSON.parse(JSON.stringify(items));
      } else if (this.gallery.activeTab === 1) {
        items = this.gallery.sharedArr.filter(item => {
          return item.name.toLowerCase().indexOf(this.gallery.searchStr.toLowerCase()) !== -1;
        });
        this.gallery.filSharedArr = JSON.parse(JSON.stringify(items));
      } else if (this.gallery.activeTab === 2) {
        items = this.gallery.starredArr.filter(item => {
          return item.name.toLowerCase().indexOf(this.gallery.searchStr.toLowerCase()) !== -1;
        });
        this.gallery.filStarredArr = JSON.parse(JSON.stringify(items));
      } else {
        items = this.gallery.trendingArr.filter(item => {
          return item.name.toLowerCase().indexOf(this.gallery.searchStr.toLowerCase()) !== -1;
        });
        this.gallery.filTrendingArr = JSON.parse(JSON.stringify(items));
      }
    }  else {
      this.gallery.filStarredArr = this.gallery.starredArr;
      this.gallery.filTrendingArr = this.gallery.trendingArr;
      this.gallery.filYoursArr = this.gallery.yoursArr;
      this.gallery.filSharedArr = this.gallery.sharedArr;
    }
  }

  checkStarArr(data): boolean {
    for (const item of this.gallery.starredArr) {
      if (item.name === data.name) {
        return true;
      }
    }
  }

  checkArr(data, item): boolean {
    for (const itm of data) {
      if (itm.name === item.name) {
        return true;
      }
    }
  }

  removeStarred(item): void {
    const arr = JSON.parse(localStorage.getItem('starred'));
    const index = arr.findIndex(i => i.name === item.name);
    arr.splice(index, 1);
    localStorage.setItem('starred', JSON.stringify(arr));
    this.gallery.starredArr = this.mapArray(arr);
    this.gallery.filStarredArr = this.gallery.starredArr;
  }

  addtoStar(itm): void {
    delete itm.imgSrc;
    let arr = [];
    if (localStorage.getItem('starred') == null) {
      arr.push(itm);
      localStorage.setItem('starred', JSON.stringify(arr));
      this.gallery.starredArr = this.mapArray(arr);
      this.gallery.filStarredArr = this.gallery.starredArr;
    } else {
      arr = JSON.parse(localStorage.getItem('starred'));
      if (!this.checkArr(arr, itm)) {
        arr.push(itm);
        localStorage.setItem('starred', JSON.stringify(arr));
        this.gallery.starredArr = this.mapArray(arr);
        this.gallery.filStarredArr = this.gallery.starredArr;
      }
    }
  }

}
