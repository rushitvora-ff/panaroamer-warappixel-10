import { Component, OnInit } from '@angular/core';
import { CalendarView } from 'angular-calendar';
import { UtilityService } from 'src/app/services/utility.service';
import { IpScheduleInterface } from '../../interfaces/componets';

@Component({
  selector: 'app-ip-schedule-calendar',
  templateUrl: './ip-schedule-calendar.component.html',
  styleUrls: ['./ip-schedule-calendar.component.scss']
})
export class IpScheduleCalendarComponent implements OnInit {
  ipScheduleInterface: IpScheduleInterface = {
    view: CalendarView.Month,
    viewDate: new Date(),
    events: []
  }

  constructor(public utility: UtilityService) { }

  ngOnInit(): void {
  }
}
