import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { ClientLibAxios } from '@rainmaker2/rainmaker2-codegen-api-clients-axios';
import { SchedulesFinderClientDefault } from '@oneb/schedules-api-clients';
import { SchedulesManagerClientDefault } from '@oneb/schedules-api-clients';

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {
  clientLibAxios = new ClientLibAxios(environment.onebApi.schedules.service.serviceUrl);
  schedulesFinder = new SchedulesFinderClientDefault(this.clientLibAxios);
  schedulesManager = new SchedulesManagerClientDefault(this.clientLibAxios);

  constructor() {
    this.schedulesFinder.setTenancy({ appId: 'rushit', dataId: 'vora'});
    this.schedulesManager.setTenancy({ appId: 'rushit', dataId: 'vora'});
  }

  async createEvent(events): Promise<any> {
    const createReq = {
      events
    };
    const createRsp = await this.schedulesManager.createBulkEvents(createReq);
    return createRsp;
  }
}
