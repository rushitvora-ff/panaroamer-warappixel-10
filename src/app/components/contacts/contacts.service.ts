import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { ClientLibAxios } from '@rainmaker2/rainmaker2-codegen-api-clients-axios';
import { ContactsFinderClientDefault } from '@oneb/contacts-api-clients';
import { ContactsManagerClientDefault } from '@oneb/contacts-api-clients';
// import { ContactsFinderClientLocal } from '../../api-local-poc/ContactsFinderClientLocal';
import { from, Observable } from 'rxjs';
// import { ContactsManagerClientLocal } from '../../api-local-poc/ContactsManagerClientLocal';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {
  clientLibAxios = new ClientLibAxios(environment.onebApi.contacts.service.serviceUrl);
  finder = new ContactsFinderClientDefault(this.clientLibAxios);
  manager = new ContactsManagerClientDefault(this.clientLibAxios);

  constructor() {
    this.finder.setTenancy({ appId: 'rushit', dataId: 'vora'});
    this.manager.setTenancy({ appId: 'rushit', dataId: 'vora'});
  }

  createContact(contact): Promise<any> {
    const createReq = {
      contact
    };
    return this.manager.createContact(createReq);
  }

  createContactPersonalProfile(createReq) {
    const request = {
      contactPersonProfile: createReq
    }
    return this.manager.createContactPersonProfile(request);
  }

  createPersonalProfile(createReq): Promise<any> {
    const request = {
      personProfile: createReq
    }
    return this.manager.createPersonProfile(request);
  }

  updateContact(updatedContact): Promise<any> {
    const createReq = {
      filter: {
        contactId: { eq: updatedContact.contactId }
      },
      contact: updatedContact
    };
    return this.manager.updateContactsByContactId(createReq);
  }

  getContacts(): Promise<any> {
    const findReq = { filter: { }};
    return this.finder.findContacts(findReq);
  }

  getContactById(id): Observable<any> {
    const findReq = { filter: { contactId: id }};
    return from(this.finder.findOneContactByContactId(findReq));
  }

  createContactMechanism(createReq): Promise<any> {
    const findReq = { contactMechanism: createReq };
    return this.manager.createContactMechanism(findReq);
  }

  getContactMechanism(id): Observable<any> {
    const findReq = { filter: { contactMechanismId: id }};
    return from(this.finder.findOneContactMechanismByContactMechanismId(findReq));
  }

  updateContactMechanism(data, id): Observable<any> {
    const findReq = { filter: { contactMechanismId: id }, contactMechanism: data};
    return from(this.manager.updateOneContactMechanismByContactMechanismId(findReq));
  }

  deleteContactMechanism(id): Observable<any> {
    const findReq = { filter: { contactMechanismId: id }};
    return from(this.manager.deleteContactMechanismsByContactMechanismId(findReq));
  }

  deleteContact(id): Promise<any> {
    const findReq = { filter: { contactId: {eq : id} }};
    return this.manager.deleteContactsByContactId(findReq);
  }
}
