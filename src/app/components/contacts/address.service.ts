import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { ClientLibAxios } from '@rainmaker2/rainmaker2-codegen-api-clients-axios';
import { AddressesFinderClientDefault } from '@oneb/addresses-api-clients';
import { AddressesManagerClientDefault } from '@oneb/addresses-api-clients';

@Injectable({
  providedIn: 'root'
})
export class AddressService {
  clientAxios = new ClientLibAxios(environment.onebApi.address.service.serviceUrl);
  finder = new AddressesFinderClientDefault(this.clientAxios);
  manager = new AddressesManagerClientDefault(this.clientAxios);

  constructor() {
    this.finder.setTenancy({ appId: 'rushit', dataId: 'vora'});
    this.manager.setTenancy({ appId: 'rushit', dataId: 'vora'});
   }

  createAddress(address): Promise<any> {
    const Address = {
      address
    };
    return this.manager.createAddress(Address);
  }

  getAddress(): Promise<any> {
    const request = { filter: {} };
    return this.finder.findAddresses(request);
  }

  updateAddress(updateAddress): Promise<any> {
    const createRequest = {
      filter: {
        addressId: {
          eq: updateAddress.addressId
        }
      },
      address: updateAddress
    };
    return this.manager.updateAddressesByAddressId(createRequest);
  }

  deleteAddress(id): Promise<any> {
    const createRequest = {
      filter: {
        addressId: {
          eq: id
        }
      }
    };
    return this.manager.deleteAddressesByAddressId(createRequest);
  }

  getOneAddress(id): Promise<any> {
    const request = {
      filter: {
        addressId: {
          eq: id
        }
      }
    };
    return this.finder.findAddressesByAddressId(request);
  }
}
