import { Component, HostListener, OnInit } from '@angular/core';
import { UtilityService } from 'src/app/services/utility.service';
import { ContactsService } from './contacts.service';
import { AddressService } from './address.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { GlobalAuthenticationContext } from '@oneb/authentication-connector-api';
import { ToastrService } from 'ngx-toastr';
import { makeContactMechanism, makePersonProfile } from '@oneb/contacts-api-clients-models';
import { makeContactPersonProfile } from '@oneb/contacts-api-clients-models';
import { makeContact } from '@oneb/contacts-api-clients-models';
import { makeAddress } from '@oneb/addresses-api-clients-models';
import { clientResponseSuccess, clientResponseHasErrors } from '@rainmaker2/rainmaker2-codegen-api-clients-common';
import { string1To1000 } from 'aws-sdk/clients/customerprofiles';
import { ContactsInterface } from 'src/app/interfaces/componets';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent implements OnInit {
  contactInterface: ContactsInterface = {
    forPersonProfile: {
      contactMechanism: [],
      address: [],
      contactMechanismsIds: [],
      addressesIds: []
    },
    contacts: [],
    isLoading: false,
    isLoggedIn: GlobalAuthenticationContext.hasSignedInUserDetails() && GlobalAuthenticationContext.getSignedInUserDetails().email
  };

  // forPersonProfile = {
  //   contactMechanism: [],
  //   address: [],
  //   contactMechanismsIds: [],
  //   addressesIds: []
  // };
  // contacts: any = [];
  // labels;
  // isLoading = false;
  // isLoggedIn = GlobalAuthenticationContext.hasSignedInUserDetails() && GlobalAuthenticationContext.getSignedInUserDetails().email;

  @HostListener('window:offline')
  setNetworkOffline(): void {
    this.utility.setLoader(false);
    this.toast.error('Network Failure');
  }
  constructor(
    private router: Router,
    private toast: ToastrService,
    private contactService: ContactsService,
    private addressService: AddressService,
    private translateSevice: TranslateService,
    public utility: UtilityService) {
      // if (GlobalAuthenticationContext.hasSignedInUserDetails() && GlobalAuthenticationContext.getSignedInUserDetails().email) {
      //   this.isLogin = true;
      // }
      this.loadTranslation();
      this.translateSevice.onLangChange.subscribe(() => {
        this.loadTranslation();
      });
      localStorage.setItem('loggedIn', this.contactInterface.isLoggedIn);
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.getContacts();
    }, 200);
  }

  getContacts(): void {
    this.contactInterface.isLoading = true;
    this.utility.setLoader(true);
    this.contactService.getContacts().then(res => {
      this.contactInterface.contacts = res.results ? res.results : [];
      console.log('getContacts', this.contactInterface.contacts);
      this.utility.setLoader(false);
      this.contactInterface.isLoading = false;
    }, error => {
      console.log('getContactsErrr', error);
      this.utility.setLoader(false);
      this.contactInterface.isLoading = false;
      this.contactInterface.contacts = [];
    });
  }

  getAddress(): void {
    this.addressService.getAddress().then(res => {
      console.log('Addresses', res);
    }, err => {
      console.log('AddressErrr', err);
    });
  }

  editContacts(event): void {
    this.utility.setLoader(true);
    if (event.action === 'edit') {
      const contact = event.contact;
      if (contact.contactMechanisms.length) {
        const contactMechanisms = this.formatContactMechanism(contact.contactMechanisms);
        for (const item of ['160', '60', '280']) {
          if (contact.groupBy && contact.groupBy[item] && contact.groupBy[item].length) {
            contact.groupBy[item].forEach((element, index) => {
              const idx = contactMechanisms[item].length - 1;
              if (index <= idx) {
                this.updateContactMechanism(contactMechanisms[item][index], element.contactMechanismId);
              } else {
                // this.deleteContactMechanism(element.contactMechanismId);
                const obj = {
                  description: '',
                  name: '',
                  type: item,
                  value: ''
                };
                this.updateContactMechanism(obj, element.contactMechanismId);
              }
            });
          }
        }
      }
      this.contactService.updateContact(event.contact)
        .then(() => {
        console.log('editContactRequest', event.contact);
        const msg = this.translateSevice.instant('Contact updated successfully.');
        this.toast.success(msg);
        this.utility.setLoader(false);
      }, () =>
      this.utility.setLoader(false));
    } else {
      if (event.contact.contactMechanisms.length) {
        this.contactMechanismApi(event.contact.contactMechanisms, event.contact.address, event.contact.name);
      }
    }
  }

  async contactMechanismApi(contactMechanismsArray: Array<any>, adddress: string, contactName: string): Promise<void> {
    const contactMechanismArray = [];
    const resArray = [];
    const resArrayIds = [];
    for (const item of contactMechanismsArray) {
      const mechanism = makeContactMechanism({}, true);
      mechanism.name = item.name;
      mechanism.type = item.type;
      mechanism.value = item.value;
      contactMechanismArray.push(mechanism);
      console.log('contactMechanismArray', contactMechanismArray);
      const createRsp = await this.contactService.createContactMechanism(contactMechanismArray[contactMechanismArray.length - 1]);
      if (clientResponseSuccess(createRsp)) {
        resArray.push(createRsp.contactMechanism);
        resArrayIds.push(createRsp.contactMechanism.contactMechanismId.toString());
        console.log('DataToPost', resArray , resArray.length, contactMechanismsArray.length);
        if (resArray.length == contactMechanismsArray.length) {
          this.contactInterface.forPersonProfile.contactMechanism = resArray;
          this.contactInterface.forPersonProfile.contactMechanismsIds = resArrayIds;
          this.addressAPI(adddress, contactName);
        }
      }
      else if (clientResponseHasErrors(createRsp)) {
        console.log('contactMechanismError', createRsp);
        this.utility.setLoader(false);
      }
    }
  }

  async addressAPI(city: string, name: string): Promise<void> {
    const addressObj = makeAddress({}, true);
    addressObj.city = city;
    const createRsp = await this.addressService.createAddress(addressObj);
    if (clientResponseSuccess(createRsp)) {
      console.log('createAddress', createRsp);
      this.contactInterface.forPersonProfile.address.push(createRsp.address);
      this.contactInterface.forPersonProfile.addressesIds.push(createRsp.address.addressId);
      this.contactApi(name);
    }
    else if (clientResponseHasErrors(createRsp)) {
      console.log('createAddressError', createRsp);
      this.utility.setLoader(false);
    }
  }

  async contactApi(name): Promise<void> {
    const contactPersonalProfile = makeContactPersonProfile({}, true);
    const profile = makePersonProfile({}, true);
    const contact = makeContact({}, true);
    const addressObj = makeAddress({}, true);
    addressObj.addressId = this.contactInterface.forPersonProfile.address[0].addressId;
    addressObj.city = this.contactInterface.forPersonProfile.address[0].city;
    console.log(contactPersonalProfile);
    console.log(profile);
    profile.addresses.push(addressObj);
    for (const item of this.contactInterface.forPersonProfile.contactMechanism) {
      const mechanism = makeContactMechanism({}, true);
      mechanism.name = item.name,
      mechanism.type = item.type,
      mechanism.value = item.value,
      mechanism.contactMechanismId = item.contactMechanismId;
      profile.contactMechanisms.push(mechanism);
    }
    // profile.contactMechanisms = this.cont.contactMechanism;
    contactPersonalProfile.profile = profile;
    contact.profile = contactPersonalProfile;
    // profile.personProfileId = ''
    contact.name = name;
    console.log('pp', contact);
    // this.contactService.createPersonalProfile(profile)
    // .then((result) => {
    //   console.log('personProfileAPI', result)
    //   this.utility.setLoader(false);
    // })
    const createRsp = await this.contactService.createContact(contact);
    if (clientResponseSuccess(createRsp)) {
      console.log('contactCreated', createRsp);
      this.utility.setLoader(false);
    }
    else if (clientResponseHasErrors(createRsp)) {
      console.log('contactCreatedError', createRsp);
      this.utility.setLoader(false);
    }
  }

  updateContactMechanism(contactMechanism, contactMechanismId): void {
    this.contactService.updateContactMechanism(contactMechanism, contactMechanismId).subscribe();
  }

  formatContactMechanism(arr): any {
    return arr.reduce((r, a) => {
      r[a.type] = r[a.type] || [];
      r[a.type].push(a);
      return r;
    }, Object.create(null));
  }

  updatedContacts(event): void {
    console.log(event);
    if (event.action === 'edit' || event.action === 'add') {
      this.editContacts(event);
    } else {
      this.contactService.deleteContact(event.contact.contactId).then(res => console.log(res));
    }
  }

  redirectEvent(event): void {
    if (event.action === 'group') {
      this.router.navigate(['/groups']);
    }
    if (event.action === 'ip') {
      this.router.navigate(['/builder']);
    }
    if (event.action === 'preview') {
      this.utility.setUrl('preview');
      this.router.navigate(['/preview']);
    }
  }

  loadTranslation(): void {
    this.translateSevice.get(['contacts.SearchPlaceHolder', 'contacts.FollowingTabTitle', 'contacts.FollowersTabTitle',
    'contacts.ContactsTabTitle', 'No Contact', 'contacts.FollowText', 'contacts.UnFollowText',
    'contacts.DeleteContactButton', 'contacts.CancelContactButton', 'DeleteModal.ConfirmButton',
    'DeleteModal.TitleText', 'addContacts.Title', 'addContacts.Update', 'addContacts.UserNamePlaceHolder', 'addContacts.PhoneNumberLabel', 'addContacts.HomeOption',
    'addContacts.OfficeOption', 'addContacts.WorkOption', 'addContacts.PhonenumberPlaceHolder', 'addContacts.EmailLabel',
    'addContacts.EmailPlaceHolder', 'addContacts.WebsitePlaceHolder', 'addContacts.WebsitesLabel',
    'contacts.AddButton', 'addGroup.UpdateButton', 'addContacts.GroupLabel']).subscribe((res: any[]) => {
      res = Object.keys(res).map((key) => res[key]);
      this.contactInterface.labels = {
        contact: {
          list: {
            placeHolder: {
              search: res[0]
            },
            following: res[1],
            followers: res[2],
            contacts: res[3],
            noContacts: res[4],
            follow: res[5],
            unfollow: res[6],
            delete: res[7],
            cancel: res[8],
            confirm: res[9],
            confirmationMessage: res[10]
          },
          create: {
            placeHolder: {
              username: res[13],
              phoneNumbers: res[14],
              home: res[15],
              office: res[16],
              work: res[17],
              number: res[18],
              emails: res[19],
              email: res[20],
              website: res[21],
              websites: res[22]
            },
            add: res[23],
            update: res[24],
            group: res[25]
          },
          add: res[11],
          update: res[12]
        }
      };
    });
  }

}
