// import * as addressReqsRsps from '@oneb/contacts-api-clients-axios-typescript/clients/src/codegen/AddressFinderClientRequestsResponses';
// import * as contactReqsRsps from '@oneb/contacts-api-clients-axios-typescript/clients/src/codegen/ContactFinderClientRequestsResponses';
// import * as contactProfileReqsRsps from '@oneb/contacts-api-clients-axios-typescript/clients/src/codegen/ContactProfileFinderClientRequestsResponses';
// import * as profileReqsRsps from '@oneb/contacts-api-clients-axios-typescript/clients/src/codegen/ProfileFinderClientRequestsResponses';
// import * as contactMechanismReqsRsps from '@oneb/contacts-api-clients-axios-typescript/clients/src/codegen/ContactMechanismFinderClientRequestsResponses';
// import * as groupReqsRsps from '@oneb/contacts-api-clients-axios-typescript/clients/src/codegen/GroupFinderClientRequestsResponses';
// import * as groupContactReqsRsps from '@oneb/contacts-api-clients-axios-typescript/clients/src/codegen/GroupContactFinderClientRequestsResponses';
// import * as personReqsRsps from '@oneb/contacts-api-clients-axios-typescript/clients/src/codegen/PersonFinderClientRequestsResponses';
// import { Rm2ClientLib } from '@rainmaker2/rainmaker2-codegen-api-clients-common-typescript/lib/Rm2ClientLib';
// import { ContactsFinderClient } from '@oneb/contacts-api-clients-axios-typescript/clients/src/codegen/ContactsFinderClient';
// export class ContactsFinderClientLocal implements ContactsFinderClient {
//     private clientLib;
//     db;
//     constructor(clientLib: Rm2ClientLib) {
//         if (!window.indexedDB) {
//             window.alert('Your browser doesn\'t support a stable version of IndexedDB.');
//         }

//         const request = window.indexedDB.open('Contacts', 1);

//         request.onerror = (event) => {
//         };

//         request.onsuccess = (event) => {
//             this.db = request.result;
//         };

//     }

//     findContact(findRequest: contactReqsRsps.FindRequest):
//         Promise<contactReqsRsps.FindResponse> {
//         return new Promise((resolve, reject) => {
//             const transaction = this.db.transaction('contact', 'readwrite');
//             const objectStore = transaction.objectStore('contact');
//             const response = objectStore.getAll();
//             response.onsuccess = () => {
//                 const results: contactReqsRsps.FindResponse = {
//                     pagination: null,
//                     matchedCount: null,
//                     changedCount: null,
//                     results: response.result
//                 };
//                 resolve(results);
//             };
//             response.onerror = () => reject(response.error);
//         });
//     }

//     findOneContactByContactId(findOneByContactIdRequest: contactReqsRsps.FindOneByContactIdRequest):
//         Promise<contactReqsRsps.FindOneByContactIdResponse> {
//         return new Promise((resolve, reject) => {
//             const transaction = this.db.transaction('contact', 'readwrite');
//             const objectStore = transaction.objectStore('contact');
//             const response = objectStore.get(findOneByContactIdRequest.filter.contactId);
//             response.onsuccess = () => {
//                 const results: contactReqsRsps.FindOneByContactIdResponse = {
//                     pagination: null,
//                     matchedCount: null,
//                     changedCount: null,
//                     result: response.result
//                 };
//                 resolve(results);
//             };
//             response.onerror = () => reject(response.error);
//         });
//     }

//     findOneAddressByAddressId(findOneByAddressIdRequest: addressReqsRsps.FindOneByAddressIdRequest):
//         Promise<addressReqsRsps.FindOneByAddressIdResponse> { return; }
//     findAddressByAddressId(findByAddressIdRequest: addressReqsRsps.FindByAddressIdRequest):
//         Promise<addressReqsRsps.FindByAddressIdResponse> { return; }
//     findAddressByCity(findByCityRequest: addressReqsRsps.FindByCityRequest):
//         Promise<addressReqsRsps.FindByCityResponse> { return; }
//     findAddressByContactMechanismsIds(findByContactMechanismsIdsRequest: addressReqsRsps.FindByContactMechanismsIdsRequest):
//         Promise<addressReqsRsps.FindByContactMechanismsIdsResponse> { return; }
//     findAddressByCountry(findByCountryRequest: addressReqsRsps.FindByCountryRequest):
//         Promise<addressReqsRsps.FindByCountryResponse> { return; }
//     findAddressByState(findByStateRequest: addressReqsRsps.FindByStateRequest):
//         Promise<addressReqsRsps.FindByStateResponse> { return; }
//     findAddressByStreet1(findByStreet1Request: addressReqsRsps.FindByStreet1Request):
//         Promise<addressReqsRsps.FindByStreet1Response> { return; }
//     findAddressByStreet2(findByStreet2Request: addressReqsRsps.FindByStreet2Request):
//         Promise<addressReqsRsps.FindByStreet2Response> { return; }
//     findAddressByZipCode(findByZipCodeRequest: addressReqsRsps.FindByZipCodeRequest):
//         Promise<addressReqsRsps.FindByZipCodeResponse> { return; }
//     findAddress(findRequest: addressReqsRsps.FindRequest):
//         Promise<addressReqsRsps.FindResponse> { return; }
//     findContactByAddressesIds(findByAddressesIdsRequest: contactReqsRsps.FindByAddressesIdsRequest):
//         Promise<contactReqsRsps.FindByAddressesIdsResponse> { return; }
//     findContactByContactId(findByContactIdRequest: contactReqsRsps.FindByContactIdRequest):
//         Promise<contactReqsRsps.FindByContactIdResponse> { return; }
//     findContactByContactMechanismsIds(findByContactMechanismsIdsRequest: contactReqsRsps.FindByContactMechanismsIdsRequest):
//         Promise<contactReqsRsps.FindByContactMechanismsIdsResponse> { return; }
//     findContactByName(findByNameRequest: contactReqsRsps.FindByNameRequest):
//         Promise<contactReqsRsps.FindByNameResponse> { return; }
//     findContactByPersonId(findByPersonIdRequest: contactReqsRsps.FindByPersonIdRequest):
//         Promise<contactReqsRsps.FindByPersonIdResponse> { return; }
//     findOneContactMechanismByContactMechanismId(findOneByContactMechanismIdRequest:
//         contactMechanismReqsRsps.FindOneByContactMechanismIdRequest):
//         Promise<contactMechanismReqsRsps.FindOneByContactMechanismIdResponse> { return; }
//     findContactMechanismByContactMechanismId(findByContactMechanismIdRequest: contactMechanismReqsRsps.FindByContactMechanismIdRequest):
//         Promise<contactMechanismReqsRsps.FindByContactMechanismIdResponse> { return; }
//     findContactMechanismByName(findByNameRequest: contactMechanismReqsRsps.FindByNameRequest):
//         Promise<contactMechanismReqsRsps.FindByNameResponse> { return; }
//     findContactMechanismByValue(findByValueRequest: contactMechanismReqsRsps.FindByValueRequest):
//         Promise<contactMechanismReqsRsps.FindByValueResponse> { return; }
//     findContactMechanism(findRequest: contactMechanismReqsRsps.FindRequest):
//         Promise<contactMechanismReqsRsps.FindResponse> { return; }
//     findOneContactProfileByContactProfileId(findOneByContactProfileIdRequest: contactProfileReqsRsps.FindOneByContactProfileIdRequest):
//         Promise<contactProfileReqsRsps.FindOneByContactProfileIdResponse> { return; }
//     findContactProfileByContactId(findByContactIdRequest: contactProfileReqsRsps.FindByContactIdRequest):
//         Promise<contactProfileReqsRsps.FindByContactIdResponse> { return; }
//     findContactProfileByContactProfileId(findByContactProfileIdRequest: contactProfileReqsRsps.FindByContactProfileIdRequest):
//         Promise<contactProfileReqsRsps.FindByContactProfileIdResponse> { return; }
//     findContactProfileByProfileId(findByProfileIdRequest: contactProfileReqsRsps.FindByProfileIdRequest):
//         Promise<contactProfileReqsRsps.FindByProfileIdResponse> { return; }
//     findContactProfile(findRequest: contactProfileReqsRsps.FindRequest):
//         Promise<contactProfileReqsRsps.FindResponse> { return; }
//     findOneGroupByGroupId(findOneByGroupIdRequest: groupReqsRsps.FindOneByGroupIdRequest):
//         Promise<groupReqsRsps.FindOneByGroupIdResponse> { return; }
//     findOneGroupByName(findOneByNameRequest: groupReqsRsps.FindOneByNameRequest):
//         Promise<groupReqsRsps.FindOneByNameResponse> { return; }
//     findGroupByGroupId(findByGroupIdRequest: groupReqsRsps.FindByGroupIdRequest):
//         Promise<groupReqsRsps.FindByGroupIdResponse> { return; }
//     findGroupByName(findByNameRequest: groupReqsRsps.FindByNameRequest):
//         Promise<groupReqsRsps.FindByNameResponse> { return; }
//     findGroup(findRequest: groupReqsRsps.FindRequest):
//         Promise<groupReqsRsps.FindResponse> { return; }
//     findOneGroupContactByGroupContactId(findOneByGroupContactIdRequest: groupContactReqsRsps.FindOneByGroupContactIdRequest):
//         Promise<groupContactReqsRsps.FindOneByGroupContactIdResponse> { return; }
//     findGroupContactByContactId(findByContactIdRequest: groupContactReqsRsps.FindByContactIdRequest):
//         Promise<groupContactReqsRsps.FindByContactIdResponse> { return; }
//     findGroupContactByGroupContactId(findByGroupContactIdRequest: groupContactReqsRsps.FindByGroupContactIdRequest):
//         Promise<groupContactReqsRsps.FindByGroupContactIdResponse> { return; }
//     findGroupContactByGroupId(findByGroupIdRequest: groupContactReqsRsps.FindByGroupIdRequest):
//         Promise<groupContactReqsRsps.FindByGroupIdResponse> { return; }
//     findGroupContact(findRequest: groupContactReqsRsps.FindRequest):
//         Promise<groupContactReqsRsps.FindResponse> { return; }
//     findOneProfileByName(findOneByNameRequest: profileReqsRsps.FindOneByNameRequest):
//         Promise<profileReqsRsps.FindOneByNameResponse> { return; }
//     findOneProfileByProfileId(findOneByProfileIdRequest: profileReqsRsps.FindOneByProfileIdRequest):
//         Promise<profileReqsRsps.FindOneByProfileIdResponse> { return; }
//     findProfileByFirstName(findByFirstNameRequest: profileReqsRsps.FindByFirstNameRequest):
//         Promise<profileReqsRsps.FindByFirstNameResponse> { return; }
//     findProfileByLastName(findByLastNameRequest: profileReqsRsps.FindByLastNameRequest):
//         Promise<profileReqsRsps.FindByLastNameResponse> { return; }
//     findProfileByMiddleName(findByMiddleNameRequest: profileReqsRsps.FindByMiddleNameRequest):
//         Promise<profileReqsRsps.FindByMiddleNameResponse> { return; }
//     findProfileByName(findByNameRequest: profileReqsRsps.FindByNameRequest):
//         Promise<profileReqsRsps.FindByNameResponse> { return; }
//     findProfileByProfileId(findByProfileIdRequest: profileReqsRsps.FindByProfileIdRequest):
//         Promise<profileReqsRsps.FindByProfileIdResponse> { return; }
//     findProfileByTitle(findByTitleRequest: profileReqsRsps.FindByTitleRequest):
//         Promise<profileReqsRsps.FindByTitleResponse> { return; }
//     findProfile(findRequest: profileReqsRsps.FindRequest):
//         Promise<profileReqsRsps.FindResponse> { return; }
//     findOnePersonByPersonId(findOneByPersonIdRequest: personReqsRsps.FindOneByPersonIdRequest):
//         Promise<personReqsRsps.FindOneByPersonIdResponse> { return; }
//     findPersonByFirstName(findByFirstNameRequest: personReqsRsps.FindByFirstNameRequest):
//         Promise<personReqsRsps.FindByFirstNameResponse> { return; }
//     findPersonByLastName(findByLastNameRequest: personReqsRsps.FindByLastNameRequest):
//         Promise<personReqsRsps.FindByLastNameResponse> { return; }
//     findPersonByMiddleName(findByMiddleNameRequest: personReqsRsps.FindByMiddleNameRequest):
//         Promise<personReqsRsps.FindByMiddleNameResponse> { return; }
//     findPersonByPersonId(findByPersonIdRequest: personReqsRsps.FindByPersonIdRequest):
//         Promise<personReqsRsps.FindByPersonIdResponse> { return; }
//     findPersonByTitle(findByTitleRequest: personReqsRsps.FindByTitleRequest):
//         Promise<personReqsRsps.FindByTitleResponse> { return; }
//     findPerson(findRequest: personReqsRsps.FindRequest):
//         Promise<personReqsRsps.FindResponse> { return; }
// }
