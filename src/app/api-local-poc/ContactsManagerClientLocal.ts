// import { ContactsManagerClient } from '@oneb/contacts-api-clients-axios-typescript/clients/src/codegen/ContactsManagerClient';
// import * as addressReqsRsps from '@oneb/contacts-api-clients-axios-typescript/clients/src/codegen/AddressManagerClientRequestsResponses';
// import * as contactReqsRsps from '@oneb/contacts-api-clients-axios-typescript/clients/src/codegen/ContactManagerClientRequestsResponses';
// import * as contactMechanismReqsRsps from '@oneb/contacts-api-clients-axios-typescript/clients/src/codegen/ContactMechanismManagerClientRequestsResponses';
// import * as contactProfileReqsRsps from '@oneb/contacts-api-clients-axios-typescript/clients/src/codegen/ContactProfileManagerClientRequestsResponses';
// import * as profileReqsRsps from '@oneb/contacts-api-clients-axios-typescript/clients/src/codegen/ProfileManagerClientRequestsResponses';
// import * as groupReqsRsps from '@oneb/contacts-api-clients-axios-typescript/clients/src/codegen/GroupManagerClientRequestsResponses';
// import * as groupContactReqsRsps from '@oneb/contacts-api-clients-axios-typescript/clients/src/codegen/GroupContactManagerClientRequestsResponses';
// import * as personReqsRsps from '@oneb/contacts-api-clients-axios-typescript/clients/src/codegen/PersonManagerClientRequestsResponses';
// import { Rm2ClientLib } from '@rainmaker2/rainmaker2-codegen-api-clients-common-typescript/lib/Rm2ClientLib';
// export class ContactsManagerClientLocal implements ContactsManagerClient {
//    private clientLib;
//    db;
//    constructor(clientLib: Rm2ClientLib) {

//       if (!window.indexedDB) {
//          window.alert('Your browser doesn\'t support a stable version of IndexedDB.');
//       } else {
//          // Adding initial data
//          const contactsData = [
//             {
//                contactId: '1',
//                name: 'test user1',
//                contactMechanisms: [
//                ]
//             },
//             {
//                contactId: '2',
//                name: 'test user2',
//                contactMechanisms: [
//                ]
//             }
//          ];
//          const contactMechanisms = [
//          ];
//          const request = window.indexedDB.open('Contacts', 1);

//          request.onerror = (event) => {
//          };

//          request.onsuccess = (event) => {
//             this.db = request.result;
//          };

//          request.onupgradeneeded = (event: any) => {
//             this.db = event.target.result;
//             const objectStore = this.db.createObjectStore('contact', { keyPath: 'contactId' });
//             const objStore = this.db.createObjectStore('contactMechanism', { keyPath: 'contactMechanismId' });

//             for (const i in contactsData) {
//                if (contactsData) {
//                   objectStore.add(contactsData[i]);
//                }
//             }

//             for (const i in contactMechanisms) {
//                if (contactMechanisms) {
//                   objStore.add(contactMechanisms[i]);
//                }
//             }
//          };
//       }
//    }



//    createContact(createRequest: contactReqsRsps.CreateRequest): Promise<contactReqsRsps.CreateResponse> {
//       return new Promise((resolve, reject) => {
//          const transaction = this.db.transaction('contact', 'readwrite');
//          const objectStore = transaction.objectStore('contact');
//          const contact = {
//             contactId: Math.random().toString(36).substring(2),
//             name: createRequest.contact.name,
//             contactMechanisms: []
//          };
//          const response = objectStore.add(contact);
//          response.onsuccess = () => {
//             const results: contactReqsRsps.CreateResponse = {
//                matchedCount: null,
//                changedCount: null,
//                contact: response.result
//             };
//             resolve(results);
//          };
//          response.onerror = () => reject(response.error);
//       });
//    }

//    updateContactByContactId(updateByContactIdRequest: contactReqsRsps.UpdateByContactIdRequest):
//       Promise<contactReqsRsps.UpdateByContactIdResponse> {
//       return new Promise((resolve, reject) => {
//          const transaction = this.db.transaction('contact', 'readwrite');
//          const objectStore = transaction.objectStore('contact');
//          const contact = {
//             contactId: updateByContactIdRequest.contact.contactId,
//             contactMechanisms: [],
//             name: updateByContactIdRequest.contact.name
//          };
//          const response = objectStore.put(updateByContactIdRequest.contact);
//          response.onsuccess = () => {
//             const results: contactReqsRsps.UpdateByContactIdResponse = {
//                matchedCount: null,
//                changedCount: null,
//                contactIds: response.result
//             };
//             resolve(results);
//          };
//          response.onerror = () => reject(response.error);
//       });
//    }

//    deleteContactByContactId(deleteByContactIdRequest: contactReqsRsps.DeleteByContactIdRequest):
//       Promise<contactReqsRsps.DeleteByContactIdResponse> {
//       return new Promise((resolve, reject) => {
//          const transaction = this.db.transaction('contact', 'readwrite');
//          const objectStore = transaction.objectStore('contact');
//          const response = objectStore.delete(deleteByContactIdRequest.filter.contactId);
//          setTimeout(() => {
//             resolve(response);
//          }, 500);
//       });
//    }


//    createAddress(createRequest: addressReqsRsps.CreateRequest):
//       Promise<addressReqsRsps.CreateResponse> { return; }
//    createBulkAddress(createBulkRequest: addressReqsRsps.CreateBulkRequest):
//       Promise<addressReqsRsps.CreateBulkResponse> { return; }
//    updateOneAddressByAddressId(updateOneByAddressIdRequest: addressReqsRsps.UpdateOneByAddressIdRequest):
//       Promise<addressReqsRsps.UpdateOneByAddressIdResponse> { return; }
//    updateAddressByAddressId(updateByAddressIdRequest: addressReqsRsps.UpdateByAddressIdRequest):
//       Promise<addressReqsRsps.UpdateByAddressIdResponse> { return; }
//    updateAddressByCity(updateByCityRequest: addressReqsRsps.UpdateByCityRequest):
//       Promise<addressReqsRsps.UpdateByCityResponse> { return; }
//    updateAddressByContactMechanismsIds(updateByContactMechanismsIdsRequest: addressReqsRsps.UpdateByContactMechanismsIdsRequest):
//       Promise<addressReqsRsps.UpdateByContactMechanismsIdsResponse> { return; }
//    updateAddressByCountry(updateByCountryRequest: addressReqsRsps.UpdateByCountryRequest):
//       Promise<addressReqsRsps.UpdateByCountryResponse> { return; }
//    updateAddressByState(updateByStateRequest: addressReqsRsps.UpdateByStateRequest):
//       Promise<addressReqsRsps.UpdateByStateResponse> { return; }
//    updateAddressByStreet1(updateByStreet1Request: addressReqsRsps.UpdateByStreet1Request):
//       Promise<addressReqsRsps.UpdateByStreet1Response> { return; }
//    updateAddressByStreet2(updateByStreet2Request: addressReqsRsps.UpdateByStreet2Request):
//       Promise<addressReqsRsps.UpdateByStreet2Response> { return; }
//    updateAddressByZipCode(updateByZipCodeRequest: addressReqsRsps.UpdateByZipCodeRequest):
//       Promise<addressReqsRsps.UpdateByZipCodeResponse> { return; }
//    updateAddress(updateRequest: addressReqsRsps.UpdateRequest):
//       Promise<addressReqsRsps.UpdateResponse> { return; }
//    deleteOneAddressByAddressId(deleteOneByAddressIdRequest: addressReqsRsps.DeleteOneByAddressIdRequest):
//       Promise<addressReqsRsps.DeleteOneByAddressIdResponse> { return; }
//    deleteAddressByAddressId(deleteByAddressIdRequest: addressReqsRsps.DeleteByAddressIdRequest):
//       Promise<addressReqsRsps.DeleteByAddressIdResponse> { return; }
//    deleteAddressByCity(deleteByCityRequest: addressReqsRsps.DeleteByCityRequest):
//       Promise<addressReqsRsps.DeleteByCityResponse> { return; }
//    deleteAddressByContactMechanismsIds(deleteByContactMechanismsIdsRequest: addressReqsRsps.DeleteByContactMechanismsIdsRequest):
//       Promise<addressReqsRsps.DeleteByContactMechanismsIdsResponse> { return; }
//    deleteAddressByCountry(deleteByCountryRequest: addressReqsRsps.DeleteByCountryRequest):
//       Promise<addressReqsRsps.DeleteByCountryResponse> { return; }
//    deleteAddressByState(deleteByStateRequest: addressReqsRsps.DeleteByStateRequest):
//       Promise<addressReqsRsps.DeleteByStateResponse> { return; }
//    deleteAddressByStreet1(deleteByStreet1Request: addressReqsRsps.DeleteByStreet1Request):
//       Promise<addressReqsRsps.DeleteByStreet1Response> { return; }
//    deleteAddressByStreet2(deleteByStreet2Request: addressReqsRsps.DeleteByStreet2Request):
//       Promise<addressReqsRsps.DeleteByStreet2Response> { return; }
//    deleteAddressByZipCode(deleteByZipCodeRequest: addressReqsRsps.DeleteByZipCodeRequest):
//       Promise<addressReqsRsps.DeleteByZipCodeResponse> { return; }
//    deleteAddress(deleteRequest: addressReqsRsps.DeleteRequest):
//       Promise<addressReqsRsps.DeleteResponse> { return; }
//    createBulkContact(createBulkRequest: contactReqsRsps.CreateBulkRequest):
//       Promise<contactReqsRsps.CreateBulkResponse> { return; }
//    updateOneContactByContactId(updateOneByContactIdRequest: contactReqsRsps.UpdateOneByContactIdRequest):
//       Promise<contactReqsRsps.UpdateOneByContactIdResponse> { return; }
//    updateContactByAddressesIds(updateByAddressesIdsRequest: contactReqsRsps.UpdateByAddressesIdsRequest):
//       Promise<contactReqsRsps.UpdateByAddressesIdsResponse> { return; }
//    updateContactByContactMechanismsIds(updateByContactMechanismsIdsRequest: contactReqsRsps.UpdateByContactMechanismsIdsRequest):
//       Promise<contactReqsRsps.UpdateByContactMechanismsIdsResponse> { return; }
//    updateContactByName(updateByNameRequest: contactReqsRsps.UpdateByNameRequest):
//       Promise<contactReqsRsps.UpdateByNameResponse> { return; }
//    updateContactByPersonId(updateByPersonIdRequest: contactReqsRsps.UpdateByPersonIdRequest):
//       Promise<contactReqsRsps.UpdateByPersonIdResponse> { return; }
//    updateContact(updateRequest: contactReqsRsps.UpdateRequest):
//       Promise<contactReqsRsps.UpdateResponse> { return; }
//    deleteOneContactByContactId(deleteOneByContactIdRequest: contactReqsRsps.DeleteOneByContactIdRequest):
//       Promise<contactReqsRsps.DeleteOneByContactIdResponse> { return; }
//    deleteContactByAddressesIds(deleteByAddressesIdsRequest: contactReqsRsps.DeleteByAddressesIdsRequest):
//       Promise<contactReqsRsps.DeleteByAddressesIdsResponse> { return; }
//    deleteContactByContactMechanismsIds(deleteByContactMechanismsIdsRequest: contactReqsRsps.DeleteByContactMechanismsIdsRequest):
//       Promise<contactReqsRsps.DeleteByContactMechanismsIdsResponse> { return; }
//    deleteContactByName(deleteByNameRequest: contactReqsRsps.DeleteByNameRequest):
//       Promise<contactReqsRsps.DeleteByNameResponse> { return; }
//    deleteContactByPersonId(deleteByPersonIdRequest: contactReqsRsps.DeleteByPersonIdRequest):
//       Promise<contactReqsRsps.DeleteByPersonIdResponse> { return; }
//    deleteContact(deleteRequest: contactReqsRsps.DeleteRequest):
//       Promise<contactReqsRsps.DeleteResponse> { return; }
//    createContactMechanism(createRequest: contactMechanismReqsRsps.CreateRequest):
//       Promise<contactMechanismReqsRsps.CreateResponse> { return; }
//    createBulkContactMechanism(createBulkRequest: contactMechanismReqsRsps.CreateBulkRequest):
//       Promise<contactMechanismReqsRsps.CreateBulkResponse> { return; }
//    updateOneContactMechanismByContactMechanismId(updateOneByContactMechanismIdRequest:
//       contactMechanismReqsRsps.UpdateOneByContactMechanismIdRequest):
//       Promise<contactMechanismReqsRsps.UpdateOneByContactMechanismIdResponse> { return; }
//    updateContactMechanismByContactMechanismId(updateByContactMechanismIdRequest:
//       contactMechanismReqsRsps.UpdateByContactMechanismIdRequest):
//       Promise<contactMechanismReqsRsps.UpdateByContactMechanismIdResponse> { return; }
//    updateContactMechanismByName(updateByNameRequest: contactMechanismReqsRsps.UpdateByNameRequest):
//       Promise<contactMechanismReqsRsps.UpdateByNameResponse> { return; }
//    updateContactMechanismByValue(updateByValueRequest: contactMechanismReqsRsps.UpdateByValueRequest):
//       Promise<contactMechanismReqsRsps.UpdateByValueResponse> { return; }
//    updateContactMechanism(updateRequest: contactMechanismReqsRsps.UpdateRequest):
//       Promise<contactMechanismReqsRsps.UpdateResponse> { return; }
//    deleteOneContactMechanismByContactMechanismId(deleteOneByContactMechanismIdRequest:
//       contactMechanismReqsRsps.DeleteOneByContactMechanismIdRequest):
//       Promise<contactMechanismReqsRsps.DeleteOneByContactMechanismIdResponse> { return; }
//    deleteContactMechanismByContactMechanismId(deleteByContactMechanismIdRequest:
//       contactMechanismReqsRsps.DeleteByContactMechanismIdRequest):
//       Promise<contactMechanismReqsRsps.DeleteByContactMechanismIdResponse> { return; }
//    deleteContactMechanismByName(deleteByNameRequest: contactMechanismReqsRsps.DeleteByNameRequest):
//       Promise<contactMechanismReqsRsps.DeleteByNameResponse> { return; }
//    deleteContactMechanismByValue(deleteByValueRequest: contactMechanismReqsRsps.DeleteByValueRequest):
//       Promise<contactMechanismReqsRsps.DeleteByValueResponse> { return; }
//    deleteContactMechanism(deleteRequest: contactMechanismReqsRsps.DeleteRequest):
//       Promise<contactMechanismReqsRsps.DeleteResponse> { return; }
//    createContactProfile(createRequest: contactProfileReqsRsps.CreateRequest):
//       Promise<contactProfileReqsRsps.CreateResponse> { return; }
//    createBulkContactProfile(createBulkRequest: contactProfileReqsRsps.CreateBulkRequest):
//       Promise<contactProfileReqsRsps.CreateBulkResponse> { return; }
//    updateOneContactProfileByContactProfileId(updateOneByContactProfileIdRequest: contactProfileReqsRsps.UpdateOneByContactProfileIdRequest):
//       Promise<contactProfileReqsRsps.UpdateOneByContactProfileIdResponse> { return; }
//    updateContactProfileByContactId(updateByContactIdRequest: contactProfileReqsRsps.UpdateByContactIdRequest):
//       Promise<contactProfileReqsRsps.UpdateByContactIdResponse> { return; }
//    updateContactProfileByContactProfileId(updateByContactProfileIdRequest: contactProfileReqsRsps.UpdateByContactProfileIdRequest):
//       Promise<contactProfileReqsRsps.UpdateByContactProfileIdResponse> { return; }
//    updateContactProfileByProfileId(updateByProfileIdRequest: contactProfileReqsRsps.UpdateByProfileIdRequest):
//       Promise<contactProfileReqsRsps.UpdateByProfileIdResponse> { return; }
//    updateContactProfile(updateRequest: contactProfileReqsRsps.UpdateRequest):
//       Promise<contactProfileReqsRsps.UpdateResponse> { return; }
//    deleteOneContactProfileByContactProfileId(deleteOneByContactProfileIdRequest: contactProfileReqsRsps.DeleteOneByContactProfileIdRequest):
//       Promise<contactProfileReqsRsps.DeleteOneByContactProfileIdResponse> { return; }
//    deleteContactProfileByContactId(deleteByContactIdRequest: contactProfileReqsRsps.DeleteByContactIdRequest):
//       Promise<contactProfileReqsRsps.DeleteByContactIdResponse> { return; }
//    deleteContactProfileByContactProfileId(deleteByContactProfileIdRequest: contactProfileReqsRsps.DeleteByContactProfileIdRequest):
//       Promise<contactProfileReqsRsps.DeleteByContactProfileIdResponse> { return; }
//    deleteContactProfileByProfileId(deleteByProfileIdRequest: contactProfileReqsRsps.DeleteByProfileIdRequest):
//       Promise<contactProfileReqsRsps.DeleteByProfileIdResponse> { return; }
//    deleteContactProfile(deleteRequest: contactProfileReqsRsps.DeleteRequest):
//       Promise<contactProfileReqsRsps.DeleteResponse> { return; }
//    createGroup(createRequest: groupReqsRsps.CreateRequest):
//       Promise<groupReqsRsps.CreateResponse> { return; }
//    createBulkGroup(createBulkRequest: groupReqsRsps.CreateBulkRequest):
//       Promise<groupReqsRsps.CreateBulkResponse> { return; }
//    updateOneGroupByGroupId(updateOneByGroupIdRequest: groupReqsRsps.UpdateOneByGroupIdRequest):
//       Promise<groupReqsRsps.UpdateOneByGroupIdResponse> { return; }
//    updateOneGroupByName(updateOneByNameRequest: groupReqsRsps.UpdateOneByNameRequest):
//       Promise<groupReqsRsps.UpdateOneByNameResponse> { return; }
//    updateGroupByGroupId(updateByGroupIdRequest: groupReqsRsps.UpdateByGroupIdRequest):
//       Promise<groupReqsRsps.UpdateByGroupIdResponse> { return; }
//    updateGroupByName(updateByNameRequest: groupReqsRsps.UpdateByNameRequest):
//       Promise<groupReqsRsps.UpdateByNameResponse> { return; }
//    updateGroup(updateRequest: groupReqsRsps.UpdateRequest):
//       Promise<groupReqsRsps.UpdateResponse> { return; }
//    deleteOneGroupByGroupId(deleteOneByGroupIdRequest: groupReqsRsps.DeleteOneByGroupIdRequest):
//       Promise<groupReqsRsps.DeleteOneByGroupIdResponse> { return; }
//    deleteOneGroupByName(deleteOneByNameRequest: groupReqsRsps.DeleteOneByNameRequest):
//       Promise<groupReqsRsps.DeleteOneByNameResponse> { return; }
//    deleteGroupByGroupId(deleteByGroupIdRequest: groupReqsRsps.DeleteByGroupIdRequest):
//       Promise<groupReqsRsps.DeleteByGroupIdResponse> { return; }
//    deleteGroupByName(deleteByNameRequest: groupReqsRsps.DeleteByNameRequest):
//       Promise<groupReqsRsps.DeleteByNameResponse> { return; }
//    deleteGroup(deleteRequest: groupReqsRsps.DeleteRequest):
//       Promise<groupReqsRsps.DeleteResponse> { return; }
//    createGroupContact(createRequest: groupContactReqsRsps.CreateRequest):
//       Promise<groupContactReqsRsps.CreateResponse> { return; }
//    createBulkGroupContact(createBulkRequest: groupContactReqsRsps.CreateBulkRequest):
//       Promise<groupContactReqsRsps.CreateBulkResponse> { return; }
//    updateOneGroupContactByGroupContactId(updateOneByGroupContactIdRequest: groupContactReqsRsps.UpdateOneByGroupContactIdRequest):
//       Promise<groupContactReqsRsps.UpdateOneByGroupContactIdResponse> { return; }
//    updateGroupContactByContactId(updateByContactIdRequest: groupContactReqsRsps.UpdateByContactIdRequest):
//       Promise<groupContactReqsRsps.UpdateByContactIdResponse> { return; }
//    updateGroupContactByGroupContactId(updateByGroupContactIdRequest: groupContactReqsRsps.UpdateByGroupContactIdRequest):
//       Promise<groupContactReqsRsps.UpdateByGroupContactIdResponse> { return; }
//    updateGroupContactByGroupId(updateByGroupIdRequest: groupContactReqsRsps.UpdateByGroupIdRequest):
//       Promise<groupContactReqsRsps.UpdateByGroupIdResponse> { return; }
//    updateGroupContact(updateRequest: groupContactReqsRsps.UpdateRequest):
//       Promise<groupContactReqsRsps.UpdateResponse> { return; }
//    deleteOneGroupContactByGroupContactId(deleteOneByGroupContactIdRequest: groupContactReqsRsps.DeleteOneByGroupContactIdRequest):
//       Promise<groupContactReqsRsps.DeleteOneByGroupContactIdResponse> { return; }
//    deleteGroupContactByContactId(deleteByContactIdRequest: groupContactReqsRsps.DeleteByContactIdRequest):
//       Promise<groupContactReqsRsps.DeleteByContactIdResponse> { return; }
//    deleteGroupContactByGroupContactId(deleteByGroupContactIdRequest: groupContactReqsRsps.DeleteByGroupContactIdRequest):
//       Promise<groupContactReqsRsps.DeleteByGroupContactIdResponse> { return; }
//    deleteGroupContactByGroupId(deleteByGroupIdRequest: groupContactReqsRsps.DeleteByGroupIdRequest):
//       Promise<groupContactReqsRsps.DeleteByGroupIdResponse> { return; }
//    deleteGroupContact(deleteRequest: groupContactReqsRsps.DeleteRequest):
//       Promise<groupContactReqsRsps.DeleteResponse> { return; }
//    createProfile(createRequest: profileReqsRsps.CreateRequest):
//       Promise<profileReqsRsps.CreateResponse> { return; }
//    createBulkProfile(createBulkRequest: profileReqsRsps.CreateBulkRequest):
//       Promise<profileReqsRsps.CreateBulkResponse> { return; }
//    updateOneProfileByName(updateOneByNameRequest: profileReqsRsps.UpdateOneByNameRequest):
//       Promise<profileReqsRsps.UpdateOneByNameResponse> { return; }
//    updateOneProfileByProfileId(updateOneByProfileIdRequest: profileReqsRsps.UpdateOneByProfileIdRequest):
//       Promise<profileReqsRsps.UpdateOneByProfileIdResponse> { return; }
//    updateProfileByFirstName(updateByFirstNameRequest: profileReqsRsps.UpdateByFirstNameRequest):
//       Promise<profileReqsRsps.UpdateByFirstNameResponse> { return; }
//    updateProfileByLastName(updateByLastNameRequest: profileReqsRsps.UpdateByLastNameRequest):
//       Promise<profileReqsRsps.UpdateByLastNameResponse> { return; }
//    updateProfileByMiddleName(updateByMiddleNameRequest: profileReqsRsps.UpdateByMiddleNameRequest):
//       Promise<profileReqsRsps.UpdateByMiddleNameResponse> { return; }
//    updateProfileByName(updateByNameRequest: profileReqsRsps.UpdateByNameRequest):
//       Promise<profileReqsRsps.UpdateByNameResponse> { return; }
//    updateProfileByProfileId(updateByProfileIdRequest: profileReqsRsps.UpdateByProfileIdRequest):
//       Promise<profileReqsRsps.UpdateByProfileIdResponse> { return; }
//    updateProfileByTitle(updateByTitleRequest: profileReqsRsps.UpdateByTitleRequest):
//       Promise<profileReqsRsps.UpdateByTitleResponse> { return; }
//    updateProfile(updateRequest: profileReqsRsps.UpdateRequest):
//       Promise<profileReqsRsps.UpdateResponse> { return; }
//    deleteOneProfileByName(deleteOneByNameRequest: profileReqsRsps.DeleteOneByNameRequest):
//       Promise<profileReqsRsps.DeleteOneByNameResponse> { return; }
//    deleteOneProfileByProfileId(deleteOneByProfileIdRequest: profileReqsRsps.DeleteOneByProfileIdRequest):
//       Promise<profileReqsRsps.DeleteOneByProfileIdResponse> { return; }
//    deleteProfileByFirstName(deleteByFirstNameRequest: profileReqsRsps.DeleteByFirstNameRequest):
//       Promise<profileReqsRsps.DeleteByFirstNameResponse> { return; }
//    deleteProfileByLastName(deleteByLastNameRequest: profileReqsRsps.DeleteByLastNameRequest):
//       Promise<profileReqsRsps.DeleteByLastNameResponse> { return; }
//    deleteProfileByMiddleName(deleteByMiddleNameRequest: profileReqsRsps.DeleteByMiddleNameRequest):
//       Promise<profileReqsRsps.DeleteByMiddleNameResponse> { return; }
//    deleteProfileByName(deleteByNameRequest: profileReqsRsps.DeleteByNameRequest):
//       Promise<profileReqsRsps.DeleteByNameResponse> { return; }
//    deleteProfileByProfileId(deleteByProfileIdRequest: profileReqsRsps.DeleteByProfileIdRequest):
//       Promise<profileReqsRsps.DeleteByProfileIdResponse> { return; }
//    deleteProfileByTitle(deleteByTitleRequest: profileReqsRsps.DeleteByTitleRequest):
//       Promise<profileReqsRsps.DeleteByTitleResponse> { return; }
//    deleteProfile(deleteRequest: profileReqsRsps.DeleteRequest):
//       Promise<profileReqsRsps.DeleteResponse> { return; }
//    createPerson(createRequest: personReqsRsps.CreateRequest):
//       Promise<personReqsRsps.CreateResponse> { return; }
//    createBulkPerson(createBulkRequest: personReqsRsps.CreateBulkRequest):
//       Promise<personReqsRsps.CreateBulkResponse> { return; }
//    updateOnePersonByPersonId(updateOneByPersonIdRequest: personReqsRsps.UpdateOneByPersonIdRequest):
//       Promise<personReqsRsps.UpdateOneByPersonIdResponse> { return; }
//    updatePersonByFirstName(updateByFirstNameRequest: personReqsRsps.UpdateByFirstNameRequest):
//       Promise<personReqsRsps.UpdateByFirstNameResponse> { return; }
//    updatePersonByLastName(updateByLastNameRequest: personReqsRsps.UpdateByLastNameRequest):
//       Promise<personReqsRsps.UpdateByLastNameResponse> { return; }
//    updatePersonByMiddleName(updateByMiddleNameRequest: personReqsRsps.UpdateByMiddleNameRequest):
//       Promise<personReqsRsps.UpdateByMiddleNameResponse> { return; }
//    updatePersonByPersonId(updateByPersonIdRequest: personReqsRsps.UpdateByPersonIdRequest):
//       Promise<personReqsRsps.UpdateByPersonIdResponse> { return; }
//    updatePersonByTitle(updateByTitleRequest: personReqsRsps.UpdateByTitleRequest):
//       Promise<personReqsRsps.UpdateByTitleResponse> { return; }
//    updatePerson(updateRequest: personReqsRsps.UpdateRequest):
//       Promise<personReqsRsps.UpdateResponse> { return; }
//    deleteOnePersonByPersonId(deleteOneByPersonIdRequest: personReqsRsps.DeleteOneByPersonIdRequest):
//       Promise<personReqsRsps.DeleteOneByPersonIdResponse> { return; }
//    deletePersonByFirstName(deleteByFirstNameRequest: personReqsRsps.DeleteByFirstNameRequest):
//       Promise<personReqsRsps.DeleteByFirstNameResponse> { return; }
//    deletePersonByLastName(deleteByLastNameRequest: personReqsRsps.DeleteByLastNameRequest):
//       Promise<personReqsRsps.DeleteByLastNameResponse> { return; }
//    deletePersonByMiddleName(deleteByMiddleNameRequest: personReqsRsps.DeleteByMiddleNameRequest):
//       Promise<personReqsRsps.DeleteByMiddleNameResponse> { return; }
//    deletePersonByPersonId(deleteByPersonIdRequest: personReqsRsps.DeleteByPersonIdRequest):
//       Promise<personReqsRsps.DeleteByPersonIdResponse> { return; }
//    deletePersonByTitle(deleteByTitleRequest: personReqsRsps.DeleteByTitleRequest):
//       Promise<personReqsRsps.DeleteByTitleResponse> { return; }
//    deletePerson(deleteRequest: personReqsRsps.DeleteRequest):
//       Promise<personReqsRsps.DeleteResponse> { return; }
// }
