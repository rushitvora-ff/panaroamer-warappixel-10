import {
  ChangeDetectorRef,
  Component,
  OnDestroy
} from '@angular/core';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { MediaMatcher } from '@angular/cdk/layout';
import { MenuItems } from '../../../shared/menu-items/menu-items';
import { SidebarInterface } from '../../../interfaces/fullModule';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: []
})
export class AppSidebarComponent implements OnDestroy {
  public config: PerfectScrollbarConfigInterface = {};
  private _mobileQueryListener: () => void;

  sidebar: SidebarInterface = {
    status: true,
    itemSelect: [],
    parentIndex: 0,
    childIndex: 0
  };

  // mobileQuery: MediaQueryList;
  // status = true;
  // itemSelect: number[] = [];
  // parentIndex = 0;
  // childIndex = 0;

  setClickedRow(i: number, j: number): void {
    this.sidebar.parentIndex = i;
    this.sidebar.childIndex = j;
  }
  subclickEvent(): void  {
    this.sidebar.status = true;
  }
  scrollToTop(): void  {
    document.querySelector('.page-wrapper')?.scroll({
      top: 0,
      left: 0
    });
  }

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    public menuItems: MenuItems
  ) {
    this.sidebar.mobileQuery = media.matchMedia('(min-width: 768px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    // tslint:disable-next-line: deprecation
    this.sidebar.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnDestroy(): void {
    // tslint:disable-next-line: deprecation
    this.sidebar.mobileQuery.removeListener(this._mobileQueryListener);
  }
}
