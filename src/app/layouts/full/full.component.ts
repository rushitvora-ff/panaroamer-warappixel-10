import { MediaMatcher } from '@angular/cdk/layout';
import { Router, NavigationEnd } from '@angular/router';
import {
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit


} from '@angular/core';
import { MenuItems } from '../../shared/menu-items/menu-items';


import { PerfectScrollbarConfigInterface, PerfectScrollbarDirective } from 'ngx-perfect-scrollbar';
import { UtilityService } from 'src/app/services/utility.service';
import { TranslateService } from '@ngx-translate/core';
import { FullInterface } from 'src/app/interfaces/fullModule';

/** @title Responsive sidenav */
@Component({
  selector: 'app-full-layout',
  templateUrl: 'full.component.html',
  styleUrls: []
})
export class FullComponent implements OnInit, OnDestroy {

  fullInterface: FullInterface = {
    sidebarOpened: false,
    status: false,
    isVisible: false,
    isLoad: false,
    languageArr: [],
  };

  public config: PerfectScrollbarConfigInterface = {};
  private _mobileQueryListener: () => void;

  clickEvent(): void {
    this.fullInterface.status = !this.fullInterface.status;
  }

  constructor(
    public router: Router,
    public translate: TranslateService,
    public utility: UtilityService,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    public menuItems: MenuItems
  ) {
    // this.selectedLanguage = 'French';
    this.fullInterface.mobileQuery = media.matchMedia('(min-width: 768px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    // tslint:disable-next-line: deprecation
    this.fullInterface.mobileQuery.addListener(this._mobileQueryListener);
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.fullInterface.isVisible = router.url.indexOf('play') !== -1 ? true : false;
      }
    });
    const getLang = localStorage.getItem('language');
    // translate.setDefaultLang('English');
    this.utility.getJSON('Panaroamer').subscribe(response => {
      this.fullInterface.languageArr = response.internationalization;
      if (this.fullInterface.languageArr) {
        for (let i = 0 ; i < this.fullInterface.languageArr.length; i++){
          if (getLang){
          if (getLang == this.fullInterface.languageArr[i].lang){
            this.fullInterface.selectedLanguage = this.fullInterface.languageArr[i];
            break;
          }
        }
        else{
          this.fullInterface.selectedLanguage = this.fullInterface.languageArr[3];
        }
        }
      }
    });
  }

  ngOnInit(): void {
    // this.selectedLanguage = this.languageArr[5];
    // let getLanguage = localStorage.getItem('language');
    // if(getLanguage){
    // this.selectedLanguage = getLanguage;
    // }
    // console.log(getLanguage);
    this.utility.getUrl().subscribe(response => {
      if (localStorage.getItem('image')){
        this.utility.preview = true;
        const getIp = JSON.parse(localStorage.getItem('image'));
        this.fullInterface.artifacts = getIp;
        this.utility.createdIP = getIp;
        if (localStorage.getItem('image')){
          localStorage.removeItem('image');
          localStorage.removeItem('fullIp');
        }
      }
      // this.onResize();
      console.log(this.utility.createdIP);
      if (response != null) {
        if (response === 'preview') {
          this.utility.preview = true;
          this.fullInterface.artifacts = this.utility.createdIP;
          for (const url of this.utility.iframeUrlArr) {
            if (this.utility.iframeUrlArr) {
              if (this.utility.iframeUrlArr[url] && this.utility.iframeUrlArr[url].isVisible) {
                this.utility.iframeUrlArr[url].isVisible = false;
              }
            }
          }
        } else {
          this['iFrame' + response].nativeElement.contentWindow.location.replace(this.utility.iframeUrlArr[response].url);
        }
      }
    });
  }

  changeLanguage(lan): void {
    this.translate.use(this.fullInterface.selectedLanguage.lang);
    localStorage.setItem('language' , this.fullInterface.selectedLanguage.lang);
    // this.selectedLanguage = lan;
    // console.log(this.selectedLanguage);
  }

  handleIframeLoad(): void {
    const agent = window.navigator.userAgent.toLowerCase();
    if ((agent.indexOf('chrome') > -1 && !!(window as any).chrome) || agent.indexOf('safari') > -1) {
      if (this.fullInterface.isLoad) {
        this.utility.setLoader(false);
        this.fullInterface.isLoad = false;
      } else {
        this.utility.setLoader(true);
        this.fullInterface.isLoad = true;
      }
    }
  }

  load(): any {
    return {
      handleIpLoad: () => {
        this.handleIframeLoad();
      }
    };
  }

  count(length: number): Array<any> {
    if (length >= 0) {
      return new Array(length);
    }
  }

  toggle(event): void {
    this.utility.dirValue = event.checked;
    this.utility.dir = (this.utility.dir == 'rtl' ? 'ltr' : 'rtl');
  }

  ngOnDestroy(): void {
    // tslint:disable-next-line: deprecation
    this.fullInterface.mobileQuery.removeListener(this._mobileQueryListener);
  }
}
