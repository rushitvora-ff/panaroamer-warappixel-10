import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FullInterface } from 'src/app/interfaces/fullModule';
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-blank',
  templateUrl: './blank.component.html',
  styleUrls: []
})

export class AppBlankComponent {
  blank: FullInterface = {
    languageArr: [],
    displayPanel: false
  };

  constructor(public translate: TranslateService, public utility: UtilityService,
              media: MediaMatcher,
              changeDetectorRef: ChangeDetectorRef) {
    // tslint:disable-next-line: deprecation
    let getLang = localStorage.getItem('language');
    this.utility.getJSON('Panaroamer').subscribe(response => {
      this.blank.languageArr = response.internationalization;
      if (this.blank.languageArr) {
        for (let i = 0 ; i < this.blank.languageArr.length; i++){
          if (getLang == this.blank.languageArr[i].lang){
            this.blank.selectedLanguage = this.blank.languageArr[i];
            break;
          }
          else{
            this.blank.selectedLanguage = this.blank.languageArr[3];
          }
        }
      }
    });
  }

  openPanel(): void{
    console.log(this.blank.displayPanel);
    this.blank.displayPanel = !this.blank.displayPanel;
  }

  changeLanguage(): void {
    this.translate.use(this.blank.selectedLanguage.lang);
    localStorage.setItem('language' , this.blank.selectedLanguage.lang);
    console.log(this.blank.selectedLanguage);
  }
  toggle(event): void {
    this.utility.dirValue = event.checked;
    this.utility.dir = (this.utility.dir == 'rtl' ? 'ltr' : 'rtl');
  }
}
